--
-- Sentinel's Custom IPS Signature Datasources
-- Last signatures update: NA
-- Written By: Sidney Eaton
-- Last Modified: 4-28-2017
--

SET @pluginID = '1001';

DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

call STICreateDatasourceEntry(@pluginID, 20151228, 4, 34, 'STI POLICY PUA Host-Header - Shop at home toolbar', 3, 1);
call STICreateDatasourceEntry(@pluginID, 1200200, 4, 37, 'STI TROJAN Non-Std TCP Client Traffic contains HX (PLUGX Variant)', 3, 5);
call STICreateDatasourceEntry(@pluginID, 1200201, 4, 37, 'STI TROJAN Non-Std TCP Client Traffic contains X-Session (PLUGX Variant)', 3, 5);
call STICreateDatasourceEntry(@pluginID, 1200202, 4, 37, 'STI TROJAN Non-Std TCP Client Traffic contains MJX (PLUGX Variant)', 3, 5);
call STICreateDatasourceEntry(@pluginID, 1200203, 4, 37, 'STI TROJAN Non-Std TCP Client Traffic contains Sym (Chches Variant)', 3, 5);
call STICreateDatasourceEntry(@pluginID, 1200205, 4, 37, 'STI TROJAN Ryuk Infection IP Detected', 3, 5);


DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;