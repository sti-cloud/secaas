#!/bin/bash
COUNT=`pgrep Suricata-Main | wc -l`
if [ $COUNT -gt 1 ]; then
  echo "Found $COUNT Suricata processes running!"
  for proc in $(pgrep Suricata-Main); do
    kill $proc
  done

  sleep 10
  service suricata start
fi