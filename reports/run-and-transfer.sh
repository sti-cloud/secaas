#!/bin/bash
cd /var/secaas/reports

# Get the hostname
if [ -f /root/reports.hostname ]; then
  hostname=`cat /root/reports.hostname`
else
  hostname=$( hostname )
fi

# Get the URL needed to upload report data
url=`curl -s https://60mcray8q1.execute-api.us-east-2.amazonaws.com/prod?hostname=$hostname`
if [ -z "$url" ]; then
  exit 0
fi

# We have a valid upload URL. Run reports to report data.
if [ ! -d /root/reports ]; then
    mkdir /root/reports
fi
if [ ! -d /var/lib/mysql-files ]; then
    mkdir /var/lib/mysql-files
    chmod a+rwx /var/lib/mysql-files
fi
./run-reports.py

# Zip up the reports and upload them.
cd ~/reports
zip -q reports.zip *.csv
curl -T reports.zip "$url"

# Clean up
cd ..
rm -fr reports/
exit 0
