#!/usr/bin/python
import MySQLdb
import os
import sys
import time
import tempfile
import datetime
import json

def GetTempFilename() :
    handle = tempfile.NamedTemporaryFile(delete=False, dir='/var/lib/mysql-files/')
    TEMP_FN = handle.name
    handle.close()
    os.remove(TEMP_FN)
    return TEMP_FN

def GetDatabasePassword() :
    handle = open("/etc/ossim/ossim_setup.conf", "r")
    for line in handle:
        if line.startswith("pass="):
            db_pass = line[5:].strip('\n\r')
            break
    handle.close()
    return db_pass

def ExecuteSQL(cmd) :
    global db
    global cur
    global range_s, range2_s, range_e, range2_e

    sql = cmd.replace("_START_", range_s)
    sql = sql.replace("_START2_", range2_s)
    sql = sql.replace("_END_", range_e)
    sql = sql.replace("_END2_", range2_e)
    sql = sql.replace("_CSV_", " INTO OUTFILE '" + TEMP_FN + "' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n'")
    cur.execute(sql)

def ConvertTempToReport() :
    global TEMP_FN
    global base
    global range_e

    # Read the SQL data and delete it
    handle = open(TEMP_FN, 'r')
    lines = handle.readlines();
    handle.close()
    os.remove(TEMP_FN)

    # Convert the report into a SQL command
    for i in range(len(lines)):
        lines[i] = lines[i].replace("\\\"", "\"\"")
    sql = "".join(lines)
    sql = sql.replace("\n", " ")
    sql += " INTO OUTFILE '" + TEMP_FN + "' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n'"

    # Execute the SQL
        
    # Create a CSV in the current directory 
    handle = open("/root/reports/" + base + "_" + range_e + ".csv", "w")
    for line in lines:
        handle.write(line)
    handle.close()

range_e = datetime.date.today()
range_s = (range_e - datetime.timedelta(days=3))
range2_e = range_e.strftime('%Y%m%d000000')
range_e = range_e.strftime('%Y-%m-%d')
range2_s = range_s.strftime('%Y%m%d000000')
range_s = range_s.strftime('%Y-%m-%d')

db = MySQLdb.connect(host="127.0.0.1", user="root", passwd=GetDatabasePassword(), db="alienvault")
cur = db.cursor()

# Process each report definition file and execute the proper SQL
for f in os.listdir("enabled"):
    if f[-4:] == ".sql" :
        base = f[:-4]

        # Read the SQL file to get the SQL code to run
        handle = open("enabled/" + f, 'r')
        lines = handle.readlines()
        handle.close()
        sql = " ".join(lines)

        # Determine an unused filename for the MySQL output
        TEMP_FN = GetTempFilename()

        # Old style reports implied this
        sql += " _CSV_"

        # Execute report
        ExecuteSQL(sql)

        # Convert the report output into proper CSV format and move it
        ConvertTempToReport()

    elif f[-5:] == ".json" :
        base = f[:-5]

        # Determine an unused filename for the MySQL output
        TEMP_FN = GetTempFilename()

        # Read the SQL file to get the SQL code to run
        handle = open("enabled/" + f, 'r')
        lines = handle.readlines()
        handle.close()
        data = " ".join(lines).replace("\n", " ")
        jdata = json.loads(data)

        # Execute the SQL
        for cmd in jdata :
          ExecuteSQL(cmd)

        # Convert the report output into proper CSV format and move it
        ConvertTempToReport()

db.close()
