SELECT
  id,
  hex(uuid),
  hex(ctx),
  title,
  date,
  ref,
  type_id,
  priority,
  status,
  last_update,
  in_charge,
  submitter,
  event_start,
  event_end
FROM incident
