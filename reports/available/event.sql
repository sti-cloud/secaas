SELECT
  hex(id),
  hex(agent_ctx),
  timestamp,
  tzone,
  hex(sensor_id),
  interface,
  type,
  plugin_id,
  plugin_sid,
  protocol,
  hex(src_ip),
  hex(dst_ip),
  src_port,
  dst_port,
  event_condition,
  value,
  time_interval,
  absolute,
  priority,
  reliability,
  asset_src,
  asset_dst,
  risk_a,
  risk_c,
  alarm,
  filename,
  username,
  password,
  userdata1,
  userdata2,
  userdata3,
  userdata4,
  userdata5,
  userdata6,
  userdata7,
  userdata8,
  userdata9,
  rulename,
  rep_prio_src,
  rep_prio_dst,
  rep_rel_src,
  rep_rel_dst,
  rep_act_src,
  rep_act_dst,
  src_hostname,
  dst_hostname,
  hex(src_mac),
  hex(dst_mac),
  hex(src_host),
  hex(dst_host),
  hex(src_net),
  hex(dst_net),
  refs
FROM event
WHERE timestamp >= '_START_'
  AND timestamp < '_END_'
