SELECT
  id,
  incident_id,
  ip,
  hex(ctx),
  port,
  nessus_id,
  risk,
  description
FROM incident_vulns
