SELECT
  id,
  incident_id,
  src_ips,
  dst_ips,
  dst_ports,
  hex(backlog_id),
  hex(event_id),
  hex(alarm_group_id)
FROM incident_alarm
