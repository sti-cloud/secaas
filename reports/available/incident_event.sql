SELECT
  id,
  incident_id,
  src_ips,
  src_ports,
  dst_ips,
  dst_ports
FROM incident_event
