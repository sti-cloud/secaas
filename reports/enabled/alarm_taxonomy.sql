SELECT
  sid,
  hex(engine_id),
  kingdom,
  category,
  subcategory
FROM alarm_taxonomy
