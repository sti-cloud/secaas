SELECT
  hex(plugin_ctx),
  plugin_id,
  sid,
  class_id,
  reliability,
  priority,
  REPLACE(REPLACE(REPLACE(name, '"', ' '), ',', ' '), '\\', '/'),
  aro,
  subcategory_id,
  category_id
FROM plugin_sid
