SELECT
  hex(host_id),
  property_ref,
  last_modified,
  source_id,
  value,
  extra,
  tzone
FROM host_properties
