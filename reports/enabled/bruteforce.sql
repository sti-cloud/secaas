SELECT
  HEX(id),
  INET_NTOA( CONV(hex(src_ip), 16, 10 )),
  INET_NTOA( CONV(hex(dst_ip), 16, 10 )),
  timestamp,
  USERNAME
FROM event
WHERE plugin_id = 1505 AND plugin_sid = 50005 AND timestamp >= '_START_' AND timestamp < '_END_'
