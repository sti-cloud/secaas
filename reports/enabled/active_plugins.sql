SELECT id, name, description
FROM plugin
WHERE id IN (SELECT DISTINCT plugin_id FROM alienvault_siem.acid_event)
ORDER BY name
