SELECT
  hex(backlog_id),
  hex(event_id),
  hex(corr_engine_ctx),
  timestamp,
  status,
  plugin_id,
  plugin_sid,
  protocol,
  hex(src_ip), 
  hex(dst_ip),
  src_port,
  dst_port,
  risk,
  efr,
  similar,
  removable,
  in_file
FROM alarm
WHERE timestamp >= '_START_'
  AND timestamp < '_END_'
