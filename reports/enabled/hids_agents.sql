SELECT sensor.name, agent_id, agent_name, agent_ip,
  CASE agent_status
    WHEN 1 THEN 'Never Connected'
    WHEN 2 THEN 'Disconnected'
    WHEN 3 THEN 'Connected'
    WHEN 4 THEN 'AlienVault Server'
  END
FROM hids_agents, sensor
WHERE sensor.id = hids_agents.sensor_id
