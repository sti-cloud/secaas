#!/bin/bash
CONF_FILE=$1

while IFS='' read -r line || [[ -n "$line" ]]; do
    if grep -F "$line" /etc/ossim/ossim.lic > /dev/null
    then
      echo "Found $line"
      /usr/bin/logger -n 199.7.247.139 -P 514 "CTX Subscription Removed from $line"
      rm -f /etc/cron.d/sti-*
      rm -f /etc/ossim/agent/plugins/*.local
      rm -f /etc/ossim/agent/plugins/*.py
      rm -f /etc/ossim/agent/plugins/*.config
      rm -f /etc/ossim/agent/plugins/lansweeper-assetmanagement*
      rm -f /etc/ossim/agent/plugins/cisco-amp.*
      rm -f /etc/ossim/agent/plugins/sti-*
      rm -rf /usr/local/bin/STITicketSystem
      rm -f /etc/ansible/facts.d/CTX.fact
      rm -f /etc/ansible/facts.d/customer.fact
      rm -f /var/log/ansible-pull.log*
      rm -rf /etc/definitions
      rm -rf /var/secaas
      rm -f /usr/local/bin/check_subscription.txt
      rm -f /usr/local/bin/check_subscription.sh

      history -c
      break
    fi
done < "$CONF_FILE"
