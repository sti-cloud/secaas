#!/bin/bash
USED_THRESHOLD=${1:-90}        #Percentage of disk space allowed to be used.
MONITORED_DIR=${2:-/mnt/logs}  #Destination mount to monitor disk space.
DELETE_DIR=${3:-/mnt/logs}     #Destination log directory to clean up.
MAX_AGE=${4:-365}              #Maximum file age.
ALWAYS_DEL_GT_MAX_AGE=${5:-1}  #1=Delete files above max age regardless of space, 0=only delete above max age when space is needed.

# Eliminate duplicates
#COUNT=`ps axfw | grep $0 | grep -v grep | grep -v cron | wc -l`
#if [ $COUNT -gt 2 ]; then
#  exit 0
#fi

# This function gets the used space of the mount point in monitored directory.
function usedSpace() {
   local used=$(df -P $MONITORED_DIR | awk '{ gsub("%",""); capacity = $5 }; END { print capacity }')
   echo $used
}

# This function will delete all files older than MAX_AGE
function deleteOlderThanMaxAge() {
   echo "Now going to delete files older than $MAX_AGE days."
   find $DELETE_DIR -type f -mtime +$MAX_AGE -regex ".*/20[0-9][0-9]/[01][0-9]/[0-3][0-9]/[0-9][0-9]/.*" -delete
   find $DELETE_DIR -type d -regex ".*/20[0-9][0-9].*" -empty -delete
}

if [[ $ALWAYS_DEL_GT_MAX_AGE -gt 0 ]]; then
   deleteOlderThanMaxAge
fi

# Loop through deleting anything old than MAX_AGE to free up space.  Progressively delete earlier and earlier files to maintain below capacity.
for (( c=0; c<=$MAX_AGE; c++)); do
  OLDER_THAN=$((MAX_AGE-c))
  if [[ "$(usedSpace)" -gt $USED_THRESHOLD ]]; then
    echo "Now going to delete files older than $OLDER_THAN days."
    find $DELETE_DIR -type f -mtime +$OLDER_THAN -regex ".*/20[0-9][0-9]/[01][0-9]/[0-3][0-9]/[0-9][0-9]/.*" -delete
    find $DELETE_DIR -type d -regex ".*/20[0-9][0-9].*" -empty -delete
  else
    exit 1 # Terminate the program and don't delete anymore.
  fi

done
