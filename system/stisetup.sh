#!/bin/sh
tempfile=`tempfile`
TITLE="Sentinel SecaaS Setup"
TICKET_CONFIG_FILE="/usr/local/bin/STITicketSystem/sti-ticket-config.cfg"
CTX_CUSTOMER_FACT_FILE="/etc/ansible/facts.d/customer.fact"
MENU_SYS_HEIGHT=25
MENU_SYS_WIDTH=65
function simpleInputBox {
  myTITLE=$1
  mySUBTITLE=$2
  myDEFVALUE=$3
  dialog --title "$myTITLE" \
    --inputbox "$mySUBTITLE" 15 55 "$myDEFVALUE" 2> $tempfile
  myreturn_value=$?
  case $myreturn_value in
    0)
      echo `cat $tempfile`;;
    1)
      echo "CANCEL";;
    255)
      echo "CANCEL";;
  esac
}

function getCurrentValue {
  myFILE=$1
  mySEARCHSTRING=$2
  echo `grep -Po "(?<=$mySEARCHSTRING).*" "$myFILE"`
}

function setValue {
  myFILE=$1
  mySEARCHSTRING=$2
  myVALUE=$3
  sed -ri "s/^($mySEARCHSTRING).*$/\1$myVALUE/" "$myFILE"
}

function SimpleSettingInputBox {
  myTITLE=$1
  mySUBTITLE=$2
  myFILE=$3
  mySEARCHSTRING=$4

  myDEFVALUE=$(getCurrentValue "$myFILE" "$mySEARCHSTRING")

  dialog --title "$myTITLE" \
    --inputbox "$mySUBTITLE" 15 $MENU_SYS_WIDTH "$myDEFVALUE" 2> $tempfile
  myreturn_value=$?
  case $myreturn_value in
    0)
      TEMP_VALUE=`cat $tempfile`
      setValue "$myFILE" "$mySEARCHSTRING" "$TEMP_VALUE";;
    1)
      echo "CANCEL";;
    255)
      echo "CANCEL";;
  esac
}

function ConfigureCTXMenu {
  while true
  do
    dialog --title "$TITLE" \
        --menu "Please choose an option you would like to configure:" $MENU_SYS_HEIGHT $MENU_SYS_WIDTH $MENU_SYS_HEIGHT \
        "1" "Set Customer Number" \
        "2" "Enable SNMP Traps" \
        "6" "Update CTX Now" \
        "7" "Run CTX Now" \
        "8" "Enable CTX" \
        "9" "Disable CTX" 2> $tempfile

    return_value=$?

    you_chose=`cat $tempfile`

    case $return_value in
      0)
        echo "'$you_chose' is the command you find most usefull."
        case $you_chose in
          1)
            SimpleSettingInputBox "$TITLE" "Customer Number:" "$CTX_CUSTOMER_FACT_FILE" "number=";;
          2)
            /usr/bin/apt install -y snmptrapd;
            update-rc.d snmptrapd defaults;
            setValue "/etc/ansible/facts.d/customer.fact" "enable\_snmp\_traps=" "true";
            echo "You must run CTX manually or wait for it to run automatically (if configured) for configuration files to be pushed down!";;
          6)
            /var/secaas/misc/troubleshooting/ctx update;;
          7)
            /var/secaas/misc/troubleshooting/ctx run;;
          8)
            /var/secaas/misc/troubleshooting/ctx enable;;
          9)
            /var/secaas/misc/troubleshooting/ctx disable;;
        esac;;
      1)
        break;;
      255)
        break;;
    esac
  done

}


function ConfigureTicketSystemMenu {
  while true
  do
    dialog --title "$TITLE" \
        --menu "Please choose an option you would like to configure:" $MENU_SYS_HEIGHT $MENU_SYS_WIDTH $MENU_SYS_HEIGHT \
        "1" "Set Customer Contact Name" \
        "2" "Set Customer Contact Email" \
        "3" "Set Customer Contact Phone" \
        "4" "Set Customer Number" \
        "5" "Set AIO NAT IP" \
        "6" "Set Client ID" \
        "7" "Set API Key" \
        "8" "Enable/Apply Ticketing System Configurations" \
        "9" "Disable Ticketing System" \
        "S" "Set Site ID" \
        "D" "Alert on ALL devices" 2> $tempfile

    return_value=$?

    you_chose=`cat $tempfile`

    case $return_value in
      0)
        echo "'$you_chose' is the command you find most usefull."
        case $you_chose in
          1)
            SimpleSettingInputBox "$TITLE" "Customer Contact Name:" "$TICKET_CONFIG_FILE" "CONTACT\_NAME=";;
          2)
            SimpleSettingInputBox "$TITLE" "Customer Contact Email:" "$TICKET_CONFIG_FILE" "CONTACT\_EMAIL=";;
          3)
            SimpleSettingInputBox "$TITLE" "Customer Contact Number:" "$TICKET_CONFIG_FILE" "CONTACT\_PHONE=";;
          4)
            SimpleSettingInputBox "$TITLE" "Customer Number:" "$TICKET_CONFIG_FILE" "CUSTOMER\_NUMBER=";;
          5)
            SimpleSettingInputBox "$TITLE" "STI AIO NAT IP:" "$TICKET_CONFIG_FILE" "STI\_NAT=";;
          6)
            SimpleSettingInputBox "$TITLE" "Ticket System Client ID:" "$TICKET_CONFIG_FILE" "CLIENTID=";;
          7)
            SimpleSettingInputBox "$TITLE" "Ticket System API Key:" "$TICKET_CONFIG_FILE" "APIKEY=";;
          8)
            setValue "/etc/ansible/facts.d/customer.fact" "enable\_sti\_ticket\_system=" "true";
            update-rc.d STITicketSystem.sh defaults;
            service STITicketSystem.sh restart;
            service STITicketSystem.sh status;;
          9)
            setValue "/etc/ansible/facts.d/customer.fact" "enable\_sti\_ticket\_system=" "false";
            rm -f /etc/monit/conf.d/STITicketingSystem.monitrc;
            service monit restart;
            service STITicketSystem.sh stop;
            update-rc.d -f STITicketSystem.sh remove;;
          S)
            SimpleSettingInputBox "$TITLE" "Customer Site ID:" "$TICKET_CONFIG_FILE" "CUSTOMER\_SITEID=";;
          D)
            echo "0.0.0.0/0" >> /usr/local/bin/STITicketSystem/sti-device-config.cfg;;
        esac;;
      1)
        break;;
      255)
        break;;
    esac
  done

}


function ConfigureFirepowerATRMenu {
  while true
  do
    dialog --title "$TITLE" \
        --menu "Please choose an option you would like to configure:" $MENU_SYS_HEIGHT $MENU_SYS_WIDTH $MENU_SYS_HEIGHT \
        "8" "Enable Firepower ATR Script" \
        "9" "Disable Firepower ATR Script" 2> $tempfile

    return_value=$?

    you_chose=`cat $tempfile`

    case $return_value in
      0)
        echo "'$you_chose' is the command you find most usefull."
        case $you_chose in
          8)
            setValue "/etc/ansible/facts.d/customer.fact" "enable\_firepower=" "true";
            update-rc.d ftd_shun.sh defaults;
            service ftd_shun.sh restart;;
          9)
            setValue "/etc/ansible/facts.d/customer.fact" "enable\_firepower=" "false";
            rm -f /etc/monit/conf.d/ftd_shun.monitrc;
            service monit restart;
            service ftd_shun.sh stop;
            update-rc.d -f ftd_shun.sh remove;
            # May not be needed, not sure.
            /usr/bin/pkill -9 -x ftd_shun.py;;
        esac;;
      1)
        break;;
      255)
        break;;
    esac
  done

}



while true
do

dialog --title "$TITLE" \
        --menu "Please choose an option you would like to configure:" $MENU_SYS_HEIGHT $MENU_SYS_WIDTH $MENU_SYS_HEIGHT \
        "1" "Configure CTX" \
        "2" "Configure Ticketing System" \
        "3" "Configure Firepower ATR" 2> $tempfile

return_value=$?

you_chose=`cat $tempfile`

case $return_value in
  0)
    echo "'$you_chose' is the command you find most usefull."
    case $you_chose in
      1)
        ConfigureCTXMenu;;
        #SimpleSettingInputBox "$TITLE" "Customer Number:" "/etc/ansible/facts.d/customer.fact" "number=";;
        #CURRENT_VALUE=$(getCurrentValue "/etc/ansible/facts.d/customer.fact" "number=")
        #simpleInputBox "$TITLE" "Customer Number:" "$CURRENT_VALUE" ;;
      2)
        ConfigureTicketSystemMenu;;
      3)
        ConfigureFirepowerATRMenu;;
    esac;;
#    dialog --title "$TITLE" \
#      --inputbox "Customer Number:" 15 55 "test" 2> $tempfile
  1)
    break;;
    #echo "You pressed cancel.";;
  255)
    break;;
    #echo "You hit Esc.";;
esac

done