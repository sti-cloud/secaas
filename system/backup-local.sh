#!/bin/bash
# Script written by Sidney Eaton
# Version 1.0.0
# Description: Perform an rsync from the local alienvault ossim/log directory
# to a local destination directory. Do not sync over todays logs or uncompressed files as
# not to copy over something and then have to remove it.  Only sync over files in their
# final state.
#
# This script should be combined with a script to delete files periodically.  For modularity,
# this script does not have that functionality built-in.

SRC_BASE=${1:-/var/ossim/logs}
DST_BASE=${2:-/mnt/logs}
YEAR=$(date --utc +%Y)
MONTH=$(date --utc +%m)
DAY=$(date --utc +%d)
RSYNC_PATH=/usr/bin

$RSYNC_PATH/rsync -aq $SRC_BASE $DST_BASE --exclude="$YEAR/$MONTH/$DAY" --exclude='*.log' --exclude='last' --exclude='searches'