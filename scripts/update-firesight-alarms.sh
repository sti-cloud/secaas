#!/bin/bash
echo "
UPDATE alarm
INNER JOIN backlog_event
   ON backlog_event.backlog_id = alarm.backlog_id
INNER JOIN event
   ON event.id = backlog_event.event_id
SET alarm.plugin_id = event.plugin_id,
    alarm.plugin_sid = event.plugin_sid
WHERE alarm.plugin_id = 1505 AND
    (alarm.plugin_sid = 710031 OR alarm.plugin_sid = 710032) AND
    event.plugin_id <> 1505
;
" | ossim-db
