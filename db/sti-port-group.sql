# Alienvault STI Port Group
# Last Modified By: Sidney Eaton
# Last Update: 4-26-2017
#
# This file is used to generate and distribute Sentinel's built in Security As A Service files.
# The file is distributed through the Sentinel Distribution and Update system and will be applied
# in full each and every time there is a change to the file.  Therefore it is absolutely critical
# that all code within this file handle error conditions and update the database regardless of what
# state or patch level the database is at.  Two critical directives in acomplishing this is the
# INSERT IGNORE MySQL syntax and the the INSERT...ON DUPLICATE KEY UPDATE directive.  The first
# should be used when values are likely to never be changed later.  It tells the database that instead
# of erroring if the key exists, just WARN and continue on.  Thus simplistic error handling.  The second
# should be used if the values are likely to be adjusted overtime.  Thus it says, if it doesn't exist yet insert it
# and if it does exist update the values.  This also avoides a fatal error.

# Get the default correlation engine context and set it as a variable and set global variables.
select @enginectx := engine_ctx FROM corr_engine_contexts WHERE descr = 'Default';
select @eventctx := event_ctx FROM corr_engine_contexts WHERE descr = 'Default';


SET @PortGroupPolicyID = 2002000001;
INSERT IGNORE into port_group (id, ctx, name, descr) VALUES (@PortGroupPolicyID, @eventctx, "STI-DNS", "STI DNS Ports");
INSERT IGNORE into port_group_reference (port_group_id, port_ctx, port_number, protocol_name) VALUES
  (@PortGroupPolicyID, @eventctx, 53, 'tcp'),
  (@PortGroupPolicyID, @eventctx, 53, 'udp');


SET @PortGroupPolicyID = 2002000002;
INSERT IGNORE into port_group (id, ctx, name, descr) VALUES (@PortGroupPolicyID, @eventctx, "STI-ALL-HTTP", "STI HTTP Ports");
INSERT IGNORE into port_group_reference (port_group_id, port_ctx, port_number, protocol_name) VALUES
  (@PortGroupPolicyID, @eventctx, 80, 'tcp'),
  (@PortGroupPolicyID, @eventctx, 80, 'udp'),
  (@PortGroupPolicyID, @eventctx, 443, 'tcp'),
  (@PortGroupPolicyID, @eventctx, 443, 'udp');