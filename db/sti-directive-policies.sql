# Alienvault STI Local Server Generated Directive Policies Core File
# Last Modified By: Sidney Eaton
# Last Update: 12-26-2015
#
# This file is used to generate and distribute Sentinels built in Security As A Service files.
# The file is distributed through the Sentinel Distribution and Update system and will be applied
# in full each and every time there is a change to the file.  Therefore it is absolutely critical
# that all code within this file handle error conditions and update the database regardless of what
# state or patch level the database is at.  Two critical directives in acomplishing this is the
# INSERT IGNORE MySQL syntax and the the INSERT...ON DUPLICATE KEY UPDATE directive.  The first
# should be used when values are likely to never be changed later.  It tells the database that instead
# of erroring if the key exists, just WARN and continue on.  Thus simplistic error handling.  The second
# should be used if the values are likely to be adjusted overtime.  Thus it says, if it does not exist yet insert it
# and if it does exist update the values.  This also avoides a fatal error.

# Get the default correlation engine context and set it as a variable and set global variables.
# ENGINE CTX = LOCAL SERVER DIRECTIVE ALERTS, EVENT CTX = EVENT PROCCESSING SECTION
select @enginectx := engine_ctx FROM corr_engine_contexts WHERE descr = 'Default';
select @eventctx := event_ctx FROM corr_engine_contexts WHERE descr = 'Default';
# Set the starting order of the policy
SET @order = 4999;

################INSTRUCTIONS#######################
# THE VARIABLES BELOW ARE TO REDUCE MISTAKES
# THEY MUST BE SPECIFIED FOR EVERY NEW POLICY!!
###################################################

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #1.
SET @order = @order + 1;
SET @policyName = 'STI-AUTOMATED-SRC-SHUN';
SET @currentPolicyID = '0200230000';
SET @correlate = 1;
SET @crosscorrelate = 1;
SET @forward_events = 0;
SET @sqlstore = 1;
SET @performSIEM = 1;
SET @riskAssessment = 1;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@enginectx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@enginectx;
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,'0200200005') ON DUPLICATE KEY UPDATE plugin_group_id='0200200005';
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #2.
SET @order = @order + 1;
SET @policyName = 'STI-AUTOMATED-DST-SHUN';
SET @currentPolicyID = '0200230001';
SET @correlate = 1;
SET @crosscorrelate = 1;
SET @forward_events = 0;
SET @sqlstore = 1;
SET @performSIEM = 1;
SET @riskAssessment = 1;
SET @active = 0;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@enginectx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@enginectx;
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,'0200200006') ON DUPLICATE KEY UPDATE plugin_group_id='0200200006';
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;