# Alienvault STI Host Group
# Last Modified By: Sidney Eaton
# Last Update: 6-12-2018
#
# This file is used to generate and distribute Sentinel's built in Security As A Service files.
# The file is distributed through the Sentinel Distribution and Update system and will be applied
# in full each and every time there is a change to the file.  Therefore it is absolutely critical
# that all code within this file handle error conditions and update the database regardless of what
# state or patch level the database is at.  Two critical directives in acomplishing this is the
# INSERT IGNORE MySQL syntax and the the INSERT...ON DUPLICATE KEY UPDATE directive.  The first
# should be used when values are likely to never be changed later.  It tells the database that instead
# of erroring if the key exists, just WARN and continue on.  Thus simplistic error handling.  The second
# should be used if the values are likely to be adjusted overtime.  Thus it says, if it doesn't exist yet insert it
# and if it does exist update the values.  This also avoides a fatal error.

# Get the default correlation engine context and set it as a variable and set global variables.
select @enginectx := engine_ctx FROM corr_engine_contexts WHERE descr = 'Default';
select @eventctx := event_ctx FROM corr_engine_contexts WHERE descr = 'Default';

# To temporarily fix previous issues.
update host_group set id = hex('15000001') where id = 102002001;

# Create and populate the STI-VULNERABILITY-SCANNERS GROUP
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000001'),@eventctx,'STI-VULNERABILITY-SCANNERS','Sentinel built-in group for rules that should contain only vulnerability scanners');
#INSERT IGNORE INTO host_group_reference (host_group_id,host_id) SELECT hex('15000001'), host_ip.host_id from host_ip, sensor where host_ip.ip = sensor.ip;

# Create Empty Groups
INSERT IGNORE into host_group (id,ctx,name,descr) values ('102002002',@eventctx,'STI-MEDIA-TV','Sentinel built-in group for 1st and 2nd generation internet enabled TVs. Purpose is to reduce false positives.  Sony TVs should be in here for example.');


# The unhex only works with a certain number of characters and older versions of AlienVault would not correctly work with the group if it wasn't unhexable.
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000003'),@eventctx,'STI-NETWORK-MONITORING','Sentinel built-in group for devices that poll and monitor devices on the network. Purpose is to reduce false positives.  Solarwinds, IPSwitch, etc. should be in here for example.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000004'),@eventctx,'STI-EMAIL-SERVERS','Sentinel built-in group for email servers and email relay devices. Purpose is to reduce false positives.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000005'),@eventctx,'STI-CONSUMER-GRADE-DEVICES','Sentinel built-in group for customers to add consumer grade devices for alerting. Like Linksys routers.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000006'),@eventctx,'STI-NO-APACHE','Sentinel built-in group for customers to add devices that should not alert on apache products.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000007'),@eventctx,'STI-IP-CAMERA','Sentinel built-in group for IP Cameras.  This group allows targeted alerting for such devices.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000008'),@eventctx,'STI-REDUNDANTLY-SPANNED-WEBFILTERS','Sentinel built-in group for redundantly spanned web filters.  This is specifically targeted for WCCP deployments with ASA.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000009'),@eventctx,'STI-FIREWALLS','Sentinel built-in group for customer firewalls.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000010'),@eventctx,'STI-NO-SSH-OUTBOUND-CHECKS','Sentinel built-in group for suppressing outbound SSH checks such as scanning.');


INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000011'),@eventctx,'STI-NO-UPDATED-SYS-CHECKS','Sentinel built-in group for suppressing alerts related to outdated systems.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000012'),@eventctx,'STI-BACKUP-SERVERS','Sentinel built-in group for suppressing various checks that commonly lead to false positives on backup systems.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000013'),@eventctx,'STI-WHITELIST-DESTINATION','Sentinel built-in group for suppressing all checks on a destination IP.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000014'),@eventctx,'STI-WHITELIST-SOURCE','Sentinel built-in group for suppressing all checks on a source IP.');

INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000015'),@eventctx,'STI-WHITELIST-UA-DESTINATION','Sentinel built-in group for suppressing all checks on a destination IP for suspicious user-agents.');
INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000016'),@eventctx,'STI-WHITELIST-UA-SOURCE','Sentinel built-in group for suppressing all checks on a source IP for suspicious user-agents.');

INSERT IGNORE into host_group (id,ctx,name,descr) values (hex('15000017'),@eventctx,'STI-WHITELIST-CLEARTEXT-PASSWORDS','Sentinel built-in group for suppressing all clear text password checks.');