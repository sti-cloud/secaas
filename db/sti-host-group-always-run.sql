# Alienvault STI Host Group Always Run
# Last Modified By: Sidney Eaton
# Last Update: 5-6-2019
#
# This file is used to generate and distribute Sentinel's built in Security As A Service files.
# The file is distributed through the Sentinel Distribution and Update system and will be applied
# in full each and every time there is a change to the file.  Therefore it is absolutely critical
# that all code within this file handle error conditions and update the database regardless of what
# state or patch level the database is at.  Two critical directives in acomplishing this is the
# INSERT IGNORE MySQL syntax and the the INSERT...ON DUPLICATE KEY UPDATE directive.  The first
# should be used when values are likely to never be changed later.  It tells the database that instead
# of erroring if the key exists, just WARN and continue on.  Thus simplistic error handling.  The second
# should be used if the values are likely to be adjusted overtime.  Thus it says, if it doesn't exist yet insert it
# and if it does exist update the values.  This also avoides a fatal error.
#
# ***** Any SQL in this file will run every CTX run via a cron job daily.


# Get the default correlation engine context and set it as a variable and set global variables.
select @enginectx := engine_ctx FROM corr_engine_contexts WHERE descr = 'Default';
select @eventctx := event_ctx FROM corr_engine_contexts WHERE descr = 'Default';

# Create global procedure
-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STIAddAssetToGroup;
DELIMITER $$
CREATE PROCEDURE STIAddAssetToGroup(HOSTGROUPID varchar(12), ASSETID varchar(12))
BEGIN
  INSERT IGNORE INTO host_group_reference (host_group_id,host_id) VALUES (hex(HOSTGROUPID), hex(ASSETID));
END$$

DELIMITER ;

# Populate the STI-VULNERABILITY-SCANNERS GROUP
INSERT IGNORE INTO host_group_reference (host_group_id,host_id) SELECT hex('15000001'), host_ip.host_id from host_ip, sensor where host_ip.ip = sensor.ip;

# Populate the STI-FIREWALLS GROUP
INSERT IGNORE INTO host_group_reference (host_group_id,host_id) SELECT hex('15000009'), host_scan.host_id from host_scan where plugin_id = 1636;

# Populate groups that must have fake assets
call STIAddAssetToGroup('15000003','0200200001');
call STIAddAssetToGroup('15000004','0200200001');
call STIAddAssetToGroup('15000005','0200200001');
call STIAddAssetToGroup('15000006','0200200001');
call STIAddAssetToGroup('15000007','0200200001');
call STIAddAssetToGroup('15000008','0200200001');
call STIAddAssetToGroup('15000009','0200200001');
call STIAddAssetToGroup('15000010','0200200001');
call STIAddAssetToGroup('15000011','0200200001');
call STIAddAssetToGroup('15000012','0200200001');
call STIAddAssetToGroup('15000013','0200200001');
call STIAddAssetToGroup('15000014','0200200001');
call STIAddAssetToGroup('15000015','0200200001');
call STIAddAssetToGroup('15000016','0200200001');
INSERT IGNORE INTO host_group_reference (host_group_id,host_id) VALUES ('102002002', hex('0200200001'));

call STIAddAssetToGroup('15000015','02002004');

# Add Konica stuff to cleartext password whitelist.
call STIAddAssetToGroup('15000017','02002005');
call STIAddAssetToGroup('15000017','02002006');

# OpenDNS block and phishing page whitelist.
call STIAddAssetToGroup('15000013','02002007');

# STI Monitoring nodes to whitelist
call STIAddAssetToGroup('15000003','02002008');
call STIAddAssetToGroup('15000003','02002009');
call STIAddAssetToGroup('15000003','02002010');
call STIAddAssetToGroup('15000003','02002010');
call STIAddAssetToGroup('15000003','02002011');
call STIAddAssetToGroup('15000003','02002012');
call STIAddAssetToGroup('15000003','02002013');
call STIAddAssetToGroup('15000003','02002014');
call STIAddAssetToGroup('15000003','02002015');
call STIAddAssetToGroup('15000003','02002016');
call STIAddAssetToGroup('15000003','02002017');
call STIAddAssetToGroup('15000003','02002018');
call STIAddAssetToGroup('15000003','02002019');
call STIAddAssetToGroup('15000003','02002020');