# Alienvault STI Host
# Last Modified By: Sidney Eaton
# Last Update: 5-6-2019
#
# This file is used to generate assets for distribution through CTX.
# The file is distributed through the Sentinel Distribution and Update system and will be applied
# in full each and every time there is a change to the file.  Therefore it is absolutely critical
# that all code within this file handle error conditions and update the database regardless of what
# state or patch level the database is at.  Two critical directives in acomplishing this is the
# INSERT IGNORE MySQL syntax and the the INSERT...ON DUPLICATE KEY UPDATE directive.  The first
# should be used when values are likely to never be changed later.  It tells the database that instead
# of erroring if the key exists, just WARN and continue on.  Thus simplistic error handling.  The second
# should be used if the values are likely to be adjusted overtime.  Thus it says, if it doesn't exist yet insert it
# and if it does exist update the values.  This also avoides a fatal error.

# Get the default correlation engine context and set it as a variable and set global variables.
select @enginectx := engine_ctx FROM corr_engine_contexts WHERE descr = 'Default';
select @eventctx := event_ctx FROM corr_engine_contexts WHERE descr = 'Default';

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateAssetEntry;
DELIMITER $$
CREATE PROCEDURE STICreateAssetEntry(HOSTID varchar(12), myCTX binary(16), myHOSTNAME varchar(128), fqdn varchar(255), myASSET smallint(6), threshc int(11), thresha int(11), myALERT int(11), myPERSISTENCE int(11), isNAT varchar(15), RRDPROFILE varchar(64), description varchar(255), myLAT varchar(255), myLON varchar(255), myCOUNTRY varchar(64), isEXTERNAL tinyint(1), myPERMISSIONS binary(8), myIP varchar(100))
BEGIN
  INSERT INTO host (id, ctx, hostname, fqdns, asset, threshold_c, threshold_a, alert, persistence, nat, rrd_profile, descr, lat, lon, icon, country, external_host, permissions, av_component, created, updated) VALUES (hex(HOSTID), myCTX, myHOSTNAME, fqdn, myASSET, threshc, thresha, myALERT, myPERSISTENCE, isNAT, RRDPROFILE, description, myLAT, myLON, NULL, myCOUNTRY, isEXTERNAL, myPERMISSIONS, 0, NULL, NULL) ON DUPLICATE KEY UPDATE descr=description,hostname=myHOSTNAME,alert=myALERT,external_host=isEXTERNAL,persistence=myPERSISTENCE,permissions=myPERMISSIONS,asset=myASSET;
  INSERT INTO host_ip (host_id, ip, mac, interface) VALUES (hex(HOSTID),UNHEX(CONV(INET_ATON(myIP),10,16)),NULL,NULL) ON DUPLICATE KEY UPDATE ip=UNHEX(CONV(INET_ATON(myIP),10,16));
END$$

DELIMITER ;

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STIAssetIntoAllSensors;
DELIMITER $$
CREATE PROCEDURE STIAssetIntoAllSensors(HOSTID varchar(8))
BEGIN
  INSERT IGNORE INTO host_sensor_reference (host_id,sensor_id) SELECT hex(HOSTID), sensor.id from sensor;
END$$

DELIMITER ;


## Need to fix the 0200200001 numbering scheme in the future.  For now it is fine since the values are cut off and unique.
call STICreateAssetEntry('0200200001',@eventctx,'STI-FAKE','',0,0,0,0,1,0,0,'Do not delete this object.  It should be used in empty AlienVault host groups to prevent match all by default.',0,0,NULL,1,unhex('0000000000000000'),'1.1.1.1');
INSERT IGNORE INTO host_sensor_reference (host_id,sensor_id) SELECT hex('0200200001'), sensor.id from sensor;

# NUMBERING BELOW SHOULD BE 02002XXX where XXX IS THE NEXT NUMBER IN THE SEQUENCE GOING OVER 1000 will exceed DB storage and cut off characters.

#call STICreateAssetEntry('02002002',@eventctx,'02002-DC','',0,0,0,0,1,0,0,'Do not delete this object.  It should be used in empty AlienVault host groups to prevent match all by default.',0,0,NULL,1,unhex('0000000000000000'),'199.7.247.145');
#call STIAssetIntoAllSensors('02002002');

#call STICreateAssetEntry('02002003',@eventctx,'02002-BACKUP','',0,0,0,0,1,0,0,'Do not delete this object.  It should be used in empty AlienVault host groups to prevent match all by default.',0,0,NULL,1,unhex('0000000000000000'),'199.7.247.135');
#call STIAssetIntoAllSensors('02002003');

call STICreateAssetEntry('02002004',@eventctx,'WebSense-BlackSpider-1','',0,0,0,0,1,0,0,'Do not delete this object.  It should be used in empty AlienVault host groups to prevent match all by default.',0,0,NULL,1,unhex('0000000000000000'),'208.87.237.161');
call STIAssetIntoAllSensors('02002004');

call STICreateAssetEntry('02002005',@eventctx,'Konica-Minolta-1','',0,0,0,0,1,0,0,'Do not delete this object.  Konica Minolta Fax sends status in using cleartext passwords.',0,0,NULL,1,unhex('0000000000000000'),'207.18.56.55');
call STIAssetIntoAllSensors('02002005');

call STICreateAssetEntry('02002006',@eventctx,'Konica-Minolta-2','',0,0,0,0,1,0,0,'Do not delete this object.  Konica Minolta Fax sends status in using cleartext passwords.',0,0,NULL,1,unhex('0000000000000000'),'207.18.56.88');
call STIAssetIntoAllSensors('02002006');

call STICreateAssetEntry('02002007',@eventctx,'OpenDNS-BlockPage-1','',0,0,0,0,1,0,0,'Do not delete this object.  OpenDNS Block page for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'204.194.238.141');
call STIAssetIntoAllSensors('02002007');

call STICreateAssetEntry('02002008',@eventctx,'STI-Monitoring-1','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.244.110');
call STIAssetIntoAllSensors('02002008');

call STICreateAssetEntry('02002009',@eventctx,'STI-Monitoring-2','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.244.111');
call STIAssetIntoAllSensors('02002009');

call STICreateAssetEntry('02002010',@eventctx,'STI-Monitoring-3','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.244.112');
call STIAssetIntoAllSensors('02002010');

call STICreateAssetEntry('02002011',@eventctx,'STI-Monitoring-4','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.244.81');
call STIAssetIntoAllSensors('02002011');

call STICreateAssetEntry('02002012',@eventctx,'STI-Monitoring-5','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.244.82');
call STIAssetIntoAllSensors('02002012');

call STICreateAssetEntry('02002013',@eventctx,'STI-Monitoring-6','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.244.83');
call STIAssetIntoAllSensors('02002013');

call STICreateAssetEntry('02002014',@eventctx,'STI-Monitoring-7','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.244.84');
call STIAssetIntoAllSensors('02002014');

call STICreateAssetEntry('02002015',@eventctx,'STI-Monitoring-8','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.244.85');
call STIAssetIntoAllSensors('02002015');

call STICreateAssetEntry('02002016',@eventctx,'STI-Monitoring-9','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.245.81');
call STIAssetIntoAllSensors('02002016');

call STICreateAssetEntry('02002017',@eventctx,'STI-Monitoring-10','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.245.82');
call STIAssetIntoAllSensors('02002017');

call STICreateAssetEntry('02002018',@eventctx,'STI-Monitoring-11','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.245.83');
call STIAssetIntoAllSensors('02002018');

call STICreateAssetEntry('02002019',@eventctx,'STI-Monitoring-12','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.245.84');
call STIAssetIntoAllSensors('02002019');

call STICreateAssetEntry('02002020',@eventctx,'STI-Monitoring-13','',0,0,0,0,1,0,0,'Do not delete this object.  STI Monitoring Node for whitelisting',0,0,NULL,1,unhex('0000000000000000'),'199.7.245.85');
call STIAssetIntoAllSensors('02002020');







