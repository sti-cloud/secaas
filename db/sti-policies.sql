# Alienvault STI Policy Core File
# Last Modified By: Sidney Eaton
# Last Update: 9-11-2017
#
# This file is used to generate and distribute Sentinels built in Security As A Service files.
# The file is distributed through the Sentinel Distribution and Update system and will be applied
# in full each and every time there is a change to the file.  Therefore it is absolutely critical
# that all code within this file handle error conditions and update the database regardless of what
# state or patch level the database is at.  Two critical directives in acomplishing this is the
# INSERT IGNORE MySQL syntax and the the INSERT...ON DUPLICATE KEY UPDATE directive.  The first
# should be used when values are likely to never be changed later.  It tells the database that instead
# of erroring if the key exists, just WARN and continue on.  Thus simplistic error handling.  The second
# should be used if the values are likely to be adjusted overtime.  Thus it says, if it does not exist yet insert it
# and if it does exist update the values.  This also avoides a fatal error.

# Get the default correlation engine context and set it as a variable and set global variables.
select @enginectx := engine_ctx FROM corr_engine_contexts WHERE descr = 'Default';
select @eventctx := event_ctx FROM corr_engine_contexts WHERE descr = 'Default';

# Set constants
SET @PluginGroupNullValue = unhex('00000000000000000000000000000000');
SET @SensorIDNullValue = unhex('00000000000000000000000000000000');
SET @HOMENET = unhex('01000000000000000000000000000000');
SET @NOTHOMENET = unhex('02000000000000000000000000000000');

# Set the starting order of the policy
SET @order = 4999;

################INSTRUCTIONS#######################
# THE VARIABLES BELOW ARE TO REDUCE MISTAKES
# THEY MUST BE SPECIFIED FOR EVERY NEW POLICY!!
###################################################

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #6.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-SENSOR-FALSE-POSITIVES';
SET @currentPolicyID = hex('20020006');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
## INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000001'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000008')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000008');
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000024')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000024');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #13.  **This policy will not force activation or deactivation.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-WEBFILTER-OUT-FALSE-POSITIVES';
SET @currentPolicyID = hex('20020013');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 0;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 2002000002;
SET @SourcePortGroupID = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx;
## INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000008'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000014')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000014');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #16.  **This policy will not force activation or deactivation.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-WEBFILTER-IN-FALSE-POSITIVES';
SET @currentPolicyID = hex('20020016');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 0;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 0;
SET @SourcePortGroupID = 2002000002;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx;
## INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000008'),'dest') ON DUPLICATE KEY UPDATE direction='dest';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000014')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000014');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #21.
SET @order = @order + 1;
SET @policyName = 'STI-REMOVE-EXTERNAL-DNS-QUERIES';
SET @currentPolicyID = hex('20020021');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 2002000001;
SET @SourcePortGroupID = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,@NOTHOMENET,'source') ON DUPLICATE KEY UPDATE host_id=@NOTHOMENET,direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,@HOMENET,'dest') ON DUPLICATE KEY UPDATE host_id=@HOMENET,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000021')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000021');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #17.  **This policy will not force activation or deactivation.
SET @order = @order + 1;
SET @policyName = 'STI-OPTIMIZE-STIWINDNS-LOGS';
SET @currentPolicyID = hex('20020017');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 0;
SET @SourcePortGroupID = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
DELETE FROM policy_plugin_group_reference where policy_id = @currentPolicyID and plugin_group_id=hex('15000014');
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000017')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000017');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #15.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-EMAIL-DNS-NOISE';
SET @currentPolicyID = hex('20020015');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 1;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000004'),'source') ON DUPLICATE KEY UPDATE direction='source';
##INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,2002000001,'dest') ON DUPLICATE KEY UPDATE port_group_id=2002000001,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,@PluginGroupNullValue) ON DUPLICATE KEY UPDATE plugin_group_id=@PluginGroupNullValue;
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #22.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-EMAIL-DNS-NOISE-REV';
SET @currentPolicyID = hex('20020022');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 1;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000004'),'dest') ON DUPLICATE KEY UPDATE direction='dest';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
DELETE FROM policy_port_reference WHERE policy_id = @currentPolicyID AND direction = 'source'; 
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,2002000001,'source') ON DUPLICATE KEY UPDATE port_group_id=2002000001,direction='source';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,@PluginGroupNullValue) ON DUPLICATE KEY UPDATE plugin_group_id=@PluginGroupNullValue;
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
-- time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #18.
SET @order = @order + 1;
SET @policyName = 'STI-SUPPRESS-SSH-OUT';
SET @currentPolicyID = hex('20020018');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 0;
SET @SourcePortGroupID = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000010'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000018')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000018');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #19.
SET @order = @order + 1;
SET @policyName = 'STI-SUPPRESS-NON-UPDATED-SYSTEMS';
SET @currentPolicyID = hex('20020019');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 0;
SET @SourcePortGroupID = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000011'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000019')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000019');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #23.
SET @order = @order + 1;
SET @policyName = 'STI-WHITELIST-DESTINATION';
SET @currentPolicyID = hex('20020023');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000013'),'dest') ON DUPLICATE KEY UPDATE direction='dest';
##INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,@PluginGroupNullValue) ON DUPLICATE KEY UPDATE plugin_group_id=@PluginGroupNullValue;
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #24.
SET @order = @order + 1;
SET @policyName = 'STI-WHITELIST-SOURCE';
SET @currentPolicyID = hex('20020024');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000014'),'source') ON DUPLICATE KEY UPDATE direction='source';
##INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,@PluginGroupNullValue) ON DUPLICATE KEY UPDATE plugin_group_id=@PluginGroupNullValue;
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #20.
SET @order = @order + 1;
SET @policyName = 'STI-SUPPRESS-BACKUP-SYSTEM-ALERTS';
SET @currentPolicyID = hex('20020020');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 0;
SET @SourcePortGroupID = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000012'),'dest') ON DUPLICATE KEY UPDATE direction='dest';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000020')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000020');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #25.
SET @order = @order + 1;
SET @policyName = 'STI-WHITELIST-SUSPIC-UA-SOURCE';
SET @currentPolicyID = hex('20020025');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000016'),'source') ON DUPLICATE KEY UPDATE direction='source';
##INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000022')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000022');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;


START TRANSACTION;
# BEGIN Sentinel Built-In Policy #26.
SET @order = @order + 1;
SET @policyName = 'STI-WHITELIST-SUSPIC-UA-DESTINATION';
SET @currentPolicyID = hex('20020026');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000015'),'dest') ON DUPLICATE KEY UPDATE direction='dest';
##INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000022')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000022');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #27.
SET @order = @order + 1;
SET @policyName = 'STI-WHITELIST-CLEARTEXT-SRC';
SET @currentPolicyID = hex('20020027');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 0;
SET @SourcePortGroupID = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000017'),'source') ON DUPLICATE KEY UPDATE direction='source';
##INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000023')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000023');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #28.
SET @order = @order + 1;
SET @policyName = 'STI-WHITELIST-CLEARTEXT-DST';
SET @currentPolicyID = hex('20020028');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
# For Destination and Source Port Group ID 0 = ALL ports/protos as defined by AlienVault
SET @DestinationPortGroupID = 0;
SET @SourcePortGroupID = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx,active=@active;
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000017'),'dest') ON DUPLICATE KEY UPDATE direction='dest';
##INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@DestinationPortGroupID,'dest') ON DUPLICATE KEY UPDATE port_group_id=@DestinationPortGroupID,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,@SourcePortGroupID,'source') ON DUPLICATE KEY UPDATE port_group_id=@SourcePortGroupID,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000023')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000023');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #7.  **This policy will not force activation or deactivation.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-KASEYE-RDP';
SET @currentPolicyID = hex('20020007');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 0;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
##INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000001'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000009')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000009');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #14.  **This policy will not force activation or deactivation.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-SKYPE-POLICY';
SET @currentPolicyID = hex('20020014');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 0;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
##INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000001'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000016')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000016');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;


START TRANSACTION;
# BEGIN Sentinel Built-In Policy #4.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-MEDIA-TV-FALSE-POSITIVES';
SET @currentPolicyID = '0200210003';
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
## INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,'102002002','source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,'0200200004') ON DUPLICATE KEY UPDATE plugin_group_id='0200200004';
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #10.
SET @order = @order + 1;
SET @policyName = 'STI-SUPPRESS-APACHE';
SET @currentPolicyID = hex('20020010');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
#INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000006'),'source') ON DUPLICATE KEY UPDATE direction='source',host_group_id=hex('15000006');
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000006'),'dest') ON DUPLICATE KEY UPDATE direction='dest',host_group_id=hex('15000006');
#INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000012')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000012');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #8.
SET @order = @order + 1;
SET @policyName = 'STI-CHECK-FOR-CONSUMER-GRADE-DEVICES';
SET @currentPolicyID = hex('20020008');
SET @correlate = 1;
SET @crosscorrelate = 1;
SET @forward_events = 0;
SET @sqlstore = 1;
SET @performSIEM = 1;
SET @riskAssessment = 1;
SET @active = 1;
SET @priority = -1;
SET @logger = 1;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
DELETE from policy_host_group_reference where policy_id = @currentPolicyID and direction = 'source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
#INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000005'),'source') ON DUPLICATE KEY UPDATE direction='source',host_group_id=hex('15000005');
DELETE from policy_host_reference where policy_id = @currentPolicyID and direction = 'dest';
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000005'),'dest') ON DUPLICATE KEY UPDATE direction='dest',host_group_id=hex('15000005');
#INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000011')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000011');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;




START TRANSACTION;
# BEGIN Sentinel Built-In Policy #9.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-CONSUMER-GRADE-CHECKS';
SET @currentPolicyID = hex('20020009');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
#INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000004'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000011')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000011');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #11.
SET @order = @order + 1;
SET @policyName = 'STI-CHECK-FOR-CAMERA-DEVICES';
SET @currentPolicyID = hex('20020011');
SET @correlate = 1;
SET @crosscorrelate = 1;
SET @forward_events = 0;
SET @sqlstore = 1;
SET @performSIEM = 1;
SET @riskAssessment = 1;
SET @active = 1;
SET @priority = -1;
SET @logger = 1;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
#INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000007'),'source') ON DUPLICATE KEY UPDATE direction='source',host_group_id=hex('15000007');
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000007'),'dest') ON DUPLICATE KEY UPDATE direction='dest',host_group_id=hex('15000007');
#INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000013')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000013');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #12.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-CAMERA-CHECKS';
SET @currentPolicyID = hex('20020012');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
#INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000007'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000013')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000013');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #5.
SET @order = @order + 1;
SET @policyName = 'STI-ELIMINATE-NET-MON-FALSE-POSITIVES';
SET @currentPolicyID = hex('20020005');
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
## INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_group_reference (policy_id,host_group_id,direction) VALUES (@currentPolicyID,hex('15000003'),'source') ON DUPLICATE KEY UPDATE direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,hex('15000007')) ON DUPLICATE KEY UPDATE plugin_group_id=hex('15000007');
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #1.
SET @order = @order + 1;
SET @policyName = 'STI-Discard';
SET @currentPolicyID = '0200210000';
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 0;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,'0200200003') ON DUPLICATE KEY UPDATE plugin_group_id='0200200003';
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #2.
SET @order = @order + 1;
SET @policyName = 'STI-NoLogNoCorrelate';
SET @currentPolicyID = '0200210001';
SET @correlate = 0;
SET @crosscorrelate = 0;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 0;
SET @riskAssessment = 0;
SET @active = 1;
SET @priority = -1;
SET @logger = 1;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,'0200200001') ON DUPLICATE KEY UPDATE plugin_group_id='0200200001';
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;

START TRANSACTION;
# BEGIN Sentinel Built-In Policy #3.
SET @order = @order + 1;
SET @policyName = 'STI-NoLogYesCorrelate';
SET @currentPolicyID = '0200210002';
SET @correlate = 1;
SET @crosscorrelate = 1;
SET @forward_events = 0;
SET @sqlstore = 0;
SET @performSIEM = 1;
SET @riskAssessment = 1;
SET @active = 1;
SET @priority = -1;
SET @logger = 1;
INSERT into policy (id,ctx,priority,active,policy.group,policy.order,descr,permissions) VALUES (@currentPolicyID,@eventctx,@priority,@active,'',@order,@policyName,'') ON DUPLICATE KEY UPDATE policy.order = @order,descr=@policyName,active=@active,ctx=@eventctx;
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','source') ON DUPLICATE KEY UPDATE host_id='',direction='source';
INSERT into policy_host_reference (policy_id,host_id,direction) VALUES (@currentPolicyID,'','dest') ON DUPLICATE KEY UPDATE host_id='',direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'dest') ON DUPLICATE KEY UPDATE port_group_id=0,direction='dest';
INSERT into policy_port_reference (policy_id,port_group_id,direction) VALUES (@currentPolicyID,0,'source') ON DUPLICATE KEY UPDATE port_group_id=0,direction='source';
INSERT into policy_plugin_group_reference (policy_id,plugin_group_id) VALUES (@currentPolicyID,'0200200002') ON DUPLICATE KEY UPDATE plugin_group_id='0200200002';
INSERT into policy_sensor_reference (policy_id,sensor_id) VALUES (@currentPolicyID,@SensorIDNullValue) ON DUPLICATE KEY UPDATE sensor_id=@SensorIDNullValue;
INSERT into policy_role_reference (policy_id,correlate,cross_correlate,store,qualify,sem,sim,resend_alarm,resend_event) VALUES (@currentPolicyID,@correlate,@crosscorrelate,@sqlstore,@riskAssessment,@logger,@performSIEM,@forward_events,@forward_events) ON DUPLICATE KEY UPDATE correlate=@correlate,cross_correlate=@crosscorrelate,store=@sqlstore,qualify=@riskAssessment,sem=@logger,sim=@performSIEM,resend_alarm=@forward_events,resend_event=@forward_events;
INSERT IGNORE into policy_target_reference (policy_id,target_id) SELECT DISTINCT @currentPolicyID,id FROM server;
## time requires a special insert function because key is autoincrement value.  Thus standard insert ignore does not work.
insert IGNORE into policy_time_reference (policy_id,minute_start,minute_end,hour_start,hour_end,week_day_start,week_day_end,month_start,month_end,timezone) select @currentPolicyID,0,59,0,23,0,0,0,0,'America/Chicago' from policy_time_reference where (policy_id = @currentPolicyID) HAVING COUNT(*) = 0;
# END OF Sentinel Policy
COMMIT;



# The following line cleans up any target id duplicates that have since been removed, such as in the case of federation testing where the federation server has been disconnected.
START TRANSACTION;
delete from policy_target_reference where target_id not in (select server.id from server);
COMMIT;
