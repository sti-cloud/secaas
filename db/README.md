# Generic Database Configuration Files #

This folder contains generalized configuration rules that are not specifically plugin related and would generally to everyone.  Most configuration tweaks here are too small to make up their own plugin (a change here or there) or have vast potential for commonalities with other small tweaks.