# Alienvault STI Datasource Group File
# Last Modified By: Sidney Eaton
# Last Update: 1-24-2020
#
# This SQL file creates and populates the built-in Sentinel datasource groups.  The file should be structured
# such that importing this will overwrite any alterations to those specific groups, thus keeping everything up to date.
# This also means you should not add changes that are incrementally dependent.  ASSUME a fresh install of Alienvault!!!

# Create and populate the STI-NoLogNoCorrelate Group.  This group should contain datasource IDs that should not be SQL logged or correlated.  RAW logs may be stored.
insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values ('0200200001','','STI-NoLogNoCorrelate', 'Sentinel built-in datasource group to exclude erronous logs');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',15971,'','1190') ON DUPLICATE KEY UPDATE plugin_sid='1190';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',1573,'','537,401,1233,171,616') ON DUPLICATE KEY UPDATE plugin_sid='537,401,1233,171,616';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',1636,'','212006,305006,713257,302016,305012,302014,710003,305011,607001,713902,313001,713041,713904,302020,302021,602303,752004,713061,713905,602304,106001,733100,722036,210007,434004,106014,434002,106006,106103') ON DUPLICATE KEY UPDATE plugin_sid='212006,305006,302016,305012,302014,710003,305011,607001,713902,313001,713041,713904,302020,302021,602303,752004,713061,713905,602304,713257,106001,733100,722036,210007,434004,106014,434002,106006,106103';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',1615,'','3') ON DUPLICATE KEY UPDATE plugin_sid='3';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',7115,'','17101') ON DUPLICATE KEY UPDATE plugin_sid='17101';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',1686,'','4') ON DUPLICATE KEY UPDATE plugin_sid='4';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',19005,'','222,2,27,196,226,1500,227,76,147,12002,146,9') ON DUPLICATE KEY UPDATE plugin_sid='222,2,27,196,226,1500,227,76,147,12002,146,9';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',7009,'','102002') ON DUPLICATE KEY UPDATE plugin_sid='102002';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200001','',1771,'','800101') ON DUPLICATE KEY UPDATE plugin_sid='800101';



# Create and populate the STI-NoLogCorrelate
insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values ('0200200002','','STI-NoLogYesCorrelate', 'Sentinel built-in datasource group to exclude SQL logging, but still correlate logs. RAW logging may still happen.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',1636,'','302013,106100,302015,106023,313005,305013,419002,106016') ON DUPLICATE KEY UPDATE plugin_sid='302013,106100,302015,106023,313005,305013,419002,106016';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',1510,'','4828') ON DUPLICATE KEY UPDATE plugin_sid='4828';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',1573,'','98') ON DUPLICATE KEY UPDATE plugin_sid='98';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',1615,'','2') ON DUPLICATE KEY UPDATE plugin_sid='2';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',21001,'','1') ON DUPLICATE KEY UPDATE plugin_sid='1';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',1695,'','100000') ON DUPLICATE KEY UPDATE plugin_sid='100000';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',1782,'','600041') ON DUPLICATE KEY UPDATE plugin_sid='600041';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',7006,'','18149') ON DUPLICATE KEY UPDATE plugin_sid='18149';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',7009,'','700008') ON DUPLICATE KEY UPDATE plugin_sid='700008';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200002','',1678,'','204,203,226') ON DUPLICATE KEY UPDATE plugin_sid='204,203,226';


# Create and populate the STI-Discard
insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values ('0200200003','','STI-Discard', 'Sentinel built-in datasource group to completely discard datasource, raw logs and all.  In general, rsyslog rules should be used to perform this function where ever possible.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200003','',1636,'','302020,106015') ON DUPLICATE KEY UPDATE plugin_sid='302020,106015';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200003','',1663,'','2630,2632') ON DUPLICATE KEY UPDATE plugin_sid='2630,2632';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200003','',1001,'','2025275,2815698,2027863') ON DUPLICATE KEY UPDATE plugin_sid='2025275,2815698,2027863';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200003','',1686,'','9998') ON DUPLICATE KEY UPDATE plugin_sid='9998';

# Create and populate the STI-MEDIA-TV-EXCLUSION
insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values ('0200200004','','STI-MEDIA-TV-EXCLUSION', 'Sentinel built-in datasource group to completely discard known false positives against media TVs such as Sony.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200004','',1001,'','2010100,2010100,2012078') ON DUPLICATE KEY UPDATE plugin_sid='2010100,2010100,2012078';

# Create and populate the STI-AUTO-SRC-AS-SRC-BLK
# STI-AUTO-SRC-AS-DST-BLK
# STI-AUTO-SRC-AS-SRC-BLK
# STI-AUTO-DST-AS-DST-BLK
# STI-AUTO-DST-AS-SRC-BLK
insert into plugin_group (group_id,group_ctx,name,descr) values ('0200200005','','STI-AUTOMATED-SRC-BLK', 'Sentinel built-in datasource group which contains directives where the source IP field, as it shows in the correlation, should be blocked on security devices.  In addition to this action install a policy that uses the group to block both inbound and outbound.') ON DUPLICATE KEY UPDATE name='STI-AUTOMATED-SRC-BLK',descr='Sentinel built-in datasource group which contains directives where the source IP field, as it shows in the correlation, should be blocked on security devices.  In addition to this action install a policy that uses the group to block both inbound and outbound.';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200005','',1505,'','43576,43669,45690,45937,50044,50090,41101,30030,45195,45593,45368,46059,45940,46011,46141,34049,30082,45202,34049,45368,46147,710055') ON DUPLICATE KEY UPDATE plugin_sid='43576,43669,45690,50044,41101,30030,50090,45937,45195,45593,45368,46059,45940,46011,46141,34049,30082,45202,34049,45368,46147,710055';

insert into plugin_group (group_id,group_ctx,name,descr) values ('0200200006','','STI-AUTOMATED-DST-BLK', 'Sentinel built-in datasource group which contains directives where the destination IP field, as it shows in the correlation, should be blocked on security devices.  In addition to this action install a policy that uses the group to block both inbound and outbound.') ON DUPLICATE KEY UPDATE name='STI-AUTOMATED-DST-BLK',descr='Sentinel built-in datasource group which contains directives where the destination IP field, as it shows in the correlation, should be blocked on security devices.  In addition to this action install a policy that uses the group to block both inbound and outbound.';
#delete from plugin_group_descr where group_id = '0200200006';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values ('0200200006','',1505,'','50028') ON DUPLICATE KEY UPDATE plugin_sid='50028';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000007'),'','STI-NETWORK-MONITORING-EXCLUSION', 'Sentinel built-in datasource group to completely discard alerts that might be generated from network monitoring devices. Example Solarwinds.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000007'),'',1001,'','2001581,2001569,2010935') ON DUPLICATE KEY UPDATE plugin_sid='2001581,2001569,2010935';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000008'),'','STI-NIDS-EXCLUSION', 'Sentinel built-in datasource group to perform policies on AlienVault NIDS.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000008'),'',1001,'','0') ON DUPLICATE KEY UPDATE plugin_sid='0';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000009'),'','STI-KASEYE-RDP', 'Sentinel built-in datasource group to filter on Kaseye RDP.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000009'),'',1001,'','36330,2804125,2804209,36330,2804125,2804209') ON DUPLICATE KEY UPDATE plugin_sid='36330,2804125,2804209,36330,2804125,2804209';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000009'),'',1621,'','37260,37689,37260,37689') ON DUPLICATE KEY UPDATE plugin_sid='37260,37689,37260,37689';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000009'),'',1727,'','36330,36330') ON DUPLICATE KEY UPDATE plugin_sid='36330,36330';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000010'),'','STI-HIDS-AD-DIR-MODS', 'Sentinel built-in datasource group for HIDS active directory modifications.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000010'),'',7099,'','0') ON DUPLICATE KEY UPDATE plugin_sid='0';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000010'),'',7107,'','0') ON DUPLICATE KEY UPDATE plugin_sid='0';


insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000011'),'','STI-CONSUMER-GRADE-CHECKS', 'Sentinel built-in datasource group for consumer grade devices like linksys.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000011'),'',1001,'','2018156,2018157,2018159,2018131,2018132,2018155,2022758,2819962,2819963,2819964,2819965,2819966,2819967,2819968,2819969,2819970,2819971,2819972,2819973,2819974,2819975,2819982,2819983,2819984,2819985') ON DUPLICATE KEY UPDATE plugin_sid='2018156,2018157,2018159,2018131,2018132,2018155,2022758,2819962,2819963,2819964,2819965,2819966,2819967,2819968,2819969,2819970,2819971,2819972,2819973,2819974,2819975,2819982,2819983,2819984,2819985';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000012'),'','STI-APACHE-CHECKS', 'Sentinel built-in datasource group for apache checks.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000012'),'',1001,'','2024044,2024038,2023535,2820185,4000218,2024045,2024094,2024095,2024096,2024097,2816603,2816604,2024808,2024663') ON DUPLICATE KEY UPDATE plugin_sid='2024044,2024038,2023535,2820185,4000218,2024045,2024094,2024095,2024096,2024097,2816603,2816604,2024808,2024663';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000013'),'','STI-IP-CAMERA-CHECKS', 'Sentinel built-in datasource group for IP Camera checks.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000013'),'',1001,'','2823167,2825343,4000330,2823167,2823168,4000256,4000257,4000258,2024917,2024918,2024919,2024920') ON DUPLICATE KEY UPDATE plugin_sid='2823167,2825343,4000330,2823167,2823168,4000256,4000257,4000258,2024917,2024918,2024919,2024920';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000014'),'','STI-WEBFILTER-EXCLUSION', 'Sentinel built-in datasource group to perform policies on AlienVault NIDS.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000014'),'',1001,'','0') ON DUPLICATE KEY UPDATE plugin_sid='0';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000015'),'','STI-ANYCONNECT-AUDIT', 'Sentinel built-in datasource group for filtering Anyconnect Events.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000015'),'',1636,'','113039,716059,722051,113019,722023') ON DUPLICATE KEY UPDATE plugin_sid='113039,716059,722051,113019,722023';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000016'),'','STI-SKYPE-POLICY-EVENTS', 'Sentinel built-in datasource group for filtering skype policy events.  Does not include skype threats.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000016'),'',1001,'','2001595,2001596,2002157,2003022') ON DUPLICATE KEY UPDATE plugin_sid='2001595,2001596,2002157,2003022';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000016'),'',1551,'','49705850,77174918,17464590') ON DUPLICATE KEY UPDATE plugin_sid='49705850,77174918,17464590';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000016'),'',1611,'','4396,11686') ON DUPLICATE KEY UPDATE plugin_sid='4396,11686';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000017'),'','STI-WIN-DNS-SUPPRESS-EVENTS', 'Sentinel built-in datasource group for filtering out STI-WinDNS plugin logs that do not provide value.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000017'),'',20004,'','1,3,4') ON DUPLICATE KEY UPDATE plugin_sid='1,3,4';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000018'),'','STI-SUPPRESS-SSH-OUT-EVENTS', 'Sentinel built-in datasource group for filtering outbound SSH checks.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000018'),'',1001,'','2003068,2001219') ON DUPLICATE KEY UPDATE plugin_sid='2003068,2001219';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000019'),'','STI-NON-UPDATED-SYS-ALERTS', 'Sentinel built-in datasource group for filtering on alerts that indicate outdated systems.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000019'),'',1001,'','2007695') ON DUPLICATE KEY UPDATE plugin_sid='2007695';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000020'),'','STI-SUPPRESS-BACKUP-ALERTS', 'Sentinel built-in datasource group for filtering false alerts associated with backup systems.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000020'),'',1001,'','2815579,2022330,2022331') ON DUPLICATE KEY UPDATE plugin_sid='2815579,2022330,2022331';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000021'),'','STI-DNS-QUERY-EVENTS', 'Sentinel built-in datasource group for filtering on DNS query requests.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000021'),'',1577,'','51,52,53,54,55,56,57,58,59,61,62,63,64,65,66,67,68,69,70,71,81') ON DUPLICATE KEY UPDATE plugin_sid='51,52,53,54,55,56,57,58,59,61,62,63,64,65,66,67,68,69,70,71,81';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000022'),'','STI-SUSPICIOUS-USER-AGENTS', 'Sentinel built-in datasource group for filtering on suspicious user-agents.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000022'),'',1001,'','2009486,2007994') ON DUPLICATE KEY UPDATE plugin_sid='2009486,2007994';

insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000023'),'','STI-CLEARTEXT', 'Sentinel built-in datasource group for filtering on clear text password event types.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000023'),'',1001,'','2006380') ON DUPLICATE KEY UPDATE plugin_sid='2006380';

# Create and populate the STI-WHITELIST-VULN-SCANS-HIDS group
insert IGNORE into plugin_group (group_id,group_ctx,name,descr) values (hex('15000024'),'', 'STI- VULN-SCAN-HIDS-EXCLUSION', 'Sentinel built-in group for suppressing HIDS events for vuln scanners.');
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000024'),'',7059,'','31154') ON DUPLICATE KEY UPDATE plugin_sid='31154';
insert into plugin_group_descr (group_id,group_ctx,plugin_id,plugin_ctx,plugin_sid) values (hex('15000024'),'',1505,'','500038') ON DUPLICATE KEY UPDATE plugin_sid='500038';


