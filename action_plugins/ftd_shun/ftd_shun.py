#!/usr/bin/python
import os
import sys
sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
import json
import time
from string import strip
import requests
import random
import ast
import re

configDir = "/etc/firepower.cfg"
values = {} #values of config variables. This reads in config options
try:
    with open(configDir,"r") as r:
        for line in r:
            pos = line.find(':')
            values[line[0:pos].strip()] = line[pos+1:-1].strip()
except IOError:
    with open(configDir, "w+") as r:
        r.write("""Username: exampleUsername
Password: examplePassword
FMCIP: 255.255.255.example
FileToRead: exampledir/badips.txt
GroupName: examplegourp
DeployableDevice: exampledevicename
Domain: global
SslVerification: true""")
        print "firepower.config created in this directory. Please configure your settings and re-run program"
        exit(0)

verifyssl = values['SslVerification'].lower() == "true"
username = values['Username']
password = values["Password"]
iplistfile = values["FileToRead"]
groupname = values["GroupName"]
domain = values["Domain"]
devicenames = values["DeployableDevice"].split(',')
for i in range(0,len(devicenames)):
    devicenames[i] = strip(devicenames[i])
authToken = ""
groupid = ""
domain_uuid = ""
firewallip = values['FMCIP']


def getToken():
    r = requests.post("https://"+firewallip+"/api/fmc_platform/v1/auth/generatetoken", auth=(username,password), verify=verifyssl)
    global authToken, domain_uuid
    authToken = r.headers["X-auth-access-token"]
    testvar = ast.literal_eval(r.headers["DOMAINS"])
    test2 = testvar[0]
    for i in ast.literal_eval(r.headers["DOMAINS"]):
        if i["name"].lower() == domain.lower():
            domain_uuid = i["uuid"]
            break
    else:
        print "couldn't find the domain name"
        exit(0)

def getNetGroups():
    global groupid
    url = "https://"+firewallip+"/api/fmc_config/v1/domain/"+domain_uuid+"/object/networkgroups"
    def getgroup(url):
        r = requests.get(url,
                         headers = {"X-auth-access-token":authToken,
                                    "Content-Type": 'application/json'}, verify = verifyssl)
        groups = json.loads(r.text[:-1]+"}")["items"]
        groupfound = False
        counter = 0
        for i in groups:
            if i["name"]!=groupname:
                counter+=1
            else:
                groupfound = True
                break
        if(groupfound == True):
            return (groupfound,json.loads(r.text[:-1]+"}")["paging"],groups[counter]["id"])
        else:
            return (groupfound, json.loads(r.text[:-1] + "}")["paging"], "no id found")

    groupfound, groups, group_id = getgroup(url)

    while(groupfound==False and groups["pages"]>1 and "next" in groups):
        groupfound, groups, group_id = getgroup(groups["next"][0])

    if groupfound==False:
        #create group
        print "\nCREATING GROUP\n"
        payload = "{\r\n\"literals\": [],\r\n\"type\": \"NetworkGroup\",\r\n\"overridable\": true,\r\n\"description\": \"Sentinel automatic blacklist network group\",\r\n \"name\": \""+groupname+"\"\r\n}\r\n"
        headers = {
            'X-auth-access-token': authToken,
            'Content-Type': "application/json",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Accept-Encoding': "gzip, deflate",
            'Content-Length': "360",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }
        r= requests.post("https://"+firewallip+"/api/fmc_config/v1/domain/"+domain_uuid+"/object/networkgroups",
                        data=payload, headers=headers, verify=verifyssl)
        print "id is "+r.text
        groupid = json.loads(r.text[:-1]+"}")["id"]
    else:
        print group_id
        groupid = group_id

def appendIps(sampleIPS):
    r = requests.get(
            "https://"+firewallip+"/api/fmc_config/v1/domain/"+domain_uuid+"/object/networkgroups/"+groupid,
            headers={"X-auth-access-token": authToken,
                     "Content-Type": 'application/json'},
            verify=verifyssl)
    #print sampleIPS
    if "literals" in json.loads(r.text[:-1]+"}"):
        iplist = json.loads(r.text[:-1]+"}")["literals"]
    else:
        iplist = []

    body = {"id": groupid,
                  "name": groupname,
                  "type": "NetworkGroup",
                  "literals": []}

    body["literals"]+=sampleIPS+iplist

    r = requests.put(
            "https://"+firewallip+"/api/fmc_config/v1/domain/"+domain_uuid+"/object/networkgroups/"+groupid,
        data=json.dumps(body),
    headers={"X-auth-access-token": authToken,
                     "Content-Type": 'application/json'},
            verify=verifyssl)
    #print(r.text)

def deployChanges():
    def _deployChanges():
        querystring = {"expanded": "true"}
        headers = {
            'X-auth-access-token': authToken,
            'Content-Type': "application/json",
            'Cache-Control': "no-cache",
        }
        r=requests.request("GET", "https://"+firewallip+"/api/fmc_config/v1/domain/"+domain_uuid+"/deployment/deployabledevices",
                           headers=headers, params=querystring,verify=verifyssl)
        items = json.loads(r.text)
        if "items" in items:
            items=items["items"]
        else:
            return True
        info = []
        devicesfound = False
        for i in items:
            for devicename in devicenames:
                if i["device"]["name"] == devicename:
                    info.append(i)
                    devicesfound = True
        else:
            if not devicesfound:
                return True

        devlist = []
        for i in info:
            devlist.append(i["device"]["id"])
        payload = {
      "type": "DepolymentRequest",
      "version": info[0]["version"],
      "forceDeploy": True,
      "ignoreWarning": True,
      "deviceList": devlist
    }
        response = requests.request("POST", "https://"+firewallip+"/api/fmc_config/v1/domain/"+domain_uuid+"/deployment/deploymentrequests",
                                data=json.dumps(payload), headers=headers, verify=verifyssl)

        if "error" in response.text:
            print "\nstill deploying\n"
            return False

        print(response.text)
        return True

    while not _deployChanges():
        TIMETOWAIT = random.uniform(1, 3)
        print "waiting "+str(TIMETOWAIT)+"seconds"
        time.sleep(TIMETOWAIT)


firstUpload = True
def startUpload():
    global firstUpload
    sampleIPS = []
    pattern = re.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
    with open(iplistfile, 'r+') as file:
        for line in file:
            if(line!="\n" and pattern.match(line)):
                sampleIPS.append({"type": "Host",
                                  "value": line}
                                 )
        file.truncate(0)
        global file_to_scan
        stamp = os.stat(file_to_scan).st_mtime
        global last_stamp
        last_stamp = stamp

    if (len(sampleIPS) > 0):
        getToken()
        if(firstUpload):
            getNetGroups()
            firstUpload = False
        appendIps(sampleIPS)
        deployChanges()
        print "\nupload finished\n"
    else:
        print "no ips found in file " + iplistfile


#---------------------------------------------------------------------------------
#watch file
#---------------------------------------------------------------------------------

file_to_scan = iplistfile


#timestap of when file was last modified
last_stamp = 0

def look():
    stamp = os.stat(file_to_scan).st_mtime
    global last_stamp
    if stamp != last_stamp:
        last_stamp = stamp
        startUpload()

while True:
    time.sleep(3600)
    look()