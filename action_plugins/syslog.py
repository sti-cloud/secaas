#!/usr/bin/python

import sys
import socket

DEBUG_MODE = 0           # 0 = normal operation (send syslog). 1 = output to console instead

SYSLOG_SERVERS = sys.argv[1]
SID_NAME = sys.argv[2]
SRC_IP = sys.argv[3]
SRC_PORT = sys.argv[4]
DST_IP = sys.argv[5]
DST_PORT = sys.argv[6]

SUBST = {
  "SRC_IP": SRC_IP,
  "DST_IP": DST_IP,
  "SRC_PORT": SRC_PORT,
  "DST_PORT": DST_PORT
}

content = '<133>alienvault: Directive: ' + SID_NAME
for key, value in SUBST.iteritems() :
  content = content.replace(key, value)

content += ', body: ' + SRC_IP 
if SRC_PORT != '0' and SRC_PORT != '' :
  content += ':' + SRC_PORT
content += '->' + DST_IP
if DST_PORT != '0' and DST_PORT != '' :
  content += ':' + DST_PORT
content += ', Other info: '

for i in range(7, len(sys.argv)) :
  field = sys.argv[i]
  if field != "" and field != "null" and field != "NULL" :
    content += 'USERDATA' + str(i-6) + ': ' + sys.argv[i]
    if i < 15 :
      content += ', '
    else :
      content += '.'

if DEBUG_MODE :
  print content
else :
  servers = SYSLOG_SERVERS.split(" ")
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  for i in range(len(servers)) :
    sock.sendto(content, (servers[i], 514))
