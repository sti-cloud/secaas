#!/usr/bin/python
import paramiko
import time
import sys, getopt

def main(argv):
   help_syntax = 'sti-fortigate-black-list.py -d <fortigate mgmt IP> -i <IP address to blacklist> -u <fortigate username> -p <fortigate password> -v <fortigate vdom> -g <fortigate groupname>'

   obj_name_prefix = 'STI-BL-'
   timestamp = time.strftime("%x")

   ip_to_black_list = 'x.x.x.x/x'
   vdom = 'root'
   user = 'admin'
   passwd = ''
   host = '0.0.0.0'
   subnet_mask = '255.255.255.255'
   fortigate_blacklist_group_name = 'STI-AUTOMATED-BLACKLIST'


   try:
      opts, args = getopt.getopt(argv,"hd:i:u:p:v:g:",["deviceip=","ip=","user=","password=","vdom=","group="])
   except getopt.GetoptError:
      print help_syntax
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print help_syntax
         sys.exit()
      elif opt in ("-i", "--ip"):
         ip_to_black_list = arg
      elif opt in ("-d", "--deviceip"):
         host = arg
      elif opt in ("-u", "--user"):
         user = arg
      elif opt in ("-p", "--password"):
         passwd = arg
      elif opt in ("-v", "--vdom"):
         vdom = 
      elif opt in ("-g", "--group"):
         fortigate_blacklist_group_name = arg

   print "The following options were specified for this plugin:"
   print "IP to blacklist is", ip_to_black_list
   print "Virtual domain is", vdom
   print "Username is", user
   print "Password is ************"
   print "Remote device is", host
   print "\nAttempting the following configuration:"
   print "---------------------------------------"

   uniq_obj_name = ip_to_black_list

   configuration = "config vdom\nedit " + vdom + "\nconfig firewall address\n edit " + obj_name_prefix +uniq_obj_name + "\nset type ipmask\nset color 32\nset subnet " + ip_to_black_list + "/" + subnet_mask + '\nset comment "STI Automated Black List Entry, created ' + timestamp + '"' + "\nend\nconfig firewall addrgrp\nedit " + fortigate_blacklist_group_name + "\nset color 32\n" + 'set comment "STI Automated Blacklist Group"' + "\nappend member " + obj_name_prefix +uniq_obj_name + "\nend\nend\nexit\n"
   print configuration
   ssh = paramiko.SSHClient()
   ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   ssh.connect(host,username=user,password=passwd)
   stdin, stdout, stderr=ssh.exec_command(configuration)
   type(stdin)
   stdout.readlines()
   ssh.close()

if __name__ == "__main__":
   main(sys.argv[1:])