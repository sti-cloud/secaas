# FortiGate Shun Script #

This is a private plugin and may only be used when properly licensed and paid.  Plugin owner is Sentinel Technologies.

### PLUGIN OVERVIEW ###

* FortiGate Shun SAASP (Sentinel Security As A Service Plugin)
* Version 1.0.0
* This python plugin allows for integration into FortiGate object groups via Alienvault actions.

### Requirements ###

* FortiGate Firewalls *(Version 5.x, may work on others)*
* Alienvault USM/OSSIM *(Version 5.x)*
* Subscription to Sentinel's Security As A Service
* This plugin package

### How do I get set up? ###

Sentinel's Security As A Service Security Feed will automatically install the script and it's pre-requisits in the directory /etc/secaas/scripts.  A sample action will also be inserted into the Alienvault database.  The sample action can be modified accordingly, but will require parameter input as noted in the parameter section.  One this plugin is setup in Alienvault, you also need to create policies on the device that utilize the group STI-AUTOMATED-BL.  This is the group where the plugin will insert new IPs into.  In the near future, you'll be allowed to specify the group as a matter of parameter input allowing for increased flexibility.

### Plugin Input Parameters ###

* Device IP - The management IP address of the device for which the script will SSH into to perform configuration modifications.
* Username - The Username of a valid user that has the ability to modify the DEVICE IP configuration
* Password - The associated password of the Username parameter.
* IP - The IP address to be added to the group.
* VDOM - The FortiGate virtual domain.  If you are not using vdoms this part of the configuration will fail but should still continue.  Use root if you are not using virtual domains.

### Who do I talk to? ###

* Creator: Sidney Eaton @ Sentinel Technologies.
