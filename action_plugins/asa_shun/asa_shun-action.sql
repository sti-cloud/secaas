# Alienvault STI Action SQL File
# Last Modified By: Sidney Eaton
# Last Update: 12-29-2015
#
# This file is used to generate and distribute Sentinels built in Security As A Service files.
# The file is distributed through the Sentinel Distribution and Update system and will be applied
# in full each and every time there is a change to the file.  Since this file is focused on inserting example
# actions, it is imperative that we exclusively use the INSERT IGNORE functionality.  This will avoid updating
# the contents of the action table should the user modified crucical parameters.

# DO NOT CHANGE BUILT-IN.  These variables can be used to make programming easier and more dynamic.
select @emailType:= type from action_type WHERE name = 'email';
select @execType:= type from action_type WHERE name = 'exec';
select @ticketType:= type from action_type WHERE name = 'ticket';

# Get the default correlation engine context and set it as a variable and set global variables. These variables can be used to make programming easier and more dynamic.
select @enginectx := engine_ctx FROM corr_engine_contexts WHERE descr = 'Default';
select @eventctx := event_ctx FROM corr_engine_contexts WHERE descr = 'Default';





START TRANSACTION;
# Input the search string to test if the action was already inserted.  If the name is not found, we will insert a new example definition.
SET @searchstring = 'ASA Shun SRC_IP Action';

# Generate random unique ID which will be used if no current action ID is found
SET @randactionID = TRUNCATE(RAND()*(3999999999-3000000000)+3000000000,0);

# A query will be performed to find an exec action and its associated action ID.
SET @actionID := (select id from action where name like CONCAT('%',@searchstring,'%') LIMIT 1);

# Base insert statement - MUST BE INCLUDED IN EVERY PLUGIN.  ALL ACTIONS HAVE AN ENTRY IN THE ACTION TABLE
INSERT IGNORE into action (id,ctx,name,action_type,cond,on_risk,descr) VALUES (IFNULL(@actionID,@randactionID),@eventctx,'ASA Shun SRC_IP Action',@execType,'True',0,'This will shun the  SRC_IP associated with the alarm');

# EXEC insert statement - MUST BE INCLUDED ONLY FOR ACTIONS THAT EXECUTE A SCRIPT.
INSERT IGNORE into action_exec (action_id,command) VALUES (IFNULL(@actionID,@randactionID),'/var/secaas/scripts/sti-asa-black-list.py -d <ASADEVICEIP> -u <ASAUSERNAME> -p <ASAPASSWORD> -i SRC_IP');
COMMIT;

START TRANSACTION;
# Input the search string to test if the action was already inserted.  If the name is not found, we will insert a new example definition.
SET @searchstring = 'ASA Shun DST_IP Action';

# Generate random unique ID which will be used if no current action ID is found
SET @randactionID = TRUNCATE(RAND()*(3999999999-3000000000)+3000000000,0);

# A query will be performed to find an exec action and its associated action ID.
SET @actionID := (select id from action where name like CONCAT('%',@searchstring,'%') LIMIT 1);

# Base insert statement - MUST BE INCLUDED IN EVERY PLUGIN.  ALL ACTIONS HAVE AN ENTRY IN THE ACTION TABLE
INSERT IGNORE into action (id,ctx,name,action_type,cond,on_risk,descr) VALUES (IFNULL(@actionID,@randactionID),@eventctx,'ASA Shun DST_IP Action',@execType,'True',0,'This will shun the  DST_IP associated with the alarm');

# EXEC insert statement - MUST BE INCLUDED ONLY FOR ACTIONS THAT EXECUTE A SCRIPT.
INSERT IGNORE into action_exec (action_id,command) VALUES (IFNULL(@actionID,@randactionID),'/var/secaas/scripts/sti-asa-black-list.py -d <ASADEVICEIP> -u <ASAUSERNAME> -p <ASAPASSWORD> -i DST_IP');
COMMIT;