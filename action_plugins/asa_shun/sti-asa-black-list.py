#!/usr/bin/python
import paramiko
import time
import sys, getopt

# This method will write basic information to a log file that can be later analysed
# for future improvement.
def recordTransactionToFile(IP,reason,date):
   f = open("autoremdiation.log","a")
   f.write(IP + ',' + date + ',"' + reason + '"\n')
   f.close()

def main(argv):
   help_syntax = 'sti-asa-black-list.py -d <ASA mgmt IP> -i <IP address to blacklist> -u <ASA username> -p <ASA password> -v <ASA vdom> -g <ASA groupname> -r <reason description> -n <object_name_prefix>'

   # Each system has it's own maximum number of characters for various naming.
   # place the device's maximum number of characters here.
   obj_name_max_length = 64
   obj_desc_max_length = 200
   
   # Set defaults.  If the user doesn't specify an option these defaults will be used.
   # Please note that some are required.  Required options should contain invalid input,
   # such that it errors out on the device.  Required parameter checking will be added later.
   obj_name_prefix = 'STI-BL-'
   timestamp = time.strftime("%x")
   ip_to_black_list = '0.0.0.0'
   vdom = 'root'
   user = 'admin'
   passwd = ''
   host = '0.0.0.0'
   subnet_mask = '255.255.255.255'
   ASA_blacklist_group_name = 'STI-AUTOMATED-BLACKLIST'
   obj_desc = 'CTX Automated Threat Remediation Entry'

   try:
      opts, args = getopt.getopt(argv,"hd:i:u:p:v:g:r:n:",["deviceip=","ip=","user=","password=","vdom=","group=","reason=","nameprefix="])
   except getopt.GetoptError:
      print help_syntax
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print help_syntax
         sys.exit()
      elif opt in ("-i", "--ip"):
         ip_to_black_list = arg
      elif opt in ("-d", "--deviceip"):
         host = arg
      elif opt in ("-u", "--user"):
         user = arg
      elif opt in ("-p", "--password"):
         passwd = arg
      elif opt in ("-v", "--vdom"):
         vdom = arg
      elif opt in ("-g", "--group"):
         ASA_blacklist_group_name = arg[0:obj_name_max_length]
      elif opt in ("-r", "--reason"):
         obj_desc = arg[0:obj_desc_max_length-12]
      elif opt in ("-n", "--nameprefix"):
         obj_name_prefix = arg[0:obj_name_max_length-18]

   if passwd == '' or user == '' or host == '0.0.0.0' or ip_to_black_list == '0.0.0.0':
      print help_syntax
      sys.exit(3)

   print "The following options were specified for this plugin:"
   print "IP to blacklist is", ip_to_black_list
   print "Virtual domain is", vdom
   print "Username is", user
   print "Password is ************"
   print "Remote device is", host
   print "IP object description input is", obj_desc
   print "IP object name prefix is", obj_name_prefix
   print "\nAttempting the following configuration:"
   print "---------------------------------------"

   uniq_obj_name = ip_to_black_list

   configuration = "config t\nobject network " + obj_name_prefix +uniq_obj_name + "\nhost " + ip_to_black_list + '\ndescription "' + obj_desc + ' on ' + timestamp + '"' + "\nexit\nobject-group network " + ASA_blacklist_group_name + '\ndescription "STI Automated Blacklist Group"\nnetwork-object object ' + obj_name_prefix +uniq_obj_name + "\nend\nwr mem\nexit\n"
   print configuration
   ssh = paramiko.SSHClient()
   ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   ssh.connect(host,username=user,password=passwd)
   stdin, stdout, stderr=ssh.exec_command(configuration)
   type(stdin)
   stdout.readlines()
   ssh.close()

   recordTransactionToFile(ip_to_black_list,obj_desc,timestamp)

if __name__ == "__main__":
   main(sys.argv[1:])
