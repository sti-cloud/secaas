# Alienvault STI Alarm Taxonomy File
# Last Modified By: Sidney Eaton
# Last Update: 6-22-2017
#
# This file is used to generate and distribute Sentinel's built in Security As A Service files.
# The file is distributed through the Sentinel Distribution and Update system and will be applied
# in full each and every time there is a change to the file.  Therefore it is absolutely critical
# that all code within this file handle error conditions and update the database regardless of what
# state or patch level the database is at.  Two critical directives in acomplishing this is the
# INSERT IGNORE MySQL syntax and the the INSERT...ON DUPLICATE KEY UPDATE directive.  The first
# should be used when values are likely to never be changed later.  It tells the database that instead
# of erroring if the key exists, just WARN and continue on.  Thus simplistic error handling.  The second
# should be used if the values are likely to be adjusted overtime.  Thus it says, if it doesn't exist yet insert it
# and if it does exist update the values.  This also avoides a fatal error.

# Get the default correlation engine context and set it as a variable and set global variables.
select @enginectx := engine_ctx FROM corr_engine_contexts WHERE descr = 'Default';
select @eventctx := event_ctx FROM corr_engine_contexts WHERE descr = 'Default';

INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710007',@enginectx,1,23,'FortiGate Generic Network Anomaly Attack');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710006',@enginectx,2,75,'IGMP Timer Configuration Error');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710004',@enginectx,5,1,'Lucky Leap Adware Virus');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710003',@enginectx,5,41,'BrowseFox Trojan Variant');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710001',@enginectx,1,57,'Possible DDoS UDP Flood');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710002',@enginectx,2,69,'Vulnerable TFTP Configuration');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710008',@enginectx,1,5,'Cisco ACS MAC Bruteforce');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710009',@enginectx,1,5,'Cisco ACS Bruteforce');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710010',@enginectx,5,51,'Virus failed to be neutralized');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710011',@enginectx,5,38,'Security risk failed to be neutralized');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710012',@enginectx,2,10,'USERNAME has made a number of AD configuration changes in a short time period');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710013',@enginectx,5,38,'Cylance suspicious program or file');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710014',@enginectx,5,52,'Cylance Ransomware detection');
INSERT into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710015',@enginectx,1,5,'WS-FTP Bruteforce Detected') ON DUPLICATE KEY UPDATE category=5;
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710016',@enginectx,1,37,'Fake from header social engineering attack');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710017',@enginectx,5,6,'Suspicious domain or URL');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710018',@enginectx,2,37,'Multiple AD Accounts Disabled or Deleted');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710019',@enginectx,5,50,'PlugX Malware RAT');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710020',@enginectx,5,51,'Repeated Malware Access Attempts');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710021',@enginectx,5,37,'General IOC');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710022',@enginectx,5,6,'C2 Traffic');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710023',@enginectx,3,51,'Malware Installed or Executed');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710024',@enginectx,5,3,'RootKit');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710025',@enginectx,2,16,'APK Installation');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710026',@enginectx,5,4,'OpenDNS Botnet Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710027',@enginectx,2,10,'SOC Test Ticket');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710028',@enginectx,2,10,'A user account was added to privileged group');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710029',@enginectx,4,66,'ZmEu Vulnerability Scanner');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710030',@enginectx,3,73,'PaloAlto SQL Injection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710034',@enginectx,5,38,'Cylance Repetitive Exploit Attempted on Host');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710033',@enginectx,5,38,'Cylance Repetitive Script Alert on Host');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710035',@enginectx,5,4,'Botnet Detected: USERDATA9');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710036',@enginectx,5,3,'Backdoor Detected: USERDATA9');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710037',@enginectx,5,48,'Internal Worm Detected: USERDATA9');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710038',@enginectx,1,5,'IPS Detected Bruteforce');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710039',@enginectx,3,9,'Data Breach') ON DUPLICATE KEY UPDATE kingdom=3;
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710040',@enginectx,1,66,'IPS Events detected on multiple systems by the same source');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710041',@enginectx,5,4,'Webshell Detected: USERDATA9');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710042',@enginectx,5,6,'OpenDNS CnC request');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710043',@enginectx,5,37,'DNS request for domain categorized as cryptomining only');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710044',@enginectx,5,51,'DNS request to a new domain also categorized as malware');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710045',@enginectx,5,51,'EPO Infected file detected');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710047',@enginectx,5,51,'EPO quarantine failed');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710048',@enginectx,5,51,'Error cleaning boot record virus');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710046',@enginectx,5,51,'Suspicious module call');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710049',@enginectx,5,51,'File infected undetermined clean error');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710050',@enginectx,5,51,'Antivirus blocked access, but could not remediate');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710051',@enginectx,4,54,'Okta Login Failure');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710052',@enginectx,2,75,'Okta User Import Failure');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710053',@enginectx,5,19,'Internal Honeypot Event');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710054',@enginectx,5,42,'User reported fraud');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710055',@enginectx,4,33,'Aggressive Port Scanning or Reconnaissance');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710056',@enginectx,2,10,'ChangedFirewall');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710057',@enginectx,2,10,'Several changes to organization');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710058',@enginectx,2,10,'Several settings changed in short time period');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710059',@enginectx,2,10,'Waf disabled');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710060',@enginectx,2,10,'Several security related things deleted');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710061',@enginectx,5,37,'Password was changed');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710062',@enginectx,3,32,'App ran a system application');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710063',@enginectx,3,32,'App injected code');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710064',@enginectx,3,32,'Code injection into all apps');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710065',@enginectx,5,2,'Carbon Black terminated a running app');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710066',@enginectx,5,1,'Potentially unwanted program found');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710067',@enginectx,5,51,'Known virus found');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710068',@enginectx,5,32,'Application executing fileless script');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710069',@enginectx,5,51,'Possible virus found');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710070',@enginectx,1,5,'Possible brute force login attempt');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710071',@enginectx,2,23, 'Data Hoarding');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710072',@enginectx,2,23, 'Data Hoarding');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710073',@enginectx,3,6, 'Command and Control');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710074',@enginectx,1,5, 'Bruteforce Login');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710075',@enginectx,3,9, 'data exfiltration');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710076',@enginectx,1,15, 'Dos attack');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710077',@enginectx,2,37, 'Policy violation');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710078',@enginectx,1,15, 'DoS Attack');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710079',@enginectx,2,37, 'connection to bad ip');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710080',@enginectx,2,37, 'Network Anomaly');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710081',@enginectx,3,51, 'Exploitation');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710082',@enginectx,2,37, 'Network Anomaly');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710083',@enginectx,2,37, 'Policy violation'); 
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710084',@enginectx,4,28, 'Scan Activity');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710085',@enginectx,5,42,'2FA Fraud Reported');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710089',@enginectx,2,23,'Message from New Domain');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710090',@enginectx,2,23,'Reply with Different Email Address');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710091',@enginectx,1,38,'Malicious File Received');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710092',@enginectx,1,18,'Malicious URL');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710093',@enginectx,1,38,'Malicious Attachment');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710095',@enginectx,1,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710096',@enginectx,1,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710097',@enginectx,1,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710098',@enginectx,1,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710099',@enginectx,1,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710100',@enginectx,5,51,'Compromise Detected');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710101',@enginectx,5,6,'Compromise Detected');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710102',@enginectx,5,52,'Ransomware Detected');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710103',@enginectx,5,51,'Compromise Detected');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710104',@enginectx,5,51,'Compromise Detected');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710105',@enginectx,1,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710106',@enginectx,5,51,'Encryption');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710107',@enginectx,2,37,'Suspicious Behavior');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710108',@enginectx,5,51,'Command And Control');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710109',@enginectx,5,37,'Security Event');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710110',@enginectx,1,12,'Data Loss');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710111',@enginectx,1,42,'Unauthorized Access');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710112',@enginectx,3,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710113',@enginectx,3,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710114',@enginectx,3,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710115',@enginectx,3,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710116',@enginectx,3,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710117',@enginectx,3,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710118',@enginectx,5,51,'Malware Infection');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710119',@enginectx,1,37,'DNS Request');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710120',@enginectx,1,38,'Suspicious Executable');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710121',@enginectx,1,37,'Suspicious Activity');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710122',@enginectx,1,37,'Suspicious Login');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710123',@enginectx,1,51,'Malicious Activity');
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710124',@enginectx,1,51,'Malicious Website');

# select sid, kingdom, category, subcategory from alarm_taxonomy where sid > 500000 AND sid < 60000;

INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710015,1,0,0,1,0,0,0,0,0,0,0,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710021,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710022,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710023,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710024,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710025,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710026,0,0,0,0,1,1,0,0,0,0,0,0,1,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710028,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710029,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710030,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710035,1,1,0,0,1,1,0,1,0,1,1,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710036,1,1,0,0,1,1,0,1,0,1,1,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710037,1,1,0,0,1,1,0,1,0,1,1,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710038,1,1,0,1,0,0,0,1,0,1,1,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710039,0,0,0,0,0,0,0,1,1,0,1,0,0,1,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710040,1,0,0,1,0,0,0,1,0,0,0,0,0,1,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710041,1,0,0,0,1,1,0,1,0,1,1,1,0,1,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710042,0,1,0,0,1,1,0,1,0,1,1,1,1,1,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710043,0,1,0,0,1,1,0,1,0,1,1,1,0,1,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710044,0,1,0,0,1,1,0,1,0,1,1,0,0,1,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710050,1,0,0,0,1,1,0,0,0,0,0,0,1,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710051,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710052,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710054,1,0,0,0,1,0,0,1,1,1,1,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710055,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710056,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710057,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710058,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710059,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710060,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710061,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710062,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710063,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710064,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710065,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710066,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710067,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710068,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710069,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710070,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710071,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710072,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710073,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710074,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710075,0,1,0,0,0,0,0,1,1,0,0,1,0,1,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710076,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710077,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710078,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710079,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710080,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710081,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710082,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710083,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710084,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710085,1,0,0,0,1,0,0,1,1,1,1,0,0,0,1);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710086,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710087,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710088,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710095,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710096,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710097,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710098,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710099,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710100,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710101,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710102,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710103,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710104,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710105,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710106,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710107,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710108,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710109,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710110,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710111,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710112,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710113,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710114,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710115,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710116,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710117,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710118,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710119,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710120,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710121,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710122,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710123,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);
INSERT IGNORE INTO datawarehouse.category (sid,targeted,untargeted,approach,exploration,penetration,generalmalware,imp_qos,imp_infleak,imp_lawful,imp_image,imp_financial,D,I,C,net_anomaly) VALUES (710124,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0);

# Under monitoring and review.
# ---------------------------------------------------------------
# This creates too many events and causes issues per AlienVault.
INSERT IGNORE into alarm_taxonomy (sid,engine_id,kingdom,category,subcategory) VALUES ('710005',@enginectx,2,23,'Suspicious SYN Attack');
