# General Directives #

This section contains directive modifications and additions (mainly additions) along with any associated taxonmy configuration.  Directives are a core functionality of the Sentinel Security As A Service and define when alarms will happen.