# CloudSelect Threat Exchange #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for the CloudSelect Threat Exchange and orchestrates the SIEM with various intelligence parameters

### How do I get set up? ###
The system is pretty well designed to just set itself up upon first sync.  First sync will usually consist of the following:
```Shell
export PATH=${PATH}:/usr/share/python/alienvault-api-core/bin
echo "localhost" >> /etc/ansible/hosts
/usr/share/python/alienvault-api-core/bin/ansible-pull -d /var/secaas -U https://bitbucket.org/sti-cloud/secaas.git
```
** It may take 2 or 3 ansible pull runs for all state to be pulled down such as fact files.

#### Setting up a testing Bracnch ####
The a test or feature branch will override a production branch when downloading on a production system. In other words, the production system will continually download the development branch from that point on making it important that your development branch is semi-recent or semi-insync with production.  It also means that you can't forget to set the branch back to production.  If you don't meet these criteria you should be developing on an exclusive development system.

```Shell
ansible-pull -d /var/secaas -U https://bitbucket.org/sti-cloud/secaas.git -C <branch name>
```

To put the system back on the production branch when you are done, do:
```Shell
ansible-pull -d /var/secaas -U https://bitbucket.org/sti-cloud/secaas.git -C master
```

### Contribution guidelines ###

All code must not error when ran infinite number of times and must put the system into the same state, working, state regardless of its previous state.  Code should avoide causing as much disruption as possible.

### Who do I talk to? ###

Sid Eaton or Tom Martin