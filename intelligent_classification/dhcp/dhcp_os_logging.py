#!/usr/bin/python
'''
    This script is still in ALPHA stages.  But serves the basis of a first revision of DHCP OS
    fingerprinting using TCPDUMP.  This script currently only supports eth1 as the span interface.
    Logging and Alienvault integration have not been built yet.
'''

import subprocess as sub
import re

fingerprints = {}
fingerprints['"MSFT 5.0"'] = "Windows XP"
fingerprints['"ecobee1"'] = "Ecobee 3 WiFi Thermostat"
fingerprints['"dhcpcd-5.5.6"'] = "Android 5.0/Linux"
fingerprints["Domain-Name-Server, Default-Gateway, Subnet-Mask, Domain-Name TFTP, BF, BS, Netbios-Name-Server Hostname, FQDN"] = "HP Printer/OfficeJet"
fingerprints["Subnet-Mask, Domain-Name, Default-Gateway, Domain-Name-Server Netbios-Name-Server, Netbios-Node, Netbios-Scope, Router-Discovery Static-Route, Classless-Static-Route-Microsoft, Vendor-Option"] = "Windows XP"
fingerprints["Subnet-Mask, Static-Route, Default-Gateway, Domain-Name-Server Domain-Name, BR, Lease-Time, RN RB"] = "Android 5.0"
fingerprints["Subnet-Mask, Default-Gateway, Domain-Name-Server, BR"] = "Ecobee 3 WiFi Thermostat"

def findWholeWord(w):
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search

hostname = ""
vendor = ""
mac = ""
ip = "0.0.0.0"
messagetype = ""
endmessage = ""
parameterrequest = ""
inparameterrequest = 0
p = sub.Popen(('sudo', 'tcpdump', '-i', 'eth1', '-l', '-s 0', '-vvv', '-n', '((udp port 67) and (udp[8:1] = 0x1))'), stdout=sub.PIPE)
for row in iter(p.stdout.readline, b''):
#    print 'LINE: ' + row #.split(' ')[-1]
    if findWholeWord('DHCP-Message')(row):
#        print 'MessageType: ' + row.split(' ')[-1]
        messagetype = row.split(' ')[-1].rstrip()
        inparameterrequest = 0
    elif findWholeWord('Vendor-Class')(row):
#        print row.split(':')[-1]
        vendor = row.split(':')[-1].rstrip().lstrip()
        inparameterrequest = 0
    elif findWholeWord('Option 12')(row):
#        print row.split(' ')[-1]
        hostname = row.split(' ')[-1].rstrip()
        inparameterrequest = 0
    elif findWholeWord('requested-ip')(row) and not findWholeWord('Discover')(messagetype):
#        print row.split(' ')[-1]
        ip = row.split(' ')[-1].rstrip()
        inparameterrequest = 0
    elif findWholeWord('Client-IP')(row) and not findWholeWord('Discover')(messagetype):
#        print row.split(' ')[-1]
        ip = row.split(' ')[-1].rstrip()
        inparameterrequest = 0
    elif findWholeWord('client-id')(row):
#        print row.split(' ')[-1]
        mac = row.split(' ')[-1].rstrip()
        inparameterrequest = 0
    elif findWholeWord('Option 55')(row):
#        print row.split(' ')[-1]
#        mac = row.split(' ')[-1].rstrip()
        inparameterrequest = 1
    elif findWholeWord('end')(row):
#        print row.split(' ')[-1]
        endmessage = row.split(' ')[-1].rstrip()
        print "Message Type: " + messagetype + ",IP: " + ip + ",MAC: " + mac + ",Vendor: " + vendor + ",Hostname: " + hostname + ",OS: " + fingerprints.get(vendor, "UNKNOWN") + ",Param OS Identification: " + fingerprints.get(parameterrequest.lstrip(),"UNKNOWN") + ",Param Request: ", parameterrequest.lstrip()
        hostname = ""
        vendor = ""
        mac = ""
        ip = ""
        messagetype = ""
        endmessage = ""
        inparameterrequest = 0
        parameterrequest = ""
    elif findWholeWord('Option ')(row):
        inparameterrequest = 0
    elif inparameterrequest > 0:
        parameterrequest = '' + parameterrequest.lstrip() + ' ' + row.rstrip().lstrip()