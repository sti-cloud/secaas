#!/usr/bin/python

"""Store input in a queue as soon as it arrives, and work on
it as soon as possible. Do something with untreated input if
the user interrupts. Do other stuff if idle waiting for
input."""

import sys
import select
import time
import threading
#import queue
import Queue
from sets import Set
import re

# Open file to write results
lfh = open('/var/log/sti-http-identification.log', 'a')
MAX_HASH_SIZE = 5000

# files monitored for input
read_list = [sys.stdin]
# select() should wait for this many seconds for input.
timeout = 0.1 # seconds
last_work_time = time.time()

def genericMatch(regex,line,capturegroup):
  try:
    m = re.search(r'' + regex + r'', line)
    return m.group(capturegroup)
  except (AttributeError, TypeError):
    return ""

def parseIP(line):
  try:
    m = re.search(r'^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}', line)
    return m.group(0)
  except:
    return ""

def treat_input(linein):
  global last_work_time
  os = ""
  browser = ""
  ip = parseIP(linein)
  print("Workin' it!" + linein)
  if "Windows NT 6.1" in linein:
    os = "Windows 7 / Windows Server 2008 R2"
  elif "Windows NT 6.2" in linein:
    os = "Windows 8 / Windows Server 2012"
  elif "Windows NT 6.3" in linein:
    os = "Windows 8.1 / Windows Server 2012 R2"
  elif "Windows NT 10" in linein:
    os = "Windows 10 / Windows Server 2016"
  elif "Windows NT 6.0" in linein:
    os = "Windows Vista / Windows Server 2008"
  elif "Android" in linein:
    os = genericMatch("Android\s?[^;]+",linein,0)
    browser = "NA"
  elif "iPod;" in linein:
    os = "iPod " + genericMatch("OS\s(\S+)",linein,1)
    browser = "NA"
  elif "iPhone" in linein:
    os = "iPhone " + genericMatch("OS\s(\S+)",linein,1)
    browser = "NA"
  elif "Linux" in linein:
    os = "Linux"
  if "MSIE" in linein:
    browser = "Microsoft Internet Explorer " + genericMatch("MSIE ([0-9]{1,})",linein,1)
  elif "Chrome" in linein:
    browser = "Chrome " + genericMatch("Chrome/(\S+)",linein,1)
  elif "Trident" in linein:
    browser = "Microsoft Internet Explorer " + genericMatch("\srv:([0-9]+)",linein,1)
  elif "Safari" in linein:
    browser = "Safari"
  elif "Firefox/" in linein:
    browser = "Firefox " + genericMatch("\sFirefox/([0-9]+)",linein,1)
  elif "Firefox" in linein:
    browser = "Firefox " + genericMatch("\srv:([0-9]+)",linein,1)
  elif "Mozilla" in linein:
    browser = "Mozilla " + genericMatch("Mozilla/(\S+)",linein,1)
  elif "Microsoft Office" in linein:
    browser = genericMatch("Microsoft Office \S+ \S[^; ]+",linein,0)
  elif "Microsoft Lync" in linein:
    browser = genericMatch("Microsoft Lync \S[^; )]+",linein,0)

  # Strip white space
  browser = browser.strip()
  os = os.strip()
  ip = ip.strip()
  if os and ip and browser:
    lfh.write("IP: " + ip + ", OS: " + os + ", Software: " + browser + "\n")
  else:
    linein = linein.rstrip()
    lfh.write(linein + "\n")
  print("IP: " + ip + ", OS: " + os + ", Software: " + browser + "\n")
  last_work_time = time.time()

def idle_work():
  global last_work_time
  now = time.time()
  # do some other stuff every 2 seconds of idleness
  if now - last_work_time > 2:
    print('Idle for too long; doing some other stuff.')
    last_work_time = now

# some sort of cleanup that involves the input
def cleanup():
  print()
  while not input_queue.empty():
    line = input_queue.get()
    #print "Didn't get to work on this line:", line, end=''
    treat_input(line)
    #print "Didn't get to work on this line:" + line
  lfh.close()

# will hold input
#input_queue = queue.Queue()
input_queue = Queue.Queue()

# will signal to the input thread that it should exit:
# the main thread acquires it and releases on exit
interrupted = threading.Lock()
interrupted.acquire()

# input thread's work: stuff input in the queue until
# there's either no more input, or the main thread exits
def read_input():
  dup = Set()
  validatepattern = re.compile(r'.*User-Agent:\s.*\.$')
  while (read_list and
         #not interrupted.acquire(blocking=False)):
         not interrupted.acquire(0)):
    ready = select.select(read_list, [], [], timeout)[0]
    for file in ready:
      line = file.readline()
      if not line: # EOF, remove file from input list
        read_list.remove(file)
      elif line.rstrip() and "Microsoft-CryptoAPI" not in line and "Microsoft BITS" not in line and validatepattern.match(line) is not None: # optional: skipping empty lines
        if line not in dup:
          input_queue.put(line)
          dup.add(line)
          while (len(dup) > MAX_HASH_SIZE):
            dup.pop()
        #elif line in dup:
        #  print "Not adding to queue, set size: ", len(dup)
  print('Input thread is done.')

input_thread = threading.Thread(target=read_input)
input_thread.start()

try:
  while True:
    # if finished reading input and all the work is done,
    # exit
    if input_queue.empty() and not input_thread.is_alive():
      break
    else:
      try:
        treat_input(input_queue.get(timeout=timeout))
        #except queue.Empty:
      except Queue.Empty:
        idle_work()
except KeyboardInterrupt:
  cleanup()

# make input thread exit as well, if still running
interrupted.release()
print('Main thread is done.')