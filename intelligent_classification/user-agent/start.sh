#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ngrep -d eth1 -q -l -W byline 'User-Agent' "dst port 80" | egrep "(T.*->|X-Forwarded-For|User-Agent)" | awk '/T.*->/{if (x)print x;x="";}{x=(!x)?$2:x","$0;}END{print x;}' | sed -rn 's/([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}):[0-9]+(.*)/\1\2/p' | python $DIR/useragent-identification.py