# New Device Types TODO MOVE TO CENTRAL FILE.
insert ignore into device_types (id,name,class) VALUES (699,'Building Sensor',6);

# First Update, then Insert
UPDATE IGNORE host_properties SET value = 'Google Nest OS' where host_id IN (SELECT host_id from host_ip where (HEX(mac)) like '18B430%') and property_ref = 3;
INSERT IGNORE into host_properties (host_id,property_ref,source_id,value) (SELECT host_id, '14', '1', 'Unknown Google Nest Device' from host_ip where (HEX(mac)) like '18B430%'); host_properties (host_id,property_ref,source_id,value) (SELECT host_id, '3', '1', 'Google Nest OS' from host_ip where (HEX(mac)) like '18B430%');

# This line will insert generic model description into device field, if one is not specified already.
INSERT IGNORE into host_properties (host_id,property_ref,source_id,value) (SELECT host_id, '14', '1', 'Unknown Google Nest Device' from host_ip where (HEX(mac)) like '18B430%' and host_id not in (select host_id from host_properties where property_ref = 14));

# This will add a generic description if the description column is null.
update ignore host SET descr = 'This is a Google Nest home/office automation device.  This could be a smoke detector, thermostat, or another device.' where id IN (SELECT host_id from host_ip where (HEX(mac)) like '18B430%') and descr is NULL;