# First Update, then Insert
UPDATE IGNORE host_properties SET value = 'Nintendo OS' where host_id IN (SELECT host_id from host_ip where (HEX(mac)) like '001AE9%') and property_ref = 3;
INSERT IGNORE into host_properties (host_id,property_ref,source_id,value) (SELECT host_id, '3', '2', 'Nintendo OS' from host_ip where (HEX(mac)) like '001AE9%');

# This line will insert generic model description into device field, if one is not specified already.
INSERT IGNORE into host_properties (host_id,property_ref,source_id,value) (SELECT host_id, '14', '2', 'Unknown Nintendo Device' from host_ip where (HEX(mac)) like '001AE9%' and host_id not in (select host_id from host_properties where property_ref = 14));

# This will add a generic description if the description column is null.
update ignore host SET descr = 'Nintendo is a maker of gaming consoles and devices.' where id IN (SELECT host_id from host_ip where (HEX(mac)) like '001AE9%') and descr is NULL;