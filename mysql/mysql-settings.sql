--
-- Sentinel Technologies MySQL Setting Modifications
-- Written By: Sidney Eaton
-- Last Modified: 10-29-2018
--

set global max_connections = 220;
set global innodb_online_alter_log_max_size=134217728000;