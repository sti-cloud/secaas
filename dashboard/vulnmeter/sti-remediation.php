<HTML>
<HEAD>
  <TITLE></TITLE>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
</HEAD>
<?php
ini_set("max_execution_time","720");
include 'sti-vuln.css';
require_once 'av_init.php';
require_once 'config.php';
require_once 'functions.inc';
require_once 'ossim_sql.inc';

Session::logcheck('environment-menu', 'EventsVulnerabilities');

$conf = $GLOBALS['CONF'];

// Set Defaults
$days = 60;
$risk_input = "7";
$family = "Windows : Microsoft Bulletins";
$asset_group = "ALL";
$additionalQueryRestrictions = ""; // This will be appended to both queries
$additionalJoins = ""; //This allows us sometimes to add additional tables depending on the value selected to the prefilter.
$prefilterQueryRestrictions = ""; // This allows the prefilter to add additional where clauses.

// Prepare counters for stats
$VulnerabilityBreakDown = array();
$HostCount = 0;


// Read in query string variables.
if (ossim_valid($_GET['days'], OSS_DIGIT))
{
  $days = $_GET['days'];
}

// Check the family and only allow certain characters or ignore input.
if (preg_match('/^[0-9a-z: -]+$/i', stripslashes($_GET['family'])) === 1)
{
  $family = $_GET['family'];
  if (strtoupper($family) !== "ALL") {
    #$additionalQueryRestrictions = $additionalQueryRestrictions . " and v.family = '$family'";
    $additionalQueryRestrictions = $additionalQueryRestrictions . " and t2.name = '$family'";
  }
}

// Validate the risk query string input.  This is to prevent security issues.  Risk must be a number or ignore.
if (ossim_valid($_GET['risk'], OSS_DIGIT))
{
  $risk_input = $_GET['risk'];
  $additionalQueryRestrictions = $additionalQueryRestrictions . " and res.risk <= $risk_input";
}

// Check the group input and only allow certain characters or ignore input.
if (preg_match('/^[0-9a-z -_]+$/i', stripslashes($_GET['group'])) === 1)
{
  $asset_group = $_GET['group'];
  if (strtoupper($asset_group) !== "ALL") {
    #$asset_group_query = "DROP TEMPORARY TABLE IF EXISTS tmpAssetGroupMappingTbl; CREATE TEMPORARY TABLE tmpAssetGroupMappingTbl AS (select trim(inet_ntoa(CONV(hex(h.ip), 16, 10))) as IP, hg.name as GROUPNAME from host_ip h   LEFT JOIN host_group_reference hgr ON h.host_id = hgr.host_id   LEFT JOIN host_group hg ON hgr.host_group_id = hg.id where hg.name = '$asset_group')";
    #$asset_group_query_result=$dbconn->execute($asset_group_query);
    #$additionalJoins = $additionalJoins . " LEFT JOIN tmpAssetGroupMappingTbl mytmpTable ON res.hostIP = mytmpTable.IP";
    $additionalJoins = $additionalJoins . " LEFT JOIN (select trim(inet_ntoa(CONV(hex(h.ip), 16, 10))) as IP, hg.name as GROUPNAME from host_ip h   LEFT JOIN host_group_reference hgr ON h.host_id = hgr.host_id   LEFT JOIN host_group hg ON hgr.host_group_id = hg.id where hg.name = '$asset_group') mytmpTable ON res.hostIP = mytmpTable.IP";
    $prefilterQueryRestrictions = $prefilterQueryRestrictions . " and res.hostIP = mytmpTable.IP";
  }
}


// This function is responsible for displaying the submission form.
function displayForm()
{
  global $dbconn, $days, $risk_input, $family, $asset_group;
?>
<form method="get" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  <fieldset>
    <table>
    <tr><th><label>Family:</label></th>
    <td><select name="family">
      <option value="ALL" <?php echo $family === "ALL" ? 'selected' : ''; ?>>ALL</option>
      <?php
      $family_query = "SELECT t2.name as family FROM vuln_nessus_family_feed t2 UNION DISTINCT SELECT t2.name as family FROM vuln_nessus_family t2";
      $family_result = $dbconn->execute($family_query);
      while ($family_result->fields)
      {
        $isSelected = $family === $family_result->fields['family'] ? 'selected' : '';
        echo '<option value="' . $family_result->fields['family'] . '" ' . $isSelected . '>' . $family_result->fields['family'] . '</option>';
        $family_result->MoveNext();
      }
      ?>
    </select></td></tr>

    <tr><th><label>Asset Group:</label></th>
    <td><select name="group">
      <option value="ALL" <?php echo $asset_group === "ALL" ? 'selected' : ''; ?>>ALL</option>
      <?php
      $ag_query = "select distinct(name) as name from host_group";
      $ag_result = $dbconn->execute($ag_query);
      while ($ag_result->fields)
      {
        $isSelected = $asset_group === $ag_result->fields['name'] ? 'selected' : '';
        echo '<option value="' . $ag_result->fields['name'] . '" ' . $isSelected . '>' . $ag_result->fields['name'] . '</option>';
        $ag_result->MoveNext();
      }
      ?>
    </select></td></tr>

    <tr><th><label>Risk Greater Than:</label></th>
    <td><select name="risk">
      <option value="1" <?php echo $risk_input === "1" ? 'selected' : ''; ?>><?php echo getrisk("1"); ?></option>
      <option value="2" <?php echo $risk_input === "2" ? 'selected' : ''; ?>><?php echo getrisk("2"); ?></option>
      <option value="3" <?php echo $risk_input === "3" ? 'selected' : ''; ?>><?php echo getrisk("3"); ?></option>
      <option value="4" <?php echo $risk_input === "4" ? 'selected' : ''; ?>><?php echo getrisk("4"); ?></option>
      <option value="5" <?php echo $risk_input === "5" ? 'selected' : ''; ?>><?php echo getrisk("5"); ?></option>
      <option value="6" <?php echo $risk_input === "6" ? 'selected' : ''; ?>><?php echo getrisk("6"); ?></option>
      <option value="7" <?php echo $risk_input === "7" ? 'selected' : ''; ?>><?php echo getRisk("7"); ?></option>
    </select></td></tr>
    <tr><th><label>Include within the last:</label></th>
    <td><input name="days" type="text" value="<?php echo $days; ?>"></td></tr>
    <tr><th></th><td><input type="submit" value="Submit"> <?php echo "<a href='./sti-remediation_csv.php?" . $_SERVER['QUERY_STRING'] . "'>Download as CSV</a>"; ?></td></tr>
  </table>
  </fieldset>
</form>
<p><b>Please note:</b> CSV data is of the last submitted search results.  In other words, the data that is currently displayed to screen will be in the downloaded CSV file.</p>
<?php
}


// This function is responsible for finding URL links within text and making them linkable in HTML.
function link_it($text)
{
  $text= preg_replace("/(^|[\n ])([\w]*?)([\w]*?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" target=\"_blank\">$3</a>", $text);
  $text= preg_replace("/(^|[\n ])([\w]*?)((www)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http://$3\" target=\"_blank\">$3</a>", $text);
  $text= preg_replace("/(^|[\n ])([\w]*?)((ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"ftp://$3\" >$3</a>", $text);
  $text= preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);
  return($text);
}



function create_index()
{
  $num_colums=4;
  echo '<h1>Table of Contents</h1>';
  echo '<div id="toc-heading"><a href="#VulnBreakdown">Vulnerability Breakdown</a></div>';
  echo '<div id="toc-heading"><a href="#HostBreakdown">Host Breakdown</a></div>';
  global $heading_result, $HostCount;
  echo '<div class="sub-toc-table">' . "\n";
  while ($heading_result->fields)
  {
    if ($HostCount == 0 or $HostCount%$num_colums == 0) {
      echo '<div class="sub-toc-row">';
    }
    // Parse out host info fields.
    $hostIP = $heading_result->fields['hostIP'];
    echo '<div class="sub-toc-cell"><a href="#' . $hostIP . '">' . $hostIP . '</a></div>' . "\n";
    $HostCount++;
    if ($HostCount%$num_colums == 0) {
      echo '</div>';
    }
    $heading_result->MoveNext();
  }
  if ($HostCount%$num_colums != 0) {
    echo '</div>';
  }
  echo '</div>';
}

#function get_msg($dbconn, $query_msg)
#{
#    $result = $dbconn->execute($query_msg);
#
#    return ($result->fields['msg']);
#}

// This function will put HTML new lines in front of the delimiters within a string.
// Usefull for Operating system lists that are not well formed.
function parseSoftware($value, $delimiter)
{
  #$value_array = explode($delimiter, "TEST OS " . $value);
  $value_array = explode($delimiter, $value);
  $new_value = "";
  $i = 0;
  #echo 'String Length:' . strlen($value_array[0]) . ":" . $value_array[0];
  #echo 'String Length:' . strlen($value_array[1]) . ":" . $value_array[1];
  if (strlen($value_array[$i]) == 0) {
    // This means the first value is blank and matched the second entry matched the delimiter.
    // Skip index 0, set the base string to index 1 and prepend the delimiter. Then set up
    // the while loop to proceed with index 2.
    $i++;
    $new_value = $delimiter;
  }
  $new_value = $new_value . $value_array[$i];
  $i++;

  while ($i < count($value_array)) {
    $new_value = $new_value . "<br>" . $delimiter . $value_array[$i];
    $i++;
  }
  return $new_value;
}


// Parent parsing function.  Allows for consistent and repeatable parsing.
function parseSoftwareParent($value)
{
  $value = parseSoftware($value, "Microsoft");
  $value = parseSoftware($value, "Adobe");
  $value = parseSoftware($value, "Apache");
  return $value;
}


// This function is used to massage operating system strings for comparison.  It
// will try to make sure that we are doing as close to like for like OS comparisons.
function stripOutCommon($value)
{
  $value = str_replace("(R) ", " ", $value);
  $value = str_replace("Microsoft ", "", $value);
  $value = str_replace("Windows ", "", $value);
  $value = str_replace("Service Pack ", "", $value);
  $value = str_replace("Server ", "", $value);
  $value = str_replace("SP ", "", $value);
  $value = str_replace("2K3 ", "2003 ", $value);
  $value = str_replace("2K ", "2000 ", $value);
  #echo "$value<br>";
  return $value;
}



// This function will highlight the closest $asset_os match within the $os_string.
function highlight_similar($asset_os, $os_string)
{
  // Parse HTML line breaks;
  $value = explode("<br>", $os_string);

  // Init variables to determine best matching string
  $high_value_count = 0;
  $high_value_line_num = -1;

  // Strip out or morph acroynms and common strings from asset's detected OS.
  $asset_os_stripped = stripOutCommon($asset_os);

  // Parse first word.  Will be used in comparison, so should be 2003, XP, Vista, 7, etc.
  $asset_os_first_word = explode(' ', trim($asset_os_stripped))[0];

  // Loop through all line HTML line break array.
  for ($i=0; $i < count($value); $i++) {
    // Only try to compare Windows operating systems not other MS products.
    if (strpos($value[$i], 'Windows') !== false) {

      // Apply same morphing of acroynms and common strings to the line and get first word.
      $os_stripped = stripOutCommon($value[$i]);
      $os_first_word = explode(' ', trim($os_stripped))[0];

      // Compare first words to make sure they are the same core OS.
      if (strpos($os_first_word, $asset_os_first_word) !== false) {
        // Account for R2 being either present or missing from both comparison lines.
        if (((strpos($os_stripped, "R2")) !== false and (strpos($asset_os_stripped, "R2") !== false)) or ((strpos($os_stripped, "R2") == false) and (strpos($asset_os_stripped, "R2") == false))) {
          $myCount = similar_text($asset_os_stripped, $os_stripped, $myPercent);
          // Used for debugging.
          #echo "$asset_os_stripped TO $os_stripped = $myCount:$myPercent%<br>";
          if ($myPercent > $high_value_count) {
            $high_value_line_num = $i;
            $high_value_count = $myPercent;
          }
        }
      }
    }
  }

  // If no HTML line breaks or no matches just return input OS_STRING.
  if ($high_value_line_num < 0 or count($value) < 2) {
    #return "No strong match";
    return $os_string;
  } else {
    // Otherwise highlight the highest matching OS and put back the OS_STRING, then return it.
    $value[$high_value_line_num] = '<span style="background-color: #FFFF00">' . $value[$high_value_line_num] . '</span>';
    $new_string = $value[0];
    for ($j=1; $j < count($value); $j++) {
      $new_string = $new_string . '<br>' . $value[$j];
    }
    return $new_string;
    #return '<span style="background-color: #FFFF00">' . $value[$high_value_line_num] . '</span>';
  }
}

function getHostID($host_ip, $ctx)
{
  global $dbconn;
  return key(Asset_host::get_id_by_ips($dbconn, $host_ip, $ctx));
}

function getAssetOS($host_id)
{
  global $dbconn;
  $asset = Asset_host::get_object($dbconn, $host_id);
  return $asset->get_os()['value'];
}

function getAssetHostname($host_id)
{
  global $dbconn;
  // Get hostname
  if(valid_hex32($host_id)) {
    return Asset_host::get_name_by_id($dbconn, $host_id);
  }
  else
  {
    return _('unknown');
  }
}

// Presently NOT USED
function scrub_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  return $data;
}

// Display the form to manipulate this document.
displayForm();

//Discover all the hosts and their latest scan time.  When multiple scans exists do not pull up old ones.
$heading_query = "select distinct(res.hostIP) as hostIP, HEX(res.ctx) as ctx, max(res.scantime) as scantime from vuln_nessus_latest_results AS res
$additionalJoins
LEFT JOIN vuln_nessus_plugins AS v ON v.id=res.scriptid
LEFT JOIN vuln_nessus_family AS t2 ON v.family=t2.id
where str_to_date(scantime, '%Y%m%d%H%i%S') > NOW() - INTERVAL $days DAY
and res.sid>=0
$additionalQueryRestrictions
$prefilterQueryRestrictions
GROUP BY hostIP, ctx
UNION DISTINCT
select distinct(res.hostIP) as hostIP, HEX(res.ctx) as ctx, max(res.scantime) as scantime from vuln_nessus_latest_results AS res
$additionalJoins
LEFT JOIN vuln_nessus_plugins_feed AS v ON v.id=res.scriptid
LEFT JOIN vuln_nessus_family AS t2 ON v.family=t2.id
where str_to_date(scantime, '%Y%m%d%H%i%S') > NOW() - INTERVAL $days DAY
and res.sid<0
$additionalQueryRestrictions
$prefilterQueryRestrictions
GROUP BY hostIP, ctx";

$heading_result=$dbconn->execute($heading_query);

// Create the index.
create_index();

// The index uses the heading_result results to avoid another lookup.  Reset the cursor.
$heading_result->MoveFirst();


echo '<h1 id="HostBreakdown">Latest Vulnerability Scan Results By Hosts</h1><br>';
while ($heading_result->fields)
{
  // Parse out and retrieve host info fields.
  $hostIP = $heading_result->fields['hostIP'];
  $hostctx = $heading_result->fields['ctx'];
  $scantime = $heading_result->fields['scantime'];
  $host_id = getHostID($hostIP, $hostctx);
  $os_name = getAssetOS($host_id);
  $hostname = getAssetHostname($host_id);

  // Start building outside host table.
  echo '<h2 id="' . $hostIP . '">' . $hostIP . " (" . $hostname . ")" . "</h2>";
  echo "<b>Detected Asset Operating System:</b> $os_name<br>";
  echo "<b>Scan time:</b> $scantime<br>";
  echo "<table border='1' class='ResultTable'>
        <thead><tr>
        <th>Vulnerability Name</th>
        <th>Risk Level</th>
        <th>CVSS</th>
        <th>Observation</th>
        <th>Impact</th>
        <th>Affected Operating System/Software</th>
        </tr></thead>";

  //Query each host information.
  $query_msg = "select res.hostIP, res.result_id, res.service, res.risk, res.falsepositive, res.scriptid, v.name, res.msg, rep.sid, v.cve_id
                from vuln_nessus_latest_results AS res
                LEFT JOIN vuln_nessus_plugins AS v ON v.id=res.scriptid
                LEFT JOIN vuln_nessus_family AS t2 ON v.family=t2.id, vuln_nessus_latest_reports rep
                  where res.msg<>''
                     and res.hostIP=rep.hostIP
                     and res.ctx=rep.ctx
                     and res.hostIP='" . $hostIP . "'
                     and res.ctx=UNHEX('" . $hostctx . "')
                     and res.username=rep.username
                     and res.sid=rep.sid
                     and rep.sid>=0
                     $additionalQueryRestrictions
                     and res.scantime = '$scantime'
                     and str_to_date(res.scantime, '%Y%m%d%H%i%S') > NOW() - INTERVAL $days DAY
                     UNION DISTINCT
               select res.hostIP, res.result_id, res.service, res.risk, res.falsepositive, res.scriptid, v.name, res.msg, rep.sid, v.cve_id
                 from vuln_nessus_latest_results AS res
                 LEFT JOIN vuln_nessus_plugins_feed AS v ON v.id=res.scriptid
                 LEFT JOIN vuln_nessus_family_feed AS t2 ON v.family=t2.id, vuln_nessus_latest_reports rep
                   where res.msg<>''
                     and res.hostIP=rep.hostIP
                     and res.ctx=rep.ctx
                     and res.hostIP='" . $hostIP . "'
                     and res.ctx=UNHEX('" . $hostctx . "')
                     and res.username=rep.username
                     and res.sid=rep.sid
                     and rep.sid<0
                     $additionalQueryRestrictions
                     and res.scantime = '$scantime'
                     and str_to_date(res.scantime, '%Y%m%d%H%i%S') > NOW() - INTERVAL $days DAY
               ORDER BY risk
               ";
  $result=$dbconn->execute($query_msg);

  while ($result->fields)
  {
    $result_id     = $result->fields['result_id'];
    $service       = $result->fields['service'];
    $risk          = $result->fields['risk'];
    $falsepositive = $result->fields['falsepositive'];
    $scriptid      = $result->fields['scriptid'];
    $pname         = $result->fields['name'];
    $msg           = $result->fields['msg'];
    $sid           = $result->fields['sid'];
    $cve_id        = $result->fields['cve_id'];

    // get CVSS
    if( preg_match("/cvss base score\s*:\s(\d+\.?\d*)/i",$msg,$found) ) {
      $cvss  = $found[1];
    }
    else {
      $cvss  = "-";
    }

    // get CVEs
    if ($cve_id != '')
    {
      $cves = $cve_id;
    }
    elseif (preg_match("/cve : (.*)\n?/i",$msg,$found))
    {
      $cves  = str_replace(", ","<br />",$found[1]);
    }
    else
    {
      $cves  = "-";
    }

    // Get risk text
    $risk_txt = getrisk($risk);

    // Parse out msg field and put parsed text into separate variables.
    $msg = htmlspecialchars_decode($msg, ENT_QUOTES);
    $msg = str_replace("&amp;","&", $msg);
    $dfields = extract_fields($msg);
    $pobservation = (empty($dfields['Summary'])) ? 'n/a' : $dfields['Summary'];
    $pimpact = (empty($dfields['Impact'])) ? 'n/a' : $dfields['Impact'];
    $os = (empty($dfields['Affected Software/OS'])) ? 'n/a' : $dfields['Affected Software/OS'];
    $pfix = (empty($dfields['Fix'])) ?      'n/a' : $dfields['Fix'];
    #$os_array = explode('Microsoft', $os);
    #$os = "";
    ##$os = $os_array[1];
    #for ($i = 0; $i < count($os_array); $i++) {
    #  $os = $os . "Microsoft" . $os_array[$i] . "<br>";
    #}
    $os = parseSoftwareParent($os);
    #$os = str_lreplace($os, "", "<br>");
    // Prepare the vulnerability breakdown by vulnerability so that another lookup of the same information
    // doesn't need to be done on the database.  This will ensure consistency if another scan is in progress.
    if (!(array_key_exists($pname,$VulnerabilityBreakDown))) {
      $VulnerabilityBreakDown[$pname] = array();
      $VulnerabilityBreakDown[$pname]['DETAIL']= array();
      $VulnerabilityBreakDown[$pname]['HOSTS']= array();
      $VulnerabilityBreakDown[$pname]['DETAIL']['IMPACT']=$pimpact;
      $VulnerabilityBreakDown[$pname]['DETAIL']['CVSS']=$cvss;
      $VulnerabilityBreakDown[$pname]['DETAIL']['OS']=$os;
      $VulnerabilityBreakDown[$pname]['DETAIL']['RISKTXT']=$risk_txt;
      $VulnerabilityBreakDown[$pname]['DETAIL']['RISK']=$risk;
      $VulnerabilityBreakDown[$pname]['DETAIL']['SUMMARY']=$pobservation;
      $VulnerabilityBreakDown[$pname]['DETAIL']['CVE']=$cves;
      $VulnerabilityBreakDown[$pname]['DETAIL']['FIX']=$pfix;
    }
    $VulnerabilityBreakDown[$pname]['HOSTS'][$hostIP]= array();
    $VulnerabilityBreakDown[$pname]['HOSTS'][$hostIP]['OS']= $os_name;
    $VulnerabilityBreakDown[$pname]['HOSTS'][$hostIP]['HOSTNAME']= $hostname;


    // Put rows together
    echo "<tr>";
    echo '<td><a href="#' . $pname . '">' . $pname . '</a></td>';
    echo "<td>" . $risk_txt . " ($risk)" . "</td>";
    echo "<td>" . $cvss . "</td>";
    echo "<td>" . $pobservation . "</td>";
    echo "<td>" . $pimpact . "</td>";
#    echo "<td>" . $os . "</td>";
    echo "<td>" . highlight_similar($os_name, $os) . "</td>";
    echo "</tr>";
    $result->MoveNext();
  }
  $heading_result->MoveNext();
  echo "</table>";
}

echo "<br><hr>";
echo '<h1 id="VulnBreakdown">Vulnerability Breakdown</h1>';
foreach($VulnerabilityBreakDown as $vuln=>$vulndetail) {
  $affectedHostCount = count($vulndetail['HOSTS']);
  echo '<hr><br><h2 id="' . $vuln . '">' . $vuln . '</h2>';
  echo '<table colspan=0 class="VulnerabilitySummary">';
  echo '<tr><th>Summary:</th><td>' . $vulndetail['DETAIL']['SUMMARY'] . "</td></tr>";
  echo '<tr><th>Impact:</th><td>' . $vulndetail['DETAIL']['IMPACT'] . "</td></tr>";
  echo '<tr><th>CVE:</th><td>' . $vulndetail['DETAIL']['CVE'] . "</td></tr>";
  echo '<tr><th>Fix:</th><td>' . link_it($vulndetail['DETAIL']['FIX']) . "</td></tr>";
  echo '</table>';
echo '<div class="divTable VulnDetailTable">
<div class="divTableHeading">
<div class="divTableRow">
<div class="divTableHead">CVSS Score</div>
<div class="divTableHead">Risk</div>
<div class="divTableHead">Hosts Affected</div>
<div class="divTableHead">Affected Operating Systems/Software</div>
</div>
</div>
<div class="divTableBody">
<div class="divTableRow">';
echo '<div class="divTableCell">' . $vulndetail['DETAIL']['CVSS'] . '</div>';
echo '<div class="divTableCell">' . $vulndetail['DETAIL']['RISKTXT'] . '</div>';
echo '<div class="divTableCell">' . $affectedHostCount . ' (' . round($affectedHostCount/$HostCount*100,1) . '%)' . '</div>';
echo '<div class="divTableCell">' . $vulndetail['DETAIL']['OS'] . '</div>';
echo '</div></div></div>';

#  echo "<table border='0'><tr>";
#  echo "<th>CVSS Score</th>";
#  echo "<th>Risk</th>";
#  echo "<th>Affected Operating Systems/Software</th>";
#  echo "</tr>";
#  echo "<tr>";
#  echo "<td><center>" . $vulndetail['DETAIL']['CVSS'] . "</center></td>";
#  echo "<td><center>" . $vulndetail['DETAIL']['RISKTXT'] . "</center></td>";
#  echo "<td>" . $vulndetail['DETAIL']['OS'] . "</td>";
#  echo "</tr></table>";
  echo "<table border='1' class='ResultTable'>
<thead><tr>
<th>Hostname</th>
<th>Host IP</th>
<th>OS</th>
</tr></thead>";
  foreach($vulndetail['HOSTS'] as $vulnhost=>$vulnhostdetail) {
    echo "<tr>";
    echo "<td>" . $vulnhostdetail['HOSTNAME'] . "</td>";
    echo "<td><a href='#$vulnhost'>" . $vulnhost . "</a></td>";
    echo "<td>" . $vulnhostdetail['OS'] . "</td>";
    echo "</tr>";
  }
  echo "</table>";
}

$dbconn->disconnect();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">