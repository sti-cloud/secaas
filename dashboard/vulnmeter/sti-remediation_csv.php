<?php
ini_set("max_execution_time","720");

require_once 'av_init.php';
require_once 'config.php';
require_once 'functions.inc';
require_once 'ossim_sql.inc';

/** Include PHPExcel */
require_once 'classes/PHPExcel.php';

Session::logcheck('environment-menu', 'EventsVulnerabilities');

$conf = $GLOBALS['CONF'];


// Set Defaults
$days = 60;
$risk_input = "7";
$family = "Windows : Microsoft Bulletins";
$asset_group = "ALL";
$additionalQueryRestrictions = ""; // This will be appended to both queries
$additionalJoins = ""; //This allows us sometimes to add additional tables depending on the value selected to the prefilter.
$prefilterQueryRestrictions = ""; // This allows the prefilter to add additional where clauses.


// Read in query string variables.
if (ossim_valid($_GET['days'], OSS_DIGIT))
{
  $days = $_GET['days'];
}

// Check the family and only allow certain characters or ignore input.
if (preg_match('/^[0-9a-z: -]+$/i', stripslashes($_GET['family'])) === 1)
{
  $family = $_GET['family'];
  if (strtoupper($family) !== "ALL") {
    $additionalQueryRestrictions = $additionalQueryRestrictions . " and t2.name = '$family'";
  }
}

// Validate the risk query string input.  This is to prevent security issues.  Risk must be a number or ignore.
if (ossim_valid($_GET['risk'], OSS_DIGIT))
{
  $risk_input = $_GET['risk'];
  $additionalQueryRestrictions = $additionalQueryRestrictions . " and res.risk <= $risk_input";
}

// Check the group input and only allow certain characters or ignore input.
if (preg_match('/^[0-9a-z -_]+$/i', stripslashes($_GET['group'])) === 1)
{
  $asset_group = $_GET['group'];
  if (strtoupper($asset_group) !== "ALL") {
    $additionalJoins = $additionalJoins . " LEFT JOIN (select trim(inet_ntoa(CONV(hex(h.ip), 16, 10))) as IP, hg.name as GROUPNAME from host_ip h   LEFT JOIN host_group_reference hgr ON h.host_id = hgr.host_id   LEFT JOIN host_group hg ON hgr.host_group_id = hg.id where hg.name = '$asset_group') mytmpTable ON res.hostIP = mytmpTable.IP";
    $prefilterQueryRestrictions = $prefilterQueryRestrictions . " and res.hostIP = mytmpTable.IP";
  }
}





function getHostID($host_ip, $ctx)
{
  global $dbconn;
  return key(Asset_host::get_id_by_ips($dbconn, $host_ip, $ctx));
}

function getAssetOS($host_id)
{
  global $dbconn;
  $asset = Asset_host::get_object($dbconn, $host_id);
  return $asset->get_os()['value'];
}

function getAssetHostname($host_id)
{
  global $dbconn;
  // Get hostname
  if(valid_hex32($host_id)) {
    return Asset_host::get_name_by_id($dbconn, $host_id);
  }
  else
  {
    return _('unknown');
  }
}

function getUniqueAssets()
{
  global $dbconn, $additionalJoins, $additionalQueryRestrictions, $prefilterQueryRestrictions, $days;
  //Discover all the hosts and their latest scan time.  When multiple scans exists do not pull up old ones.
  $heading_query = "select distinct(res.hostIP) as hostIP, HEX(res.ctx) as ctx, max(res.scantime) as scantime from vuln_nessus_latest_results AS res
  $additionalJoins
  LEFT JOIN vuln_nessus_plugins AS v ON v.id=res.scriptid
  LEFT JOIN vuln_nessus_family AS t2 ON v.family=t2.id
  where str_to_date(scantime, '%Y%m%d%H%i%S') > NOW() - INTERVAL $days DAY
  and res.sid>=0
  $additionalQueryRestrictions
  $prefilterQueryRestrictions
  GROUP BY hostIP, ctx
  UNION DISTINCT
  select distinct(res.hostIP) as hostIP, HEX(res.ctx) as ctx, max(res.scantime) as scantime from vuln_nessus_latest_results AS res
  $additionalJoins
  LEFT JOIN vuln_nessus_plugins_feed AS v ON v.id=res.scriptid
  LEFT JOIN vuln_nessus_family AS t2 ON v.family=t2.id
  where str_to_date(scantime, '%Y%m%d%H%i%S') > NOW() - INTERVAL $days DAY
  and res.sid<0
  $additionalQueryRestrictions
  $prefilterQueryRestrictions
  GROUP BY hostIP, ctx";

  $heading_result=$dbconn->execute($heading_query);
  $dataArray = array();

  while ($heading_result->fields)
  {
    $hostIP = $heading_result->fields['hostIP'];
    $hostctx = $heading_result->fields['ctx'];
    $scantime = $heading_result->fields['scantime'];
    $host_id = getHostID($hostIP, $hostctx);
    $os_name = getAssetOS($host_id);
    $hostname = getAssetHostname($host_id);

    $row = array();
    $row['HOSTNAME'] = $hostname;
    $row['HOSTIP'] = $hostIP;
    $row['OS'] = $os_name;
    $row['SCANTIME'] = $scantime;
    $row['CTX'] = $hostctx;
    $dataArray[] = $row;
    $heading_result->MoveNext();

  }
  return $dataArray;
}



function getVulnByHost() {
  global $dbconn, $additionalQueryRestrictions, $days;
  $myArray = getUniqueAssets();
  $dataArray = array();
  foreach ($myArray as $item) {
    //Query each host information.
    $query_msg = "select res.hostIP, res.result_id, res.service, res.risk, res.falsepositive, res.scriptid, v.name, res.msg, rep.sid, v.cve_id
                  from vuln_nessus_latest_results AS res
                    LEFT JOIN vuln_nessus_plugins AS v ON v.id=res.scriptid
                    LEFT JOIN vuln_nessus_family AS t2 ON v.family=t2.id, vuln_nessus_latest_reports rep
                  where res.msg<>''
                     and res.hostIP=rep.hostIP
                     and res.ctx=rep.ctx
                     and res.hostIP='" . $item['HOSTIP'] . "'
                     and res.ctx=UNHEX('" . $item['CTX'] . "')
                     and res.username=rep.username
                     and res.sid=rep.sid
                     and rep.sid>=0
                     $additionalQueryRestrictions
                     and res.scantime = '" . $item['SCANTIME'] . "'
                     and str_to_date(res.scantime, '%Y%m%d%H%i%S') > NOW() - INTERVAL $days DAY
                     UNION DISTINCT
                 select res.hostIP, res.result_id, res.service, res.risk, res.falsepositive, res.scriptid, v.name, res.msg, rep.sid, v.cve_id
                 from vuln_nessus_latest_results AS res
                   LEFT JOIN vuln_nessus_plugins_feed AS v ON v.id=res.scriptid
                   LEFT JOIN vuln_nessus_family_feed AS t2 ON v.family=t2.id, vuln_nessus_latest_reports rep
                 where res.msg<>''
                     and res.hostIP=rep.hostIP
                     and res.ctx=rep.ctx
                     and res.hostIP='" . $item['HOSTIP'] . "'
                     and res.ctx=UNHEX('" . $item['CTX'] . "')
                     and res.username=rep.username
                     and res.sid=rep.sid
                     and rep.sid<0
                     $additionalQueryRestrictions
                     and res.scantime = '" . $item['SCANTIME'] . "'
                     and str_to_date(res.scantime, '%Y%m%d%H%i%S') > NOW() - INTERVAL $days DAY
                 ORDER BY risk
               ";
    #echo $query_msg;
    $result=$dbconn->execute($query_msg);

    while ($result->fields) {
      // Make copy of row host stuff.
      $row = $item;

      $result_id     = $result->fields['result_id'];
      $service       = $result->fields['service'];
      $risk          = $result->fields['risk'];
      $falsepositive = $result->fields['falsepositive'];
      $scriptid      = $result->fields['scriptid'];
      $pname         = $result->fields['name'];
      $msg           = $result->fields['msg'];
      $sid           = $result->fields['sid'];
      $cve_id        = $result->fields['cve_id'];
      // get CVSS
      if( preg_match("/cvss base score\s*:\s(\d+\.?\d*)/i",$msg,$found) ) {
        $cvss  = $found[1];
      }
      else {
        $cvss  = "-";
      }

      // get CVEs
      if ($cve_id != '')
      {
        $cves = $cve_id;
      }
      elseif (preg_match("/cve : (.*)\n?/i",$msg,$found))
      {
        $cves  = str_replace(", ","<br />",$found[1]);
      }
      else
      {
        $cves  = "-";
      }


      // Parse out msg field and put parsed text into separate variables.
      $msg = htmlspecialchars_decode($msg, ENT_QUOTES);
      $msg = str_replace("&amp;","&", $msg);
      $dfields = extract_fields($msg);
      $pobservation = (empty($dfields['Summary'])) ? 'n/a' : $dfields['Summary'];
      $pimpact = (empty($dfields['Impact'])) ? 'n/a' : $dfields['Impact'];
      $os = (empty($dfields['Affected Software/OS'])) ? 'n/a' : $dfields['Affected Software/OS'];
      $pfix = (empty($dfields['Fix'])) ?      'n/a' : $dfields['Fix'];

      // Add additional columns
      $row['VULNNAME'] = $pname;
      $row['OBSERVATION'] = $pobservation;
      $row['IMPACT'] = $pimpact;
      $row['CVSS'] = $cvss;
      $row['CVES'] = $cves;
      $row['RISK_TXT'] = getrisk($risk);
      $dataArray[] = $row;
      $result->MoveNext();
    }
  }
  return $dataArray;
}




function getPresentableData() {
  $myArray = getVulnByHost();
  $dataArray = array();
  foreach ($myArray as $item) {
    $row = array();
    $row[] = $item['HOSTIP'];
    $row[] = $item['HOSTNAME'];
    $row[] = $item['RISK_TXT'];
    $row[] = $item['CVSS'];
    $row[] = $item['VULNNAME'];
    $row[] = $item['OBSERVATION'];
    $row[] = $item['IMPACT'];
    $row[] = $item['OS'];
    $dataArray[] = $row;
  }
  return $dataArray;
}







$dataArray=getPresentableData();

$dbconn->disconnect();


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Sentinel Technologies");


// Default font size and style alignment
$objPHPExcel->getDefaultStyle()->getFont()->setSize(14);
$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

$titles = array(
    'font'  => array(
        'bold'  => true

    )
);

// Report details

$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:M1")->setCellValue('A1', 'I.T Security Vulnerabiliy Report')
    ->getStyle('A1')->applyFromArray($titles);


$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A5', 'Host IP')
    ->getStyle('A5')->applyFromArray($titles)
    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setWidth(25);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B5', 'Hostname')
    ->getStyle('B5')->applyFromArray($titles)
    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setWidth(25);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C5', 'Risk Level')
    ->getStyle('C5')->applyFromArray($titles)
    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D5', 'CVSS')
    ->getStyle('D5')->applyFromArray($titles)
    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('D')->setWidth(5);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E5', 'Vulnerability Name')
    ->getStyle('E5')->applyFromArray($titles)
    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('E')->setWidth(40);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('E')->getAlignment()->setWrapText(TRUE);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F5', 'Observation')
    ->getStyle('F5')->applyFromArray($titles)
    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('F')->setWidth(40);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('F')->getAlignment()->setWrapText(TRUE);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G5', 'Impact')
    ->getStyle('G5')->applyFromArray($titles)
    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('G')->setWidth(40);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('G')->getAlignment()->setWrapText(TRUE);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H5', 'Operating System')
    ->getStyle('H5')->applyFromArray($titles)
    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('H')->setWidth(50);
$objPHPExcel->setActiveSheetIndex(0)->getStyle('H')->getAlignment()->setWrapText(TRUE);

$objPHPExcel->setActiveSheetIndex(0)->fromArray($dataArray, NULL, 'A6');

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Vulnerability Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$output_name = "ExportResult.xlsx";
$output_name = preg_replace( '/-_/', "", $output_name);


// Redirect output to a client web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$output_name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');


exit;
?>