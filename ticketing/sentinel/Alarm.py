#!/usr/bin/python
import datetime

class Alarm:

  def __init__(self, CollectionOfEvents, OverallRiskScore = "1", SourceIP = '0.0.0.0', DestinationIP = '0.0.0.0', SourcePort = 'NA', DestinationPort = 'NA', Protocol = 'NA', AlarmDate = None):
    self.RiskScore = OverallRiskScore
    self.Events = CollectionOfEvents
    self.SourceIP = SourceIP
    self.DestinationIP = DestinationIP
    self.SourcePort = SourcePort
    self.DestinationPort = DestinationPort
    self.Protocol = Protocol
    self.AlarmDate = AlarmDate


  def getAlarmDate(self):
    if self.AlarmDate is None:
      return str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"))
    else:
      return self.AlarmDate

  RiskScore = property()
  Events = property()
  SourceIP = property()
  DestinationIP = property()
  SourcePort = property()
  DestinationPort = property()
  Protocol = property()
  AlarmDateTime = property(getAlarmDate)

  def toString(self):
    print "Security Alarm:"
    print "---------------------------------"
    print "Date: %s, Risk: %s" % (self.AlarmDateTime,self.RiskScore)
    print "TRAFFIC: %s %s:%s -> %s:%s" % (self.protocolName(),self.SourceIP,self.SourcePort,self.DestinationIP,self.DestinationPort)
#    print "RELATED EVENTS:\n%s" % (self.printRelatedEvents(10))

  def printRelatedEvents(self, num = -1):
    i = 0
    RETURN = ""
    for row in self.Events:
      if i < num or num < 0:
        RETURN+="\n"+row.toString()
        i += 1
    return RETURN.lstrip()

  def protocolName(self):
    try:
      if int(self.Protocol) == 6:
        return "TCP"
      elif int(self.Protocol) == 17:
        return "UDP"
      else:
        return "NA"
    except:
      return self.Protocol