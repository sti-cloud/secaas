#!/usr/bin/python
# IMPORTANT STUFF TO FIX: DATE TIME MAY NOT BE TIMEZONE AWARE, CAN BE FIXED LATER.
import MySQLdb
from AlienVaultConfig import AlienVaultConfig
from datetime import datetime
from AlarmObject import AlarmObject, AlarmDetailObject
import time
import logging
import os

# Testing
from contextlib import closing

class AlarmQuerier:
  def __init__(self, startDateTimeUTC = None, debug = False):
    self.DEBUG = debug
    if startDateTimeUTC is None:
      if os.path.exists("/root/ticket.start") :
        with open("/root/ticket.start", "r") as din :
          self.startDateTime = datetime.strptime(din.read().strip(), "%Y-%m-%d %H:%M:%S")
        os.remove("/root/ticket.start")
      else:
        self.startDateTime = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    else:
      self.startDateTime = startDateTimeUTC

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("AlarmQuerier(%s): %s" % (method,log_entry))

#  def openConnection(self):
#    AVCONFIG = AlienVaultConfig()
#    return MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)

  # This is the new generic query.  Trying to make sure everything closes right in extremely high performance situations.
  def genericQuery(self, queryString):
    db = None
    results = None
    AVCONFIG = AlienVaultConfig()

    while True:
      try:
        db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)

        #self.logDebug("genericQuery", "Executing SQL command: %s" % queryString)
        logging.debug("Executing SQL command: %s" % queryString)
        with closing( db.cursor(MySQLdb.cursors.DictCursor) ) as cursor:
          cursor.execute("%s" % (queryString))
          results = cursor.fetchall()
        return results
      except MySQLdb.OperationalError, e:
        # An operational error generally includes "too many connections" thus this may not be a database problem but rather
        # a code issue. We will attempt to retry the operation.
        #self.logDebug("genericQuery", "Operational Error in executing query: %s  -- ERROR DETAILS: %s" % (queryString,e))
        logging.warning("Operational Error in executing query: %s  -- ERROR DETAILS: %s" % (queryString,e))
        time.sleep(30)
      except Exception, e:
        logging.exception("Error in executing query: %s  -- ERROR DETAILS: %s" % (queryString,e))
        #self.logDebug("genericQuery", "Error in executing query: %s  -- ERROR DETAILS: %s" % (queryString,e))
        return results
      finally:
        try:
          db.close()
        except:
          pass
    

#  # This is a generic query function that should implement connection error handling on program <-> database
#  def genericQuery(self, queryString):
#    AVCONFIG = AlienVaultConfig()
#    while True:
#      #cursor = None
#      #conn = None
#      try:
#        conn = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
#        cursor = conn.cursor(MySQLdb.cursors.DictCursor)
#        self.logDebug("genericQuery", "Performing Query: %s" % queryString)
#
#        cursor.execute("%s" % (queryString))
#        results = cursor.fetchall()
#
#        try:
#          cursor.close()
#        except:
#          self.logDebug("genericQuery", "Error closing the cursor: %s" % e)
#
#        try:
#          conn.close()
#        except Exception, e:
#          self.logDebug("genericQuery", "Error attempting to close database: %s" % e)
#        return results
#      except Exception, e:
#        self.logDebug("genericQuery", "Database connectivity error, retrying: %s" % e)
#        time.sleep(30)




  # The use of the limit can be dangerous as the smallest interval is one second in the database.  Therefore
  # it is possible that if the limit breaks on technically the same timestamp t
  def getAlarms(self, limit = -1):
    #extraSQLOpts = "a.plugin_sid = '30046' and "
    ##extraSQLOpts = "plugin_sid = '32051' and "
    ##extraSQLOpts = "A.plugin_id = '1594' and "
    extraSQLOpts = ""
#    conn = self.openConnection()
#    cursor = conn.cursor(MySQLdb.cursors.DictCursor)
    #self.logDebug("getAlarms","Getting alarms from database that are newer than %s." % self.startDateTime)
    logging.info("Getting alarms from database that are newer than %s." % self.startDateTime)
    #sql = "select ps.name AS NAME, A.plugin_id as PLUGIN_ID, A.plugin_sid as PLUGIN_SID, A.risk as RISK, INET_NTOA(CONV(HEX(A.src_ip),16,10)) as SRC_IP, INET_NTOA(CONV(HEX(A.dst_ip),16,10)) as DST_IP, A.src_port as SRC_PORT, A.dst_port as DST_PORT, A.protocol as PROTOCOL, HEX(A.backlog_id) as BACKLOG_ID, timestamp AS TIMESTAMP from alarm as A LEFT JOIN plugin_sid AS ps on ps.plugin_id = A.plugin_id and ps.sid = A.plugin_sid where " + extraSQLOpts + "A.timestamp > '%s' and A.status = 'open' ORDER BY A.timestamp ASC;" % (self.startDateTime) # ORIGINAL CODE
    ####sql = "select plugin_id as PLUGIN_ID, plugin_sid as PLUGIN_SID, risk as RISK, INET_NTOA(CONV(HEX(src_ip),16,10)) as SRC_IP, INET_NTOA(CONV(HEX(dst_ip),16,10)) as DST_IP, src_port as SRC_PORT, dst_port as DST_PORT, protocol as PROTOCOL, HEX(backlog_id) as BACKLOG_ID, timestamp AS TIMESTAMP from alarm where " + extraSQLOpts + "timestamp > '%s' and status = 'open' ORDER BY timestamp ASC" % (self.startDateTime)
    # TESTING THIS NEW QUERY
    sql = "select  ps.name AS NAME, a.plugin_id AS PLUGIN_ID, a.plugin_sid as PLUGIN_SID, hex(a.backlog_id) as BACKLOG_ID, hex(a.event_id) as EVENT_ID, a.status as STATUS, e.userdata1 as USERDATA1, e.userdata2 as USERDATA2, e.userdata3 as USERDATA3, e.userdata4 as USERDATA4, e.userdata5 as USERDATA5, e.userdata6 as USERDATA6, e.userdata7 as USERDATA7, e.userdata8 as USERDATA8, e.userdata9 as USERDATA9,INET_NTOA(CONV(HEX(a.src_ip),16,10)) as SRC_IP, INET_NTOA(CONV(HEX(a.dst_ip),16,10)) as DST_IP, a.timestamp AS TIMESTAMP, a.protocol as PROTOCOL, a.dst_port as DST_PORT, a.src_port as SRC_PORT, a.risk as RISK from alarm AS a, event AS e, plugin_sid AS ps where " + extraSQLOpts + "a.timestamp > '%s' and a.event_id = e.id and a.plugin_id = ps.plugin_id and a.plugin_sid = ps.sid and a.status = 'open' ORDER BY a.timestamp ASC;"  % (self.startDateTime)

    if limit > 0:
      sql += " LIMIT %s" % str(limit)

 
#    cursor.execute("%s;" % (sql))
#    results = cursor.fetchall()


    results = self.genericQuery(sql)
    if len(results) > 0:
      self.startDateTime = results[len(results)-1]["TIMESTAMP"]

#      if self.DEBUG:
#        for row in results:
#           print "%s,%s" % (row["BACKLOG_ID"],row["TIMESTAMP"])
#           self.getAlarmDetails(row["BACKLOG_ID"])
    return results

  def getAlarmDetails(self, backlogID):
#    cursor = self.openConnection().cursor(MySQLdb.cursors.DictCursor)
    #self.logDebug("getAlarmsDetails","Getting alarm details for %s." % backlogID)
    logging.info("Getting alarm details for %s." % backlogID)
    sql = "SELECT SQL_CALC_FOUND_ROWS plugin_sid.name AS NAME, hex(backlog_event.event_id) AS EVENT_ID, hex(backlog_event.backlog_id) AS BACKLOG_ID, hex(event.agent_ctx) AS CTX, event.timestamp AS BACKLOG_TIMESTAMP, event.plugin_id AS PLUGIN_ID, event.plugin_sid AS PLUGIN_SID, event.protocol AS PROTOCOL, event.src_port AS SRC_PORT, event.dst_port AS DST_PORT, INET_NTOA( CONV( HEX( event.src_ip ), 16, 10 )) as SRC_IP, INET_NTOA( CONV( HEX( event.dst_ip ), 16, 10 )) AS DST_IP, hex(event.src_host) as SRC_HOST, hex(event.dst_host) as DST_HOST, hex(event.src_net) as SRC_NET, hex(event.dst_net) as DST_NET, event.risk_c AS RISK_C, event.risk_a AS RISK_A, event.asset_src AS ASSET_SRC, event.asset_dst AS ASSET_DST, hex(event.sensor_id) AS SENSOR, event.alarm AS ALARM, event.tzone AS TZONE, backlog_event.rule_level AS RULE_LEVEL, case when event.plugin_id = 1505 THEN 0 ELSE 1 END AS dir_event FROM backlog_event, event, plugin_sid WHERE backlog_event.event_id = event.id and plugin_sid.sid =event.plugin_sid and plugin_sid.plugin_id = event.plugin_id AND backlog_event.backlog_id = unhex('%s') ORDER BY BACKLOG_TIMESTAMP,RULE_LEVEL DESC" % (backlogID)
#    cursor.execute("%s;" % (sql))
#    results = cursor.fetchall()

    results = self.genericQuery(sql)
#    if len(results) > 0:
#      if self.DEBUG:
#        self.alarmDetailsToString(results)
    return results

  def alarmDetailsToString(self, MySQLDictResults):
    if len(MySQLDictResults) > 0:
      for row in MySQLDictResults:
        print "  Name: %s " % (row['NAME'])
        print "  Date: %s\tEvent ID: %s\tPluginInfo: (%s,%s)" % (row['BACKLOG_TIMESTAMP'],row['EVENT_ID'],row['PLUGIN_ID'],row['PLUGIN_SID'])
        print "  %s %s:%s -> %s:%s\tRisk C: %s, Risk A: %s, Level: %s" % (row['PROTOCOL'],row['SRC_IP'],row['SRC_PORT'],row['DST_IP'],row['DST_PORT'],row['RISK_C'],row['RISK_A'],row['RULE_LEVEL'])
        print "  Sensor: %s, Alarm: %s, TZONE: %s\n" % (row['SENSOR'],row['ALARM'],row['TZONE'])

  def getAlarmsWithDetails(self):
    results = self.getAlarms()
    for row in results:
      sub_results = self.getAlarmDetails(backlogID = row["BACKLOG_ID"])
      row["ALARM_DETAILS"] = sub_results
    return results

  ########################################### NEW STUFF

  def getAlarmObjects(self):
    alarm_list = []
    results = self.getAlarms()
    logging.info("I GOT %s ALARM RESULTS" % str(len(results)))
    #print "I GOT %s ALARM RESULTS" % str(len(results))
    
    for row in results:
#      print "#########ROW: ", results
      myAlarmDetailObj = self.getAlarmDetails(backlogID = row["BACKLOG_ID"])
      AlarmObj = AlarmObject(row, myAlarmDetailObj)
      logging.debug("BACKLOG ID: %s, DATE: %s, RESULTS: %s" % (row["BACKLOG_ID"],row['TIMESTAMP'],str(len(myAlarmDetailObj))))
      alarm_list.append(AlarmObj)
#      alarm_list.append(AlarmObject(row, self.getAlarmDetails(backlogID = row["BACKLOG_ID"])))
    return alarm_list
      

#x = AlarmQuerier(startDateTimeUTC = "2016-08-08 19:47:47", debug = True)
#x = AlarmQuerier(startDateTimeUTC = "2016-08-09 15:36:08", debug = False)
#x.getAlarms()
#x.getAlarms()
#x.getAlarmsWithDetails()



