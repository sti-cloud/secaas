#!/usr/bin/python
import MySQLdb
import fileinput
import re
import binascii
config_file = "/etc/ossim/framework/ossim.conf"

def readDbInfo():

    user = ''
    password = ''

    for line in fileinput.input(config_file):
        p = re.compile(r"ossim_pass=(?P<pass>\S+)")
        m = p.match(line)
        if (m):
            password = m.group(1)
        p = re.compile(r"ossim_user=(?P<user>\S+)")
        m = p.match(line)
        if (m):
            user = m.group(1)

    return user, password

cred = readDbInfo()
dbUser = cred[0]
dbPass = cred[1]

def dbConn():
    db=MySQLdb.connect(host='localhost',user=dbUser , passwd=dbPass ,db='alienvault')
    cursor=db.cursor()
    return cursor


def getAlarms():
     cursor = dbConn()
     sql="select risk, INET_NTOA(CONV(HEX(src_ip),16,10)), INET_NTOA(CONV(HEX(dst_ip),16,10)), src_port, dst_port, protocol, HEX(backlog_id), timestamp from alarm LIMIT 1"
     cursor.execute(sql)
     alarms= cursor.fetchall()
     if alarms:
        for a in alarms:
                print "************************************ALARMS*******************************************"
                print "%s\tRisk: %s" % (a[7],a[0])
                print "%s %s:%s -> %s:%s" % (a[5],a[1],a[3],a[2],a[4])
#                print a[1],":",a[3],"----------->",a[2],":",a[4],"      ",a[5],"\n"
                sql = "SELECT SQL_CALC_FOUND_ROWS plugin_sid.name AS Name, hex(backlog_event.event_id) AS event_id, hex(backlog_event.backlog_id) AS backlog_id, hex(event.agent_ctx) AS ctx, event.timestamp AS backlog_time, event.timestamp AS timestamp, event.plugin_id AS plugin_id, event.plugin_sid AS plugin_sid, event.protocol AS protocol, event.src_port AS src_port, event.dst_port AS dst_port, INET_NTOA( CONV( HEX( event.src_ip ), 16, 10 )), INET_NTOA( CONV( HEX( event.dst_ip ), 16, 10 )), hex(event.src_host) as src_host, hex(event.dst_host) as dst_host, hex(event.src_net) as src_net, hex(event.dst_net) as dst_net, event.risk_c AS risk_c, event.risk_a AS risk_a, event.asset_src AS asset_src, event.asset_dst AS asset_dst, hex(event.sensor_id) AS sensor, event.alarm AS alarm, event.tzone AS tzone, backlog_event.rule_level AS rule_level, case when event.plugin_id = 1505 THEN 0 ELSE 1 END AS dir_event FROM backlog_event, event, plugin_sid WHERE backlog_event.event_id = event.id and plugin_sid.sid =event.plugin_sid and plugin_sid.plugin_id = event.plugin_id AND backlog_event.backlog_id = unhex('%s')" % (a[6])
                cursor.execute(sql)
                evs=cursor.fetchall()
                print "Related Events:"
                print "---------------"
                for e in evs:
                      print "  Name: %s " % (e[0])
                      print "  Date: %s\tEvent ID: %s\tPluginInfo: (%s,%s)" % (e[5],e[1],e[6],e[7])
                      print "  %s %s:%s -> %s:%s\tRisk C: %s, Risk A: %s, Level: %s" % (e[8],e[11],e[9],e[12],e[10],e[17],e[18],e[24])
                      print "  Sensor: %s, Alarm: %s, TZONE: %s\n" % (e[21],e[22],e[23])
     else:
        print "No alarms to display"

getAlarms()