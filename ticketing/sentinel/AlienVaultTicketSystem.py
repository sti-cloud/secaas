#!/usr/bin/python

import MySQLdb
import ConfigParser
import uuid
import datetime
from AlienVaultConfig import AlienVaultConfig
from contextlib import closing
from TicketConfig import TicketConfig
import logging
from SCNPEngine import scnpEngine

class TicketNotFoundException(Exception):
  pass

class AlienVaultTicketSystem:
  SentinelClosedTicketTag = 59001
  SentinelOpenTicketTag = 59000

  def __init__(self, OSSIM_CONFIG_FILE='/etc/ossim/ossim_setup.conf', database = 'alienvault', debug = False):
    #self.SentinelClosedTicketTag = 59001
    #self.SentinelOpenTicketTag = 59000
    from io import StringIO
    self.Database = database
    self.DEBUG = debug
    self.TC = TicketConfig()
    self.executeMultipleCommands([
      "INSERT INTO incident_tag_descr (id,name,descr) VALUES (" + str(AlienVaultTicketSystem.SentinelOpenTicketTag) + ",'Sentinel Open Ticket', 'A ticket that is still open in Sentinels ticket system.') ON DUPLICATE KEY UPDATE name='Sentinel Open Ticket',descr='A ticket that is still open in Sentinels ticket system.'",
      "INSERT INTO incident_tag_descr (id,name,descr) VALUES (" + str(AlienVaultTicketSystem.SentinelClosedTicketTag) + ",'Sentinel Closed Ticket', 'A ticket that is now closed in Sentinels ticket system.') ON DUPLICATE KEY UPDATE name='Sentinel Closed Ticket',descr='A ticket that is now closed in Sentinels ticket system.'"
    ])

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("AlienVaultTicketSystem(%s): %s" % (method,log_entry))

  def assignTag(self, AlienVaultTicketNumber, TagNumber):
    sql = "INSERT IGNORE INTO incident_tag (id, incident_id) VALUES ('%s','%s');" % (TagNumber,AlienVaultTicketNumber)
    results = self.executeCommand(sql)
    return results

  def removeTag(self, AlienVaultTicketNumber, TagNumber):
    sql = "DELETE FROM incident_tag WHERE id = '%s' AND incident_id = '%s';" % (TagNumber,AlienVaultTicketNumber)
    results = self.executeCommand(sql)
    return results

  # This is a generic query function that should implement connection error handling on program <-> database
  # THIS SHOULD BE USED GOING FORWARD AS IT CREATES DICTIONARIES.
  def genericQuery(self, queryString):
      AVCONFIG = AlienVaultConfig()
      conn = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
      cursor = conn.cursor(MySQLdb.cursors.DictCursor)
      #self.logDebug("genericQuery", "Performing Query: %s" % queryString)
      logging.debug("Performing Query: %s" % queryString)

      cursor.execute("%s" % (queryString))
      results = cursor.fetchall()

      try:
        cursor.close()
      except Exception, e:
        logging.exception("Error attempting to close cursor: %s" % e)
        #logDebug("genericQuery", "Error attempting to close cursor: %s" % e)

      try:
        conn.close()
      except Exception, e:
        #logDebug("genericQuery", "Error attempting to close database: %s" % e)
        logging.exception("Error attempting to close database: %s" % e)
      return results


  def executeCommand(self, statement):
    db = None
    try:
      AVCONFIG = AlienVaultConfig()
      db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
      results = None
      with closing( db.cursor() ) as cursor:
        #self.logDebug("executeCommand", "Executing SQL command: %s" % statement)
        logging.debug("Executing SQL command: %s" % statement)
        results = cursor.execute(statement)
        db.commit()
      return results
    except Exception, e:
      #self.logDebug("executeCommand", "Error attempting to execute sql command: %s" % e)
      logging.exception("Error attempting to execute sql command: %s" % e)
      raise
    finally:
      db.close()

  def executeMultipleCommands(self, statementList):
    db = None
    try:
      AVCONFIG = AlienVaultConfig()
      db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
      db.autocommit(False)
      results = None
      with closing( db.cursor() ) as cursor:
        for statement in statementList:
          #self.logDebug("executeMultipleCommands", "Executing SQL command: %s" % statement)
          logging.debug("Executing SQL command: %s" % statement)
          results = cursor.execute(statement)
        db.commit()
      return results
    except Exception, e:
      #self.logDebug("executeMultipleCommands", "Error attempting to execute sql command: %s" % e)
      logging.exception("Error attempting to execute sql command: %s" % e)
      raise
    finally:
      try:
        db.close()
      except Exception, e:
        logging.exception("Error closing DB: %s" % (str(e)))
 

  def simpleQuery(self, statement):
    AVCONFIG = AlienVaultConfig()
    db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
    #db = MySQLdb.connect( self.DB_IP, self.DB_USER, self.DB_PASSWD, self.Database )
    cursor = db.cursor()
    cursor.execute(statement)
    results = cursor.fetchall()

    try:
      cursor.close()
    except Exception, e:
      logDebug("genericQuery", "Error attempting to close cursor: %s" % e)

    try:
      db.close()
    except Exception, e:
      self.logDebug("simpleQuery", "Error closing the database connection: %s" % e)
    return results

  def getEventCorrelationContextInHex(self, description):
    return str(self.simpleQuery("SELECT hex(event_ctx) from corr_engine_contexts where descr = '%s';" % description)[0][0])

  # Title gives an added accuracy bonus in the very unlikely event that the UUID matches.
  def getAlienVaultIncidentID(self, incident_uuid=None, title=None):
    if title is None and not incident_uuid is None:
      return str(self.simpleQuery("SELECT id from incident where uuid = UNHEX('%s') ORDER BY date DESC" % (incident_uuid))[0][0])
    elif not (title is None and incident_uuid is None):
      return str(self.simpleQuery("SELECT id from incident where uuid = UNHEX('%s') and title = '%s' ORDER BY date DESC" % (incident_uuid,title))[0][0])
    elif incident_uuid is None and not title is None:
      return str(self.simpleQuery("SELECT id from incident where title = '%s' ORDER BY date DESC" % (incident_uuid,title))[0][0])
    else:
      return "ERROR"


#  def createTicket(self, Title, Priority, Submitter = "admin", Status = "Open", Reference = "Alarm", src_ips = '0.0.0.0',src_ports = 'NA',dst_ips = '0.0.0.0',dst_ports = 'NA', backlog_id = '', event_id='', alarm_group_id=''):
#    incident_uuid = "%s" % uuid.uuid4()
#    incident_uuid = incident_uuid.replace('-', '')
#    MAIN_TICKET_STATEMENT = "INSERT INTO incident (uuid,ctx,title,date,ref,type_id,priority,status,last_update,in_charge,submitter,event_start,event_end) VALUES (UNHEX('%s'),UNHEX('%s'),'%s',utc_timestamp(),'%s','%s','%s','%s',utc_timestamp(),'%s','%s',utc_timestamp(),utc_timestamp());" % (incident_uuid,self.getEventCorrelationContextInHex("Default"), Title, 'Alarm', 'Sentinel Ticket', Priority, Status, 'Sentinel SOC', Submitter)
#    self.logDebug("createTicket","MAIN TICKET SQL STATEMENT: %s" % MAIN_TICKET_STATEMENT)
#    GET_TICKET_NUMBER_STATEMENT = "select @IncidentNumber := id FROM incident where uuid = UNHEX('%s') and title = '%s' ORDER BY date DESC LIMIT 1" % (incident_uuid,Title)
#    ALARM_TICKET_STATEMENT = "INSERT INTO incident_alarm (incident_id,src_ips,src_ports,dst_ips,dst_ports,backlog_id,event_id, alarm_group_id) VALUES (@IncidentNumber,'%s','%s','%s','%s',UNHEX('%s'),UNHEX('%s'),UNHEX('%s'));" % (src_ips,src_ports,dst_ips,dst_ports,backlog_id,event_id, alarm_group_id)
#    self.logDebug("createTicket","ALARM TICKET SQL STATEMENT: %s" % ALARM_TICKET_STATEMENT)
#    AVCONFIG = AlienVaultConfig()
#    db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
##    db = MySQLdb.connect( self.DB_IP, self.DB_USER, self.DB_PASSWD, self.Database )
#    db.autocommit(False)
#    cursor = db.cursor()
#    try:
#      cursor.execute(MAIN_TICKET_STATEMENT)
#      cursor.execute(GET_TICKET_NUMBER_STATEMENT)
#      cursor.execute(ALARM_TICKET_STATEMENT)
#      db.commit()
#      # Find and return the AlienVault ticket number that was just created.
#      return self.getAlienVaultIncidentID(incident_uuid=incident_uuid, title=Title)
#      db.close()
#    except:
#      db.rollback()


  def getRelatedOpenTickets(self, title):
#    sql = "select id, title, status, type_id, ref from incident where status <> 'Closed' and title = '%s';" % title
    sql = "select id AS TICKET_ID, title as TITLE, status as STATUS from incident where status <> 'Closed' and title = '%s';" % title
    results = self.genericQuery(sql)
    return results

  def isTicketAlreadyOpen(self, title):
    results = self.getRelatedOpenTickets(title = title)
    if len(results) > 0:
      logging.info("Ticket %s is open in AlienVault!" % title)
      #self.logDebug("isTicketAlreadyOpen", "Ticket %s is open in AlienVault!" % title)
      return True
    else:
      logging.info("Ticket %s is not open in AlienVault!" % title)
      #self.logDebug("isTicketAlreadyOpen", "Ticket %s is not open in AlienVault!" % title)
      return False

#### NEW STUFF
  def createTicket(self, AlarmObj):

    # Generatate a unique incident UUID
    incident_uuid = "%s" % uuid.uuid4()
    incident_uuid = incident_uuid.replace('-', '')

    # get highest alarmed event
    myAlarmEvent = AlarmObj.getHighestCorrelationLevelEvent()

    # get Correlation context
    ctx = myAlarmEvent.Context

    # Easier to read, set other common variables
    refType = 'Alarm'
    Status = 'Open'
    in_charge = 'Sentinel SOC'
    Submitter = 'admin'
    alarm_group_id = '' # DON'T EVEN KNOW WHAT THIS IS.
    Priority = '1'
    
    logging.info("AlienVault Ticket Creation, EXPAND: %s" % AlarmObj.getExpand())
    #self.logDebug("createTicket","AlienVault Ticket Creation, EXPAND: %s" % AlarmObj.getExpand())
    # Generate the SQL statement to insert
    #MAIN_TICKET_STATEMENT = "INSERT INTO incident (uuid,ctx,title,date,ref,type_id,priority,status,last_update,in_charge,submitter,event_start,event_end) VALUES (UNHEX('%s'),UNHEX('%s'),'%s',utc_timestamp(),'%s','%s','%s','%s',utc_timestamp(),'%s','%s',utc_timestamp(),utc_timestamp());" % (incident_uuid,ctx, myAlarmEvent.Name, refType,'Sentinel Ticket', AlarmObj.Risk, Status, in_charge, Submitter)
    MAIN_TICKET_STATEMENT = "INSERT INTO incident (uuid,ctx,title,date,ref,type_id,priority,status,last_update,in_charge,submitter,event_start,event_end) VALUES (UNHEX('%s'),UNHEX('%s'),'%s',utc_timestamp(),'%s','%s','%s','%s',utc_timestamp(),'%s','%s',utc_timestamp(),utc_timestamp());" % (incident_uuid,ctx, AlarmObj.getTicketTitleName(), refType,'Sentinel Ticket', AlarmObj.Risk, Status, in_charge, Submitter)
    #self.logDebug("createTicket","MAIN TICKET SQL STATEMENT: %s" % MAIN_TICKET_STATEMENT)
    logging.debug("MAIN TICKET SQL STATEMENT: %s" % MAIN_TICKET_STATEMENT)
    #GET_TICKET_NUMBER_STATEMENT = "select @IncidentNumber := id FROM incident where uuid = UNHEX('%s') and title = '%s' ORDER BY date DESC LIMIT 1" % (incident_uuid,myAlarmEvent.Name)
    GET_TICKET_NUMBER_STATEMENT = "select @IncidentNumber := id FROM incident where uuid = UNHEX('%s') and title = '%s' ORDER BY date DESC LIMIT 1" % (incident_uuid,AlarmObj.getTicketTitleName())
    #self.logDebug("createTicket","GET TICKET NUMBER SQL STATEMENT: %s" % GET_TICKET_NUMBER_STATEMENT)
    logging.debug("GET TICKET NUMBER SQL STATEMENT: %s" % GET_TICKET_NUMBER_STATEMENT)
    ALARM_TICKET_STATEMENT = "INSERT INTO incident_alarm (incident_id,src_ips,src_ports,dst_ips,dst_ports,backlog_id,event_id, alarm_group_id) VALUES (@IncidentNumber,'%s','%s','%s','%s',UNHEX('%s'),UNHEX('%s'),UNHEX('%s'));" % (AlarmObj.SourceIP,AlarmObj.SourcePort,AlarmObj.DestinationIP,AlarmObj.DestinationPort,AlarmObj.BacklogID,myAlarmEvent.EventID, alarm_group_id)
    #self.logDebug("createTicket","ALARM TICKET SQL STATEMENT: %s" % ALARM_TICKET_STATEMENT)
    logging.debug("ALARM TICKET SQL STATEMENT: %s" % ALARM_TICKET_STATEMENT)
    FIND_NEXT_DESCRIPTION_ID = "select if(@i=id,@i:=id+1,@i) from incident_ticket order by id;"
    
    # Load up the new SOC Customer Notification Procedure Engine for dynamic SCNP link creation and read configuration file
    # Next pass through the Alarms SRC and DST IP and URL to get a DOCID and replace [DOCID] if exists in the SCNP URL.
    mySCNPEng = scnpEngine()
    mySCNPEng.readConfigurationFile()
    SCNP = mySCNPEng.populateVariables(str(AlarmObj.SourceIP),str(AlarmObj.DestinationIP),self.TC.SCNP)
    #SCNP = self.TC.SCNP

    CA = self.TC.CA
    STI_EVENT_LINK=AlarmObj.getAlarmRefFullURL().replace("STI_NAT",str(self.TC.STI_NAT))
    print CA
    CA = CA.replace("[PID]",str(AlarmObj.PluginID))
    CA = CA.replace("[SID]",str(AlarmObj.PluginSID))
    print AlarmObj.getTicketDescription()
    description = str(AlarmObj.getTicketDescription()) + ", SCNP: " + SCNP + ", CA: " + CA + ", STI EVENT LINK: " + STI_EVENT_LINK
    TICKET_DESCRIPTION_STATEMENT = "INSERT INTO incident_ticket (id,incident_id, date, status, priority, users, description, action, in_charge) VALUES (@i,@IncidentNumber,utc_timestamp(),'%s','%s','%s','%s','%s','%s');" % (Status,Priority,Submitter,description,"",in_charge)
    self.logDebug("createTicket","TICKET DESCRIPTION SQL STATEMENT: %s" % TICKET_DESCRIPTION_STATEMENT)

    AVCONFIG = AlienVaultConfig()
    db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
#    db = MySQLdb.connect( self.DB_IP, self.DB_USER, self.DB_PASSWD, self.Database )
    db.autocommit(False)
    #cursor = db.cursor()
    try:
      with closing( db.cursor() ) as cursor:
        cursor.execute(MAIN_TICKET_STATEMENT)
        cursor.execute(GET_TICKET_NUMBER_STATEMENT)
        cursor.execute(ALARM_TICKET_STATEMENT)
        cursor.execute("set @i=0;")
        cursor.execute(FIND_NEXT_DESCRIPTION_ID)
        cursor.execute(TICKET_DESCRIPTION_STATEMENT)
        db.commit()
        # Find and return the AlienVault ticket number that was just created.
        #return self.getAlienVaultIncidentID(incident_uuid=incident_uuid, title=myAlarmEvent.Name)
        return self.getAlienVaultIncidentID(incident_uuid=incident_uuid, title=AlarmObj.getTicketTitleName())
#        try:
#          cursor.close()
#          db.close()
#        except:
#          self.logDebug("createTicket","Error closing cursor or db!")
#        finally:
    except Exception, e:
      #self.logDebug("createTicket",e)
      logging.exception("Unknown exception while attempting to create AlienVault ticket: %e" % (str(e)))
      db.rollback()
    finally:
      try:
        db.close()
      except Exception, e:
        logging.exception("Unknown exception while attempting to close AlienVault ticket DB: %e" % (str(e)))

  def getPushableTickets(self, includeManualTickets = False):
    sql = "select i.id as TICKET_ID, HEX(i.uuid) as UUID, i.title as TITLE, i.status as STATUS, it.description AS DESCRIPTION FROM incident as i, (select it.incident_id, min(date) as EARLY_DATE from incident_ticket as it group by it.incident_id) sq, incident_custom as ic, incident_ticket as it WHERE sq.EARLY_DATE = it.date and it.incident_id = sq.incident_id and it.incident_id = i.id and i.status <> 'Closed' and type_id = 'Sentinel Ticket' and i.date > NOW() - INTERVAL 10 DAY and i.id != ic.incident_id"

    if not includeManualTickets:
      sql += " and i.ref = 'Alarm'"
    sql += ";"
    results = self.genericQuery(sql)
    return results

  def AddLogToTicket(self,ticketNumber, description, action = "", status = "Open", users = "admin", inCharge = "Sentinel SOC", priority = "1"):
    sql = [ "set @i=0;",
            "select if(@i=id,@i:=id+1,@i) from incident_ticket order by id;",
            "INSERT INTO incident_ticket (id, incident_id, date, status, priority, users, description, action, in_charge) VALUES (@i, '%s',utc_timestamp(),'%s','%s','%s','%s','%s','%s');" % (ticketNumber,status,priority,users,description,action,inCharge)
          ]
    results = self.executeMultipleCommands(sql)
#    sql = "set @i=0; select if(@i=id,@i:=id+1,@i) from incident_ticket order by id; INSERT INTO incident_ticket (id, incident_id, date, status, priority, users, description, action, in_charge) VALUES (@i, '%s',utc_timestamp(),'%s','%s','%s','%s','%s','%s');" % (ticketNumber,status,priority,users,description,action,inCharge)
#    results = self.executeCommand(sql)
    return results
    

#  def updateTicketSQLString(self,ticketNumber, description, action = "", status = "Open", users = "admin", inCharge = "Sentinel SOC", priority = "1"):
#    sql = "INSERT INTO incident_ticket (incident_id, date, status, priority, users, description, action, in_charge) VALUES ('%s',utc_timestamp(),'%s','%s','%s','%s','%s','%s');" % (ticketNumber,status,priority,users,description,action,inCharge)
    
#    return str(sql)

  def getTicketDescription(self, TicketNumber):
    #sql = "select itmain.description as DESCRIPTION from incident_ticket as itmain, (select min(date) as date from incident_ticket where incident_id = " + str(TicketNumber) + ") as it where itmain.incident_id = " + str(TicketNumber) + " and itmain.date = it.date;"
    #self.logDebug("getTicketDescription", "Searching for Ticket Number %s in AlienVault DB." % TicketNumber)
    logging.info("Searching for Ticket Number %s in AlienVault DB." % TicketNumber)
    results = self.getTicket(TicketNumber = TicketNumber)
    #print "\n\n\nDESCRIPTION RESULTS: ", results[0]['DESCRIPTION']
    return results[0]['DESCRIPTION']

  # This function will get an AlienVault Ticket with the initial description. 2-17-17 Mod by Sid
  def getTicket(self, TicketNumber):
    #PATCHED CODE (BWEN):
    sql = "select i.id as TICKET_ID, HEX(i.uuid) as UUID, i.title as TITLE, i.status as STATUS, sq.description AS DESCRIPTION from incident as i, (select incident_id, description, date from incident_ticket where incident_id = %s order by date ASC LIMIT 1) as sq where i.id = sq.incident_id;" % (TicketNumber)
    
    # OLD CODE:
    #sql = "select i.id as TICKET_ID, HEX(i.uuid) as UUID, i.title as TITLE, i.status as STATUS, it.description AS DESCRIPTION FROM incident as i, (select it.incident_id, min(date) as EARLY_DATE from incident_ticket as it where it.incident_id = %s group by it.incident_id) sq, incident_custom as ic, incident_ticket as it WHERE sq.EARLY_DATE = it.date and it.incident_id = sq.incident_id and it.incident_id = i.id and i.id != ic.incident_id and i.id = %s;" % (TicketNumber,TicketNumber)
    logging.debug("Ticket Number %s SQL CODE: %s." % (TicketNumber,sql))
    results = self.genericQuery(sql)
    if len(results) > 0:
      return results
    else:
      raise TicketNotFoundException("Ticket Number %s was not found or could not be retrieved." % TicketNumber)

  def addRemoteTicketSystemInformation(self, AlienVaultTicketNumber, STITicketNumber):
    sql = "INSERT INTO incident_custom (id, incident_id, name, incident_custom_type_id, content) VALUES (null, '%s', 'STI_TICKET', 'Textbox', '%s');" % (str(AlienVaultTicketNumber),str(STITicketNumber))
    results = self.executeCommand(sql)
    return results


  def getAlienVaultTicketsNotClosedInSTI(self):
    sql = "select i.id as TICKET_ID, i.status AS STATUS, i.type_id as TYPE_ID, i.ref as REF, ic.content as STI_TICKET from incident as i RIGHT JOIN (select incident_id, content from incident_custom where name = 'STI_TICKET') AS ic ON i.id = ic.incident_id LEFT OUTER JOIN (select incident_id from incident_tag where tag_id = '%s') AS itag ON i.id = itag.incident_id where i.type_id = 'Sentinel Ticket' and i.ref = 'Alarm' and itag.incident_id is NULL;" % (str(AlienVaultTicketSystem.SentinelClosedTicketTag))
    #self.logDebug("getAlienVaultTicketsNotClosedInSTI", "Getting tickets in AlienVault that are not closed in Universe. SQL CODE: %s." % (sql))
    logging.debug("Getting tickets in AlienVault that are not closed in Universe. SQL CODE: %s." % (sql))
    results = self.genericQuery(sql)
    return results

  def getAlienVaultTicketMD5(self, AlienVaultTicketNumber):
    sql = "select md5(it.description) as MD5, it.description as DESCRIPTION from incident_ticket as it where it.incident_id = '%s';" % AlienVaultTicketNumber
    #self.logDebug("getAlienVaultTicketMD5", "Getting MD5 hashes of entries in AlienVault. SQL CODE: %s." % (sql))
    results = self.genericQuery(sql)
    return results

  def closeTicket(self, AlienVaultTicketNumber, notes = ""):
    notes = str(MySQLdb.escape_string(notes))
    sql = [ "set @i=0;",
            "select if(@i=id,@i:=id+1,@i) from incident_ticket order by id;",
            "INSERT INTO incident_ticket (id, incident_id, date, status, priority, users, description, action, in_charge) VALUES (@i, '%s',utc_timestamp(),'Closed','1','Sentinel SOC','%s','','Sentinel SOC');" % (AlienVaultTicketNumber,notes),
            "UPDATE incident SET status = 'Closed' where id = '%s';" % (AlienVaultTicketNumber),
            "INSERT INTO incident_tag (incident_id, tag_id) VALUES ('%s','%s');" % (AlienVaultTicketNumber,str(AlienVaultTicketSystem.SentinelClosedTicketTag))
          ]
    results = self.executeMultipleCommands(sql)
    return results
#x = AlienVaultTicketSystem()
#print x.createTicket("Test Sentinel Ticket with Alarm","1")
