import datetime
import hashlib
import re

import TicketSystemGlobal as tsg

class TicketNoteEntry(object):

  def __init__(self, rawData):
    # Removes unicode characters that may not be printable causing an error.
    rawData = rawData.encode('utf-8')
    # Split of the time and name from the description
    splitter = rawData.split('</B><br/ >')
    self.description = re.sub(r"(<br/ >)+$","",splitter[1])
    
    # Further split up the time and name.
    #self.f = "%m/%d/%Y %I:%M%p %Z"
    self.f = "%m/%d/%Y %I:%M%p"
    splitter = splitter[0].split(' ')
    #self.timestamp = datetime.datetime.strptime("%s %s CST" % (splitter[0],splitter[1]),self.f)
    self.timestamp = tsg.getTicketSystemDateTimeTZObj("%s %s" % (splitter[0],splitter[1]),self.f)

    # Piece back together the name.
    self.owner = ""
    for index in range(2,len(splitter)):
      self.owner = self.owner + " " + splitter[index]
    self.owner = self.owner.lstrip()

  def getTimestamp(self):
    return self.timestamp

  def getNote(self):
    return self.description

  def getOwner(self):
    return self.owner

  def __str__(self):
    return "%s (%s) - %s" % (str(self.timestamp),self.owner,self.description)

  def getHash(self):
    m = hashlib.md5()
    #m.update(self.__str__())
    print "perform md5 on: %s" % self.description
    m.update(self.description)
    return m.hexdigest()
