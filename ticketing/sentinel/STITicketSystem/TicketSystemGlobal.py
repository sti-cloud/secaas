# AlienVault hides some of the packages and certain packages here are in this path.
import sys
import re
sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
from datetime import datetime
from dateutil import tz

def getTicketSystemTimeZone():
  return "America/Chicago"

# Date/time string should be in the format: '%m/%d/%Y %I:%M%p' example: '01/01/2011 10:05pm'.
# This method returns a timezone aware datetime object from a datetime string.
def getTicketSystemDateTimeTZObj(dateTimeString, dateTimeStringFormat = "%m/%d/%Y %I:%M%p"):
  tctz = tz.gettz(getTicketSystemTimeZone())
  mydtobj = getTicketSystemDateTimeObj(dateTimeString,dateTimeStringFormat)
  mydtobj = mydtobj.replace(tzinfo=tctz)
  return mydtobj

# This method puts the datetime string into a datetime object.  It isn't necessarily timezone aware or correctly
# timezone aware.
def getTicketSystemDateTimeObj(dateTimeString, dateTimeStringFormat = "%m/%d/%Y %I:%M%p"):
  myTimestamp = datetime.strptime(str(dateTimeString),dateTimeStringFormat)
  return myTimestamp

# Return serial number
def getSerial():
  try:
    lines = None
    with open('/etc/serial.txt', "r") as f:
      lines = f.readlines()
    for line in lines:
      line = line.strip()
      if re.match(r'^[0-9a-fA-F]+$', line):
        return "SECAV" + line
        break
    return "SECAV000000000000"
  except Exception:
    return "SECAV000000000000"
