import sys
import datetime
# Required for AlienVault package imports
sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
import requests

from Ticket import Ticket

from AlienVaultLicense import AlienVaultLicense
from TicketConfig import TicketConfig
import TicketSystemGlobal as tsg
import logging
class STITicketSystem:

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("STITicketSystem(%s): %s" % (method,log_entry))

  def __init__(self, server, port, clientid, apikey, debug = False, custNum = "00000", urlPrefix = "https://"):
    self.DEBUG = debug
    self.server = server
    #self.query_server = "uws.sentinel.com"
    self.query_server = server
    self.query_server_port = port
    #self.query_server_port = "8084"
    self.port = port
    self.clientID = clientid
    self.apiKey = apikey
    self.urlPrefix = urlPrefix
    #self.urlPrefix = "http://"
    self.setCustomerNumber(custNum)
    self.AlienVaultLic = tsg.getSerial()
    self.TC = TicketConfig()
#    self.custNum = "00000"

  def setCustomerNumber(self, customernumber):
    self.custNum = str(customernumber)

  def createTicket(self, jsonData):
    headers = {'Content-Type': 'application/json', 'User-Agent': 'STI Ticket System API', 'Accept': 'application/json' }
    sub_uri = self.TC.CREATE_TICKET_URI
    #self.logDebug("createTicket", "Making post request to %s%s:%s/%s" % (self.urlPrefix, self.server, self.port, sub_uri))
    logging.info("Making post request to %s%s:%s/%s" % (self.urlPrefix, self.server, self.port, sub_uri))
    logging.debug("POST DATA: %s" % (str(jsonData)))
    #self.logDebug("createTicket", "POST DATA: %s" % (str(jsonData)))
    r = requests.post("%s%s:%s/%s" % (self.urlPrefix, self.server, self.port, sub_uri), json=str(jsonData), auth=(self.clientID,self.apiKey), headers=headers)

    #self.logDebug("createTicket", "RETURN: " + str(r.json()))
    try:
      logging.info("RETURN: " + str(r.json()))
    except:
      logging.exception("JSON Exception in displaying json return. Attempting text version: %s" % (str(r.text)))
    return r.json()

  def createTicketHelper(self, Priority = "I1", TicketTitle = "Test Security Ticket", TicketDescription = "Test Security Description", AlienVaultTicketNumber = "NA"):
    #Some Universe Quirks....
    #Universe only allows CPS at the moment.  AlienVault
    jsonData = {
      "CUSTOMER": str(self.TC.CUSTOMER_NUMBER) + "-" + str(self.TC.CUSTOMER_SITEID),
      "CONTACT_NAME": str(self.TC.CUSTOMER_CONTACT_NAME),
      "CONTACT_PHONE": str(self.TC.CUSTOMER_CONTACT_PHONE),
      "CONTACT_EMAIL": str(self.TC.CUSTOMER_CONTACT_EMAIL),
      "PRIORITY": Priority,
      "SERIAL_NUMBER": self.AlienVaultLic,
      "AV_AIO": self.AlienVaultLic,
      "CALL_TYPE":"Security Incident",
      "EMAIL": "seaton@sentinel.com",
      "TITLE":TicketTitle,
      "EQUIPMENT_TYPE":"AlienVault",
      "DESCRIPTION": str(TicketDescription)
    }
    return self.createTicket(jsonData)



  def addNote(self, callNumber, note):
    headers = {'Content-Type': 'application/json', 'User-Agent': 'STI Ticket System API', 'Accept': 'application/json' }
    #headers = {'Content-Type': 'application/json', 'User-Agent': 'STI Ticket System API' }
    sub_uri = "uws/api/addnote"
    logging.info("Adding note to CallNumber %s with URL %s%s:%s/%s" % (callNumber, self.urlPrefix, self.server, self.port, sub_uri))
    #self.logDebug("addNote", "Adding note to CallNumber %s with URL %s%s:%s/%s" % (callNumber, self.urlPrefix, self.server, self.port, sub_uri))
    dataString = {
      "customer": str(self.custNum),
      "callNumber": str(callNumber),
      "note": str(note)
    }
#    print str(dataString)
    # For some reason if I put json=str(dataString) there is no exception here, but then the post doesn't work.  If I don't add the str the post works
    # but throughs a JSON decode error.  For now ignore the error and revisit later.
    try:
      r = requests.post("%s%s:%s/%s" % (self.urlPrefix, self.server, self.port, sub_uri), json=dataString, auth=(self.clientID,self.apiKey), headers=headers)
      return True
    except Exception:
      logging.exception("RETURN: " + str(r.json()))
      #self.logDebug("addNote", "RETURN: " + str(r.json()))
      return r.json()

  def getCallURL(self, callNumber):
    url = str(self.TC.QUERY_TICKET_URI)
    try:
        url = url.replace("[CALL_NUMBER]", str(callNumber))
        logging.debug("Composing call request URL: %s" % url)
    except Exception, e:
        logging.exception("Error in composing call retrieval URL: %s" % (str(e)))
    return str(url)
    

  def getCall(self, callNumber):
    headers = {'Content-Type': 'application/json', 'User-Agent': 'STI Ticket System API', 'Accept': 'application/json' }
    sub_uri = self.getCallURL(callNumber)
    #sub_uri = "uws/api/UniverseCallDetails/%s" % callNumber
    #self.logDebug("getCall", "Retrieving call details for call #%s with URL %s%s:%s/%s" % (callNumber, self.urlPrefix, self.server, self.port, sub_uri))
    logging.info("Retrieving call details for call #%s with URL %s%s:%s/%s" % (callNumber, self.urlPrefix, self.query_server, self.query_server_port, sub_uri))
    #r = requests.get("%s%s:%s/%s" % (self.urlPrefix, self.server, self.port, sub_uri), auth=(self.clientID,self.apiKey), headers=headers)
    r = requests.get("%s%s:%s/%s" % (self.urlPrefix, self.query_server, self.query_server_port, sub_uri), auth=(self.clientID,self.apiKey), headers=headers)
    #print r.json()
    return Ticket(r.json())

  def getCallRaw(self, callNumber):
    headers = {'Content-Type': 'application/json', 'User-Agent': 'STI Ticket System API', 'Accept': 'application/json' }
    sub_uri = self.getCallURL(callNumber)
    #sub_uri = "uws/api/UniverseCallDetails/%s" % callNumber
    #self.logDebug("getCall", "Retrieving call details for call #%s with URL %s%s:%s/%s" % (callNumber, self.urlPrefix, self.server, self.port, sub_uri))
    logging.info("Retrieving call details for call #%s with URL %s%s:%s/%s" % (callNumber, self.urlPrefix, self.query_server, self.query_server_port, sub_uri))
    r = requests.get("%s%s:%s/%s" % (self.urlPrefix, self.query_server, self.query_server_port, sub_uri), auth=(self.clientID,self.apiKey), headers=headers)
    #r = requests.get("%s%s:%s/%s" % (self.urlPrefix, "uws.sentinel.com", "8084", sub_uri), auth=(self.clientID,self.apiKey), headers=headers)
    #print r.json()
    return r.text
