from TicketNoteEntry import TicketNoteEntry
import logging

class Ticket(object):
  def __init__(self, jsonData):
    logging.debug("Ticket(__init__): Ticket Retrieval JSON: %s" % (jsonData))
    self.Number = jsonData['Call Number']
    self.Status = jsonData['Call Status']
    self.Description = jsonData['Problem Description']

    # The following will 
    self.Notes = self.parseTicketNotes(jsonData['Call Notes'])
    self.rawData = jsonData

  def getRaw(self):
    return self.rawData

  def getTicketNumber(self):
    return str(self.Number)

  def getStatus(self):
    return str(self.Status)

  def isClosed(self):
    if self.Status.lower() == "closed" or self.Status.lower() == "close":
      return True
    else:
      return False

  def getNotes(self):
    return self.Notes

  # Returns the json raw http request Call Notes and encodes as utf-8 rather than standard ASCII.
  def getCloseNotes(self):
    return self.rawData['Call Notes'].encode('utf-8')

  def getDescription(self):
    return self.Description

  def parseTicketNotes(self, notes):
    split_text = '<B>'
    notesplit = notes.split(split_text)
    for note in notesplit:
      if not note:
        notesplit.remove(note)
    for index in range(len(notesplit)):
      notesplit[index] = TicketNoteEntry(notesplit[index].rstrip().lstrip())
#    for note in notesplit:
#      print "ELEMENT: %s" % note
    return notesplit
    

  

  def __str__(self):
    return str(self.getRaw())
