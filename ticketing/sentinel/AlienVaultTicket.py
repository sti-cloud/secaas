class AlienVaultTicket(object):
  def __init__(self, AlienVaultTicketNumber, STITicketNumber = None, TypeID = None, Ref = None):
    self.AVTicketNumber = AlienVaultTicketNumber
    self.STITicketNumber = STITicketNumber
    self.TypeID = TypeID
    self.Ref = Ref

  def getAlienVaultTicketNumber(self):
    return self.AVTicketNumber

  def getSTITicketNumber(self):
    return self.STITicketNumber

  def getType(self):
    return self.TypeID

  def getRef(self):
    return self.Ref
