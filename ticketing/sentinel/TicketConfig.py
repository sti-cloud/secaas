#!/usr/bin/python

from datetime import datetime, timedelta
import ConfigParser
import logging
import time
import os

class TicketConfig:

  def __init__(self, configurationFile=os.path.dirname(os.path.realpath(__file__)) + '/sti-ticket-config.cfg', debug = False):
    self.configFile = configurationFile
    self.DEBUG = debug
    self.config = ConfigParser.RawConfigParser()
    #self.Path = os.path.dirname(os.path.realpath(__file__))
    #self.config.read(self.Path + "/" + configurationFile)
    #self.logDebug("__init__","Now opening configuration file: %s" % configurationFile)
    self.config.read(configurationFile)

    # Order here matters, this creates a cached instance of the mapping to minimize rebuilt but allow
    # for reconfigurations and setting adjustments without restarting the program over time.
    self.LastCachedRefreshDateTime = None
    self.CacheRefreshTimeInSeconds = 60 * 20
    self.CachedRiskMappingDatabase = {}
    self.CachedRiskMappingDatabase = self.getRiskMappings()

  def readConfigurationFile(self):
    logging.debug("Now opening configuration file: %s" % self.configFile)
    self.config.read(self.configFile)

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("%s(%s): %s" % (self.__class__,method,log_entry))

  def constant(f):
    def fset(self, value):
        raise TypeError
    def fget(self):
        location = f()
        self.logDebug("Property","Searching in %s, for value in %s." % (location["SECTION"],location["KEY"]))
        if self.config.has_option(location["SECTION"],location["KEY"]):
          return str(self.config.get(location["SECTION"],location["KEY"]))
        else:
          return str(location["DEFAULT"])
    return property(fget, fset)



  def url_composition_helper(self, KEY, SECTION = 'CUSTOMER_INFO'):
    url = "NO-URL-SPECIFIED"
    if self.config.has_option(SECTION,KEY):
      url = str(self.config.get(SECTION,KEY))
      try:
        url = url.replace("[CUSTOMER_NUMBER]", str(self.CUSTOMER_NUMBER))
        self.logDebug("url_composition_helper","SECTION: %s, KEY: %s, VALUE: %s." % (SECTION,KEY,url))
      except Exception, e:
        self.logDebug("url_composition_helper","Error trying to compose URL in %s, for value in %s." % (SECTION,KEY))
        url = url.replace("[CUSTOMER_NUMBER]", "00000")
    return url

  def SCNP_getter(self):
    return self.url_composition_helper(KEY='SCNP')

  def CA_getter(self):
    return self.url_composition_helper(KEY='CA')

  def raiseError(self, value):
    raise TypeError

  def RiskToTicketMapping(f):
    def fset(self, value):
        raise TypeError
    def fget(self):
        # The config file format:
        # RISK_1_TICKETS=plugin_id:plugin_sid(-plugin_sid_end),plugin_sid
        # ; = plugin_id separator
        # , = plugin_sid separator (so that plugin_id doesn't have to be specified multiple times,
        #     even though that is valid as well.)
        # - = plugin_sid range (all numbers in between range are included)
        location = f()
        self.logDebug("Property","Searching in %s, for value in %s." % (location["SECTION"],location["KEY"]))
        if self.config.has_option(location["SECTION"],location["KEY"]):
          mapping = { }
          element_list = str(self.config.get(location["SECTION"],location["KEY"])).split(";")
          
          for element in element_list:

            # Find out if extended options are specified or not with the plus (+) sign.  Quick n dirty way to add customizing.
            option_dict = {}
            if "+" in element:
              option_dict = self.parseOptionString(element.split("+")[1])
              element = element.split("+")[0]
            else:
              option_dict = self.parseOptionString("")
            
            self.logDebug("RiskToTicketMapping","KEY %s - Now parsing element %s." % (location["KEY"], element))
            try:
              plugin_id = element.split(":")[0]

              # Get sids associated with plugin_id entry.
              subelement_list = element.split(":")[1].split(",")
  
              # Check to see if we are creating new or adding more sids to a plugin map.  This allows us to
              # specify a plugin_id more than one per line in the config file.
              if not plugin_id in mapping:
                #mapping[plugin_id] = []
                mapping[plugin_id] = {}

              for subelement in subelement_list:
                self.logDebug("RiskToTicketMapping","KEY %s - Now parsing sub element %s." % (location["KEY"], subelement))
                # Allows ranges and not ranges.
                if "-" in subelement:
                  plugin_sid_start = subelement.split("-")[0]
                  plugin_sid_end = subelement.split("-")[1]
                  for x in xrange(int(plugin_sid_start),int(plugin_sid_end)+1):
                    if plugin_id in mapping:
                      mapping[plugin_id][str(x)] = option_dict
                else:
                  if plugin_id in mapping:
                    mapping[plugin_id][str(subelement)] = option_dict
            except Exception, e:
              self.logDebug("RiskToTicketMapping","ERROR %s." % (e))
            
              return location["DEFAULT"]
          self.logDebug("RiskToTicketMapping","KEY %s - RAW DICTIONARY: %s." % (location["KEY"], mapping))
          return mapping
        else:
          return location["DEFAULT"]
    return property(fget, fset)

  def parseOptionString(self, optionStr):
    isDyanmic = False
    isExpanded = False
    opt_array = []
    try:
      if "&" in optionStr:
        opt_array = optionStr.split("&")
      else:
        opt_array.append(optionStr)

      for option in opt_array:
        if "D" in option[:1]:
          isDyanmic = True
        elif "E" in option[:1]:
          isExpanded = True
    except:
      pass
    finally:
      return { "DYNAMIC": isDyanmic, "EXPAND": isExpanded }



  @constant
  def STI_TICKET_API_DEBUG():
        return { "SECTION": "STI", "KEY":"DEBUG","DEFAULT":"True"}

  @constant
  def APIKEY():
        return { "SECTION": "STI", "KEY":"APIKEY","DEFAULT":""}

  @constant
  def CLIENTID():
        return { "SECTION": "STI", "KEY":"CLIENTID","DEFAULT":""}

  @constant
  def SERVER():
        return { "SECTION": "STI", "KEY":"SERVER","DEFAULT":"devweb1.sentinel.com"}

  @constant
  def PORT():
        return { "SECTION": "STI", "KEY":"PORT","DEFAULT":"8084"}

  #@constant
  #def QUERY_TICKET_PORT():
  #      return { "SECTION": "STI", "KEY":"QUERY_TICKET_PORT","DEFAULT": constant(PORT) }

  #@constant
  #def QUERY_TICKET_SERVER():
  #      return { "SECTION": "STI", "KEY":"QUERY_TICKET_SERVER","DEFAULT": property(SERVER) }

  @constant
  def URL_PREFIX():
        return { "SECTION": "STI", "KEY":"PREFIX","DEFAULT":"http://"}

  @constant
  def CREATE_TICKET_URI():
        return { "SECTION": "STI", "KEY":"CREATE_TICKET_URI","DEFAULT":"/uws/api/call"}

  @constant
  def QUERY_TICKET_URI():
        return { "SECTION": "STI", "KEY":"QUERY_TICKET_URI","DEFAULT":"/uws/api/UniverseCallDetails/[CALL_NUMBER]"}

  @constant
  def CUSTOMER_NUMBER():
        return { "SECTION": "CUSTOMER_INFO", "KEY":"CUSTOMER_NUMBER","DEFAULT":"00000"}

  @constant
  def CUSTOMER_SITEID():
        return { "SECTION": "CUSTOMER_INFO", "KEY":"CUSTOMER_SITEID","DEFAULT":"00"}

  @constant
  def CUSTOMER_CONTACT_NAME():
        return { "SECTION": "CUSTOMER_INFO", "KEY":"CONTACT_NAME","DEFAULT":"Sentinel SOC"}

  @constant
  def CUSTOMER_CONTACT_EMAIL():
        return { "SECTION": "CUSTOMER_INFO", "KEY":"CONTACT_EMAIL","DEFAULT":"SOC@SENTINEL.COM"}

  @constant
  def CUSTOMER_CONTACT_PHONE():
        return { "SECTION": "CUSTOMER_INFO", "KEY":"CONTACT_PHONE","DEFAULT":"000-000-0000"}


  SCNP = property(SCNP_getter,raiseError)
  CA = property(CA_getter,raiseError)

  @constant
  def STI_NAT():
        return { "SECTION": "CUSTOMER_INFO", "KEY":"STI_NAT","DEFAULT":"127.0.0.1"}
  
  @constant
  def STI_TICKET_CREATION_CHECK_INTERVAL():
        return { "SECTION": "THREAD", "KEY":"STI_TICKET_CREATION_CHECK_INTERVAL","DEFAULT":"10"}

  @constant
  def ENABLE_STI_TICKET_CREATION_THREAD():
        return { "SECTION": "THREAD", "KEY":"ENABLE_STI_TICKET_CREATION_THREAD","DEFAULT":"False"}

  @constant
  def ALARM_CHECK_INTERVAL():
        return { "SECTION": "THREAD", "KEY":"ALARM_CHECK_INTERVAL","DEFAULT":"10"}

  @constant
  def STI_TICKET_QUERY_CHECK_INTERVAL():
        return { "SECTION": "THREAD", "KEY":"STI_TICKET_QUERY_CHECK_INTERVAL","DEFAULT":"10"}


  @RiskToTicketMapping
  def RISK_1_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_1_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_2_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_2_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_3_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_3_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_4_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_4_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_5_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_5_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_6_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_6_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_7_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_7_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_8_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_8_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_9_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_9_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def RISK_10_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"RISK_10_TICKETS", "DEFAULT": {}}

  @RiskToTicketMapping
  def DEFAULT_RISK_LEVEL_TICKET():
        return { "SECTION": "TICKET_ENGINE", "KEY":"DEFAULT_RISK_LEVEL_TICKET", "DEFAULT": "3" }

  @RiskToTicketMapping
  def NO_TICKETS():
        return { "SECTION": "TICKET_ENGINE", "KEY":"NO_TICKETS", "DEFAULT": {}}


  # This method should be called to get the RiskToTicketMapping configurations.  It contains a dictionary of
  # risks where their values then reference a dictionary of plugin_ids, who then reference a list of plugin_sids.
  def getRiskMappings(self):
    # This function is designed to implement a simple caching scheme that allows for a tradeoff of realtime
    # configuration adjustments in the config file with decreasing disk IO.  Caching the values solely in
    # memory would result in ticket config changes taking place only when the script restarts.
    if self.LastCachedRefreshDateTime is None:
      self.LastCachedRefreshDateTime = datetime.utcnow() - timedelta(seconds=(self.CacheRefreshTimeInSeconds+1))
      self.logDebug("getRiskMappings","Last cache time was none, now %s" % self.LastCachedRefreshDateTime)

    if (datetime.utcnow() - self.LastCachedRefreshDateTime).total_seconds() >= self.CacheRefreshTimeInSeconds:
      self.logDebug("getRiskMappings","Cache is old, last time was %s." % self.LastCachedRefreshDateTime)
      self.readConfigurationFile() # Just added by Sidney
      # A temp dictionary is used here to build the values, just in case (though not anticipated) that threading
      # may have consequences later on, this would minimize the operational time of the cache being incomplete
      # while concurrently being accessed.
      self.CachedRiskMappingDatabase = {
        "NO TICKETS": self.NO_TICKETS,
        "1": self.RISK_1_TICKETS,
        "2": self.RISK_2_TICKETS,
        "3": self.RISK_3_TICKETS,
        "4": self.RISK_4_TICKETS,
        "5": self.RISK_5_TICKETS,
        "6": self.RISK_6_TICKETS,
        "7": self.RISK_7_TICKETS,
        "8": self.RISK_8_TICKETS,
        "9": self.RISK_9_TICKETS,
        "10": self.RISK_10_TICKETS,
        "DEFAULT": self.DEFAULT_RISK_LEVEL_TICKET
      }
      self.LastCachedRefreshDateTime = datetime.utcnow()
    return self.CachedRiskMappingDatabase
      
      





#x = TicketConfig(debug = True)
#print x.QUERY_TICKET_URI
#print x.QUERY_TICKET_PORT
#print x.QUERY_TICKET_SERVER
#print x.SCNP
#print x.CA
#print x.RISK_1_TICKETS
#print x.getRiskMappings()
#time.sleep(10)
#print x.getRiskMappings()
