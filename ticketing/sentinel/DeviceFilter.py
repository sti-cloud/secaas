#!/usr/bin/python

from datetime import datetime, timedelta
from netaddr import IPNetwork, IPAddress
import logging
import os

#import time # is used for testing this module locally.
class DeviceFilter(object):
  def __init__(self, configurationFile=os.path.dirname(os.path.realpath(__file__)) + '/sti-device-config.cfg', debug = False):
    self.configFile = configurationFile
    self.DEBUG = debug
    self.AuthorizedDevices = ()

    # Order here matters, this creates a cached instance of the mapping to minimize rebuilt but allow
    # for reconfigurations and setting adjustments without restarting the program over time.
    self.LastCachedRefreshDateTime = None
    self.CacheRefreshTimeInSeconds = 60 * 20
    #self.CacheRefreshTimeInSeconds = 5 # For testing caching

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("%s(%s): %s" % (self.__class__,method,log_entry))

  def read(self):
    self.logDebug("read","Attempting to read file: %s" % self.configFile)
    with open(self.configFile, 'r') as fdata:
      try: 
        self.AuthorizedDevices = list(map(str.strip, fdata.readlines()))
        # The following for code is a quick n dirty way to remove comments from a file, to speed up processing.  It a pound sign
        # will void the line regardless of where it exists in the line.
        for item in self.AuthorizedDevices:
          if "#" in item or ";" in item:
            self.AuthorizedDevices.remove(item)
        self.logDebug("read","Configuration file read, values: %s" % self.AuthorizedDevices)
        return True
      except Exception:
        return False
    return False

  def getAuthorizedDevices(self):
    # This function is to implement a primitive caching function on authorized devices to cut tickets.  This allows the cache
    # to update without stopping the program and reduces the the disk IO by not reading from the disk every time this function
    # is called.
    if self.LastCachedRefreshDateTime is None:
      self.LastCachedRefreshDateTime = datetime.utcnow() - timedelta(seconds=(self.CacheRefreshTimeInSeconds+1))
      self.logDebug("getAuthorizedDevices","Last cache time was none, now %s" % self.LastCachedRefreshDateTime)

    if (datetime.utcnow() - self.LastCachedRefreshDateTime).total_seconds() >= self.CacheRefreshTimeInSeconds:
      self.logDebug("getAuthorizedDevices","Cache is old, last time was %s." % self.LastCachedRefreshDateTime)
      if self.read():
        self.LastCachedRefreshDateTime = datetime.utcnow()
    return self.AuthorizedDevices

  # To include all devices in a config make sure that you use 0.0.0.0/0 which should match everything.
  def isLicensed(self, srcip, dstip):
    for entry in self.getAuthorizedDevices():
      try:
        if (IPAddress(srcip) in IPNetwork(entry)) or (IPAddress(dstip) in IPNetwork(entry)):
          self.logDebug("isLicensed","%s or %s are in %s." % (srcip,dstip,entry))
          return True
      except Exception:
        pass
    self.logDebug("isLicensed","%s or %s did not return a match." % (srcip,dstip))
    return False



#x = DeviceFilter(debug = True)
#print x.isLicensed("172.16.58.23","192.168.10.32")
#print x.isLicensed("192.168.10.32","172.32.23.23")
#print x.isLicensed("172.16.58.23","192.169.10.32")
#time.sleep(10)
#print x.isLicensed("0.0.0.0","0.0.0.0")
