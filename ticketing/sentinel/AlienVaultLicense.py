import ConfigParser
from io import StringIO


class AlienVaultLicense:

  def __init__(self, OSSIM_LICENSE_FILE='/etc/ossim/ossim.lic'):
    self.OSSIM_FILENAME = OSSIM_LICENSE_FILE
    config_parser = ConfigParser.ConfigParser()
    vfile = StringIO(u'[misc]\n%s'  % open(self.OSSIM_FILENAME).read())
    config_parser.readfp(vfile)

    try:
      self.KEY=str(config_parser.get("appliance", "key"))
    except:
      print "Error getting AlienVault License Key"

    try:
      self.SYSTEMID=str(config_parser.get("appliance", "system_id"))
    except:
      print "Error getting AlienVault System ID"

  def fset(self, value):
    return TypeError
  def fget(self):
    return self.__attr__

  def getSerial(self):
    return self.KEY

  KEY = property(fget,fset)
  SYSTEMID = property(fget,fset)
