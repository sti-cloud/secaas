#!/usr/bin/python
from TicketConfig import TicketConfig
from STITicketSystem import STITicketSystem
import logging
import logging.handlers
import datetime
import time

config = TicketConfig()
LOG_FILENAME="/var/log/STITicketSystem-diagnostic.log"
LOG_LEVEL = logging.INFO

def log_setup():
    log_handler = logging.handlers.WatchedFileHandler(LOG_FILENAME)
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)s %(module)s(%(funcName)s) [%(process)d-%(thread)d]: %(message)s',
        '%b %d %H:%M:%S')
    formatter.converter = time.gmtime  # if you want UTC time
    log_handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)

def create_test_ticket():
    try:
      STITS = STITicketSystem(server = config.SERVER, port = config.PORT, clientid = config.CLIENTID, apikey = config.APIKEY, debug = True, custNum = config.CUSTOMER_NUMBER, urlPrefix = config.URL_PREFIX)
      jsondata = STITS.createTicketHelper(
            TicketTitle = str("***SOC CUSTOMER TEST TICKET***"),
            TicketDescription = "This is a SOC test ticket, generally used to verify customer turnup or connectivity.  DO NOT CALL CUSTOMER. If you get this ticket please send email notification to Joe Aurelius, Matt Weiss, Chris Yacoumakis and SEC-OPS@sentinel.com, then close. FOR QUESTIONS, CONTACT JOE AURELIUS, MATT WEISS, OR CHRIS YACOUMAKIS",
            AlienVaultTicketNumber = "TEST"
          )
    except:
      pass

    ReturnedStatus = "FAIL"

    try:
      ReturnedStatus = str(jsondata['Status'].encode('utf-8')).upper()
    except:
      try:
        ReturnedStatus = str(jsondata['Error Description'].encode('utf-8')).upper()
      except:
        pass
    logging.info("Creating test ticket....%s" % (str(ReturnedStatus)))



log_setup()
logging.info("Starting ticket system diagnostic tests...")
create_test_ticket()