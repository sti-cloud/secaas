#!/usr/bin/python
import datetime
import re
class AlarmDetailObject:
  def __init__(self,SingleAlarmDetailDict):
    self.rawSingleAlarmDetailDict = SingleAlarmDetailDict  
  def AlarmDetailProperty(f):
    def fset(self, value):
        raise TypeError
    def fget(self):
        COLUMN_DICT = f()
        try:
          return self.rawSingleAlarmDetailDict[COLUMN_DICT["KEY"]]
        except:
          return self.rawSingleAlarmDetailDict[COLUMN_DICT["DEFAULT"]]
    return property(fget, fset)

  def AlarmDetailPropertyBoolean(f):
    def fset(self, value):
        raise TypeError
    def fget(self):
        COLUMN_NAME = f()
        try:
          if int(self.rawSingleAlarmDetailDict[COLUMN_NAME]) > 0:
            return True
          else:
            return False
        except:
          return False
    return property(fget, fset)

  @AlarmDetailProperty
  def RuleLevel():
        return { "KEY": "RULE_LEVEL", "DEFAULT": "0" }

  @AlarmDetailProperty
  def RiskA():
        return { "KEY": "RISK_A", "DEFAULT": "0" }

  @AlarmDetailProperty
  def RiskC():
        return { "KEY": "RISK_C", "DEFAULT": "0" }

  @AlarmDetailProperty
  def Sensor():
        return { "KEY": "SENSOR", "DEFAULT": "UNDEF" }

  @AlarmDetailPropertyBoolean
  def isAlarm():
        return "ALARM"

  @AlarmDetailProperty
  def Context():
        return { "KEY": "CTX", "DEFAULT": "UNDEF" }

  @AlarmDetailProperty
  def EventDate():
        return { "KEY": "BACKLOG_TIMESTAMP", "DEFAULT": "None" }

  @AlarmDetailProperty
  def Timezone():
        return { "KEY": "TZONE", "DEFAULT": "UNDEF" }

  @AlarmDetailProperty
  def Protocol():
        return { "KEY": "PROTOCOL", "DEFAULT": "0" }

  @AlarmDetailProperty
  def Name():
        return { "KEY": "NAME", "DEFAULT": "Error parsing event name" }

  @AlarmDetailProperty
  def EventID():
        return { "KEY": "EVENT_ID", "DEFAULT": "None" }

  @AlarmDetailProperty
  def BacklogID():
        return { "KEY": "BACKLOG_ID", "DEFAULT": "None" }

  @AlarmDetailProperty
  def PluginID():
        return { "KEY": "PLUGIN_ID", "DEFAULT": "None" }

  @AlarmDetailProperty
  def PluginSID():
        return { "KEY": "PLUGIN_SID", "DEFAULT": "None" }

  @AlarmDetailProperty
  def SourceIP():
        return { "KEY": "SRC_IP", "DEFAULT": "0.0.0.0" }

  @AlarmDetailProperty
  def SourcePort():
        return { "KEY": "SRC_PORT", "DEFAULT": "None" }

  @AlarmDetailProperty
  def DestinationPort():
        return { "KEY": "DST_PORT", "DEFAULT": "None" }

  @AlarmDetailProperty
  def DestinationIP():
        return { "KEY": "DST_IP", "DEFAULT": "0.0.0.0" }

  def toString(self):
    RETURN = "Date %s (%s), Name: %s" % (self.EventDate,self.Timezone,self.Name)
    return RETURN

  def toStringExtended(self):
    RETURN = self.toString()
    RETURN += "TRAFFIC: %s %s:%s -> %s:%s" % (self.protocolName(),self.SourceIP,self.SourcePort,self.DestinationIP,self.DestinationPort)
    return RETURN

  def protocolName(self):
    try:
      if int(self.Protocol) == 6:
        return "TCP"
      elif int(self.Protocol) == 17:
        return "UDP"
      else:
        return "NA"
    except:
      return self.Protocol


class AlarmObject:

  # This class takes a single named row of an Alarm and allows for consistent formatting.
  def __init__(self, AlarmDict, MultiAlarmDetailDict):
    self.rawAlarmInfo = AlarmDict
    self.EventsList = []
    self.parseAlarmDetails(MultiAlarmDetailDict)
    self.Expand = False # whether or not the name should perform variable matching.  This is a carry over quickndirty way.  Should be set by Alarm Processing class only

  def AlarmProperty(f):
    def fset(self, value):
        raise TypeError
    def fget(self):
        COLUMN_DICT = f()
        try:
          return self.rawAlarmInfo[COLUMN_DICT["KEY"]]
        except:
          return self.rawAlarmInfo[COLUMN_DICT["DEFAULT"]]
    return property(fget, fset)

  def parseAlarmDetails(self, MultiAlarmDetailDict):
    self.EventsList = []
    for row in MultiAlarmDetailDict:
      ADO = AlarmDetailObject(row)
      #print "ADO DETAILS: ", ADO.toStringExtended()
      self.EventsList.append(ADO)
      
  @AlarmProperty
  def Risk():
        return { "KEY": "RISK", "DEFAULT":"0"}

  @AlarmProperty
  def Protocol():
        return { "KEY": "PROTOCOL", "DEFAULT": None }

  @AlarmProperty
  def EventDate():
        return { "KEY": "TIMESTAMP", "DEFAULT": None }

  @AlarmProperty
  def BacklogID():
        return { "KEY": "BACKLOG_ID", "DEFAULT": "None" }

  @AlarmProperty
  def PluginID():
        return { "KEY": "PLUGIN_ID", "DEFAULT": "None" }

  @AlarmProperty
  def PluginSID():
        return { "KEY": "PLUGIN_SID", "DEFAULT": "None" }

  @AlarmProperty
  def SourceIP():
        return { "KEY": "SRC_IP", "DEFAULT": "0.0.0.0" }

  @AlarmProperty
  def SourcePort():
        return { "KEY": "SRC_PORT", "DEFAULT": "None" }

  @AlarmProperty
  def DestinationPort():
        return { "KEY": "DST_PORT", "DEFAULT": "None" }

  @AlarmProperty
  def DestinationIP():
        return { "KEY": "DST_IP", "DEFAULT": "0.0.0.0" }

  #@AlarmProperty
  #def USERDATA1():
  #      return { "KEY": "USERDATA1", "DEFAULT": "" }

  @AlarmProperty
  def Name():
        return { "KEY": "NAME", "DEFAULT": "No name defined for alarm" }

  def getAlarmRefURL(self):
    return "../#analysis/alarms/alarms-%s" % (str(self.BacklogID))

  def getAlarmRefFullURL(self):
    return "https://STI_NAT/ossim/#analysis/alarms/alarms-%s" % (str(self.BacklogID))

  def getAlarmHyperLink(self):
    return "[%s %s]" % (str(self.getAlarmRefURL()),str(self.BacklogID))

  def setExpand(self, boolean):
    self.Expand = bool(boolean)

  def getExpand(self):
    return bool(self.Expand)

  def getTicketTitleName(self):
    if self.Expand:
        return self.getExpandedName()
    else:
        return self.Name


#  def getAlarmDate(self):
#    if self.AlarmDate is None:
#      return str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"))
#    else:
#      return self.AlarmDate

  def toString(self, withDetails = True, limit = -1):
    ENTRY = "Date: %s, Risk: %s\nTRAFFIC: %s %s:%s -> %s:%s" % (self.EventDate,self.Risk,self.protocolName(),self.SourceIP,self.SourcePort,self.DestinationIP,self.DestinationPort)
    if withDetails:
      ENTRY += "\nRELATED EVENTS:\n"
      ENTRY += self.relatedEventsToString(num = limit)
    return ENTRY

  def relatedEventsToString(self, num = -1):
    i = 0
    RETURN = ""
    if not self.EventsList is None:
      for row in self.EventsList:
        if i < num or num < 0:
          RETURN+="\n"+row.toString()
          i += 1
    return RETURN.lstrip()

  def getHighestCorrelationLevelEvent(self):
    RL = "-1"
    ED = None
    #print "ROW=%s", self.EventsList
    for row in self.EventsList:
      if int(RL) < int(row.RuleLevel) and row.isAlarm:
        RETURN = row.RuleLevel
        ED = row
    return ED

  # Non 1594 events are generating a none error here.  Modified SQL to look up the name field on the alarm. 
  def getHighestDirectiveName(self):
    return str(self.Name)
    #return str(self.getHighestCorrelationLevelEvent().Name)

  def getExpandedName(self):
    name = self.getHighestDirectiveName()
    for key in list(self.rawAlarmInfo):
      try:
        name = re.sub(r'\b' + re.escape(str(key)) + r'\b',str(self.rawAlarmInfo[key]),name)
        #print name
      except Exception, e:
        print e
    return name

  def __str__(self):
    return str(self.getHighestDirectiveName())

  def protocolName(self):
    try:
      if int(self.Protocol) == 6:
        return "TCP"
      elif int(self.Protocol) == 17:
        return "UDP"
      else:
        return "NA"
    except:
      return self.Protocol

  def getTicketDescription(self):
    RETURN = "<!--wiki-->%s occurred on %s with overall risk %s.  Traffic was %s %s:%s -> %s:%s SIEM REF: %s" % (self.getHighestDirectiveName(),self.EventDate,self.Risk,self.protocolName(),self.SourceIP,self.SourcePort,self.DestinationIP,self.DestinationPort,self.getAlarmHyperLink())
    return str(RETURN)

  def toWikiString(self, withDetails = True):
    RETURN = "<!--wiki-->"
    RETURN += "'''%s'''\n%s, Risk: %s" % (self.getHighestDirectiveName(),self.EventDate,self.Risk)
    RETURN += "\n%s %s:%s -> %s:%s" % (self.protocolName(),self.SourceIP,self.SourcePort,self.DestinationIP,self.DestinationPort)
    if withDetails:
      RETURN += "\n\n'''RELATED EVENTS:'''\n"
      RETURN += self.relatedEventsToString()
    return RETURN

  def toSimpleWikiString(self, withDetails = True):
    RETURN = "<!--wiki-->"
    RETURN += "%s\n%s, Risk: %s" % (self.getHighestDirectiveName(),self.EventDate,self.Risk)
    RETURN += "\n%s %s:%s -> %s:%s" % (self.protocolName(),self.SourceIP,self.SourcePort,self.DestinationIP,self.DestinationPort)
    if withDetails:
      RETURN += "\n\nRELATED EVENTS:\n"
      RETURN += self.relatedEventsToString()
    return RETURN  
