#!/usr/bin/python
###############################################################################################
#
# SOC Customer Notification Procedure Selection Engine (SCNPEngine)
# Written By: Sidney Eaton on 10/17/17
#
# Description:
# This module implements an alaphanumeric ini-based rule engine to determine which document
# ID should be returned to the main program which will allow selecting one SCNP from another
# on a per device basis.
##############################################################################################

import ConfigParser
import logging
import os
from netaddr import IPNetwork, IPAddress

# Test case class to make testing easier
class scnpTestCases:
  def __init__(self):
    self.x = scnpEngine()
    self.x.readConfigurationFile()
    print("Now running test cases....")
    self.performTest("10.0.0.50", "10.0.1.5")
    self.performTest("10.0.0.50", "10.0.1.50")
    self.performTest("161.0.0.50", "10.0.0.5")
    self.performTest("161.0.0.50", "10.0.1.5")
    self.performTest("161.0.0.50", "10.0.1.50")
    self.performTest("161.0.0.50", "161.0.1.50")

  def performTest(self, srcip, dstip, url = "http://kb.noc.sentinel.com/kb/customer:[CUSTOMER_NUMBER]/scnp/[DOCID]"):
    y = self.x.getDocID(srcip,dstip)
    print("Test case (%s,%s) yielded %s." % (srcip,dstip,y))
    print("Test case (%s,%s) url now %s.\n" % (srcip,dstip,self.x.populateVariables(srcip,dstip,url)))

# Defines an individual SCNP rule.
class scnpRule:
  def __init__(self, docid="0", networks="0.0.0.0/0"):
    self.NETS=networks.split(",")
    self.DOCID=str(docid)

  # To include all devices in a config make sure that you use 0.0.0.0/0 which should match everything.
  def inIPRange(self, srcip, dstip):
    for entry in self.NETS:
      #logging.debug("Now checking %s."
      try:
        if (IPAddress(srcip) in IPNetwork(entry)) or (IPAddress(dstip) in IPNetwork(entry)):
          #logging.debug("%s or %s is in %s." % (srcip,dstip,entry))
          return True
      except Exception, e:
        logging.exception("Exception in IP range of SCNP - %s" % e)
        pass
    return False

  def getDocID(self):
    return str(self.DOCID)


# Main SOC Notification Procedure Engine.  Reads the engine rules and processes them.
class scnpEngine:

  def __init__(self, configurationFile=os.path.dirname(os.path.realpath(__file__)) + '/sti-scnp-config.cfg'):
    self.configFile = configurationFile
    self.config = {}
    self.DEFAULT_DOCID="0"
    #logging.getLogger().setLevel(logging.DEBUG)

  # This method reads the configuration INI file.
  def readConfigurationFile(self):
    logging.debug("Now opening configuration file: %s" % self.configFile)

    # Assign defaults to any options in the rules that aren't assigned and assign program defaults here.
    cp = ConfigParser.RawConfigParser({"docid":self.DEFAULT_DOCID, "networks":"0.0.0.0/0"})

    # Read the configuration file
    cp.read(self.configFile)

    # Assume that alternate defaults may have been entered into the configuration file and assign the value back here.
    self.DEFAULT_DOCID=cp.defaults()["docid"]

    # Build the rule-based dictionary.
    for item in cp.sections():
      logging.debug("Creating Rule %s containing %s." % (item,cp.items(item)))
      self.config[item] = scnpRule(docid=cp.get(item,"docid"), networks=cp.get(item,"networks"))

  # This method gets the Document ID based upon the highest matching rule of either SRCIP or DSTIP.
  # Rules are sorted and checked in alphanumeric order.  The rule name (ini section title) is crucial
  # in determining the order.
  def getDocID(self, srcip, dstip):
    for item in sorted(self.config.keys()):
      if self.config[item].inIPRange(srcip,dstip):
        return str(self.config[item].getDocID())
    return str(self.DEFAULT_DOCID)

  # This is a built-in function to allow maximum portablility and more independence among modules.
  # It will take a string and populate all the variables this module knows about.  Typical use case
  # is a SOC CNP URL that could vary by device.
  def populateVariables(self, srcip, dstip, myString):
    try:
      myString = myString.replace("[DOCID]", self.getDocID(srcip,dstip))
      myString = myString.replace("[SRC_IP]", srcip)
      myString = myString.replace("[DST_IP]", dstip)
    except Exception, e:
      logging.exception("Error populating SCNP variables! - %s" % e)
    return myString


  # This method is not used, but returns the rules that are read.
  def getRules(self):
    return self.config