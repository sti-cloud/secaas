import ConfigParser
from io import StringIO


class AlienVaultConfig:

  def __init__(self, OSSIM_CONFIG_FILE='/etc/ossim/ossim_setup.conf', database = 'alienvault'):
    self.OSSIM_FILENAME = OSSIM_CONFIG_FILE
    self.DB = database
    db_config = ConfigParser.ConfigParser()
    vfile = StringIO(u'[misc]\n%s'  % open(OSSIM_CONFIG_FILE).read())
    db_config.readfp(vfile)
    self.DB_IP=db_config.get("database", "db_ip")
    self.DB_USER=db_config.get("database", "user")
    self.DB_PASSWD=db_config.get("database", "pass")
    self.TZONE=db_config.get("sensor", "tzone")

  def fset(self, value):
    return TypeError
  def fget(self):
    return self.__attr__

  DB_IP = property(fget,fset)
  DB_USER = property(fget,fset)
  DB_PASSWD = property(fget,fset)
  DB = property()
  TZONE = property(fget,fset)