#!/usr/bin/python
import threading
import datetime
import time
import traceback
import os

from TicketConfig import TicketConfig
from AlarmQuerier import AlarmQuerier
from AlarmProcessing import AlarmProcessing
from AlienVaultTicketSystem import AlienVaultTicketSystem
from STITicketSystem import STITicketSystem
from requests.exceptions import ConnectionError
DEBUG = True
import logging
import logging.handlers
import signal
LOG_FILENAME="/var/log/STITicketSystem.log"
LOG_LEVEL = logging.DEBUG
#logger=None

def log_setup():
    log_handler = logging.handlers.WatchedFileHandler(LOG_FILENAME)
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)s %(module)s(%(funcName)s) [%(process)d-%(thread)d]: %(message)s',
        '%b %d %H:%M:%S')
    formatter.converter = time.gmtime  # if you want UTC time
    log_handler.setFormatter(formatter)
#    global logger
    logger = logging.getLogger()
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)


log_setup()




# Old debugger.
def logDebug(method, log_entry):
    if DEBUG:
      print "%s - main(%s): %s" % (str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),str(method),str(log_entry))
      logging.debug("main(%s): %s" % (method,log_entry))

# This function performs the check to make sure all other components of the application are working correctly.  TRUE = Good, FALSE = BAD
def isRunning():
  if not (AlienVaultTicketCreationThread.is_alive() and AlienVaultTicketUpdateThread.is_alive() and (SentinelTicketCreationThread.is_alive() or not bool(config.ENABLE_STI_TICKET_CREATION_THREAD))):
    return False
  else:
    return True


# This method tries to make sure all the threads exit gracefully.
def cleanup():
  stop_event.set()
  #logDebug("cleanup","Program has been interrupted, performing cleanup tasks.")
  logging.warn("Program has been interrupted, performing cleanup tasks.")
  while isRunning():
    time.sleep(3)
  


# will signal to the input thread that it should exit:
# the main thread acquires it and releases on exit
#interrupted = threading.Lock()
#interrupted.acquire()

def subThreadCreateSTITicketNow(stop_event, AlarmObj, AlienVaultTicketNumber):
    #logDebug("subThreadCreateSTITicketNow","AlienVault Ticket Number %s: Thread created." % AlienVaultTicketNumber)
    logging.info("AlienVault Ticket Number %s: Thread created." % AlienVaultTicketNumber)
    STITS = STITicketSystem(server = config.SERVER, port = config.PORT, clientid = config.CLIENTID, apikey = config.APIKEY, debug = True, custNum = config.CUSTOMER_NUMBER, urlPrefix = config.URL_PREFIX)
    AVTS = AlienVaultTicketSystem(debug = True)
#    ReturnedStatus = "EMPTY"
#    while ReturnedStatus != "SUCCESS" || not stop_event.is_set():
    try:
      jsondata = None
      while jsondata is None: #and not stop_event.is_set():  # Keep looping until the command succeeds, we may not want to follow stop_event later on.
        try:
          logging.info("Attempting to create Sentinel ticket from AlienVault ticket #%s." % (AlienVaultTicketNumber))
          jsondata = STITS.createTicketHelper(
            TicketTitle = str(AlarmObj),
            TicketDescription = AVTS.getTicketDescription(TicketNumber = AlienVaultTicketNumber),
            AlienVaultTicketNumber = AlienVaultTicketNumber
          )
          logging.debug("AlienVault ticket #%s, generated the returned json payload: %s" % (AlienVaultTicketNumber, jsondata))
        except ConnectionError as e:
          #logDebug("subThreadCreateSTITicketNow","There was a connection error while creating a Sentinel ticket for AlienVault ticket #%s, will retry. Error: %s" % (AlienVaultTicketNumber,e))
          logging.warn("There was a connection error while creating a Sentinel ticket for AlienVault ticket #%s, will retry. Error: %s" % (AlienVaultTicketNumber,e))
          time.sleep(60)

      ReturnedStatus = str(jsondata['Status'].encode('utf-8')).upper() # The encode here may not be required.
      #print "### RETURNED STATUS #### : %s" % ReturnedStatus
      if ReturnedStatus == "SUCCESS":
        ReturnedTicketNumber = jsondata['Call Number']
        #logDebug("subThreadCreateSTITicketNow","Sentinel ticket #%s created for AlienVault ticket #%s." % (ReturnedTicketNumber,AlienVaultTicketNumber))
        logging.info("Sentinel ticket #%s created for AlienVault ticket #%s." % (ReturnedTicketNumber,AlienVaultTicketNumber))
        AVTS.addRemoteTicketSystemInformation(AlienVaultTicketNumber = AlienVaultTicketNumber, STITicketNumber = ReturnedTicketNumber)
        with NoteLock:
          AVTS.AddLogToTicket(
            ticketNumber = AlienVaultTicketNumber,
            description = "Sentinel Ticket #%s" % ReturnedTicketNumber)
      else:
        #logDebug("subThreadCreateSTITicketNow","There was an error creating a Sentinel ticket for AlienVault ticket #%s.  JSON RETURN PAYLOAD IS: %s." % (AlienVaultTicketNumber,jsondata))
        logging.error("There was an error creating a Sentinel ticket for AlienVault ticket #%s.  JSON RETURN PAYLOAD IS: %s." % (AlienVaultTicketNumber,jsondata))
    except Exception, e:
      #logDebug("subThreadCreateSTITicketNow","A fault occurred while attempting to create a sentinel ticket. Exception: %s" % (e))
      logging.exception("A fault occurred while attempting to create a sentinel ticket. Exception: %s" % (e))
      #traceback.print_exc()



def createAlienVaultTickets(stop_event, interval):
  #while not interrupted.acquire(0) and not stop_event.is_set():
  #AQ = AlarmQuerier(startDateTimeUTC = "2016-10-12 10:32:08", debug = True)
  #AQ = AlarmQuerier(startDateTimeUTC = "2016-12-06 00:00:00", debug = True)
  AQ = AlarmQuerier(debug = True)
  AP = AlarmProcessing(config, debug = True)
  AVTS = AlienVaultTicketSystem(debug = True)
  while not stop_event.is_set():
      #logDebug("createAlienVaultTickets","Checking for new AlienVault ticketable alarms!")
      logging.info("Checking for new AlienVault ticketable alarms!")
      #results = AQ.getAlarmsWithDetails()
      results = AQ.getAlarmObjects()
      #logDebug("createAlienVaultTickets","Found %s Alarms" % str(len(results)))
      logging.info("Found %s Alarms" % str(len(results)))
      for row in results:
          isTicketable = AP.isAlarmTicketable(row)
          #logDebug("createAlienVaultTickets","%s\tFinal Ticketable Verdict: %s" % (row,str(isTicketable)))
          logging.debug("%s\tFinal Ticketable Verdict: %s" % (row,str(isTicketable)))
          if isTicketable:

            #### Checking AlienVault has now moved to an extension of the new suppression engine.
            # Check to see if an Alienvault Ticket is already created
#            if not AVTS.isTicketAlreadyOpen(row.getHighestDirectiveName()):
              #logDebug("createAlienVaultTickets","Create ticket for %s" % (row))
              logging.info("Going to create a ticket for %s" % (row))
              # Create AlienVault Ticket
              ticketNumber = AVTS.createTicket(row)
              #logDebug("createAlienVaultTickets","AlienVault Ticket #%s created for %s, DelayedSTITicketCreation: %s" % (ticketNumber,row,str(config.ENABLE_STI_TICKET_CREATION_THREAD)))
              logging.info("AlienVault Ticket #%s created for %s, DelayedSTITicketCreation: %s" % (ticketNumber,row,str(config.ENABLE_STI_TICKET_CREATION_THREAD)))
              # If the ticket creation thread isn't running, we do not poll the database for tickets to create.
              # In this case we assume creating the ticket immediately. Polling the database has its advantages,
              # In that it manages the retries automatically and failures.  We will create a specialized
              # thread per ticket, to manage this process.
              if str(config.ENABLE_STI_TICKET_CREATION_THREAD).upper() in "FALSE" :
                logDebug("createAlienVaultTickets","STI Ticket Creation Thread Disabled, create ticket now.  NOT IMPLEMENTED")
                STIImmediateCreateThread = threading.Thread(target=subThreadCreateSTITicketNow, args=(stop_event,row,ticketNumber))
                STIImmediateCreateThread.start()


###########END OF NEW STUFF
#        print AP.isTicketable(row)

      # This section would allow a quick shutdown of threads should the process be killed while in a safe
      # sleep state.
      for QuickAbortCheck in xrange(0,int(interval)):
        if not stop_event.is_set():
          time.sleep(1)
#  AP.stopSuppressionThread() ### NO LONGER NEEDED WITH NEW SUPPRESSION ENGINE
  #logDebug("createAlienVaultTickets","Thread is no longer running under viable conditions....exiting!")
  logging.critical("Thread is no longer running under viable conditions....exiting!")



# Function: createSentinelTickets
# This is designed to be run as a thread and to look for tickets that
# need to be created in the Sentinel System.
def createSentinelTickets(stop_event, interval):
  #while not interrupted.acquire(0) and not stop_event.is_set():
  while not stop_event.is_set():
      #logDebug("createSentinelTickets","Checking for Sentinel Tickets in AlienVault that need to be pushed to Sentinel!")
      logging.info("Checking for Sentinel Tickets in AlienVault that need to be pushed to Sentinel!")

      # This section would allow a quick shutdown of threads should the process be killed while in a safe
      # sleep state.
      for QuickAbortCheck in xrange(0,int(interval)):
        if not stop_event.is_set():
          time.sleep(1)
  #logDebug("createSentinelTickets","Thread is no longer running under viable conditions....exiting!")
  logging.critical("Thread is no longer running under viable conditions....exiting!")


# Function: updateAlienVaultTickets
# This is designed to be run as a thread and to look for tickets that
# need to be created in the Sentinel System.
def updateAlienVaultTickets(stop_event, interval):
  #while not interrupted.acquire(0) and not stop_event.is_set():
  while not stop_event.is_set():
    if True: #False: # Temporarily Disable this section for Mark Combs
      #logDebug("updateAlienVaultTickets","Checking for Sentinel Tickets in AlienVault that need to be updated!")
      logging.info("Checking for Sentinel Tickets in AlienVault that need to be updated!")

      AVTS = AlienVaultTicketSystem(debug = True)
      STITS = STITicketSystem(server = config.SERVER, port = config.PORT, clientid = config.CLIENTID, apikey = config.APIKEY, debug = True, custNum = config.CUSTOMER_NUMBER, urlPrefix = config.URL_PREFIX)
      AVOpenTickets = AVTS.getAlienVaultTicketsNotClosedInSTI()
      for ticket in AVOpenTickets:
        #logDebug("updateAlienVaultTickets","Now querying remote ticket system for ticket #%s" % ticket['STI_TICKET'])
        logging.info("Now querying remote ticket system for ticket #%s" % ticket['STI_TICKET'])
        try:
          sti_ticket = STITS.getCall(ticket['STI_TICKET'])
          if sti_ticket.isClosed():
            with NoteLock:
              AVTS.closeTicket(ticket['TICKET_ID'], notes = sti_ticket.getCloseNotes())
        except KeyError, e:
          #logDebug("updateAlienVaultTickets","Key error when querying remote ticket system for ticket #%s.  Error: %s" % (ticket['STI_TICKET'],e))
          logging.error("Key error when querying remote ticket system for ticket #%s.  Error: %s" % (ticket['STI_TICKET'],e))
          if os.path.exists("/root/ticket.closekeys"):
            with NoteLock:
              AVTS.closeTicket(ticket['TICKET_ID'], notes = "Key error")
        except ConnectionError as e:
          #logDebug("updateAlienVaultTickets","Connection error when querying remote ticket system for Sentinel ticket #%s/AlienVault ticket #%s.  Error: %s" % (ticket['STI_TICKET'],ticket['TICKET_ID'],e))
          logging.error("Connection error when querying remote ticket system for Sentinel ticket #%s/AlienVault ticket #%s.  Error: %s" % (ticket['STI_TICKET'],ticket['TICKET_ID'],e))
        except Exception, e:
          logging.exception("Unknown exception in updateAlienVaultTickets thread: %s" % (str(e)))
          #traceback.print_exc()


### This was the start of doing some note replications but given time constraints and the messiness of universe this will take more time than I originally thought.
#        av_ticket_entries = AVTS.getAlienVaultTicketMD5(ticket['TICKET_ID'])
#        for sti_entry in sti_ticket.getNotes():
#          update_av = True
#          for desc_entry in av_ticket_entries:
#            logDebug("updateAlienVaultTickets","Comparing '%s' TO STI '%s' in AlienVault ticket %s and Sentinel ticket %s!" % (desc_entry['DESCRIPTION'],sti_entry.getNote(),ticket['TICKET_ID'],sti_ticket.getTicketNumber()))
##            logDebug("updateAlienVaultTickets","Comparing %s to STI %s in STI ticket %s and AlienVault ticket %s!" % (sti_entry.getHash(),desc_entry['MD5'],sti_ticket.getTicketNumber(),ticket['TICKET_ID']))
#
#            if desc_entry['MD5'] in sti_entry.getHash():
#              update_av = False
#        
#        logDebug("updateAlienVaultTickets","STI Note %s in STI ticket %s does not have a matching entry in AlienVault ticket %s!" % (sti_entry.getHash(),sti_ticket.getTicketNumber(),ticket['TICKET_ID']))
          
          
      

    # This section would allow a quick shutdown of threads should the process be killed while in a safe
    # sleep state.
    for QuickAbortCheck in xrange(0,int(interval)):
        if not stop_event.is_set():
          time.sleep(1)
  logging.critical("Thread is no longer running under viable conditions....exiting!")
  #logDebug("updateAlienVaultTickets","Thread is no longer running under viable conditions....exiting!")


# The AddLogEntry function in AlienVault table doesn't have auto increment.  Until I find a better way I have to find the next number but avoid conflicts.
# This lock only needs to be applied to functions that would update the ticket note table.
NoteLock = threading.Lock()


# This represents a global TicketConfig object.  For threads should create their own for now so that code
# locking is minimized.
config = TicketConfig()

stop_event = threading.Event()

AlienVaultTicketCreationThread = threading.Thread(target=createAlienVaultTickets, args=(stop_event,int(config.ALARM_CHECK_INTERVAL)))
AlienVaultTicketCreationThread.start()

SentinelTicketCreationThread = threading.Thread(target=createSentinelTickets, args=(stop_event,int(config.STI_TICKET_CREATION_CHECK_INTERVAL)))

AlienVaultTicketUpdateThread = threading.Thread(target=updateAlienVaultTickets, args=(stop_event,int(config.STI_TICKET_CREATION_CHECK_INTERVAL)))
AlienVaultTicketUpdateThread.start()

if bool(config.ENABLE_STI_TICKET_CREATION_THREAD):
  SentinelTicketCreationThread.start()


def sigterm_handler(signal, frame):
    # save the state here or do whatever you want
    logging.critical("Caught signal to terminate, performing graceful cleanup first...")
    cleanup()
    sys.exit(0)

signal.signal(signal.SIGTERM, sigterm_handler)
signal.signal(signal.SIGINT, sigterm_handler)

try:
  while True:
    # If any of the main threads have an issue.  Abort
    logging.info("Alienvault Ticket Thread Alive: %s" % AlienVaultTicketCreationThread.is_alive())
    logging.info("Alienvault Ticket Update Thread Alive: %s" % AlienVaultTicketUpdateThread.is_alive())
    logging.info("Sentinel Ticket Create Thread Alive: %s" % SentinelTicketCreationThread.is_alive())

    if isRunning():
      time.sleep(15)
    else:
      break

#    if not (AlienVaultTicketCreationThread.is_alive() and AlienVaultTicketUpdateThread.is_alive() and (SentinelTicketCreationThread.is_alive() or not bool(config.ENABLE_STI_TICKET_CREATION_THREAD))):
#      break
#    else:
#      time.sleep(15)
except KeyboardInterrupt:
  pass
finally:
  cleanup()


# make input thread exit as well, if still running
#stop_event.set()
#interrupted.release()
logging.warn("Main thread is done.")
#logDebug("main","Main thread is done.")
