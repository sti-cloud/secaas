#!/usr/bin/python

import MySQLdb, datetime
from AlienVaultConfig import AlienVaultConfig

class TicketConfigBuilder(object):

  def __init__(self, debug = False):
    self.AVCONFIG = AlienVaultConfig()
    self.DEBUG = debug
    self.PLUGIN_SEPARATOR = ";"

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))

  def genericQuery(self, queryString):
      conn = MySQLdb.connect(host=self.AVCONFIG.DB_IP,user=self.AVCONFIG.DB_USER , passwd=self.AVCONFIG.DB_PASSWD ,db=self.AVCONFIG.DB)
      cursor = conn.cursor(MySQLdb.cursors.DictCursor)
      self.logDebug("genericQuery", "Performing Query: %s" % queryString)

      cursor.execute("%s" % (queryString))
      results = cursor.fetchall()

      try:
        cursor.close()
      except Exception, e:
        logDebug("genericQuery", "Error attempting to close cursor: %s" % e)

      try:
        conn.close()
      except Exception, e:
        logDebug("genericQuery", "Error attempting to close database: %s" % e)
      return results

  def getDirectiveListInKingdom(self, kingdomID):
    sql = "select distinct(sid) as SID from alarm_taxonomy where kingdom = '%s' ORDER BY SID ASC;" % (str(kingdomID))
    return self.genericQuery(sql)

  def getFormattedString(self, kingdomID):
    results = self.getDirectiveListInKingdom(kingdomID = kingdomID)
    returnString = ""
    firstSID = None
    lastSID = None
    isRange = False
    endItem = results[-1]['SID']
    try:
      for row in results:
        if firstSID is None or lastSID is None:
          lastSID = int(row['SID'])
          firstSID = int(row['SID'])
        else:
          if (int(row['SID']) == int(lastSID) + 1): # and (int(row['SID']) != int(endItem)):
            isRange = True
            lastSID = int(row['SID'])
            # If the item is the last item and in range print
            if (int(row['SID']) == int(endItem)):
              returnString = returnString + "1505:" + str(firstSID) + "-" + str(row['SID']) + self.PLUGIN_SEPARATOR
          else:
            if isRange:
              returnString = returnString + "1505:" + str(firstSID) + "-" + str(lastSID) + self.PLUGIN_SEPARATOR
            else:
              returnString = returnString + "1505:" + str(firstSID) + self.PLUGIN_SEPARATOR
              if (int(row['SID']) == int(endItem)):
                returnString = returnString + "1505:" + str(row['SID']) + self.PLUGIN_SEPARATOR
            lastSID = int(row['SID'])
            firstSID = int(row['SID'])
            isRange = False
    except Exception, e:
      self.logDebug("getFormattedString", "Error while formatting the string: %s" % e)

    returnString = returnString.rstrip(self.PLUGIN_SEPARATOR)
    return returnString




