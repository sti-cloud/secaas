--
-- Sentinel Technologies Ticketing Database Modifications
-- Written By: Sidney Eaton
-- Last Modified: 2-17-2017
--

insert into incident_type (id, descr, keywords) VALUES ("Sentinel Ticket","Sentinel CloudSelect SOC Ticket Type","custom custom custom custom custom") ON DUPLICATE KEY UPDATE descr = "Sentinel CloudSelect SOC Ticket Type", keywords = "custom custom custom custom custom";

insert into incident_custom_types (id, name, type, required, ord) VALUES ("Sentinel Ticket","STI_TICKET","Textbox",1,1) ON DUPLICATE KEY UPDATE name = "STI_TICKET", type = "Textbox", required = 1, ord = 1;