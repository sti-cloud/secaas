#!/usr/bin/python

#import SuppressionModule
from SuppressionEngineBase import SuppressionEngineBase
from AlienVaultConfig import AlienVaultConfig
import MySQLdb
from contextlib import closing
import logging

class AlienVaultSuppressionEngine(SuppressionEngineBase):

  def __init__(self, debug = True):
    super(AlienVaultSuppressionEngine, self).__init__(debug = debug)
  
  def isSuppressed(self, key):
    #self.logDebug("isSuppressed","Checking to see if ticket is open for %s" % key)
    logging.info("Checking to see if ticket is open for %s" % key)
    return self.isTicketOpen(key = key)

  def genericQuery(self, queryString):
    results = {}
    db = None
    try:
      AVCONFIG = AlienVaultConfig()
      db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
      with closing( db.cursor(MySQLdb.cursors.DictCursor) ) as cursor:
        #self.logDebug("genericQuery", "Executing SQL query: %s" % queryString)
        logging.debug("Executing SQL query: %s" % queryString)
        cursor.execute(queryString)
        results = cursor.fetchall()
    except Exception, e:
      logging.exception("ERROR attempting to execute sql query: %s" % e)
      #self.logDebug("genericQuery", "ERROR attempting to execute sql query: %s" % e)
    finally:
      if not db is None:
        try:
          db.close()
        except:
          pass
      #print results
      return results

  def getOpenTicketsByKey(self, key):
    sql = "select id AS TICKET_ID, title as TITLE, status as STATUS from incident where status <> 'Closed' and title = '%s';" % key
    results = self.genericQuery(sql)
    return results

  def isTicketOpen(self, key):
    results = self.getOpenTicketsByKey(key = key)
    if len(results) > 0:
      #self.logDebug("isTicketOpen", "Ticket %s is open in AlienVault!" % key)
      logging.info("Ticket %s is open in AlienVault!" % key)
      return True
    else:
      #self.logDebug("isTicketOpen", "Ticket %s is not open in AlienVault!" % key)
      logging.info("Ticket %s is not open in AlienVault!" % key)
      return False
