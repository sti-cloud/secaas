#!/usr/bin/python

#from threading import Timer, RLock
from threading import Thread, RLock
from datetime import datetime, timedelta
from time import sleep
from AlienVaultSuppressionEngine import AlienVaultSuppressionEngine
import logging
class SuppressionEngine(object):

  def __init__(self, debug = False):
    self.SuppressionEngine = {}
    self.DEBUG = debug
    self.SuppressionLock = RLock()


    # Maintenance thread
    self.MaintenanceInterval = 60
    self.MaintenanceThread = Thread(target=self.suppressionEngineMaintenanceThread)
    self.MaintenanceThread.setDaemon(True)
    self.MaintenanceThread.start()

    # Supporting module if required, otherwise leave None.
    self.SupportingSuppressionEngine = AlienVaultSuppressionEngine(debug = debug)

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("%s(%s): %s" % (self.__class__,method,log_entry))

#  # Timer main mathod.  Should be very simple. Add Try block for del, with fail to clean up any stopped timers.
#  def timer_func(self, AlarmObjKeyString):
#    # Remove object from supression queue.
#    with self.SuppressionLock:
#        self.logDebug("timer_func","Expired entry %s from suppression engine." % AlarmObjKeyString)
#      #if AlarmObjKeyString in self.SuppressionEngine:
#        del self.SuppressionEngine[AlarmObjKeyString]

  def suppressionEngineMaintenanceThread(self):
    while True:
      #self.logDebug("suppressionEngineMaintenanceThread","Starting maintenance on suppression engine, current contents: %s" % str(self.SuppressionEngine))
      logging.debug("Starting maintenance on suppression engine, current contents: %s" % str(self.SuppressionEngine))
      with self.SuppressionLock:
        for entry in list(self.SuppressionEngine):
          if self.SuppressionEngine[str(entry)]["EXIT_TIME"] <= datetime.utcnow():
            #self.logDebug("suppressionEngineMaintenanceThread","Expired entry %s from suppression engine." % str(entry))
            logging.debug("Expired entry %s from suppression engine." % str(entry))
            del self.SuppressionEngine[str(entry)]
      sleep(int(self.MaintenanceInterval))

    

  # DYNAMIC = TRUE resets the timer every time a duplicate alarm comes in.
  # EXPAND = True will attempt to perform AlienVault variable substitution with real values.  The purpose
  #          being that variables will also be counted.
  def isSuppressed(self, AlarmObj, delayInSeconds = 3600, Dynamic = False, Expand = False):
    key = self.getAlarmObjDedupKey(AlarmObj, Expand)
    with self.SuppressionLock:
        if key in self.SuppressionEngine:
          if self.SuppressionEngine[key]["EXIT_TIME"] <= datetime.utcnow():
            #self.logDebug("isSuppressed", "Entry (%s) exists in suppression engine, but is already expired.  Should not be suppressed." % (str(key)))
            logging.debug("Entry (%s) exists in suppression engine, but is already expired.  Should not be suppressed." % (str(key)))
            self.SuppressionEngine[key] = { "EXIT_TIME": datetime.utcnow() + timedelta(seconds=delayInSeconds)}
            return self.isSuppressedOnSupportingSuppressionEngine(key, False)
          elif Dynamic:
            # One of these methodologies must be choosen.  Additionally incremement or restart the timer.
            #self.SuppressionEngine[key] = { "EXIT_TIME": datetime.utcnow() + timedelta(seconds=delayInSeconds)} # Restart the timer
            self.SuppressionEngine[key]["EXIT_TIME"] = self.SuppressionEngine[key]["EXIT_TIME"] + timedelta(seconds=delayInSeconds) # Additionally increment
            #self.logDebug("isSuppressed", "Dynamic enabled, updating exit time on entry (%s) to %s" % (str(key),self.SuppressionEngine[key]["EXIT_TIME"]))
            logging.debug("Dynamic enabled, updating exit time on entry (%s) to %s" % (str(key),self.SuppressionEngine[key]["EXIT_TIME"]))
          #self.logDebug("isSuppressed", "Alarm %s should be suppressed." % (str(key)))
          logging.info("Alarm %s should be suppressed." % (str(key)))
          return self.isSuppressedOnSupportingSuppressionEngine(key, True)
        else:
          # Create the Timer
          logging.debug("Entry (%s) not in suppression engine.  Should not be suppressed." % (str(key)))
          #self.logDebug("isSuppressed", "Entry (%s) not in suppression engine.  Should not be suppressed." % (str(key)))
          self.SuppressionEngine[key] = { "EXIT_TIME": datetime.utcnow() + timedelta(seconds=delayInSeconds)}
          return self.isSuppressedOnSupportingSuppressionEngine(key, False)
          #myTimer = Timer(int(delayInSeconds),self.timer_func, [ key ])
          #myTimer.setDaemon(True)
          #self.SuppressionEngine[AlarmObj] = myTimer
          #self.SuppressionEngine[AlarmObj].start()

  # This method is for third-party integration.  If it is disabled then the defaultBooleanReturnValue is simply returned.
  def isSuppressedOnSupportingSuppressionEngine(self, key, defaultBooleanReturnValue):
    if self.SupportingSuppressionEngine is None:
      return defaultBooleanReturnValue
    else:
      return self.SupportingSuppressionEngine.isSuppressed(key = key)

  # This is a generic method that will return a deduplication key for the suppression engine.
  # This is meant for internal class use only and should only be called internally.
  # This is to allow the rest of the functions here to dynamically adapt if changes occur later.
  def getAlarmObjDedupKey(self, AlarmObj, Expand = False):
    try:
      if bool(Expand):
        return str(AlarmObj.getExpandedName())
      else:
        return str(AlarmObj)
    except Exception, e:
      logging.exception("Error in expansion function: %e, (EXPAND: %s)" % (e,str(Expand)))
      #self.logDebug("getAlarmObjDedupKey", "Error in expansion function: %e, (EXPAND: %s)" % (e,str(Expand)))
      return str(AlarmObj)


# THIS IS TEST STUFF
#x = SuppressionEngine(debug = True)
#for number in range(1,10):
#  print number
#  x.isSuppressed("Check #%s" % number, number)
#sleep(5)
#x.isSuppressed("Check #%s" % 2, 100, True)
#sleep(40)
