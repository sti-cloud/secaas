import datetime
import logging

class SuppressionEngineBase(object):

  def __init__(self, debug = False):
    self.DEBUG = debug

  # This should return true or false
  def isSuppressed(self, key):
    raise NotImplementedError()

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("%s(%s): %s" % (self.__class__,method,log_entry))

  def getOpenTicketsByKey(self, key):
    raise NotImplementedError()


  # This should return TRUE OR FALSE
  def isTicketOpen(self, key):
    raise NotImplementedError()

