#!/usr/bin/python

from datetime import datetime, timedelta
from time import sleep
import threading
from DeviceFilter import DeviceFilter
from SuppressionModule import SuppressionEngine
import logging

# This class controls the processing logic to determine whether or not an alarm should be ticketed.
# It is purely a helper class to organize code and reduce main thread logic.  It is not designed
# to be implemented on its own.
class AlarmProcessing:

  def __init__(self, TicketConfigObject, debug = False):
    self.TC = TicketConfigObject
    self.DEBUG = debug
    #self.stop_event = threading.Event()

    self.DeviceFilterModule = DeviceFilter(debug = self.DEBUG)
    self.SuppressionEngine = SuppressionEngine(debug = self.DEBUG) # NEW

    # Threading components for SuppressionDictionary
#    self.SuppressionDictLock = threading.Lock()
#    self.SuppressionDictLock.acquire()
#    self.SuppressionDict = {}
#    self.SuppressionDictLock.release()

 #   self.SuppressionQueueThread = threading.Thread(target=self.manage_suppression_queue)
 #   self.SuppressionQueueThread.setDaemon(True)
 #   self.SuppressionQueueThread.start()

#  def manage_suppression_queue(self):
#    SUBITEMCOUNT=0
#    while not self.stop_event.is_set():
#      self.logDebug("manage_suppression_queue","Suppression list currently holds %s plugin(s) and %s subitem(s)." % (len(self.SuppressionDict),str(SUBITEMCOUNT)))
#      self.logDebug("manage_suppression_queue","Queue content dump before operations: %s" % self.SuppressionDict)
#
#      # RESET SUBITEM COUNT FOR STATS ONLY
#      SUBITEMCOUNT=0
#      for entry in list(self.SuppressionDict):
#        for subentry in list(self.SuppressionDict[str(entry)]):
#          self.logDebug("manage_suppression_queue", "Now checking entry: (%s,%s)." % (entry,subentry))
#          SUBITEMCOUNT = SUBITEMCOUNT + 1
#          try:
#            # Attempt to pull EXIT_TIME, but don't through error if an entry was only partially added, rather
#            # just ignore the entry untill next poll for now.  By doing this we can avoid locking the object
#            # until we are ready to make changes which should speed up multi-threaded processing.
#              if self.SuppressionDict[str(entry)][str(subentry)]["EXIT_TIME"] <= datetime.utcnow():
#                self.SuppressionDictLock.acquire()
#                self.logDebug("manage_suppression_queue","Expired entry, removing (%s,%s,%s)" % (str(entry),str(subentry),self.SuppressionDict[str(entry)][str(subentry)]["EXIT_TIME"]))
#                self.SuppressionDict[str(entry)].pop(str(subentry))
#                if len(self.SuppressionDict[str(entry)]) < 1:
#                  self.SuppressionDict.pop(str(entry))
#                self.SuppressionDictLock.release()
#          except TypeError, e:
#                self.logDebug("manage_suppression_queue","TYPE ERROR: (%s,%s): %s" % (entry,subentry,self.SuppressionDict[str(entry)][str(subentry)]))
#          except KeyError, e:
#                self.logDebug("manage_suppression_queue","KEY ERROR: (%s,%s) (could be a concurrency issue and will be ignored for now): %s" % (entry,subentry,self.SuppressionDict[str(entry)][str(subentry)]))
#      # This section would allow a quick shutdown of threads should the process be killed while in a safe
#      # sleep state.
#      for QuickAbortCheck in xrange(0,3):
#        if not self.stop_event.is_set():
#          sleep(1)

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("%s(%s): %s" % (self.__class__,method,log_entry))

#  def stopSuppressionThread(self):
#    self.stop_event.set()
#    self.SuppressionQueueThread.join()

#  # This method is very much under development.  Here is how it works:
#  # FUNCTION: addAlarmToSuppressionQueue
#  #   RETURN: True/False - Whether or not Alarm was already in the queue. Generally if true is returned, a ticket
#  #                        should not be created.  If false is returned a ticket should be created.
#  # PARAMETERS:
#  #     PLUGIN_SID = Plugin_sid/message that should be correlated.
#  #     DYNAMIC = True - If a message is in the queue already extend its exit time by delayInSeconds.
#  #                      This provides a more dynamic guard against prolonged security events.
#  #               False - If a message is in the queue, return False, but do not affect the existing message's
#  #                       exit time.  In other words a ticket is eligible to be cut at the end of delayInSeconds
#  #                       interval of which the plugin_sid was previsouly added to the queue.
#  #     DELAYINSECONDS = Fairly self explanatory, but now() + delayInSeconds = SuppressionQueue exit time.
#  #                      If dynamic is set then this is modified slightly if an existing event is already in the
#  #                      suppression queue.
#  def addAlarmToSuppressionQueue(self,plugin_id,plugin_sid,delayInSeconds = 1800, dynamic = False):
#    plugin_id = str(plugin_id)
#    plugin_sid = str(plugin_sid)
#    self.SuppressionDictLock.acquire()
#    # Probably not the best naming, but reusing isInRiskTable code here.
#    if not self.isInAlarmSuppressionQueue(str(plugin_id),str(plugin_sid)):
#      if plugin_id in self.SuppressionDict:
#        self.SuppressionDict[plugin_id][plugin_sid] = { "EXIT_TIME": datetime.utcnow() + timedelta(seconds=delayInSeconds)}
#      else:
#        self.SuppressionDict[plugin_id] = { plugin_sid: { "EXIT_TIME": datetime.utcnow() + timedelta(seconds=delayInSeconds)}}
#
#      # Do regardless
#      self.logDebug("addAlarmToSuppressionQueue","Plugin (%s,%s) not in suppression queue, adding it: %s" % (plugin_id,plugin_sid,self.SuppressionDict))
#      self.SuppressionDictLock.release()
#      return False
#    else:
#      if dynamic:
#        self.SuppressionDict[str(plugin_id)][str(plugin_sid)]["EXIT_TIME"] = self.SuppressionDict[str(plugin_id)][str(plugin_sid)]["EXIT_TIME"] +timedelta(seconds=delayInSeconds)
#        self.logDebug("addAlarmToSuppressionQueue", "Dynamic enabled, updating exit time on (%s,%s) to %s" % (plugin_id,plugin_sid,self.SuppressionDict[str(plugin_id)][str(plugin_sid)]["EXIT_TIME"]))
#
#      # Make sure to release locks otherwise they will block.
#      self.SuppressionDictLock.release()
#      return True

  # Probably not the best naming, so I created a better name, but for now the code is the same as isInRiskTable
  def isInAlarmSuppressionQueue(self,plugin_id,plugin_sid):
    return self.isInRiskTable(self.SuppressionDict,str(plugin_id),str(plugin_sid))


#  def isTicketable(self, AlarmDict):
#    mappings = self.TC.getRiskMappings()
#    plugin_id = str(AlarmDict["PLUGIN_ID"])
#    plugin_sid = str(AlarmDict["PLUGIN_SID"])
#    try:
#       # Check to see if alarm is in suppression status
#       if addAlarmToSuppressionQueue(plugin_id,plugin_sid):
#         return False
#
#
#       # First check to make sure the alarm is not explicitly blacklisted from ticketing.
#       if self.isInRiskTable(mappings["NO TICKETS"],plugin_id,plugin_sid):
#         return False
#       else:
#         # Next check all risk levels equal to or lower, if found ticket should be created.
#         for x in xrange(1, int(AlarmDict["RISK"])+1):
#           self.logDebug("isAlarmable", "Risk is %s. Now checking risk dictionary %s for (%s,%s) to see if it should create a ticket." % (AlarmDict["RISK"],str(x),plugin_id,plugin_sid))
#           if self.isInRiskTable(mappings[str(x)],plugin_id,plugin_sid):
#             return True
#
#         # At this point the alarm hasn't been specified in an equal or lower alarm priority. Now
#         # check that it is at least greater than the default alarm priority, if so further check that it is
#         # Not specificed in a higher priority that would override the default priority.
#         if int(AlarmDict["RISK"]) >= mappings["DEFAULT"]:
#           for y in xrange(int(AlarmDict["RISK"])+1,11):
#             self.logDebug("isAlarmable", "Now checking risk dictionary %s for (%s,%s) to see if it should not create a ticket." % (AlarmDict["RISK"],plugin_id,plugin_sid))
#             if self.isInRiskTable(mappings[str(x)],plugin_id,plugin_sid):
#               return False
#           return True
#           
#
#         # If it makes it here it is neither specified in the RISK dicitionaries nor higher than the default
#         # ticket risk level.  This alarm should not be a ticket
#         return False
#    except Exception, e:
#      print e
#      return False

  def isInRiskTable(self, riskdictionary, plugin_id, plugin_sid):
    if plugin_id in riskdictionary:
      if plugin_sid in riskdictionary[plugin_id]:
        logging.debug("Plugin ID %s, SubID: %s is in dictionary." % (plugin_id,plugin_sid))
        #self.logDebug("isInRiskTable", "Plugin ID %s, SubID: %s is in dictionary." % (plugin_id,plugin_sid))
        return True
    return False

  def getPluginAlertOptions(self,riskdictionary, plugin_id, plugin_sid):
    try:
      return riskdictionary[plugin_id][plugin_sid]
    except:
      return {}


#  def __del__(self):
#    self.stopSuppressionThread()

  ################NEW STUFF
  def isAlarmTicketable(self, AlarmObject):
    mappings = self.TC.getRiskMappings()
    plugin_id = str(AlarmObject.PluginID)
    plugin_sid = str(AlarmObject.PluginSID)
    risk = str(AlarmObject.Risk)
    try:
       if not self.DeviceFilterModule is None:
         if not self.DeviceFilterModule.isLicensed( srcip = str(AlarmObject.SourceIP), dstip = str(AlarmObject.DestinationIP)):
           return False

       # Check to see if alarm is in suppression status, if it is stop a ticket can't be created. Currently this is after the risk dictionary check.


       # First check to make sure the alarm is not explicitly blacklisted from ticketing.
       if self.isInRiskTable(mappings["NO TICKETS"],plugin_id,plugin_sid):
         return False
       else:
         # Next check all risk levels equal to or lower, if found ticket should be created.
         for x in xrange(1, int(risk)+1):
           #self.logDebug("isAlarmTicketable", "Now checking risk dictionary %s for (%s,%s) to see if it should create a ticket." % (str(x),plugin_id,plugin_sid))
           logging.debug("Now checking risk dictionary %s for (%s,%s) to see if it should create a ticket." % (str(x),plugin_id,plugin_sid))
           if self.isInRiskTable(mappings[str(x)],plugin_id,plugin_sid):
             #self.logDebug("isAlarmTicketable", "Plugin (%s,%s) was specified manually at a risk level and matched." % (plugin_id,plugin_sid))
             logging.debug("Plugin (%s,%s) was specified manually at a risk level and matched." % (plugin_id,plugin_sid))
             # Added the suppression engine check here for now. I'd like this to be one of the first things ideally.
             options_dict = self.getPluginAlertOptions(mappings[str(x)],plugin_id,plugin_sid)
             AlarmObject.setExpand(options_dict["EXPAND"])
             return not self.SuppressionEngine.isSuppressed(AlarmObject, Dynamic = options_dict["DYNAMIC"], Expand = options_dict["EXPAND"])
             #return True

         # At this point the alarm hasn't been specified in an equal or lower alarm priority. Now
         # check that it is at least greater than the default alarm priority, if so further check that it is
         # Not specificed in a higher priority that would override the default priority.
         if int(risk) >= mappings["DEFAULT"]:
           for y in xrange(int(risk)+1,11):
             #self.logDebug("isAlarmTicketable", "Now checking risk dictionary %s for (%s,%s) to see if it should not create a ticket." % (risk,plugin_id,plugin_sid))
             logging.debug("Now checking risk dictionary %s for (%s,%s) to see if it should not create a ticket." % (risk,plugin_id,plugin_sid))
             if self.isInRiskTable(mappings[str(x)],plugin_id,plugin_sid):
               return False
           # Don't forget to check the suppression engine here as well for now.
           return not self.SuppressionEngine.isSuppressed(AlarmObject)


         # If it makes it here it is neither specified in the RISK dicitionaries nor higher than the default
         # ticket risk level.  This alarm should not be a ticket
         return False
    except Exception, e:
      logging.exception("ERROR in alarm processing: %s." % (e))
      #self.logDebug("isAlarmTicketable", "ERROR in alarm processing: %s." % (e))
      return False


#from TicketConfig import TicketConfig
#y = TicketConfig()
#mappings = y.getRiskMappings()
#x = AlarmProcessing(TicketConfig(), debug = True)
#print x.isInRiskTable(mappings["1"],"1505","27004")
#print x.getPluginAlertOptions(mappings["1"],"1505","27004")
#print x.addAlarmToSuppressionQueue("1505","50000", delayInSeconds=200)
#print x.addAlarmToSuppressionQueue("1505","50000", delayInSeconds=900)
#print x.addAlarmToSuppressionQueue("1505","50000", delayInSeconds=200)
#print x.addAlarmToSuppressionQueue("1505","50001", delayInSeconds=300)
#print x.addAlarmToSuppressionQueue("1505","50002", delayInSeconds=500)
#print x.addAlarmToSuppressionQueue("1505","50000", delayInSeconds=200)
#print x.addAlarmToSuppressionQueue("1505","50012", delayInSeconds=200)
#print x.addAlarmToSuppressionQueue("1505","50003", delayInSeconds=200)
#print x.addAlarmToSuppressionQueue("1505","500045", delayInSeconds=200)
#print x.addAlarmToSuppressionQueue("1505","500231423", delayInSeconds=900)
#print x.addAlarmToSuppressionQueue("1505","50000", delayInSeconds=2)
#sleep(10)
#x.stopSuppressionThread()
#  @constant
#  def ALARM_RESULT():
#        return { "SECTION": "STI", "KEY":"DEBUG","DEFAULT":"True"}
#  def 
