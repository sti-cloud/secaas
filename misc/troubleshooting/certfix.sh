#!/bin/bash

# stop services
echo "Stopping OpenVAS services gracefully...."
service openvas-scanner stop
service openvas-manager stop

echo "Waiting 4 seconds...."
sleep 4

echo "Forcefully killing any remaining OpenVAS services"
pkill -9 openvassd
pkill -9 openvasmd

# You must run this on all sensors and AIO.
# Adjust cert lifetime (v5.5.0) to 1460 days
sed -i /usr/sbin/openvas-mkcert -e "s/SRVCERT_LIFETIME=\"365\"/SRVCERT_LIFETIME=\"1460\"/"
sed -i /usr/sbin/openvas-mkcert-client -e "s/default_days.*=.*/default_days = 1460/"
sed -i /usr/sbin/openvas-mkcert-client -e "s/^DFL_CERT_LIFETIME=.*$/DFL_CERT_LIFETIME=\$\{x\:\-1460\}/"

# Recreate certs
openvas-mkcert -f -q

openvas-mkcert-client -n -i

# Install scanner key
scannerid=$(openvasmd --get-scanners | grep Default | head -n 1 | cut -d ' ' -f1)

openvasmd --modify-scanner $scannerid --scanner-ca-pub /var/lib/openvas/CA/cacert.pem --scanner-key-pub /var/lib/openvas/CA/clientcert.pem --scanner-key-priv /var/lib/openvas/private/CA/clientkey.pem

# Restart services
service openvas-manager start
service openvas-scanner start


# After the scanner is reloaded, rebuild the NVT cache

# Wait until the scanner is done reloading NVTs
while [ $(ps aux | grep openvassd | grep Reloaded | wc -l) -ne 0 ]
do
    echo "Waiting 3 secs for the OpenVas scanner to reload..."
    sleep 3
done

openvasmd --rebuild --verbose --progress

# Additional manual checks to make sure the service is working

echo "####You should see OK in the two items below####"
OPENVAS_DIR=/var/lib/openvas
openssl verify -CAfile $OPENVAS_DIR/CA/cacert.pem $OPENVAS_DIR/CA/clientcert.pem
openssl verify -CAfile $OPENVAS_DIR/CA/cacert.pem $OPENVAS_DIR/CA/servercert.pem

echo "####Verify that the cert below matches the one in the database####"
cat $OPENVAS_DIR/CA/cacert.pem

echo "####Database Cert####"
sqlite3 $OPENVAS_DIR/mgr/tasks.db "select uuid, ca_pub from scanners"


echo "Make sure openvassd process actually restarted.  The start time should be today"
ps -eo pid,lstart,cmd | grep openvas

# Uncomment to run database checks on vulnerability related tables only.  Must have password read for input.
# mysqlcheck  -u root -p -c alienvault vuln_hosts vuln_job_assets vuln_job_schedule vuln_jobs vuln_nessus_category vuln_nessus_category_feed vuln_nessus_family vuln_nessus_family_feed vuln_nessus_latest_reports vuln_nessus_latest_results vuln_nessus_plugins vuln_nessus_plugins_feed vuln_nessus_preferences vuln_nessus_preferences_defaults vuln_nessus_report_stats vuln_nessus_reports vuln_nessus_results vuln_nessus_servers vuln_nessus_settings vuln_nessus_settings_category vuln_nessus_settings_family vuln_nessus_settings_plugins vuln_nessus_settings_preferences