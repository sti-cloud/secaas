if [ -z "$1" -o -z "$2" ]; then
  echo "Specify the numberic plugin ID for the first parameter and numeric plugin SID for the second parameter to delete"
  echo "execute as $0 <plugin_id> <subplugin_id> | ossim-db"
  exit 1
fi

cat << EOF
START TRANSACTION;

# Get the event ID and backlog ID of alarms that need to be removed
CREATE TEMPORARY TABLE IF NOT EXISTS mytmptable AS
    (SELECT backlog_event.event_id as event_id, backlog_event.backlog_id as backlog_id
        FROM alarm, backlog_event
        WHERE alarm.backlog_id = backlog_event.backlog_id
            AND alarm.plugin_id = $1
            AND alarm.plugin_sid = $2
    );

# Remove the associated events
DELETE e FROM event e, mytmptable t WHERE e.id=t.event_id;

# Remove the associated backlog entry
DELETE b FROM backlog b, mytmptable t WHERE b.id=t.backlog_id;

# Remove the associated backlog events
DELETE backlog_event
FROM backlog_event INNER JOIN mytmptable
    ON backlog_event.backlog_id = mytmptable.backlog_id AND
        backlog_event.event_id = mytmptable.event_id;

# Remove the alarms
DELETE alarm
FROM alarm INNER JOIN mytmptable
    ON alarm.backlog_id = mytmptable.backlog_id AND
        alarm.event_id = mytmptable.event_id;

# Clean up
DROP TEMPORARY TABLE mytmptable;

# Clean up orphans
DELETE tg FROM component_tags tg LEFT JOIN alarm a ON tg.id_component = a.backlog_id, tag ta WHERE ta.id=tg.id_tag AND ta.type='alarm' AND a.backlog_id IS NULL;
DELETE ac FROM alarm_ctxs ac LEFT JOIN alarm a ON ac.id_alarm = a.backlog_id WHERE a.backlog_id IS NULL;
DELETE ah FROM alarm_hosts ah LEFT JOIN alarm a ON ah.id_alarm = a.backlog_id WHERE a.backlog_id IS NULL;
DELETE an FROM alarm_nets an LEFT JOIN alarm a ON an.id_alarm = a.backlog_id WHERE a.backlog_id IS NULL;
DELETE idm FROM idm_data idm LEFT JOIN event e ON idm.event_id = e.id WHERE e.id IS NULL;
DELETE otx FROM otx_data otx LEFT JOIN event e ON otx.event_id = e.id WHERE e.id IS NULL;
DELETE ed FROM extra_data ed LEFT JOIN event e ON ed.event_id = e.id WHERE e.id IS NULL;
DELETE a FROM alarm a LEFT JOIN event e ON a.event_id = e.id WHERE e.id IS NULL and a.plugin_id = $1 and a.plugin_sid = $2;

COMMIT;
EOF