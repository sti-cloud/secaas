#!/bin/bash
ROWS=${1:-1000000}
echo "Going to check everything less than $ROWS rows....."
rm /var/lib/mysql-files/sti_mysqlcheck_everything.sh
PASS=`cat /etc/ossim/ossim_setup.conf | grep "^pass=" | cut -d "=" -f 2`
echo 'SELECT
     concat("mysqlcheck  -u root --password=$PASS -c alienvault ", table_schema,".",table_name,";")
    FROM information_schema.TABLES
    WHERE table_schema <> "information_schema" AND table_schema <> "performance_schema" AND table_rows < 1000000
    ORDER BY (data_length + index_length) ASC
    into outfile "/var/lib/mysql-files/sti_mysqlcheck_everything.sh";' | ossim-db

sed -i "s/\$PASS/$PASS/g" /var/lib/mysql-files/sti_mysqlcheck_everything.sh

chmod +x /var/lib/mysql-files/sti_mysqlcheck_everything.sh
/bin/bash /var/lib/mysql-files/sti_mysqlcheck_everything.sh