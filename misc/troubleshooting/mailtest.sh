#!/bin/bash
DOMAIN=""
TO="soc@sentinel.com"

helpscreen () {
  echo "$0 [-d <domain>] [-t <email_address>]"
  echo "-d <cust_domain>   - The customer base domain, will be used to compose"
  echo "                     the from address."
  echo "-t <email_address> - Email to send test message to."
  echo "--------------------------------------------------------------------"
  exit 1
}
# Make sure that an option is specified.
if [ -z "$1" ]; then
  helpscreen >&2
fi
# Parse out options
while getopts ":d:t:" opt; do
  case $opt in
    d)
      echo "-$opt was triggered, Parameter: $OPTARG" >&2
      DOMAIN="$OPTARG"
      ;;
    t)
      echo "-$opt was triggered, Parameter: $OPTARG" >&2
      TO="$OPTARG"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      helpscreen >&2
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      helpscreen >&2
      ;;
    h)
      helpscreen >&2
      ;;
    *)
      helpscreen >&2
      ;;
  esac
done

# Insurance the domain field is required.
if [ -z "$DOMAIN" ]; then
  helpscreen >&2
fi

FROM="siem@$DOMAIN"
SUBJECT="Mailserver Relay Test for $DOMAIN"
BODY="This is a test email to validate email communication and setup with Sentinel's SecuritySelect SIEM."
MAILFILE="/var/spool/mail/root"

rm -f $MAILFILE
mail -v -s "$SUBJECT" -aFrom:$FROM "$TO" <<< "$BODY"
touch $MAILFILE
echo "Once you see the action, press CTRL+C to quit!"
tail -f $MAILFILE | egrep "^(From:|Action:|To:|Status:|Diagnostic-Code:)"