#!/bin/bash
re='^[0-9]+$'
filter=""
helpscreen () {
  echo "$0 [-p <plugin_id>] [-s <plugin_sid>] [-c]"
#  echo "-p <plugin_id>  - Delete alarms that match the plugin_id."
#  echo "-s <plugin_sid> - Delete alarms that match the plugin_sid (sub ID)."
  echo "-c              - Show row counts of each table."
  echo "-d              - Query database for unused space including orphaned delete operations."
  echo "-a              - Show event break down of acid_event table (can take 10 mins to run)."
  echo "-z              - Try to optimize database tables automatically."
  echo "-r              - Same as -a, but add event descriptions."
  echo "-l              - Show long running queries."
  echo "-o              - Clean orphans event tables (takes a long time)."
  echo "-q              - Quick clean events older than 60 days. Targets bulk events in chunks."
  echo "-e              - Extensive clean of events older than 60 days."
  echo "-f              - Show problematic correlations."
  echo "-----------------------------------------------------------------------"
  echo "For an Alarm to be deleted it must match all options when multiple"
  echo "options are specified."
  exit 1
}

getUnixTimeStamp() {
  echo `/bin/date +%s`
}

showTableSize() {
  echo 'SELECT
     rpad(table_schema,18, " ") AS `--Database Name--`, rpad(table_name, 30, " ") as `---Table Name---`,
     rpad(round(((data_length + index_length) / 1024 / 1024), 0), 8, " ") `--Table Size MB--`, rpad(round(((data_free) / 1024 / 1024), 0), 8, " ") `--Empty Used Space--`
    FROM information_schema.TABLES
    WHERE round(((data_length + index_length) / 1024 / 1024), 0) > 1000 OR round(((data_free) / 1024 / 1024), 0) > 499
    ORDER BY (data_length + index_length) DESC;' | ossim-db
}

showTokuSize() {
  echo 'SELECT table_name, table_dictionary_name, round(bt_size_allocated/1024/1024,2) as size_allocated, round(bt_size_in_use/1024/1024,2) as size_in_use, round((bt_size_allocated-bt_size_in_use)/1024/1024,2) as size_free FROM information_schema.`TokuDB_fractal_tree_info`;' | ossim-db
}

showTotalTableFreeSpace() {
  echo 'SELECT CONCAT(round((SUM(data_free) / 1024 / 1024), 2), " MB of free space being consumed by AlienVault") ``
    FROM information_schema.TABLES;' | ossim-db
}

showTotalOrphanedFreeSpace() {
  echo 'SELECT CONCAT(round((SUM(data_length + index_length) / 1024 / 1024), 2), " MB of space being consumed by orphaned deletes") ``
    FROM information_schema.TABLES
    WHERE table_name LIKE "del\_%";' | ossim-db
}

showOrphanedTables() {
  echo "######################################################################################"
  echo "# Orphaned table size on disk in megabytes (MB)                                      #"
  echo "#                                                                                    #"
  echo "# WARNING:                                                                           #"
  echo "# This program does not determine if these tables are orphaned. AlienVault format    #"
  echo "# is: del_<unix timestamp>, therefore if more than one table exists in this list all #"
  echo "# but the one with the largest number is likely orphaned.  To be absolutely sure you #"
  echo "# should cross check the table names with any that are actively used in queries.     #"
  echo "# Also, if the unix timestamp represents today or yesterday there is a high          #"
  echo "# probability that it is actively being used.                                        #"
  echo "######################################################################################"
  echo 'SELECT
     rpad(table_schema,18, " ") AS `---Database Name---`, rpad(table_name, 16, " ") as `---Table Name---`,
     rpad(round(((data_length + index_length) / 1024 / 1024), 0), 10, " ") `---MB---`, TABLE_ROWS as `---Row Count---`
    FROM information_schema.TABLES
    WHERE table_name LIKE "del\_%"
    ORDER BY (data_length + index_length) DESC;' | ossim-db
  echo ""

  echo "######################################################################################"
  echo "# The following bash code can be copied and pasted into the shell to delete these    #"
  echo "# tables if they are in fact orphaned. However, the operation cannot be undone so be #"
  echo "# sure you know what you are doing!                                                  #"
  echo "######################################################################################"
  echo "SELECT CONCAT( 'echo \"DROP TABLE ', GROUP_CONCAT(CONCAT(table_schema,'.',table_name)) , ';\" | ossim-db' ) AS '' FROM information_schema.tables      WHERE table_name LIKE 'del\_%';" | ossim-db
}

generateDeleteProcesses() {
  sessionFilename="/var/lib/mysql-files/sti_optimize_$(getUnixTimeStamp).txt"
  echo 'select concat("KILL ",Id,";") as PID from INFORMATION_SCHEMA.PROCESSLIST where Time > 86400 and command = "Query" and Info NOT LIKE "%DELETE%" into outfile "'$sessionFilename'";' | ossim-db
  echo ""
  echo ""
  echo "*** Generated script to kill all these ***"
  cat $sessionFilename
  rm -f "$sessionFilename"
}

compressTables() {
  echo "The following tables will be analyzed for compression and optimization and smaller tables will be done first in order to use space savings in the larger operations:"
  showTableSize
  sessionFilename="/var/lib/mysql-files/sti_optimize_$(getUnixTimeStamp).txt"

  echo 'SELECT
     concat("OPTIMIZE TABLE ", table_schema,".",table_name,";")
    FROM information_schema.TABLES
    WHERE round(((data_free) / 1024 / 1024), 0) > 499
    ORDER BY (data_length + index_length) ASC
    into outfile "'$sessionFilename'";' | ossim-db

  echo ""
  echo ""
  echo "Generated the script $sessionFilename, CONTENTS:"
  cat $sessionFilename
  echo ""
  echo ""
  echo "Now performing optimizations....Depending upon database sizes this can take a long time and require a lot of space."
  echo "source $sessionFilename;" | ossim-db
  rm -f "$sessionFilename"
}

quickCleanPlugin() {
  TempTableName="tmpQuickCleanTable"
  SEARCHCRITERA=""
  DB="alienvault_siem"
  pid=${1:-0}
  sid=${2:-0}
  days=${3:-60}
  echo "Now quick cleaning problem tables for PID: $pid, SID: $sid, Days: $days"
# echo 'DELETE from alienvault_siem.acid_event where plugin_id = '7044' and plugin_sid = '18103' AND (timestamp <= NOW() - INTERVAL 60 DAY);'
  if [[ $pid -gt 0 ]]; then
    SEARCHCRITERA="$SEARCHCRITERA and plugin_id = '$pid'"
  fi

  if [[ $sid -gt 0 ]]; then
    SEARCHCRITERA="$SEARCHCRITERA and plugin_sid = '$sid'"
  fi

  echo "
    CREATE TEMPORARY TABLE $TempTableName SELECT id FROM $DB.acid_event b WHERE timestamp <= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL $days DAY))$SEARCHCRITERA LIMIT 200000000;

    ALTER TABLE $TempTableName ADD PRIMARY KEY (id);

    DELETE IGNORE FROM $DB.idm_data USING $DB.idm_data INNER JOIN $TempTableName ON $DB.idm_data.event_id = $TempTableName.id;

    DELETE IGNORE FROM $DB.reputation_data USING $DB.reputation_data INNER JOIN $TempTableName ON $DB.reputation_data.event_id = $TempTableName.id;

    DELETE IGNORE FROM $DB.extra_data USING $DB.extra_data INNER JOIN $TempTableName ON $DB.extra_data.event_id = $TempTableName.id;

    DELETE IGNORE FROM $DB.acid_event USING $DB.acid_event INNER JOIN $TempTableName ON $DB.acid_event.id = $TempTableName.id;

    DROP TABLE $TempTableName;
  " | ossim-db

}

cleanTable() {
  TempTableName="tmpCleanTable"
  DB=$1
  TableToClean=$2
  FQTableName="$DB.$TableToClean"
  echo "Now cleaning table $FQTableName..."
  echo "
    CREATE TEMPORARY TABLE $TempTableName SELECT event_id FROM $FQTableName b LEFT OUTER JOIN alienvault_siem.acid_event a ON b.event_id = a.id WHERE a.id IS NULL;

    ALTER TABLE $TempTableName ADD PRIMARY KEY (event_id);

    DELETE FROM $FQTableName WHERE event_id IN (SELECT event_id FROM $TempTableName);

    DROP TABLE $TempTableName;
  " | ossim-db
}

acidEventStats() {
  TempTableName="tmpAcidStats"
  echo "
    CREATE TEMPORARY TABLE $TempTableName SELECT plugin_id, plugin_sid, count(plugin_sid) as count from alienvault_siem.acid_event group by plugin_id, plugin_sid order by count;
    SELECT rpad(a.plugin_id,8,' ') as PluginID, rpad(a.plugin_sid,15,' ') as PluginSID, rpad(a.count,10,' ') as Count, rpad(b.name,30,' ') as EventName FROM $TempTableName a, alienvault.plugin_sid b WHERE a.plugin_id = b.plugin_id AND a.plugin_sid = b.sid order by a.count;
    DROP TABLE $TempTableName;
  " | ossim-db
}


# Make sure that an option is specified.
if [ -z "$1" ]; then
  helpscreen >&2
fi

# Make sure the first argument has a - then a letter
if ! [[ "$1" =~ ^\-[a-zA-Z]+$ ]]; then
  helpscreen >&2
fi

# Parse out options
while getopts ":p:s:croadqelzf" opt; do
  case $opt in
    p)
      if ! [[ $OPTARG =~ $re ]]; then
        echo "-$opt was specified, but parameter $OPTARG is not a number!" >&2
        helpscreen >&2
        exit 1
      else
        #echo "-$opt was triggered, Parameter: $OPTARG" >&2
        filter+=" AND alarm.plugin_id = $OPTARG"
      fi
      ;;
    s)
      if ! [[ $OPTARG =~ $re ]]; then
        echo "-$opt was specified, but parameter $OPTARG is not a number!" >&2
        helpscreen >&2
        exit 1
      else
        #echo "-$opt was triggered, Parameter: $OPTARG" >&2
        filter+=" AND alarm.plugin_sid = $OPTARG"
      fi
      ;;
    c)
      #echo "-c was triggered, Parameter: $OPTARG" >&2
      #filter+=" AND alarm.status = 'closed'"
      echo -e "select rpad(table_schema,18, ' ') AS DatabaseName, rpad(table_name, 30, ' ') as TableName, table_rows as RowCount from information_schema.tables where table_rows > 10000 order by table_rows;" | ossim-db
      exit 1
      ;;
    d)
      showTotalTableFreeSpace
      showTotalOrphanedFreeSpace
      echo ""
      echo ""
      echo "######################################################################################"
      echo "# Large tables are concerning because they increase data access times.  Tables with  #"
      echo "# a significant amount of \"Empty Used Space\" are also consuming disk space without   #"
      echo "# any value. Other Apps and tables cannot use this space. Some free space is desired #"
      echo "# for performance reasons and highly fluid tables should contain even more.          #"
      echo "# Optimizing the tables should eliminate excess free space but requires free disk    #"
      echo "# equal to the table size - the free space in order to complete the optimization     #"
      echo "# successfully.  The mysql command is: OPTIMIZE TABLE <database>.<table_name>        #"
      echo "# ** ALL SIZES ARE IN MEGABYTES (MB) **                                              #"
      echo "######################################################################################"
      showTableSize
      echo ""
      echo ""
      showOrphanedTables
      exit 1
      ;;
    a)
      echo "select rpad(plugin_id,8,' ') as PluginID, rpad(plugin_sid,15,' ') as PluginSID, count(plugin_sid) as Count from alienvault_siem.acid_event group by plugin_id, plugin_sid order by Count;" | ossim-db
      exit 1
      ;;
    z)
      compressTables
      exit 1
      ;;
    q)
      #echo 'DELETE from alienvault_siem.acid_event where plugin_id = '21001' and plugin_sid = '1' AND (timestamp <= NOW() - INTERVAL 60 DAY);' | ossim-db
      #echo 'DELETE from alienvault_siem.acid_event where plugin_id = '21000' and plugin_sid = '1' AND (timestamp <= NOW() - INTERVAL 60 DAY);' | ossim-db
      #echo 'DELETE from alienvault_siem.acid_event where plugin_id = '7044' and plugin_sid = '18103' AND (timestamp <= NOW() - INTERVAL 60 DAY);' | ossim-db
      quickCleanPlugin 19005
      quickCleanPlugin 21001 1
      quickCleanPlugin 21000 1
      quickCleanPlugin 7044 18103 60
      exit 1
      ;;
    e)
     echo 'DELETE from alienvault_siem.acid_event where timestamp <= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 60 DAY));' | ossim-db
     ;;
    r)
      acidEventStats
      exit 1
      ;;
    f)
      echo "select alienvault_siem.ac_acid_event.plugin_id, alienvault_siem.ac_acid_event.plugin_sid, count(alienvault_siem.ac_acid_event.plugin_sid) as entries, plugin_sid.name FROM alienvault_siem.ac_acid_event, alienvault.plugin_sid WHERE alienvault_siem.ac_acid_event.plugin_id = alienvault.plugin_sid.plugin_id AND alienvault_siem.ac_acid_event.plugin_sid = alienvault.plugin_sid.sid group by alienvault_siem.ac_acid_event.plugin_id, alienvault_siem.ac_acid_event.plugin_sid HAVING entries > 99999 ORDER BY entries;" | ossim-db
      exit 1
      ;;
    o)
      cleanTable "alienvault_siem" "extra_data"
      cleanTable "alienvault_siem" "idm_data"
      cleanTable "alienvault_siem" "reputation_data"
      exit 1
      ;;
    l)
      echo "select Id as PID, Time as RunTimeInSecs, Info, State from INFORMATION_SCHEMA.PROCESSLIST where Time > 86400 and command = 'Query' and Info NOT LIKE '%DELETE%';" | ossim-db
      generateDeleteProcesses
      exit 1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      helpscreen >&2
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      helpscreen >&2
      ;;
    h)
      helpscreen >&2
      ;;
    *)
      helpscreen >&2
      ;;
  esac
done

# Insurance the filter should never be empty.
if [ -z "$filter" ]; then
  helpscreen >&2
fi

echo "The following filter will be applied: $filter"
