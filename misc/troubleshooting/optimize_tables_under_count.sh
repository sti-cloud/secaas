#!/bin/bash

rm /var/lib/mysql-files/sti_optimize_everything.txt
echo 'SELECT
     concat("OPTIMIZE TABLE ", table_schema,".",table_name,";")
    FROM information_schema.TABLES
    WHERE table_schema <> "information_schema" AND table_schema <> "performance_schema" AND table_rows < 1000000
    ORDER BY (data_length + index_length) ASC
    into outfile "/var/lib/mysql-files/sti_optimize_everything.txt";' | ossim-db

echo "source /var/lib/mysql-files/sti_optimize_everything.txt;" | ossim-db