#!/usr/bin/python
import ConfigParser
from io import StringIO
import MySQLdb
from datetime import datetime
import time
import logging
import collections
from contextlib import closing

class DB:
  def __init__(self, debug = True):
    self.DEBUG = debug

  def logDebug(self, method, log_entry):
    if self.DEBUG:
      print "%s - %s(%s): %s" % (str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")),self.__class__,str(method),str(log_entry))
      logging.debug("DB(%s): %s" % (method,log_entry))

  # This is the new generic query.  Trying to make sure everything closes right in extremely high performance situations.
  def genericQuery(self, queryString):
    db = None
    results = None
    AVCONFIG = AlienVaultConfig()

    while True:
      try:
        db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)

        logging.debug("Executing SQL command: %s" % queryString)
        with closing( db.cursor(MySQLdb.cursors.DictCursor) ) as cursor:
          cursor.execute("%s" % (queryString))
          results = cursor.fetchall()
        return results
      except MySQLdb.OperationalError, e:
        # An operational error generally includes "too many connections" thus this may not be a database problem but rather
        # a code issue. We will attempt to retry the operation.
        logging.warning("Operational Error in executing query: %s  -- ERROR DETAILS: %s" % (queryString,e))
        time.sleep(30)
      except Exception, e:
        logging.exception("Error in executing query: %s  -- ERROR DETAILS: %s" % (queryString,e))
        return results
      finally:
        try:
          db.close()
        except:
          pass


class AlienVaultConfig:

  def __init__(self, OSSIM_CONFIG_FILE='/etc/ossim/ossim_setup.conf', database = 'alienvault'):
    self.OSSIM_FILENAME = OSSIM_CONFIG_FILE
    self.DB = database
    db_config = ConfigParser.ConfigParser()
    vfile = StringIO(u'[misc]\n%s'  % open(OSSIM_CONFIG_FILE).read())
    db_config.readfp(vfile)
    self.DB_IP=db_config.get("database", "db_ip")
    self.DB_USER=db_config.get("database", "user")
    self.DB_PASSWD=db_config.get("database", "pass")
    self.TZONE=db_config.get("sensor", "tzone")

  def fset(self, value):
    return TypeError
  def fget(self):
    return self.__attr__

  DB_IP = property(fget,fset)
  DB_USER = property(fget,fset)
  DB_PASSWD = property(fget,fset)
  DB = property()
  TZONE = property(fget,fset)


def printSimpleCounts(myArray):
  for item in collections.Counter(myArray).items():
    print '{ColumnName:40}{Count:10}'.format(ColumnName=item[0],Count=item[1])

def printHeading(Message, Length):
  print ''
  print '-'.ljust(Length, '-')
  print '- ' + Message.ljust(Length-3, ' ') + '-'
  print '-'.ljust(Length, '-')

# This dictionary quickly categorizes plugins according to contracts.  Just add an entry.
DevicesToContractMapping = {
  "cisco-asa": "Firewalls",
  "cisco-wlc": "Wireless LAN Controllers",
  "cisco-amp": "Centralized Endpoint Integrations",
  "symantec-epm": "Centralized Endpoint Integrations",
  "websense7": "Web Filtering Devices",
  "cisco-ise": "Network Access Control Devices",
  "Websense ESG": "Email Filtering Devices",
  "fireeye": "Intrusion Detection/Prevention Devices",
  "cisco-defensecenter": "Intrusion Detection/Prevention Devices",
  "cisco-router": "Router/Switch",
  "cisco-nexus-nx-os": "Router/Switch"
}

#PER_DEVICE_PLUGIN_QUERY="select INET_NTOA(CONV(hex(host_ip.ip),16,10)) as IP, sensor.name as Sensor, plugin.name as PluginName, plugin.description as PluginDescription, product_type.name as ProductType, plugin.vendor as ProductVendor from host_scan, plugin, host_ip, product_type, host_sensor_reference, sensor where plugin.id=host_scan.plugin_id AND host_scan.host_id=host_ip.host_id AND host_scan.host_id=host_sensor_reference.host_id AND host_sensor_reference.sensor_id=sensor.id AND plugin.product_type=product_type.id;"
PER_DEVICE_PLUGIN_QUERY="select hex(host_scan.host_id), INET_NTOA(CONV(hex(host_ip.ip),16,10)) as IP, plugin.name as PluginName, plugin.description as PluginDescription, plugin.vendor as ProductVendor, product_type.name as ProductType from host_scan LEFT JOIN host_ip ON host_scan.host_id=host_ip.host_id LEFT JOIN plugin ON host_scan.plugin_id=plugin.id LEFT JOIN product_type ON plugin.product_type=product_type.id;"
myDB = DB(True)

results=myDB.genericQuery(PER_DEVICE_PLUGIN_QUERY)
device_ips = []
device_types= []
contract_types=[]
null_ip_configurations=[]
print '{IP:16}{ProductVendor:20}{PluginName:25}{ProductType:35}{PluginDescription}'.format(ProductVendor="Product Vendor",PluginName="Plugin Name",IP="IP",PluginDescription='Plugin Description',ProductType='Product Type')
for key in results:
   print '{IP:16}{ProductVendor:20}{PluginName:25}{ProductType:35}{PluginDescription}'.format(ProductVendor=key["ProductVendor"],PluginName=key["PluginName"],IP=key["IP"],PluginDescription=key['PluginDescription'],ProductType=key['ProductType'])
   if key["IP"] is None:
     null_ip_configurations.append(key["PluginName"])
   else:
     device_ips.append(key["IP"])
     device_types.append(key['PluginName'])
     if key['PluginName'] in DevicesToContractMapping:
       contract_types.append(DevicesToContractMapping[key['PluginName']])
     else:
       contract_types.append(key['PluginName'])


# Print out potential misconfigurations
printHeading("DUPLICATE PLUGIN DETECTION", 50)
print [item for item, count in collections.Counter(device_ips).items() if count > 1]
print "* Misconfigurations should be fixed and then this\nscript ran again. This script assumes no\nmisconfigurations on the server."

# Print potential db issues
printHeading("PLUGINS ASSIGNED TO NON-EXISTENT ASSET", 50)
printSimpleCounts(null_ip_configurations)
print "* Misconfigurations should be fixed.  These\nplugins are EXCLUDED from counts below."

printHeading("DEVICE TYPE COUNTS", 50)
printSimpleCounts(device_types)


printHeading("CONTRACT COUNTS", 50)
printSimpleCounts(contract_types)

# Print out HIDS information
printHeading("HIDS/OSSEC Agents",89)
HIDS_QUERY="select agent_name as AgentName, agent_ip as AgentIP, INET_NTOA(CONV(hex(host_ip.ip),16,10)) as IP, sensor.name as Sensor, agent_status as AgentStatusCode from hids_agents, host_ip, sensor where hids_agents.host_id=host_ip.host_id AND hids_agents.sensor_id=sensor.id AND agent_status <> 4;"
hidsresults=myDB.genericQuery(HIDS_QUERY)
print '{IP:16}{AgentName:40}{Sensor:22}{AgentStatusCode:2}'.format(IP="IP",AgentName="Agent Name",Sensor="Sensor",AgentStatusCode='Status Code')
for key in hidsresults:
   print '{IP:16}{AgentName:40}{Sensor:22}{AgentStatusCode:2}'.format(IP=key["IP"],AgentName=key["AgentName"],Sensor=key["Sensor"],AgentStatusCode=key['AgentStatusCode'])