#!/bin/bash

function monitor_file {
     local LOG_FILE=$1
     local SECS=$2
     local GREP=$3
     local OUTPUT=`(tail -f $LOG_FILE & P=$!; sleep $SECS; kill -9 $P) | grep "$GREP" | wc -l`
     echo $OUTPUT
}


CACHE_ERROR_COUNT=$(monitor_file "/var/log/alienvault/agent/agent.log" 120 "An error has occurred while inserting the events in the cache: file is encrypted or is not a database")
if [ $CACHE_ERROR_COUNT -gt 0 ]; then echo "Cache corruption errors.....fixing"; find /var/ossim/agent_events/ -type f -name "*.db*" -mtime -1 -delete; /etc/init.d/ossim-agent restart; fi