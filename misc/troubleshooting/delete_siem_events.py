import time
import datetime
import re
import sys
import argparse
from subprocess import call
sys.path.insert(0, '/usr/share/ossim-framework/ossimframework')
# These two are in ossimframework
from OssimConf import OssimMiniConf
from OssimConf import OssimConf
from OssimDB import OssimDB
from DBConstantNames import *
from DoWS import *
import Util
ossim_setup = OssimMiniConf(config_file='/etc/ossim/ossim_setup.conf')
 
def delete_events(older_than_date_time, delete):
 
   older_than_date_time = older_than_date_time
   delete = delete
 
   raw_now = datetime.datetime.now()
   now = raw_now.strftime('%Y-%m-%d-%H-%M-%S')
 
   # First get a list of all event ids for events older than the older_than_date
 
   sql_events = "SELECT hex(id) as event_id FROM alienvault_siem.acid_event WHERE timestamp < '%s'" % older_than_date_time
   events_to_delete = db.exec_query(sql_events)
 
   if events_to_delete:
      num_events_to_delete = len(events_to_delete)
 
      print "\nStarting delete events: %s\n" % raw_now
      print "Found %s to delete.\n" % num_events_to_delete
      if delete == 1:
         print "--delete is set to YES so events will be deleted."
      else:
         print "--delete is not set so events will NOT be deleted."
 
      # Take the event_ids from the list and delete them from alienvault_siem.reputation_data, alienvault_siem.otx_data, alienvault_siem.idm_data, alienvault_siem.extra_data and alienvault_siem.acid_event
      outfile = "/tmp/delete_events_%s" % (now)
      writeout = open(outfile, 'a+')
      out = "Starting delete events: %s\n" % raw_now
      writeout.write(out)
      out = "Will delete %s events\n" % num_events_to_delete
      writeout.write(out)
      for event_id in events_to_delete:
         sql_delete = "DELETE e FROM alienvault_siem.reputation_data e WHERE e.event_id = unhex('%s')" % event_id['event_id']
         out = "%s - reputation_data\n" % event_id['event_id']
         writeout.write(out)
         if delete:
            db.exec_query(sql_delete)
 
         sql_delete = "DELETE e FROM alienvault_siem.otx_data e WHERE e.event_id = unhex('%s')" % event_id['event_id']
         out = "%s - otx_data\n" % event_id['event_id']
         writeout.write(out)
         if delete:
            db.exec_query(sql_delete)
 
         sql_delete = "DELETE e FROM alienvault_siem.idm_data e WHERE e.event_id = unhex('%s')" % event_id['event_id']
         out = "%s - idm_data\n" % event_id['event_id']
         writeout.write(out)
         if delete:
            db.exec_query(sql_delete)
 
         sql_delete = "DELETE e FROM alienvault_siem.extra_data e WHERE e.event_id = unhex('%s')" % event_id['event_id']
         out = "%s - extra_data\n" % event_id['event_id']
         writeout.write(out)
         if delete:
            db.exec_query(sql_delete)
 
         sql_delete = "DELETE e FROM alienvault_siem.acid_event e WHERE e.id = unhex('%s')" % event_id['event_id']
         out = "%s - acid_event\n" % event_id['event_id']
         writeout.write(out)
         if delete:
            db.exec_query(sql_delete)
 
      if delete:
         # If we deleted events, recheck and make sure they are gone
         sql_events = "SELECT hex(id) as event_id FROM alienvault_siem.acid_event WHERE timestamp < '%s'" % older_than_date_time
         new_events_to_delete = db.exec_query(sql_events)
         if not new_events_to_delete:
            print "\nDeleted a total of %d events.\n" % num_events_to_delete
            out = "Deleted %d events\n" % num_events_to_delete
            writeout.write(out)
         else:
            not_deleted_outfile = "/tmp/not_deleted_%s" % (now)
            notout = open(not_deleted_outfile, 'a+')
            print "\n%d events were NOT deleted. Check log %s for event IDs.\n" % (len(new_events_to_delete), outfile)
            for event_id in new_events_to_delete:
               out = "%s\n" % event_id['backlog_id']
               notout.write(out)
 
            notout.close()
            print "\n-----------------------------------------------"
            print "The logs of events that were not deleted is in file: %s" % not_deleted_outfile
            print "\n-----------------------------------------------"
 
      else:
         print "\n%d events found that are older than %s. To delete, re-run script with --delete YES.\n" % (len(events_to_delete), older_than_date_time)
 
      raw_now = datetime.datetime.now()
      out = "Finished delete events: %s\n" % raw_now
      print "Finished delete events: %s\n" % raw_now
      writeout.write(out)
      writeout.close()
      print "The log file: %s\n" % outfile
 
   else:
      print "\nDid not find any events older than %s.\n" % (older_than_date_time)
 
 
if __name__ == '__main__':
 
   # Set initial values, these might get changed based on arguments passed in at run time
   delete = 0
 
   parser = argparse.ArgumentParser(description='Delete events from SIEM database using an "older than" date. If run without --delete YES, will\
                                                 log the events found. Log files are output to /tmp.')
   parser.add_argument("--delete", choices=["YES"], help='Pass YES (exactly) to delete the events.')
   parser.add_argument("--older_than_date", required=True, help='The older than date for search in format YYYY-MM-DD in UTC (not local time).')
   args = parser.parse_args()
 
   if args.delete == 'YES':
      delete = 1
 
   if args.older_than_date:
      # Test data and make sure it's valid
      date_regex = "^\\d{4}[-]?\\d{1,2}[-]?\\d{1,2}$"
      date_test = re.match(date_regex, args.older_than_date)
      if date_test:
         older_than_date = args.older_than_date
         older_than_date_time = older_than_date + ' 00:00:00'
      else:
         print "\n--older_than_date is not in format YYYY-MM-DD...exiting.\n"
         sys.exit(2)
 
   # Get config info to connect to DB
   conf = OssimConf()
   db = OssimDB(conf[VAR_DB_HOST], \
                      conf[VAR_DB_SCHEMA], \
                      conf[VAR_DB_USER],
                      conf[VAR_DB_PASSWORD]
                      )
   try:
      # Try to connect to DB
      db.connect()
   except Exception, e:
      call("echo %s >> /tmp/alarm_delete_script_errors" % e, shell=True)
 
   # Assuming we get this far, fuego
 
   delete_events(older_than_date_time, delete)