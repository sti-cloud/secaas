#!/bin/bash
#Copied from http://linoxide.com/linux-shell-script/shell-script-check-linux-system-health/
#Here we put email address to send email with report. If no email provided log file will be just saved.
#EMAIL='alerts@account.com'
EMAIL=''
#We will create function to easily manage what to do with output.
function sysstat {

    OUTFILE=$1

    # Check for A API
    AVAPI=`which alienvault-api`

    #Print header, hostname (hostname command used), Kernel version (uname -r) ,
    #Uptime (from uptime command) and Last reboot time (from who command)


##### Add from MAS script
# Name=Top - Command=/usr/bin/top -b -c -n 1 -w 512

    echo -e "

#################################################################################################

Health Check Report (CPU, Process, Disk Usage, Memory, Connections, Database, EPS)

Version: 1.2, 6 October 2016

#################################################################################################

Date/Time run: `date`
Hostname : `hostname` " | tee -a $OUTFILE
echo -e "
Kernel Version : `uname -r`
Uptime : `uptime | sed 's/.*up \([^,]*\), .*/\1/'`
Last Reboot Time : `who -b | awk '{print $3,$4}'`" >> $OUTFILE

# Gather installation specific information in this section
# About details, assets, EPS (from curl), db events, alarms

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering AlienVault Installation Details..."

echo -e "
AlienVault Installation Details:
`$AVAPI about`
" >> $OUTFILE

LICDEV=`grep devices /etc/ossim/ossim.lic | awk -F'=' '{print $2}'`
if [ "$LICDEV" -eq 0 ]
then
    echo -e "Licensed Assets: Unlimited" >> $OUTFILE
else
    echo -e "Licensed Assets: $LICDEV" >> $OUTFILE
fi

VERDEV=`grep expire /etc/ossim/ossim.lic | awk -F'=' '{print $2}'`
if [ "$VERDEV" == '9999-12-31' ]
then
    echo -e "System license: Professional" >> $OUTFILE
else
    echo -e "System license: Trial, expires $VERDEV" >> $OUTFILE
fi

LASTUPFILE=`ls -tr /var/log/alienvault/update/*update-script*.log | tail -1`
if [ "$LASTUPFILE" == '' ]
then
    echo -e "Last update: No updates applied yet." >> $OUTFILE
else
    UPSTAT=`tail -1 $LASTUPFILE | awk '{print $3}'`

    if [ "$UPSTAT" == 'finished' ]
    then
        UPDTIME=`tail -1 $LASTUPFILE | awk -F"[[:space:]]{1}on[[:space:]]{1}" '{print $2}'`
        echo -e "Last successful update: $UPDTIME" >> $OUTFILE
    else
        echo -e "It appears an update was attempted but might not have succeeded.
                 Check Update file $LASTUPFILE for details." >> $OUTFILE
    fi
fi

DEPTYPE=''
if [ `dpkg -l | egrep '^ii\s+alienvault\-(vmware)'|awk '{print $2}'` ]
then
    DEPTYPE="VMWare"

elif [ `dpkg -l | egrep '^ii\s+alienvault\-(ami)'|awk '{print $2}'` ]
then
    DEPTYPE="Amazon AMI"

elif [ `dpkg -l | egrep '^ii\s+alienvault\-(hw)'|awk '{print $2}'` ]
then
    DEPTYPE="Hardware"

else
   DEPTYPE="Undetermined deployment type"
fi

echo -e "Deployment type: $DEPTYPE " >> $OUTFILE

SOFTPRO=`grep 'profile=' /etc/ossim/ossim_setup.conf | awk -F'=' '{print $2}'`
if [ "$SOFTPRO" == '' ]
then
    echo -e "No software profile configuration found in /etc/ossim/ossim_setup.com" >> $OUTFILE
else
    echo -e "Software profile: $SOFTPRO" >> $OUTFILE
fi

BACKFILE=`ls -tr /var/alienvault/backup/*configuration*.tar.gz | tail -1`
if [ "$BACKFILE" == '' ]
then
    echo -e "Last configuration backup: No backups found" >> $OUTFILE
else
    BACKDATE=`ls -l $BACKFILE | awk '{print $6, $7, $8}'`
    echo -e "Last configuration backup: $BACKDATE" >> $OUTFILE
fi

INSERTDBBACKFILE=`ls -rt /var/lib/ossim/backup/insert*.sql.gz | tail -1`
if [ "$INSERTDBBACKFILE" == '' ]
then
    echo -e "Last database backup for inserts: No backups found" >> $OUTFILE
else
    INSERTDBBACKDATE=`ls -l $INSERTDBBACKFILE | awk '{print $6, $7, $8}'`
    echo -e "Last database backup for inserts: $INSERTDBBACKDATE" >> $OUTFILE
fi

DELETEDBBACKFILE=`ls -rt /var/lib/ossim/backup/delete*.sql.gz | tail -1`
if [ "$DELETEDBBACKFILE" == '' ]
then
    echo -e "Last database backup for deletes: No backups found" >> $OUTFILE
else
    DELETEDBBACKDATE=`ls -l $DELETEDBBACKFILE | awk '{print $6, $7, $8}'`
    echo -e "Last database backup for deletes: $DELETEDBBACKDATE" >> $OUTFILE
fi

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering System Configuration Details..."

echo -e "
System configuration details: " >> $OUTFILE
if [ `echo "select value from config where conf='backup_store';" | ossim-db | grep -v value` -eq 0 ]
then
    SIEMBAK="No"
else
    SIEMBAK="Yes"
fi

echo -e "
Enable SIEM database backup: $SIEMBAK
Number of Backup files to keep in the system: `echo "select value from config where conf='frameworkd_backup_storage_days_lifetime';" | ossim-db | grep -v value`
Events to keep in the Database (Number of days): `echo "select value from config where conf='backup_day';" | ossim-db | grep -v value`
Events to keep in the Dababase (Number of events): `echo "select value from config where conf='backup_events';" | ossim-db | grep -v value`" >> $OUTFILE

ALARMEXP=`echo "select value from config where conf='alarms_expire';" | ossim-db | grep -v value`
echo -e "Alarms expire: $ALARMEXP" >> $OUTFILE
if [ "$ALARMEXP" == 'yes' ]
then
    ALARMSDAYS=`echo "select value from config where conf='alarms_lifetime';" | ossim-db | grep -v value`
    echo -e "Alarms lifetime in days: $ALARMSDAYS" >> $OUTFILE
fi

LOGEXP=`echo "select value from config where conf='logger_expire';" | ossim-db | grep -v value`
echo -e "Logger Expiration: $LOGEXP" >> $OUTFILE
if [ "$LOGEXP" == 'yes' ]
then
    echo -e "Logger expiration days: `echo "select value from config where conf='logger_storage_days_lifetime';" | ossim-db | grep -v value`" >> $OUTFILE
fi

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering USM Visible Systems..."

echo -e "
USM Visible Systems:

`$AVAPI systems` " >> $OUTFILE


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Total Number Of Assets..."

echo -e "
Total number of assets: `echo "use alienvault;select count(*) from host;" | /usr/bin/ossim-db | grep -v count` " >> $OUTFILE


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Total Number Of Networks..."

echo -e "
Total number of networks: `echo "use alienvault;select count(*) from net;" | /usr/bin/ossim-db | grep -v count` " >> $OUTFILE


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering SIEM Event Information..."

echo -e "
Total number of SIEM events in the database: `echo "select count(*) from alienvault_siem.acid_event;" | /usr/bin/ossim-db | grep -v count`

Time of most recent database event: `echo "use alienvault_siem; select timestamp from acid_event order by timestamp desc limit 1;" | ossim-db | grep -v timestamp`

Time of oldest database event: `echo "use alienvault_siem; select timestamp from acid_event order by timestamp asc limit 1;" | ossim-db | grep -v timestamp` " >> $OUTFILE


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Alarm Information..."

echo -e "
Total Alarms: `echo "use alienvault;select count(*) from alarm;" | /usr/bin/ossim-db | grep -v count`
Open Alarms: `echo "use alienvault;select count(*) from alarm where status='open';" | /usr/bin/ossim-db | grep -v count`
Closed Alarms: `echo "use alienvault;select count(*) from alarm where status='closed';" | /usr/bin/ossim-db | grep -v count`

Time of most recent alarm: `echo "use alienvault;select timestamp from alarm order by timestamp desc limit 1;" | ossim-db | grep -v timestamp`

Time of oldest alarm: `echo "use alienvault;select timestamp from alarm order by timestamp asc limit 1;" | ossim-db | grep -v timestamp` " >> $OUTFILE

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Plugin Information..."

echo -e "
Enabled plugins:
" >> $OUTFILE

    GLOBALPLUGS=`grep /plugins /etc/ossim/agent/config.cfg | awk -F"=" '{print $1}'`
    GLOBALTOTAL=`grep /plugins /etc/ossim/agent/config.cfg | awk -F"=" '{print $1}' | wc -l`
    ASSETPLUGINS=''

    if [ -e /etc/ossim/agent/config.yml ]
    then
        ASSETPLUGINS=`grep /plugins /etc/ossim/agent/config.yml | awk -F"/" '{print $6}'| sed 's/.cfg://' | sort | uniq`
        ASSETTOTAL=`grep /plugins /etc/ossim/agent/config.yml | awk -F"/" '{print $6}'| sed 's/.cfg://' | wc -l`
        #ASSETTOTAL=`grep /plugins /etc/ossim/agent/config.yml | awk -F"/" '{print $6}'| sed 's/.cfg://' | sort | uniq | wc -l`
    fi

echo -e "Global plugins - $GLOBALTOTAL
$GLOBALPLUGS
" >> $OUTFILE

    if [ "$ASSETPLUGINS" == '' ]
    then
        ASSETPLUGINS='None'
        ASSETTOTAL=0
    fi

echo -e "Per asset - $ASSETTOTAL
$ASSETPLUGINS " >> $OUTFILE


    TOTALPLUGINS=$((GLOBALTOTAL + ASSETTOTAL))
echo -e "
Total enabled plugins enable on system - $TOTALPLUGINS" >> $OUTFILE


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering EPS Information..."

echo -e "
Total, Average and maximum EPS " >> $OUTFILE

TOTALEPSRAW=`echo "SELECT SUM( stat ) AS eps FROM acl_entities_stats;" | ossim-db | grep -v eps`
TOTALEPS=`printf "%.2f" $TOTALEPSRAW`

echo -e "
Total EPS for server: $TOTALEPS

EPS Per Context: " >> $OUTFILE

# Pull average EPS for all server contexts for 24 hours, 1 week and 1 month
# Uses rrdtool

# Get server_id from DB
SERVER_ID=`echo "select value from config where conf='server_id';" | ossim-db | grep -v value | sed 's/-//g'`

# Pull contexts from DB
#echo "select hex(id), hex(server_id), name, hex(parent_id), entity_type from acl_entities where server_id=unhex('$SERVER_ID') and entity_type='context';" | ossim-db

    for ID in `echo "select hex(id) from acl_entities where server_id=unhex('$SERVER_ID') and entity_type='context';" | ossim-db | grep -v hex`
    do
        CONTEXT=`echo "select name from acl_entities where entity_type='context' and id=unhex('$ID');" | ossim-db | grep -v name`

        CURRENTEPSRAW=`echo "select stat from acl_entities_stats where entity_id=unhex('$ID');" | ossim-db | grep -v stat`
        CURRENTEPS=`printf "%.2f" $CURRENTEPSRAW`

        #Averages
        TOTAL=0
        COUNT=0
        for i in `rrdtool fetch /var/lib/ossim/rrd/event_stats/$ID.rrd AVERAGE -r 300 -s -1d | grep ':' | grep -v nan | awk -F":" '{print $2'} | sed 's/\s//' | awk -F'.' '{print $1}'`
        do
            TOTAL=$((TOTAL + i))
            COUNT=$((COUNT + 1))
        done
        if [ $COUNT -ne 0 ]
        then
                DTOTALAVG=`echo "scale=2;$TOTAL/$COUNT" | bc -l`
        else
                DTOTALAVG=0
        fi

        TOTAL=0
        COUNT=0
        for i in `rrdtool fetch /var/lib/ossim/rrd/event_stats/$ID.rrd AVERAGE -r 1800 -s -1w | grep ':' | grep -v nan | awk -F":" '{print $2'} | sed 's/\s//' | awk -F'.' '{print $1}'`
        do
            TOTAL=$((TOTAL + i))
            COUNT=$((COUNT + 1))
        done
        if [ $COUNT -ne 0 ]
        then
                WTOTALAVG=`echo "scale=2;$TOTAL/$COUNT" | bc -l`
        else
                WTOTALAVG=0
        fi

        TOTAL=0
        COUNT=0
        for i in `rrdtool fetch /var/lib/ossim/rrd/event_stats/$ID.rrd AVERAGE -r 7200 -s -1m | grep ':' | grep -v nan | awk -F":" '{print $2'} | sed 's/\s//' | awk -F'.' '{print $1}'`
        do
            TOTAL=$((TOTAL + i))
            COUNT=$((COUNT + 1))
        done
        if [ $COUNT -ne 0 ]
        then
                MTOTALAVG=`echo "scale=2;$TOTAL/$COUNT" | bc -l`
        else
                MTOTALAVG=0
        fi

        TOTAL=0
        COUNT=0
        for i in `rrdtool fetch /var/lib/ossim/rrd/event_stats/$ID.rrd AVERAGE -r 86400 -s -1y | grep ':' | grep -v nan | awk -F":" '{print $2'} | sed 's/\s//' | awk -F'.' '{print $1}'`
        do
                TOTAL=$((TOTAL + i))
                COUNT=$((COUNT + 1))
        done
        if [ $COUNT -ne 0 ]
        then
                YTOTALAVG=`echo "scale=2;$TOTAL/$COUNT" | bc -l`
        else
                YTOTALAVG=0
        fi

        #Maximums
        MAX=0
        for i in `rrdtool fetch /var/lib/ossim/rrd/event_stats/$ID.rrd MAX -r 300 -s -1d | grep ':' | grep -v nan | awk -F":" '{print $2'} | egrep -o '[0-9]*\.[0-9]{2}'`
        do
            if [ `echo $i '>' $MAX | bc -l` -eq 1 ]
            then
                MAX=$i
            fi
        done
        DMAX=$MAX

        MAX=0
        for i in `rrdtool fetch /var/lib/ossim/rrd/event_stats/$ID.rrd MAX -r 1800 -s -1w | grep ':' | grep -v nan | awk -F":" '{print $2'} | egrep -o '[0-9]*\.[0-9]{2}'`
        do
            if [ `echo $i '>' $MAX | bc -l` -eq 1 ]
            then
                MAX=$i
            fi
        done
        WMAX=$MAX

        MAX=0
        for i in `rrdtool fetch /var/lib/ossim/rrd/event_stats/$ID.rrd MAX -r 7200 -s -1m | grep ':' | grep -v nan | awk -F":" '{print $2'} | egrep -o '[0-9]*\.[0-9]{2}'`
        do
            if [ `echo $i '>' $MAX | bc -l` -eq 1 ]
            then
                MAX=$i
            fi
        done
        MMAX=$MAX

        MAX=0
        for i in `rrdtool fetch /var/lib/ossim/rrd/event_stats/$ID.rrd MAX -r 86400 -s -1y | grep ':' | grep -v nan | awk -F":" '{print $2'} | egrep -o '[0-9]*\.[0-9]{2}'`
        do
            if [ $i != ''  ]
            then
                if [ `echo $i '>' $MAX | bc -l` -eq 1 ]
                then
                        MAX=$i
                fi
            else
                MAX="No data yet"
            fi
        done
        YMAX=$MAX

        echo -e "Context: $CONTEXT
---------------------------------

Current EPS: $CURRENTEPS

Average EPS last day: $DTOTALAVG
Average EPS last week: $WTOTALAVG
Average EPS last month: $MTOTALAVG
Average EPS last year: $YTOTALAVG

Maximum EPS for last day: $DMAX
Maximum EPS for last week: $WMAX
Maximum EPS for last month: $MMAX
Maximum EPS for last year: $YMAX
" >> $OUTFILE
    done

echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering CPU Information..."

echo -e "
CPU Load - > Threshold < 1 Normal > 1 Caution , > 2 Unhealthy " >> $OUTFILE

    #here we check if mpstat command is in our system
    MPSTAT=`which mpstat`

    #here we check in same way if lscpu installed
    LSCPU=`which lscpu`

    echo -e "
CPU `lscpu | grep "Model name"`" >> $OUTFILE

echo -e "
CPU# : Utilization" >> $OUTFILE

    #if we have lscpu installed, we can get number of CPU's on our system and get statistic for each using mpstat command.
    cpus=`lscpu | grep -e "^CPU(s):" | cut -f2 -d: | awk '{print $1}'`

    i=0

    #here we make loop to get and print CPU usage statistic for each CPU.
    while [ $i -lt $cpus ]

        do

        #here we get statistic for CPU and print it. Awk command help to do this, since output doesn't allow this to do with grep. AWK check if third value is equal to variable $i (it changes from 0 to number of CPU), and print %usr value for this CPU
        echo "CPU$i : `mpstat -P ALL | awk -v var=$i '{ if ($2 == var ) print $3 }' `" >> $OUTFILE

        #here we increment $i variable for loop
        let i=$i+1

    done

LOADAVERAGE=`uptime | awk -F'load average:' '{ print $2 }' | cut -f3 -d,`
REALAVERAGE=0
if [ `echo $LOADAVERAGE '==' 0 | bc -l` -eq 0 ]
then
        REALAVERAGE=`echo "scale=2;$LOADAVERAGE/$cpus"|bc -l`
fi
if [ `echo $REALAVERAGE '>=' 2 | bc -l` -eq 1 ]
then
        HEALTHSTATUS="Unhealthy"
elif [ `echo $REALAVERAGE '>=' 1 | bc -l` -eq 1 ]
then
        HEALTHSTATUS="Caution"
else
        HEALTHSTATUS="Normal"
fi

    echo -e "
Load Average Across All CPUs : $REALAVERAGE
Health Status : $HEALTHSTATUS" >> $OUTFILE

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Process Information..."

echo -e "
Processes:
" >> $OUTFILE
    DEFUNCTPROC=`ps aux |grep -i "defunct" | grep -v grep`
    if [ "$DEFUNCTPROC" == '' ]
    then
        echo -e "Defunct processes: None" >> $OUTFILE
    else
        echo -e "Defunct processes:
        `ps aux |grep -i "defunct" | grep -v grep` " >> $OUTFILE
    fi

    echo -e "
Top memory using process/application:
PID %MEM RSS COMMAND
`ps aux | awk '{print $2, $4, $6, $11}' | sort -k3rn | head -n 10`

Top CPU using process/application:
`top b -n1 | head -17 | tail -11`

Processes swap utilization:
Process Swap
`for file in /proc/*/status ; do awk '/VmSwap|Name/{printf $2 " " $3}END{ print ""}' $file ; done | sort -k 2 -n -r`
" >> $OUTFILE
echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Disk Information..."

echo -e "
Disk Usage - > Threshold < 90 Normal > 90% Caution > 95 Unhealthy
    " >> $OUTFILE
    #we get disk usage with df command. -P key used to have postfix like output (there was problems with network shares, etc and -P resolve this problems). We print output to temp file to work with info more than one.
    df -Pkh | grep -v 'Filesystem' > /tmp/df.status

    echo -e "Disk\t\tPartition\tUsed%\tUsed\tFree" >> $OUTFILE

    #We create loop to process line by line from df.status
    while read DISK

    do

        DISKNAME=`echo $DISK | awk '{print $1}'`
        if [ "$DISKNAME" != "tmpfs" ] && [ "$DISKNAME" != "udev"  ] ; then

            #here we get line from df.status and print result formatted with awk command
            #LINE=`echo $DISK | awk '{print $1,"\t",$6,"\t",$5," used","\t",$4," free space"}'`

            PART=`echo $DISK | awk '{print $6}'`
            USEPERCENT=`echo $DISK | awk '{print $5}'`
            AVAIL=`echo $DISK | awk '{print $4}'`
            USED=`echo $DISK | awk '{print $3}'`

            #echo -ne $LINE
            echo -e "$DISKNAME\t$PART\t\t$USEPERCENT\t$USED\t$AVAIL" >> $OUTFILE

            echo >> $OUTFILE
        fi

    done < /tmp/df.status

    #here almost same loop, but we check disk usage, and print Normal if value less 90, Caution if between 90 and 95, and Unhealthy if greater than 95)
    echo -e "Health Status" >> $OUTFILE
    echo -e "Disk\t\tPartition\tStatus" >> $OUTFILE
    while read DISK

    do

        DISKNAME=`echo $DISK | awk '{print $1}'`
        if [ "$DISKNAME" != "tmpfs" ] && [ "$DISKNAME" != "udev"  ] ; then
            USAGE=`echo $DISK | awk '{print $5}' | cut -f1 -d%`

            if [ $USAGE -ge 95 ]

            then

            STATUS='Unhealthy'

            elif [ $USAGE -ge 90 ]

            then

                STATUS='Caution'

            else

                STATUS='Normal'

            fi

            #LINE=`echo $DISK | awk '{print $1,"\t",$6}'`
            #DISKNAME=`echo $DISK | awk '{print $1}'`
            PART=`echo $DISK | awk '{print $6}'`

            #here we print result with status
            echo -e "$DISKNAME\t$PART\t\t$STATUS
            " >> $OUTFILE
        fi

    done < /tmp/df.status

    # Here we get disk write speed
    echo -e "Disk speed" >> $OUTFILE
    while read DISK
    do
        DISKNAME=`echo $DISK | awk '{print $1}'`
        if [ "$DISKNAME" != "tmpfs" ] && [ "$DISKNAME" != "udev"  ] ; then
            DISKSPEED=`hdparm -t "$DISKNAME" | tail -1`
            echo -e "$DISKNAME" >> $OUTFILE
            echo -e "$DISKSPEED
            " >> $OUTFILE
        fi

    done < /tmp/df.status

    #here we remove df.status file
    rm /tmp/df.status

    echo -e "Folder sizes for /var" >> $OUTFILE
    du -sh /var/* >> $OUTFILE
    du -sh /var/lib/mongodb >> $OUTFILE
    du -sh /var/lib/mysql >> $OUTFILE

    #here we get Total Memory, Used Memory, Free Memory, Used Swap and Free Swap values and save them to variables.
    TOTALMEM=`free -m | head -2 | tail -1| awk '{print $2}'`
    #All variables like this is used to store values as float (we are using bc to make all mathematics operations, since without bc all values will be integer). Also we use if to add zero before value, if value less than 1024, and result of dividing will be less than 1.
    TOTALBC=`echo "scale=2;if($TOTALMEM == 0) print 0;$TOTALMEM/1024"| bc -l`
    USEDMEM=`free -m | head -2 | tail -1| awk '{print $3}'`
    USEDBC=`echo "scale=2;if($USEDMEM == 0) print 0;$USEDMEM/1024"|bc -l`
    FREEMEM=`free -m | head -2 | tail -1| awk '{print $4}'`
    FREEBC=`echo "scale=2;if($FREEMEM == 0) print 0;$FREEMEM/1024"|bc -l`

    TOTALSWAP=`free -m | tail -1| awk '{print $2}'`
    TOTALSBC=`echo "scale=2;if($TOTALSWAP == 0) print 0;$TOTALSWAP/1024"| bc -l`
    USEDSWAP=`free -m | tail -1| awk '{print $3}'`
    USEDSBC=`echo "scale=2;if($USEDSWAP == 0) print 0;$USEDSWAP/1024"|bc -l`
    FREESWAP=`free -m | tail -1| awk '{print $4}'`
    FREESBC=`echo "scale=2;if($FREESWAP == 0) print 0;$FREESWAP/1024"|bc -l`

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gthering Memory Information..."

echo -e "
Memory usage:

Physical Memory:
Total\tUsed\tFree\t%Free
${TOTALBC}GB\t${USEDBC}GB \t${FREEBC}GB\t$(($FREEMEM * 100 / $TOTALMEM ))%

Swap Memory:
Total\tUsed\tFree\t%Free
${TOTALSBC}GB\t${USEDSBC}GB\t${FREESBC}GB\t$(($FREESWAP * 100 / $TOTALSWAP ))% " >> $OUTFILE



    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Interface Information...
"

echo -e "
Interface information:
" >> $OUTFILE
    /sbin/ifconfig -s >> $OUTFILE

    INTFACES=(`ifconfig -a | grep eth | awk '{print $1}'`)

    for i in "${INTFACES[@]}"
    do
        mii-tool -v "$i" >> $OUTFILE
        echo >> $OUTFILE
        ifconfig "$i" >> $OUTFILE
        echo -e "------------------\n" >> $OUTFILE
    done

    echo -e "*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Routing Table Information..."

echo -e "
Routing table:

`netstat -nr` " >> $OUTFILE

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering DNS Information"

echo -e "
DNS Servers:
`grep nameserver /etc/resolv.conf| awk '{print $2}'`

Search domains:
`grep search /etc/resolv.conf| awk '{print $2}'`" >> $OUTFILE


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gather OSSIM Server Connection Information..."

echo -e "
Ossim server connections:
" >> $OUTFILE
echo -e "`netstat -putna | grep -E ':40001.*ossim\-server|:40004.*python'`" >> $OUTFILE


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Listening Port Information..."

echo -e "
Listening ports:

`netstat -tupln` " >> $OUTFILE

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Active TCP Connection Information"

echo -e "
Active TCP connections:

`netstat -tupn` " >> $OUTFILE

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering MYSQL Information..."

echo -e "
Mysql deletes, updates, inserts
" >> $OUTFILE

    echo -e "Deletes:" >> $OUTFILE
    MYDEL=''
    MYDEL=`grep -i delete /root/.mysql_history`
    if [ "$MYDEL" != '' ]
    then
        echo -e "$MYDEL
        " >> $OUTFILE
    else
        echo -e "None
        " >> $OUTFILE
    fi


    echo -e "Updates:" >> $OUTFILE
    MYUPS=''
    MYUPS=`grep -i update /root/.mysql_history`
    if [ "$MYUPS" != '' ]
    then
        echo -e "$MYUPS
        " >> $OUTFILE
    else
        echo -e "None
        " >> $OUTFILE
    fi


    echo -e "Inserts:" >> $OUTFILE
    MYINS=''
    MYINS=`grep -i insert /root/.mysql_history`
    if [ "$MYINS" != '' ]
    then
        echo -e "$MYINS
        " >> $OUTFILE
    else
        echo -e "None " >> $OUTFILE
    fi


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering OSSIM Component Information..."

echo -e "
Ossim components:
" >> $OUTFILE

echo -e "`dpkg -l | grep -E 'ossim-agent|ossim-server|ossim-framework'`" >> $OUTFILE


    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Checking Connection To Redis Database..."

echo -e "
Redis connection test:
" >> $OUTFILE
REDTEST=''
if [ "`redis-cli ping`" == 'PONG' ]
then
    echo -e "Test successful" >> $OUTFILE
else
    echo -e "Test NOT successful, check the system" >> $OUTFILE
fi

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Sever Configuration Information..."

echo -e "
Server configuration:
" >> $OUTFILE
echo -e "`echo \"select conf, value from config where conf = 'server_id' or conf = 'server_address' or conf = 'ossim_server_version';\" | /usr/bin/ossim-db | grep -v conf`" >> $OUTFILE

CACHEDU=`du -sh /var/ossim/agent_events/ | awk '{print $1}'`
    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Cached Database Event Information..."

echo -e "
Cached database events:

Size of cache folder /var/ossim/agent_events: $CACHEDU

Files:" >> $OUTFILE
ls -ltr /var/ossim/agent_events/ | grep -v total >> $OUTFILE

    echo -e "
*************************************************************************************************" | tee -a $OUTFILE
echo -e "
Gathering Server Status Information..."

echo -e "
Server status:
" >> $OUTFILE
echo -e "`curl -s -k -H 'Accept: application/json' 'http://127.0.0.1:40009/server/status'`" >> $OUTFILE


}

##################################################################
#Set script run start time
RES1=$(date +%s.%N)

#here we make filename value, using hostname, and date.
FILENAME="healthcheck-`hostname`-`date +%d%m%y`_`date +%H%M`.txt"

#here we run function and save result to generated filename
# sysstat > $FILENAME
sysstat $FILENAME

#here we check if user provide his email address to send email
if [ "$EMAIL" != '' ]

    then

    #if email provided we check if we have mailx command to send email
    STATUS=`which mail`
    #if mailx command not exist on system (previous command returned non-zero exit code we warn user that mailx is not installed
    if [ "$?" != 0 ]

        then

        echo "The program 'mail' is currently not installed."

        #if mailx installed, we send email with report to user
    else

        cat $FILENAME | mail -s "$FILENAME" $EMAIL

    fi

fi

# Calculate and display the run time of the script
# First get end time
RES2=$(date +%s.%N)
# Now do the match and clean the times up
DT=$(echo "$RES2 - $RES1" | bc)
DD=$(echo "$DT/86400" | bc)
DT2=$(echo "$DT-86400*$DD" | bc)
DH=$(echo "$DT2/3600" | bc)
DT3=$(echo "$DT2-3600*$DH" | bc)
DM=$(echo "$DT3/60" | bc)
DS=$(echo "$DT3-60*$DM" | bc)

# Append time to end of file
echo -e "
#################################################################################################

All done!
Report file name: $FILENAME
`printf "Total runtime: %02d:%02d:%02.0f\n" $DH $DM $DS`" | tee -a $FILENAME
echo -e "
Report file $FILENAME generated in current directory."
echo -e "
#################################################################################################

" | tee -a $FILENAME