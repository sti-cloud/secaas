#!/bin/bash
# Remove files that are older than this number of days
DAYS_TO_KEEP=${1:-7}
echo "This will find and remove agent back log events that are older than $DAYS_TO_KEEP."
echo "The files listed below will be REMOVED!"
find /var/ossim/agent_events/*.db -type f -mtime +$DAYS_TO_KEEP -exec ls -lh {} \;
echo "Do you want to remove these files now?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) find /var/ossim/agent_events/*.db -type f -mtime +$DAYS_TO_KEEP -exec rm -f {} \;; break;;
        No ) echo "Files not removed!"; break;;
    esac
done