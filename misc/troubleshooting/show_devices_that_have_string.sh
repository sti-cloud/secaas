#!/bin/bash
MYPATH="/var/log/alienvault/devices/*/*.log"
echo "The following logging devices have the string $1 in them"
grep -c -R -m 1 "$1" $MYPATH | egrep -v ":0$" | cut -f 6 -d "/"