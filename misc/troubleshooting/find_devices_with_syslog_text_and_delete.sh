#!/bin/bash
find /var/log/alienvault/devices/* -maxdepth 1 -type f -name "*.log.1.gz" -exec zgrep -m 1 -H -l -e "$1" {} \; 2>/dev/null | cut -f -6 -d "/" | xargs -0 -d "\n" -n 1 rm -rf;