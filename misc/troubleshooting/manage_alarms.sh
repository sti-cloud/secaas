#!/bin/bash

# Fix to make sure this only runs on servers. The cron task hangs when run on a sensor (without MySQL)
# and fills up the process list with hung manage_alarms.sh processes. Note: did not troubleshoot the
# cause of the hang (which cannot be reproduced interactively). Since it only seems to hang on
# dedicated sensors, and the script is only useful on servers, this is an easy fix.
cat /etc/ossim/ossim_setup.conf | grep ^profile= | grep Server >/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
  echo "This can only be run on an AlienVault USM server."
  exit 1
fi

re='^[0-9]+$'
filter=""
helpscreen () {
  echo "$0 [-p <plugin_id>] [-s <plugin_sid>] [-c]"
  echo "-p <plugin_id>  - Delete alarms that match the plugin_id."
  echo "-s <plugin_sid> - Delete alarms that match the plugin_sid (sub ID)."
  echo "-c              - Delete alarms that match the status 'closed'."
  echo "-z              - Do not delete, but close all non 1505 alarms."
  echo "-v              - Do not delete, but view alarm break down."
  echo "-r              - Do not delete, but reset hosts with high priority."
  echo "-n <int>        - Do not delete, Close ALL alarms older than <int> days."
  echo "-o              - Show the number of alarms older than 90 days."
  echo "-----------------------------------------------------------------------"
  echo "For an Alarm to be deleted it must match all options when multiple"
  echo "options are specified."
  exit 1
}
# Make sure that an option is specified.
if [ -z "$1" ]; then
  helpscreen >&2
fi

# Make sure the first argument has a - then a letter
if ! [[ "$1" =~ ^\-[a-zA-Z]+$ ]]; then
  helpscreen >&2
fi

# Parse out options
while getopts ":p:s:cvzron:" opt; do
  case $opt in
    p)
      if ! [[ $OPTARG =~ $re ]]; then
        echo "-$opt was specified, but parameter $OPTARG is not a number!" >&2
        helpscreen >&2
        exit 1
      else
        #echo "-$opt was triggered, Parameter: $OPTARG" >&2
        filter+=" AND alarm.plugin_id = $OPTARG"
      fi
      ;;
    s)
      if ! [[ $OPTARG =~ $re ]]; then
        echo "-$opt was specified, but parameter $OPTARG is not a number!" >&2
        helpscreen >&2
        exit 1
      else
        #echo "-$opt was triggered, Parameter: $OPTARG" >&2
        filter+=" AND alarm.plugin_sid = $OPTARG"
      fi
      ;;
    c)
      #echo "-c was triggered, Parameter: $OPTARG" >&2
      filter+=" AND alarm.status = 'closed'"
      ;;
    v)
      echo "select plugin_id, plugin_sid, status, count(plugin_sid) AS count from alarm GROUP BY plugin_id, plugin_sid, alarm.status;" | ossim-db
      exit 1
      ;;
    z)
      echo "update alarm SET status = 'closed' where plugin_id != 1505;" | ossim-db
      exit 1
      ;;
    r)
      echo "update host SET asset = 2 where asset > 2;" | ossim-db
      exit 1
      ;;
    o)
      echo "select count(timestamp) as 'Alarms older than 90 days' from alarm where timestamp < (NOW() - INTERVAL 91 DAY);" | ossim-db
      exit 1
      ;;
    n)
      if ! [[ $OPTARG =~ $re ]]; then
        echo "-$opt was specified, but parameter $OPTARG is not a number!" >&2
        helpscreen >&2
      else
        echo "update alarm SET status = 'closed' where timestamp < (NOW() - INTERVAL $OPTARG DAY);" | ossim-db
      fi
      exit 1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      helpscreen >&2
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      helpscreen >&2
      ;;
    h)
      helpscreen >&2
      ;;
    *)
      helpscreen >&2
      ;;
  esac
done

# Insurance the filter should never be empty.
if [ -z "$filter" ]; then
  helpscreen >&2
fi

echo "The following filter will be applied: $filter"

echo "Cleaning up events, backlog, backlog_event, and alarm tables...(this takes a long time)"
echo "
START TRANSACTION;

# Get the event ID and backlog ID of alarms that need to be removed
CREATE TEMPORARY TABLE IF NOT EXISTS mytmptable AS
    (SELECT backlog_event.event_id as event_id, backlog_event.backlog_id as backlog_id
        FROM alarm, backlog_event
        WHERE alarm.backlog_id = backlog_event.backlog_id$filter
    );

# Remove the associated events
DELETE e FROM event e, mytmptable t WHERE e.id=t.event_id;

# Remove the associated backlog entry
DELETE b FROM backlog b, mytmptable t WHERE b.id=t.backlog_id;

# Remove the associated backlog events
DELETE backlog_event
FROM backlog_event INNER JOIN mytmptable
    ON backlog_event.backlog_id = mytmptable.backlog_id AND
        backlog_event.event_id = mytmptable.event_id;

# Remove the alarms
DELETE alarm
FROM alarm INNER JOIN mytmptable
    ON alarm.backlog_id = mytmptable.backlog_id AND
        alarm.event_id = mytmptable.event_id;

# Clean up
DROP TEMPORARY TABLE mytmptable;

COMMIT;
" | ossim-db

# Clean up orphans
echo "Cleaning up component_tags table..."
echo "DELETE tg FROM component_tags tg LEFT JOIN alarm a ON tg.id_component = a.backlog_id, tag ta WHERE ta.id=tg.id_tag AND ta.type='alarm' AND a.backlog_id IS NULL;" | ossim-db

echo "Cleaning up alarm_ctxs table..."
echo "DELETE ac FROM alarm_ctxs ac LEFT JOIN alarm a ON ac.id_alarm = a.backlog_id WHERE a.backlog_id IS NULL;" | ossim-db

echo "Cleaning up alarm_hosts table..."
echo "DELETE ah FROM alarm_hosts ah LEFT JOIN alarm a ON ah.id_alarm = a.backlog_id WHERE a.backlog_id IS NULL;" | ossim-db

echo "Cleaning up alarm_nets table..."
echo "DELETE an FROM alarm_nets an LEFT JOIN alarm a ON an.id_alarm = a.backlog_id WHERE a.backlog_id IS NULL;" | ossim-db

echo "Cleaning up idm_data table..."
echo "DELETE idm FROM idm_data idm LEFT JOIN event e ON idm.event_id = e.id WHERE e.id IS NULL;" | ossim-db

echo "Cleaning up otx_data table..."
echo "DELETE otx FROM otx_data otx LEFT JOIN event e ON otx.event_id = e.id WHERE e.id IS NULL;" | ossim-db

echo "Cleaning up extra_data table..."
echo "DELETE ed FROM extra_data ed LEFT JOIN event e ON ed.event_id = e.id WHERE e.id IS NULL;" | ossim-db

echo "Cleaning up orphaned alarms..."
echo "DELETE alarm FROM alarm LEFT JOIN event e ON alarm.event_id = e.id WHERE e.id IS NULL$filter;" | ossim-db
