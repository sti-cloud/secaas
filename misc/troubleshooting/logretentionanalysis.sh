#!/bin/bash


OSSIM_DIR=/var/ossim/logs

# All numbers in MB unless noted otherwise
TOTAL_DAYS=`du $OSSIM_DIR --max-depth=3 --separate-dirs | cut -f 2 | egrep "$OSSIM_DIR/[0-9]{4}\/[0-9]{2}\/[0-9]{2}" | wc -l`
TOTAL_DISK_SPACE=`/bin/df -BM | egrep -o "sda1\s*(ext4)?\s*[0-9]+" | cut -f 2 | egrep -o "[0-9]+$"`
SPACE_AVAILABLE=`/bin/df -BM | grep sda1 | egrep -o "[0-9]+\S\s+[0-9]{1,3}%" | cut -f 1 -d " " | egrep -o "[0-9]+"`

# This is in KB
TOTAL_RAW_LOG_SPACE_USED=`du $OSSIM_DIR -sk | grep $OSSIM_DIR | cut -f 1`

PER_DAY_CONSUMPTION=$((TOTAL_RAW_LOG_SPACE_USED / TOTAL_DAYS / 1024))
RESERVE=$((TOTAL_DISK_SPACE * 3 / 10))
SPACE_AVAILABLE_FOR_LOGS=$((SPACE_AVAILABLE - RESERVE))
MORE_DAYS=$((SPACE_AVAILABLE_FOR_LOGS / PER_DAY_CONSUMPTION))

echo "#####################################################################################"
echo "CURRENT STATS:"
echo "$TOTAL_DAYS days found in raw logs."
echo "$PER_DAY_CONSUMPTION MB / day average compressed consumption."
echo "$((TOTAL_RAW_LOG_SPACE_USED / 1024)) MB of raw logs found."
echo ""
echo "ASSUMPTIONS:"
echo "$SPACE_AVAILABLE MB of total space available."
echo "$SPACE_AVAILABLE_FOR_LOGS MB of space available for raw logs."
echo "20% Head room for staging and modest growth. (RESERVE SPACE)"
echo "10% Minimum space required. Emergency delete happens at this point. (RESERVE SPACE)."
echo "Total reserve space is $RESERVE MB."
echo "0% SIEM DB growth."
echo "0% Netflow growth."
echo ""
echo "CONCLUSION:"
echo "Possible to store $MORE_DAYS more days of raw logs"
echo "#####################################################################################"