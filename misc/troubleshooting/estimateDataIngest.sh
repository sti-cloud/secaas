#!/bin/bash

SYSLOGINGEST=` du -hc --max-depth=0 /var/log/alienvault/devices/ | grep total | cut -f 1`
OSSECINGEST=` du -hc --max-depth=0 /var/ossec/logs/alerts/ | grep total | cut -f 1`
#GLOBALINGEST=` du -hc --max-depth=0 /var/log/ | grep total | cut -f 1`
GLOBALINGEST="2G"
echo "Estimated Syslog for this Sensor: $SYSLOGINGEST"
echo "Estimated Syslog for this Sensor: $OSSECINGEST"
echo "Assumed Syslog for this Sensor: $GLOBALINGEST"