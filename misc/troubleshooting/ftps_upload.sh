#!/bin/bash
# This script will leverage curl to do FTP with SSL (FTPS) uploads of ossim log files.
# It has simplified checking (designed in an emergency) but should not upload the file if the same name exists.

AUTH="<username@domain>:<password>"
#AUTH=$1
PRE_DEST="IT-General/Security"  # Base of the FTP server root
PORT=990   # FTP Server Port Number
FTPSURL="ftps://domain.com"


# LOG FOLDER TO FIND FILES
LOG_FOLDER="/var/ossim/logs/2018/10"
#LOG_FOLDER="/var/ossim/logs"

# Find all files and check to see if they need to be uploaded.
find $LOG_FOLDER -type f -name '*.log*' -print0 | while IFS= read -r -d '' file; do
    POST_DEST=${file/\/var\/ossim\/logs/}
    DEST_FOLDER="$PRE_DEST$POST_DEST"

    if ( curl --ftp-ssl --user "$AUTH" -k -o /dev/null -sfI "$FTPSURL:$PORT/$DEST_FOLDER" ); then
      echo "Found file $DEST_FOLDER .... Skipping!" >> /var/log/ftps_status.txt
      echo "Found file $DEST_FOLDER .... Skipping!"
    else
      echo "Did not find file $DEST_FOLDER .... Uploading" >> /var/log/ftps_status.txt
      echo "Did not find file $DEST_FOLDER .... Uploading"
      curl --ftp-create-dirs --ftp-ssl --user "$AUTH" -k -T "$file" $FTPSURL:$PORT/$DEST_FOLDER
    fi
done