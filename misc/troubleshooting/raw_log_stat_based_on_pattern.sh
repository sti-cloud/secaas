#!/bin/bash
DIRECTORY=$1
PATTERN=$2

# Find the number of total logs in the base directory
echo "Now finding total logs...."
TOTAL_LOGS=`find $DIRECTORY -name "*.log.count" | xargs cat | awk '{s+=$1} END {print s}'`
echo "Found total logs of $TOTAL_LOGS"

# Find uncompressed matches
echo "Now searching uncompressed logs for matches...."
TOTAL_MATCHED_LOGS_UNCOMPRESSED=`find $DIRECTORY -name "*.log" -exec grep -i "$PATTERN" {} \; | wc -l`
echo "Found $TOTAL_MATCHED_LOGS_UNCOMPRESSED uncompressed matches!"

# Find compressed matches
echo "Now searching compressed logs for matches...."
TOTAL_MATCHED_LOGS_COMPRESSED=`find $DIRECTORY -name "*.log.gz" -exec zgrep -i "$PATTERN" {} \; | wc -l`
echo "Found $TOTAL_MATCHED_LOGS_COMPRESSED compressed matches!"

# Calculate total logs
let TOTAL_MATCHED_LOGS=($TOTAL_MATCHED_LOGS_UNCOMPRESSED+$TOTAL_MATCHED_LOGS_COMPRESSED)
let PERCENT_MATCHED=$TOTAL_MATCHED_LOGS*100/$TOTAL_LOGS

# Find disk stats for logs in the directory.
TOTAL_DIR_SIZE=`du -hc $DIRECTORY | tail -n 1 | egrep -o -i "[0-9.]+[kbmgt]\S*"`
# Find total space occupied by indexes
#TOTAL_DIR_SIZE_RAW=`find $DIRECTORY -name "*" -exec ls -lt {} \; | cut -f 5 -d " " | awk '{s+=$1} END {print s}'`
#let TOTAL_DIR_SIZE=$TOTAL_DIR_SIZE_RAW/1024/1024

# Find total space occupied by indexes
TOTAL_DIR_INDEX_SIZE_RAW=`find $DIRECTORY -name "index.inx*" -exec ls -lt {} \; | cut -f 5 -d " " | awk '{s+=$1} END {print s}'`
let TOTAL_DIR_INDEX_SIZE=$TOTAL_DIR_INDEX_SIZE_RAW/1024/1024

# Attempt to find the total space allocated to raw logs minus index.
TOTAL_DIR_LOG_SIZE_RAW=`find $DIRECTORY -name "*.log*" -exec ls -lt {} \; | cut -f 5 -d " " | awk '{s+=$1} END {print s}'`
let TOTAL_DIR_LOG_SIZE=$TOTAL_DIR_LOG_SIZE_RAW/1024/1024

# Estimate disk space usage for pattern type.
#let $ESTIMATED_DISK_USAGE_FOR_PATTERN=$TEMP*$PERCENT_MATCHED/100

echo "Directory Searched: $DIRECTORY"
echo "Pattern Searched: $PATTERN"
echo "Total Logs: $TOTAL_LOGS"
echo "Total Matched Logs: $TOTAL_MATCHED_LOGS"
echo "Percentage Match: $PERCENT_MATCHED"
echo "----------------------------------"
echo "Base Directory Size: $TOTAL_DIR_SIZE"
echo "Base Directory Index Consumption: $TOTAL_DIR_INDEX_SIZE MB"
echo "Base Directory Log Consumption: $TOTAL_DIR_LOG_SIZE MB"
echo "----------------------------------"
echo "Estimated Log Pattern Disk Consumption: $((($TOTAL_DIR_INDEX_SIZE_RAW + $TOTAL_DIR_LOG_SIZE_RAW) * $PERCENT_MATCHED / 100 / 1024 / 1024)) MB"