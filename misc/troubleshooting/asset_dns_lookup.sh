#!/bin/bash
# This script will attempt to perform a reverse lookup for each host entry in the AlienVault database that doesn't have one and rename it with the FQDN.
USER=`cat /etc/ossim/ossim_setup.conf | grep "^user=" | egrep -o "[^=]+$"`
PASS=`cat /etc/ossim/ossim_setup.conf | grep "^pass=" | egrep -o "[^=]+$"`
HOST=`cat /etc/ossim/ossim_setup.conf | grep "^db_ip=" | egrep -o "[^=]+$"`
DB=alienvault

hostentries=`sshpass -p $PASS mysql --default-character-set=utf8 -A -u $USER -h $HOST $DB -p -N -e "select hostname from host where hostname like 'Host-%';"`

echo $hostentries

for hostentry in $hostentries
do
  addr=`echo $hostentry | sed -r 's/Host-//g;s/-/./g'`
  fqdn=`dig -x $addr +short |sed s/.$//`
  echo "Address: $addr, FQDN: $fqdn"
  if [ -n "$fqdn" ]; then
    hostname=`echo $fqdn | cut -d"." -f1`
    echo $hostentry \($addr\) is going to be replaced for $fqdn \($hostname\)
    sshpass -p $PASS mysql --default-character-set=utf8 -A -u $USER -h $HOST $DB -p -e "update host set hostname='$hostname',fqdns='$fqdn' where hostname='$hostentry';"
  fi
done