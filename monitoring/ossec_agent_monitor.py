#!/usr/bin/python
import subprocess
import socket
import time
from datetime import datetime

SOCK = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
AGENT_CONTROL = '/var/ossec/bin/agent_control'
NOW = datetime.now()
NOWSTR = NOW.strftime("%b %d %H:%M:%S %Y")

def send_syslog_to(server, message) :
  SOCK.sendto(message, (server, 514))

def send_syslog(id, name, ip, os, client, keepalive) :
  send_syslog_to('199.7.244.72', "<132>" + NOWSTR + " ossec_agent_mon: inactive agent: agent_name=" + name + " agent_id=" + id + " agent_ip=" + ip + " last_keepalive=" + keepalive + " os=" + os + " client=" + client)
  send_syslog_to('199.7.245.72', "<132>" + NOWSTR + " ossec_agent_mon: inactive agent: agent_name=" + name + " agent_id=" + id + " agent_ip=" + ip + " last_keepalive=" + keepalive + " os=" + os + " client=" + client)
  time.sleep(0.1)

def GetValue(line) :
  sep = line.index(':')
  if ( sep == -1 ) :
    return line
  return line[sep+1:].strip()

info = subprocess.check_output([AGENT_CONTROL, '-la'])
info_lines = info.split("\n")
for i in range(len(info_lines)) :
  info_line = info_lines[i].strip()
  if (info_line.startswith("ID: ")) :
    info_fields = info_line.split(" ")
    AGENT_ID = info_fields[1].strip(",")
    AGENT_NAME = info_fields[3].strip(",")
    AGENT_IP = info_fields[5].strip(",")
    AGENT_OS = '?'
    AGENT_CLIENT = '?'
    AGENT_KEEPALIVE = '?'
    IS_LOCALHOST = False

    agent_info = subprocess.check_output([AGENT_CONTROL, '-i', AGENT_ID])
    agent_lines = agent_info.split("\n")
    for j in range(len(agent_lines)) :
      agent_line = agent_lines[j].strip()
      if (agent_line.startswith("Operating system:")) :
        AGENT_OS = GetValue(agent_line)
      if (agent_line.startswith("Client version:")) :
        AGENT_CLIENT = GetValue(agent_line)
      if (agent_line.startswith("Last keep alive:")) :
        AGENT_KEEPALIVE = GetValue(agent_line)
      if (agent_line.startswith("IP address:")) :
        AGENT_IP = GetValue(agent_line)
        if (AGENT_IP == "127.0.0.1") :
          IS_LOCALHOST = True

    if not IS_LOCALHOST :
      try :
        keepalive = datetime.strptime(AGENT_KEEPALIVE, "%a %b %d %H:%M:%S %Y")
        timedelta = NOW - keepalive
        if ( timedelta.days >= 1 ) :
          send_syslog(AGENT_ID, AGENT_NAME, AGENT_IP, AGENT_OS, AGENT_CLIENT, AGENT_KEEPALIVE)
          # print AGENT_ID, AGENT_NAME, AGENT_IP, AGENT_OS, AGENT_CLIENT, AGENT_KEEPALIVE
      except :
        send_syslog(AGENT_ID, AGENT_NAME, AGENT_IP, AGENT_OS, AGENT_CLIENT, AGENT_KEEPALIVE)
        # print AGENT_ID, AGENT_NAME, AGENT_IP, AGENT_OS, AGENT_CLIENT, AGENT_KEEPALIVE
