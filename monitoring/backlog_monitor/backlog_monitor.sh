#!/bin/bash
AGENT_EVENT_DIR="/var/ossim/agent_events"

query_sqlite () {
   local DB_FILE="$1"
   local QUERY="$2"
   RESULTS=`/usr/bin/sqlite3 "$DB_FILE" "$QUERY" 2> /dev/null`

   count=0
   while [ -z "$RESULTS" ]; do
        sleep 1
        RESULTS=`/usr/bin/sqlite3 "$DB_FILE" "$QUERY" 2> /dev/null`
        count=$(( count + 1 ))
        if [ $count -eq 3 ]; then
            echo "0"
            return
        fi
   done;
   echo "$RESULTS"
}



TOTALNOTSENT=0
TOTALSENT=0
TOTALTOTAL=0


for i in `ls $AGENT_EVENT_DIR/*.db`;
do
  NOTSENT=$(query_sqlite "$i" "select count(sent) from stored_events where sent = 0;")
  SENT=$(query_sqlite "$i" "select count(sent) from stored_events where sent = 1;")
  TOTAL=$(query_sqlite "$i" "select count(sent) from stored_events;")
  TOTALNOTSENT=$((TOTALNOTSENT + NOTSENT))
  TOTALSENT=$((TOTALSENT + SENT))
  TOTALTOTAL=$((TOTALTOTAL + TOTAL))
done


echo $TOTALTOTAL > /etc/snmp/total_backlog_events.out
echo $TOTALSENT > /etc/snmp/totalsent_backlog_events.out
echo $TOTALNOTSENT > /etc/snmp/totalnotsent_backlog_events.out


echo "--------------------------------------------------------------------------------------------"
echo "Total Records: $TOTALTOTAL"
echo "Total Sent Records: $TOTALSENT"
echo "Total Not Sent Records: $TOTALNOTSENT"
echo "--------------------------------------------------------------------------------------------"
