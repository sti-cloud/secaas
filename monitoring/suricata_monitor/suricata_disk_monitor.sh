#!/bin/bash
#Get total disk space used in Suricata log folders.
#/usr/bin/du -S -d 0 -BM /var/log/suricata | egrep -o "[0-9]+"
{ /usr/bin/du -S -d 0 -BM /var/log/suricata 2> /dev/null | egrep -o "[0-9]+"; }  || { echo 0; }