#! /bin/bash
###################################################
# Written By: Jeff Bell
# Date: 3/29/17
# Modified: 5/2/17
# This script performs removal of archived logs
# in the event disk usage reaches the specified
# maximum threshold
# Added getDiskSpaceUsage Function.
###################################################

##Variables if needed to modify script
thresh_config="/etc/snmp"
min_thresh=$(/bin/cat $thresh_config/disk_space_monitor.min || echo 78)
max_thresh=$(/bin/cat $thresh_config/disk_space_monitor.max || echo 82)
LOG_DIR="/var/ossim/logs"
MAX_NUM_TO_DELETE_AT_A_TIME=2

# This function gets current disk utilization in a percentage.
function getDiskSpaceUsage()
{
  local usage_perc=$(df -h | grep sda1 | grep -Po "(?<=\s)[0-9]{1,3}(?=%\s)") || exit
  echo "$usage_perc"
}

echo "Running delete script...THRESHOLD: $min_thresh (min), $max_thresh (max)"
echo "Current disk utilization: $(getDiskSpaceUsage)"
##Checks if DU is greater than max thresh, if so finds oldest archived log directory and deletes.
##Finally, removes any empty directories

if [ -d "$LOG_DIR" ]; then
  if [[ $(getDiskSpaceUsage) -ge $max_thresh ]]; then
    while [ $(getDiskSpaceUsage) -gt $min_thresh ]; do
      # Bugfix: If there are no logs to delete, avoid the infinite loop
      DELCOUNT=`find $LOG_DIR -maxdepth 3 -type d -path "$LOG_DIR/20[0-9][0-9]/[01][0-9]/[0-3][0-9]" | wc -l`
      if [ $DELCOUNT -eq 0 ]; then
        break
      fi
      find $LOG_DIR -maxdepth 3 -type d -path "$LOG_DIR/20[0-9][0-9]/[01][0-9]/[0-3][0-9]" | sort -d | head -n $MAX_NUM_TO_DELETE_AT_A_TIME | xargs -d '\n' rm -rf
    done
    find $LOG_DIR -maxdepth 2 -type d -path "$LOG_DIR/20[0-9][0-9]*" -empty -delete
  fi
else
  echo "Directory $LOG_DIR does not exists.  Exiting..."
fi
