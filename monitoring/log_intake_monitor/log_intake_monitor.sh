#! /bin/bash
###################################################
# Written By: Jeff Bell
# Date: 3/1/17
# Modified: 3/22/17
# This script performs monitoring of log files
# and attempts to alert Sentinel when logging has
# stopped for a 24 hour period of time
###################################################

sensor_check()
{
        curr_month=$(echo $(date +"%F-%H") | cut -d '-' -f 2) 
        echo 'Curr_Month='$curr_month
        curr_day=$(echo $(date +"%F-%H") | cut -d '-' -f 3)
        echo 'Curr_Day='$curr_day
        curr_year=$(echo $(date +"%F-%H") | cut -d '-' -f 1)
        echo 'Curr year='$curr_year
        curr_hour=$(echo $(date +"%F-%H") | cut -d '-' -f 4)
        echo 'Curr_hour='$curr_hour

        if [ "$curr_hour" -ge 20 ]; then
                echo 'First Option selected'
                curr_day=$[$curr_day+1]
                echo 'New Curr_Day='$curr_day
                curr_hour=$[$curr_hour-20]
                echo 'Adjusted Curr_hour='curr_hour
                cd /var/ossim/logs/$curr_year/$curr_month/$curr_day/$curr_hour
                pwd
                tot_dev=$(ls|wc -l)
                echo 'Total Devices='$tot_dev
                devices=$(ls)
                echo 'Devices listed='$devices
                i="0"
                log_count='0'
                dir_count=$(ls | grep [0-9].* | wc -l)
                echo 'Dir Count='$dir_count
                array=($(ls | grep [0-9].*))
                while [ $i -ne $((dir_count)) ]; do
                        cd ${array[$i]}
                        pwd
                        echo 'Array Entry='${array[$i]}
                        hour_log=$(ls -ltr | grep log$)
                        hour_log=$(echo $hour_log | cut -d ' ' -f 9)
                        echo 'Hour log='$hour_log
                        line_count=$(tail -10 $hour_log | wc -l)
                        echo 'Line Count='$line_count
                        if [ $line_count -ne 10 ]; then
                                logger -n 199.7.244.72 -p 4  Sensor $array[i] is not sending logs to AV from device $HOSTNAME
                                logger -n 199.7.245.72 -p 4  Sensor $array[i] is not sending logs to AV from device $HOSTNAME
                        fi
                        cd ..
                        i=$((i+1))
                done

        elif  [ "$curr_hour" -ge 0 ] && [ "$curr_hour" -le 19 ]; then
                curr_hour=$[$curr_hour+5]
                echo 'Adjusted Current Hour:'$curr_hour
                cd /var/ossim/logs/$curr_year/$curr_month/$curr_day/$curr_hour
                pwd
                i='0'
                log_count='0'
                dir_count=$(ls | grep [0-9].* | wc -l)
                array=($(ls | grep [0-9].*))
                while [ $i -ne $((dir_count)) ]; do
                        cd ${array[$i]}
                        pwd
                        echo 'Array Entry='${array[$i]}
                        hour_log=$(ls -ltr | grep log$)
                        hour_log=$(echo $hour_log | cut -d ' ' -f 9)
                        echo 'Hour log='$hour_log
						line_count='0'
                        line_count=$(tail -10 $hour_log | wc -l)
                        echo 'Line Count='$line_count
                        if [ $line_count -ne 10 ]; then
                                logger -n 199.7.244.72 -p 4  Sensor $array[i] is not sending logs to AV from device $HOSTNAME
                                logger -n 199.7.245.72 -p 4  Sensor $array[i] is not sending logs to AV from device $HOSTNAME
                        fi
                        cd ..
                        i=$((i+1))
                done
        fi
}

rsyslog_check()
{
        cd /var/log/alienvault/devices
        main_dir=/var/log/alienvault/devices
        tot_dev=$(ls|wc -l)
        devices=$(ls)

        i="1"
        echo -e '\n' >> /var/log/dev_logging_error.log
        echo "Starting device event detection: "$(date +%F-%T) >> /var/log/dev_logging_error.log 
        echo "----------------------------------------------------" >> /var/log/dev_logging_error.log

        ##Main function 
        while [ $i -ne $((tot_dev+1)) ]; do
                dev_ip=$(echo $devices | cut -d ' ' -f $i)
                cd $main_dir'/'$dev_ip
                echo 'Checking device '$dev_ip >> /var/log/dev_logging_error.log
                log_count='0'
                log_count=$(ls |grep log | wc -l)
                echo 'Log Count: '$log_count >> /var/log/dev_logging_error.log 
                array=($(ls $main_dir'/'$dev_ip | grep log))
                main_log=" "
                main_log=$(stat ${array[0]} | grep 'Mod' | cut -d ' ' -f 2)
                echo 'Current logfile date: '$main_log >> /var/log/dev_logging_error.log

                ##CSV file holds device metric history 
                file=./dev_hist.csv

                ##Checks CSV file and total number of logs
                if [[ -e "$file" && "$log_count" -ge 2  ]]; then
                        echo "Located CSV" >> /var/log/dev_logging_error.log
                        echo "Checking device history" >> /var/log/dev_logging_error.log
                        curr_month=$(echo $(date +%F) | cut -d '-' -f 2)
                        curr_day=$(echo $(date +%F) | cut -d '-' -f 3)
                        curr_year=$(echo $(date +%F) | cut -d '-' -f 1)
                        curr_hour=$(echo $(date +%T) | cut -d ':' -f 1)
                        curr_min=$(echo $(date +%T) | cut -d ':' -f 2)

                        csv_month=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 1 | cut -d '-' -f 2)
                        csv_day=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 1 | cut -d '-' -f 3)
                        csv_year=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 1 | cut -d '-' -f 1)
                        csv_hour=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 1 | cut -d '-' -f 4 | cut -d ':' -f 1)
                        csv_min=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 1 | cut -d '-' -f 4 | cut -d ':' -f 2)

                        rec_month=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 3)
                        rec_day=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 5)
                        rec_hour=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 7)
                        rec_min=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 9)
                        rec_fail=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 11)


                        month_comp=$(echo "$curr_month-$csv_month" | bc | tr -d '-')
                        day_comp=$(echo "$curr_day-$csv_day"| bc | tr -d '-')
                        hour_comp=$(echo "$curr_hour-$csv_hour"| bc | tr -d '-')
                        min_comp=$(echo "$curr_min-$csv_min"| bc | tr -d '-')

        ##Creates logfile metric and send syslog alert if current date exceeds device logging behavior metric         
                        if [[ $month_comp -ge $rec_month && $day_comp -gt $day_rec && $hour_comp -gt $rec_hour ]]; then
                                echo "No events received exceeded metric" >> /var/log/dev_logging_error.log
                                echo "Generating new metric"  >> /var/log/dev_logging_error.log
                                n="0"
                                nxt_month="0"
                                nxt_day="0"
                                nxt_hour="0"
                                nxt_min="0"
                                prev_month="0"
                                prev_day="0"
                                prev_hour="0"
                                prev_min="0"
                                day_calc="0"
                                month_calc="0"
                                hour_calc="0"
                                min_calc="0"
                                day_diff="0"
                                month_diff="0"
                                hour_diff="0"
                                min_diff="0"

                                while [ $n -ne $((log_count-1)) ]; do
                                        prev_day=$(stat ${array[$n]} | grep 'Mod' | cut -d ' ' -f 2 | cut -d '-' -f 3 | sed 's/^0*//')
                                        nxt_day=$(stat ${array[$((n+1))]} | grep 'Mod' | cut -d ' ' -f 2 | cut -d '-' -f 3 | sed 's/^0*//')
                                        day_calc=$(echo $((day_calc+$((prev_day-nxt_day))))| tr -d '-')
                                        prev_month=$(stat ${array[$n]} | grep 'Mod' | cut -d ' ' -f 2 | cut -d '-' -f 2 | sed 's/^0*//')
                                        nxt_month=$(stat ${array[$((n+1))]} | grep 'Mod' | cut -d ' ' -f 2 | cut -d '-' -f 2 | sed 's/^0*//')
                                        month_calc=$(echo $((month_calc+$((prev_month-nxt_month)))) | tr -d '-')
                                        prev_hour=$(stat ${array[$n]} | grep 'Mod' | cut -d ' ' -f 3 | cut -d ':' -f 1 | sed 's/^0*//')
                                        nxt_hour=$(stat ${array[$((n+1))]} | grep 'Mod' | cut -d ' ' -f 3 | cut -d ':' -f 1 | sed 's/^0*//')
                                        hour_calc=$(echo $((hour_calc+$((prev_hour-nxt_hour)))) | tr -d '-')
                                        prev_min=$(stat ${array[$n]} | grep 'Mod' | cut -d ' ' -f 3 | cut -d ':' -f 2 | sed 's/^0*//')
                                        nxt_min=$(stat ${array[$((n+1))]} | grep 'Mod' | cut -d ' ' -f 3 | cut -d ':' -f 2 | sed 's/^0*//')
                                        min_calc=$(echo $((min_calc+$((prev_min-nxt_min)))) | tr -d '-')
                                        n=$((n+1))
                                done

                                day_diff=$((day_calc/log_count))
                                month_diff=$((month_calc/log_count))
                                hour_diff=$((hour_calc/log_count))
                                min_diff=$((min_calc/log_count))
                                echo $(date +%F-%T)',Month,'$month_diff',Day,'$day_diff',Hour,'$hour_diff',Min,'$min_diff',fail,0' >> dev_hist.csv
                                echo "Sending syslog alert"  >> /var/log/dev_logging_error.log
                                logger -n 199.7.244.72 -p 4  $dev_ip is not sending logs to AV from device $HOSTNAME
                                logger -n 199.7.245.72 -p 4  $dev_ip is not sending logs to AV from device $HOSTNAME

                fi

        ##Check for CSV file and total logs. Checks safety check and either alerts or increments the check. Max 7 days
                elif [[ -e "$file" && "$log_count" -lt 2 ]]; then
                        rec_fail=$(tail -n 1 ./dev_hist.csv | cut -d ',' -f 11)

                        if [ "$rec_fail" -ge 7 ]; then
                                echo "Located CSV, device exceeded safety check, sending syslog alert"  >> /var/log/dev_logging_error.log
                                logger -n 199.7.244.72 -p 4  $dev_ip is not sending logs to AV from device $HOSTNAME
                                logger -n 199.7.245.72 -p 4  $dev_ip is not sending logs to AV from device $HOSTNAME

                        elif [ "$rec_fail" -ge 1 ]; then
                                echo "Located CSV, device is quiet, increasing safety check"  >> /var/log/dev_logging_error.log
                                echo $(date +%F-%T)',Month,0,Day,0,Hour,0,Min,0,Fail,'$((rec_fail+1)) >> dev_hist.csv

                        else
                                echo "Located CSV, device logging is quiet, starting safety check"  >> /var/log/dev_logging_error.log
                                echo $(date +%F-%T)',Month,0,Day,0,Hour,0,Min,0,Fail,1' >> dev_hist.csv

                fi

        ##Check for CSV file and total logs.Generates initial logging behavior metric and may alert if logfile is not current
                elif [[ ! -e "$file" && "$log_count" -ge 2 ]]; then 
                        echo "CSV Does Not Exist"  >> /var/log/dev_logging_error.log
                        curr_month=$(echo $(date +%F) | cut -d '-' -f 2)
                        curr_day=$(echo $(date +%F) | cut -d '-' -f 3)
                        curr_year=$(echo $(date +%F) | cut -d '-' -f 1)
                        curr_hour=$(echo $(date +%T) | cut -d ':' -f 1)
                        curr_min=$(echo $(date +%T) | cut -d ':' -f 2)

                        n="0"
                        nxt_month="0"
                        nxt_day="0"
                        nxt_hour="0"
                        nxt_min="0"
                        prev_month="0"
                        prev_day="0"
                        prev_hour="0"
                        prev_min="0"
                        day_calc="0"
                        month_calc="0"
                        hour_calc="0"
                        min_calc="0"
                        day_diff="0"
                        month_diff="0"
                        hour_diff="0"
                        min_diff="0"

                        echo "Generating initial metric"  >> /var/log/dev_logging_error.log
                        while [ $n -lt $((log_count-1)) ]; do
                                prev_day=$(stat ${array[$n]} | grep 'Mod' | cut -d ' ' -f 2 | cut -d '-' -f 3 | sed 's/^0*//')
                                nxt_day=$(stat ${array[$((n+1))]} | grep 'Mod' | cut -d ' ' -f 2 | cut -d '-' -f 3 | sed 's/^0*//')
                                day_calc=$(echo $((day_calc+$((prev_day-nxt_day))))| tr -d '-')
                                prev_month=$(stat ${array[$n]} | grep 'Mod' | cut -d ' ' -f 2 | cut -d '-' -f 2 | sed 's/^0*//')
                                nxt_month=$(stat ${array[$((n+1))]} | grep 'Mod' | cut -d ' ' -f 2 | cut -d '-' -f 2 | sed 's/^0*//')
                                month_calc=$(echo $((month_calc+$((prev_month-nxt_month)))) | tr -d '-')
                                prev_hour=$(stat ${array[$n]} | grep 'Mod' | cut -d ' ' -f 3 | cut -d ':' -f 1 | sed 's/^0*//')
                                nxt_hour=$(stat ${array[$((n+1))]} | grep 'Mod' | cut -d ' ' -f 3 | cut -d ':' -f 1 | sed 's/^0*//')
                                hour_calc=$(echo $((hour_calc+$((prev_hour-nxt_hour)))) | tr -d '-')
                                prev_min=$(stat ${array[$n]} | grep 'Mod' | cut -d ' ' -f 3 | cut -d ':' -f 2 | sed 's/^0*//')
                                nxt_min=$(stat ${array[$((n+1))]} | grep 'Mod' | cut -d ' ' -f 3 | cut -d ':' -f 2 | sed 's/^0*//')
                                min_calc=$(echo $((min_calc+$((prev_min-nxt_min)))) | tr -d '-')
                                n=$((n+1))
                        done

                        day_diff=$((day_calc/log_count))
                        month_diff=$((month_calc/log_count))
                        hour_diff=$((hour_calc/log_count))
                        min_diff=$((min_calc/log_count))
                        echo $(date +%F-%T)',Month,'$month_diff',Day,'$day_diff',Hour,'$hour_diff',Min,'$min_diff',Fail,0' >> dev_hist.csv

                        if [ $main_log != $(date +%F) ]; then
                                echo "Current logfile does not match current date"  >> /var/log/dev_logging_error.log
                                echo "Sending syslog alert"  >> /var/log/dev_logging_error.log
                                logger -n 199.7.244.72 -p 4  $dev_ip is not sending logs to AV from device $HOSTNAME
                                logger -n 199.7.245.72 -p 4  $dev_ip is not sending logs to AV from device $HOSTNAME
                        fi

                ##Checks for CSV file, starts the safety check
                elif [[ ! -e "$file" && "$log_count" -le 1 ]]; then
                        echo "No CSV found, starting safety check"   >> /var/log/dev_logging_error.log
                        echo $(date +%F-%T)',Month,0,Day,0,Hour,0,Min,0,Fail,1' >> dev_hist.csv

                fi
                i=$[$i+1]
        done
}
sensor_check
rsyslog_check