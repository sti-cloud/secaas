#!/bin/bash

# At the top of the hour, prune orphaned files. "lsof" can be expensive, so avoid doing it every 5 minutes
# when this script is run.

MINUTE=$( date +%M )
if [ $MINUTE == "00" ] ; then
  # AIOs do not backlog. Only look for orphaned files on servers that can.
  DBFILES=$( ls /var/ossim/agent_events | grep db$ )
  if [ ! -z "$DBFILES" ]; then
    # Get a list of all database files that are being referenced
    cd /var/ossim/agent_events
    INUSE=`lsof | grep 'agent_events.*db' | sed -e 's/.* //' -e 's/.*\///' | sort -u | tr '\n' '|' | sed -E 's/\|$//'`

    # Orphaned files are all files that are not being referenced
    if [ ! -z "$INUSE" ]; then
      ORPHANED=`ls -1 *.db | grep -v $INUSE`
    else
      ORPHANED=`ls -1 *.db`
    fi

    # If any orphaned files were detected, remove them.
    if [ ! -z "$ORPHANED" ]; then
      rm -f $ORPHANED
    fi
  fi
fi

# Now it is safe to calculate the directory size of backlogged events
/usr/bin/du -S -d 0 -BM /var/ossim/agent_events | egrep -o "[0-9]+"
