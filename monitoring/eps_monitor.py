#!/usr/bin/python

import MySQLdb
import ConfigParser
import smtplib
import socket
from contextlib import closing


default_interval_mins=5
DEBUG=False


# Read AlienVault Creds
db_ip=None
db_user=None
db_password=None


try:
  from io import StringIO
  filename = '/etc/ossim/ossim_setup.conf'
  vfile = StringIO(u'[misc]\n%s'  % open(filename).read())

  db_config = ConfigParser.ConfigParser()
  db_config.readfp(vfile)
  db_ip=db_config.get("database", "db_ip")
  db_user=db_config.get("database", "user")
  db_password=db_config.get("database", "pass")
except Exception, e:
  print "-1"
  sys.exit(-1)




def executeCommand(statement, db_name):
  global db_ip, db_user, db_password
  db = None
  try:
    db = MySQLdb.connect(host=db_ip,user=db_user,passwd=db_password,db=db_name)
    results = None
    with closing( db.cursor() ) as cursor:
      #logging.debug("Executing SQL command: %s" % statement)
      cursor.execute(statement)
      results = cursor.fetchall()
    return results
  except Exception, e:
    #logging.exception("Error attempting to execute sql command: %s" % e)
    raise
  finally:
    db.close()

total_eps = 0
sql = 'SELECT SUM( stat ) AS eps FROM acl_entities_stats;'
#sql = 'select COUNT(device_id) from alienvault_siem.acid_event WHERE timestamp >= utc_timestamp() - INTERVAL %s MINUTE and timestamp <= utc_timestamp();' % str(default_interval_mins)
#sql = 'select COUNT(device_id) from alienvault_siem.acid_event WHERE timestamp > now() - INTERVAL %s MINUTE and timestamp <= now();' % str(default_interval_mins)
sresults = executeCommand(sql,"alienvault")
#sresults = executeCommand(sql,"alienvault_siem")
total_eps = round(sresults[0][0],0)
#total_events = sresults[0][0]
#print total_events
#events_per_sec = int(total_events)/(int(default_interval_mins)*60)
#total_eps = total_eps+events_per_sec
try :
  float(total_eps)
  print total_eps
except :
  print 0
