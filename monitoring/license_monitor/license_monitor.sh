#!/bin/bash
LICENSEFILE=/etc/ossim/ossim.lic
KEY=""
PREFIX="http://data.alienvault.com/alienvault5/feed_"
SUFFIX="/binary/Packages"
OUTPUTFILE="/etc/snmp/license_monitor.results"

while read -r line; do
  myline="$line"
  if [[ $line =~ ^key=(.+)$ ]]; then
    KEY="${BASH_REMATCH[1]}"
    break
  fi
done < "$LICENSEFILE"

URL="$PREFIX$KEY$SUFFIX"
STATUS=$(curl -s -o /dev/null -w "%{http_code}" $URL)
ERROR_CODE=$?
if [ $STATUS -ne 000 ]; then
   echo $STATUS > $OUTPUTFILE
else
   echo $ERROR_CODE > $OUTPUTFILE
fi