#!/bin/bash

IFACE=$(/bin/cat /etc/snmp/span_monitor.conf || echo eth1)
OUTPUT_FILE=/etc/snmp/span_monitor.out
IFCONFIG_EXE="/sbin/ifconfig"
CURRENT_VALUE=`$IFCONFIG_EXE $IFACE | egrep -o "RX packets:[0-9]+" | egrep -o "[0-9]+"`
sleep 10
NEW_VALUE=`$IFCONFIG_EXE $IFACE | egrep -o "RX packets:[0-9]+" | egrep -o "[0-9]+"`

if  [[ $NEW_VALUE -lt $CURRENT_VALUE ]]; then
  DIFF=$((CURRENT_VALUE-NEW_VALUE))
else
  DIFF=$((NEW_VALUE-CURRENT_VALUE))
fi

echo $DIFF > $OUTPUT_FILE