#!/usr/bin/python
#Cloudflare.config should be a file with 4 options
#organization_name: <org name here or * or blank>
#email: <email/user here>
#authKey: <auth_key_here>
#TimeInterval: 10
#cloudflareLog: /var/log/cloudflare.log
#organization name can be left blank to get logs from user account
#organizatio name can be * to get logs from all groups and user account
#organization can be comma delimited to get logs from several accounts.

import sys
sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
import ast
import datetime
import json
import time

import dateutil.parser
#import pause as pause
import requests

configDir = "/etc/ossim/agent/plugins/cloudflare.config"
values = {} #values of config variables. This reads in config options
with open(configDir,"r") as r:
    for line in r:
        pos = line.find(':')
        values[line[0:pos].strip()] = line[pos+1:-1].strip()

TIMETOWAIT = int(values['TimeInterval'])  # seconds
email = values['email']#email the account is associated with
authKey = values['authKey'] #authkey of the account
organization_name = values['organization_name']
logDict = values['cloudflareLog']
organization_id = ''
logGetters = [] #every account needs its own log getter
class LogGetter:
    def __init__(self, url):
        self.url = url
        self.head = {'X-Auth-Email': email,
                     'X-Auth-Key': authKey,
                     'Content-Type': 'application/json'}
    def rawLogs(self, time):
        r = requests.get(self.url+time, headers=self.head)
        returned = r.text[:-1].replace("\n","")
        return json.loads(returned)

def getUrl(orgName):
    head = {'X-Auth-Email': email,
            'X-Auth-Key': authKey,
            'Content-Type': 'application/json'}

    r = requests.get("https://api.cloudflare.com/client/v4/user", headers=head)
    returned = (r.text[:-1]+'}').replace("\n", "")
    idData = json.loads(returned)
    if(orgName != ''):
        if len(idData['result']['organizations']) ==0:
            url = "https://api.cloudflare.com/client/v4/user/audit_logs?since="
            logGetters.append(LogGetter(url))
        for i in idData['result']['organizations']:
            orgs = organization_name.split(',')
            for j in orgs:
                j=j.strip()
                if i['name'] == j or organization_name=='*':
                    organization_id = i['id']
                    if(len(organization_id)<3):
                        url = "https://api.cloudflare.com/client/v4/user/audit_logs?since="
                    else:
                        url = "https://api.cloudflare.com/client/v4/organizations/" + organization_id + "/audit_logs?since="
                    logGetters.append(LogGetter(url))
    else:
        logGetters.append(LogGetter("https://api.cloudflare.com/client/v4/user/audit_logs?since="))

#pause function, sleeps program until certain time
def wait_until(execute_it_now):
    while True:
        diff = (execute_it_now - datetime.datetime.utcnow()).total_seconds()
        if diff <= 0:
            return
        elif diff <= 0.1:
            time.sleep(0.001)
        elif diff <= 0.5:
            time.sleep(0.01)
        elif diff <= 1.5:
            time.sleep(0.1)
        else:
            time.sleep(1)

while len(logGetters)==0:
    try:
        getUrl(organization_name)
    except:
        print "request failed, couldn't get organization id, make sure organization name and apikey are correct in the config file and that there is an internet connection"
    wait_until(datetime.datetime.utcnow()+datetime.timedelta(0, TIMETOWAIT))

def getLogs(time):
    #changes the json logs into logs all on one line. Formate is jsonKey=data
    #nessted json will be jsonkey.subkey = data
    #function does not handle arrays inside of the json
    def jsontoSingleLineLogs(data):
        allRet = []
        def DictToKeyValue(p,d):
            ret = ""
            for key, j in d.iteritems():
                if type(j) is list: #if an array is encountered it will just turn it to a string and add it to the key value pair
                    try :
                      str1 = ''.join(j)
                      ret+=","+p+"."+key+"="+str1
                    except :
                        pass
                elif type(j) is dict:
                    ret+=DictToKeyValue(p+"."+key,j)
                elif type(j) is bool:
                    ret += "," + p + "." + key + "=" + str(j)
                elif type(j) is str:
                    ret+=","+p+"."+key+"="+str(j)
                elif type(j) is int:
                    ret+=","+p+"."+key+"="+str(j)
                elif type(j) is unicode:
                    ret+=","+p+"."+key+"="+str(j)
                elif j == None:
                    pass    # null value
                else:
                    print(str(type(j)) + " [unhandled1] " + p + "." + key, j)
            return ret

        dcount=0
        for i in data["result"]:
            ret = ""
            if type(i) != dict:
                print("ERROR: type needs to be dict, was", type(i))
                continue
            for key, j in i.iteritems():
                if type(j) is dict:
                    ret+=DictToKeyValue(str(key),j)
                elif type(j) is str:
                    ret+=","+key+"="+j
                elif type(j) is unicode:
                    ret+=","+key+"="+str(j)
                else:
                    print(str(type(j)) + " unhandled2:", j)
            allRet.append(ret[1:])

        return allRet

    allRet = []
    for i in logGetters:
    #data is a json representation of the logs.
        data = i.rawLogs(time)
        allRet+=(jsontoSingleLineLogs(data))

    allRet = list(dict.fromkeys(allRet))
    with open(logDict,"a+") as f:
        for i in allRet:
            f.write(i+',\n')

def main():
    WaitTillTime = datetime.datetime.utcnow()
    if len(sys.argv) > 1 :
        since = sys.argv[1]
    else :
        since = WaitTillTime.isoformat('T')

    while True:
        try:
            print "Getting logs since " + since
            getLogs(since)
            since = WaitTillTime.isoformat('T')
        except Exception as e:
            print ("couldn't get logs, maybe internet is down or credentials are incorrect? Will try again in "+str(TIMETOWAIT)+" seconds")
            print e
        WaitTillTime = WaitTillTime + datetime.timedelta(0, TIMETOWAIT)
        wait_until(WaitTillTime)

try:
    main()
except KeyboardInterrupt, e:
    print("Keyboard interrupt received....exiting")
    sys.exit(0)
