-- Sentinel Cloudflare Plugin
-- Written By: Adam Arato
-- Last Modified: 2019-07-11

SET @pluginID = '30001';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
VALUES (@pluginID, 1, 'sti-cloudflare', 'cloudflare', 21);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 100200, 2, 24, 'cloudflare: Login', 1, 1);
call STICreateDatasourceEntry(@pluginID, 200200, 2, 27, 'cloudflare: Logout', 1, 1);
call STICreateDatasourceEntry(@pluginID, 300300, 11, 138, 'cloudflare: api key viewed', 1, 1);/*system notification*/
call STICreateDatasourceEntry(@pluginID, 400200, 11, 138, 'cloudflare: api key rotated', 1, 1);
call STICreateDatasourceEntry(@pluginID, 500400, 11, 138, 'cloudflare: api key created', 1, 1);
call STICreateDatasourceEntry(@pluginID, 3100200, 2, 79, 'cloudflare: MFA enabled', 1, 1); /*authenticatoin policy added */
call STICreateDatasourceEntry(@pluginID, 600200, 2, 92, 'cloudflare: accepted member', 1, 1);
call STICreateDatasourceEntry(@pluginID, 7001300, 11, 138, 'cloudflare: access_create_organization', 1, 1); /*group changed */
call STICreateDatasourceEntry(@pluginID, 800200, 2, 92, 'cloudflare: account member expire', 1, 1); /* group changed */
call STICreateDatasourceEntry(@pluginID, 900600, 13, 228, 'cloudflare: add dns record', 1, 1); /*application DNS succesfull zone transfer*/
call STICreateDatasourceEntry(@pluginID, 900100, 13, 228, 'cloudflare: add zone', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1000200, 11, 138, 'cloudflare: add_member', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1100200, 11, 187, 'cloudflare: addsetting', 1, 1); /* system configureation_changed*/
call STICreateDatasourceEntry(@pluginID, 1200100, 8, 188, 'cloudflare: agro smart routing', 1, 1); /* network misc*/
call STICreateDatasourceEntry(@pluginID, 1300100, 11, 187, 'cloudflare: change setting', 1, 1); /*system configuration_changed*/
call STICreateDatasourceEntry(@pluginID, 14001000, 18, 203, 'cloudflare: create pagerule', 1, 1); /*inventory service change */
call STICreateDatasourceEntry(@pluginID, 1300100, 11, 138, 'cloudflare: created certificate_pack', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1400200, 2, 86, 'cloudflare: create account', 1, 1); /*user created */
call STICreateDatasourceEntry(@pluginID, 1500200, 2, 86, 'cloudflare: created account', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1600200, 2, 87, 'cloudflare: delete account', 1, 1); /*User deleted */
call STICreateDatasourceEntry(@pluginID, 1600500, 11, 138, 'cloudflare: delete certificate pack', 1, 1); /*system notification*/
call STICreateDatasourceEntry(@pluginID, 1700500, 11, 138, 'cloudflare: delete requested', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1800500, 11, 138, 'cloudflare: deleted certificate pack', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1900500, 11, 138, 'cloudflare: deployed certificate pack', 1, 1);
call STICreateDatasourceEntry(@pluginID, 20001000, 11, 138, 'cloudflare: disable pagerule', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2000600, 13, 230, 'cloudflare: disabled DNS record', 1, 1); /*application dns misc */
call STICreateDatasourceEntry(@pluginID, 2100600, 13, 230, 'clo440udflare: enabled DNS record', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2200700, 11, 138, 'cloudflare: filter created', 1, 1); /*sys notification*/
call STICreateDatasourceEntry(@pluginID, 2300700, 11, 138, 'cloudflare: filter updated', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2400800, 3, 121, 'cloudflare: firewall access rule created', 1, 1); /*firewall misc event */
call STICreateDatasourceEntry(@pluginID, 2500800, 3, 121, 'cloudflare: firewall access rule deleted', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2600900, 3, 121, 'cloudflare: firewall rule created', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2700900, 3, 121, 'cloudflare: firewall rule deleted', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2800900, 3, 121, 'cloudflare: firewall rule updated', 1, 1);
call STICreateDatasourceEntry(@pluginID, 3200100, 13, 230, 'cloudflare: nameserver confirmed', 1, 1); /*dns misc*/
call STICreateDatasourceEntry(@pluginID, 33001000, 11, 138, 'cloudflare: ordered certificate pack', 1, 1); /*sys note */
call STICreateDatasourceEntry(@pluginID, 3400200, 11, 223, 'cloudflare: payment method set', 1, 1);  /*system configureation changed */
call STICreateDatasourceEntry(@pluginID, 38001100, 11, 138, 'cloudflare: route created', 1, 1);
call STICreateDatasourceEntry(@pluginID, 39001100, 11, 138, 'cloudflare: route deleted', 1, 1);
call STICreateDatasourceEntry(@pluginID, 40001100, 11, 138, 'cloudflare: route updated', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4100600, 11, 138, 'cloudflare: script created', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4200600, 11, 138, 'cloudflare: script deleted', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4300600, 11, 138, 'cloudflare: script updated', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4400600, 11, 138, 'cloudflare: set dns record', 1, 1);

call STICreateDatasourceEntry(@pluginID, 2900300, 2, 90, 'cloudflare: join org', 1, 1); /*Group added*/
call STICreateDatasourceEntry(@pluginID, 31001000, 11, 138, 'cloudflare: login pagerule', 1, 1);
call STICreateDatasourceEntry(@pluginID, 35001100, 11, 138, 'cloudflare: pending', 1, 1);
call STICreateDatasourceEntry(@pluginID, 3500100, 11, 138, 'cloudflare: pending zone', 1, 1);

call STICreateDatasourceEntry(@pluginID, 36001100, 11, 138, 'cloudflare: purge everything', 1, 1);
call STICreateDatasourceEntry(@pluginID, 3600100, 11, 138, 'cloudflare: purge everything', 1, 1);

call STICreateDatasourceEntry(@pluginID, 3700200, 2, 92, 'cloudflare: remove member', 1, 1); /* group changed */
call STICreateDatasourceEntry(@pluginID, 37001200, 2, 92, 'cloudflare: remove member', 1, 1); /* group changed */

call STICreateDatasourceEntry(@pluginID, 4700100, 11, 138, 'cloudflare: toggle WAF rule', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4800100, 11, 138, 'cloudflare: toggle WAF set', 1, 1);
call STICreateDatasourceEntry(@pluginID, 30001000, 11, 138, 'cloudflare: update pagerule', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4900100, 13, 230, 'cloudflare: zone reset check', 1, 1); /*dns misc*/

call STICreateDatasourceEntry(@pluginID, 16001000, 11, 138, 'cloudflare: delete pagerule', 1, 1); 
call STICreateDatasourceEntry(@pluginID, 21001000, 2, 92, 'cloudflare: set member role pagerule', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4500200, 13, 230, 'cloudflare: set member role account', 1, 1); /* group changed */
call STICreateDatasourceEntry(@pluginID, 1600100, 13, 230, 'cloudflare: delete zone', 1, 1); /*dns misc*/
call STICreateDatasourceEntry(@pluginID, 4600200, 13, 230, 'cloudflare: zone reset check', 1, 1); /*dns misc*/

call STICreateDatasourceEntry(@pluginID, 5000300, 2, 85, 'cloudflare: Password Reset', 1, 1); /*Password_Change_Succeeded*/

call STICreateDatasourceEntry(@pluginID, 2000500, 11, 138, 'cloudflare: Disable pagerule/certificate pack', 1, 1); /*disable certificate pack*/

call STICreateDatasourceEntry(@pluginID, 41001200, 11, 138, 'cloudflare: script created', 1, 1);

call STICreateDatasourceEntry(@pluginID, 42001200, 1, 138, 'cloudflare: script deleted', 1, 1); 

call STICreateDatasourceEntry(@pluginID, 43001200, 1, 138, 'cloudflare: script updated', 1, 1);



