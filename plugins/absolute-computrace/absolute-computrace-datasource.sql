--
-- Absolute CompuTrace Update By Sentinel Technologies
-- Written By: Sidney Eaton
-- Last Modified: 3-26-2018
--

SET @pluginID = '20008';

DELETE FROM plugin WHERE id = @pluginID;
DELETE FROM plugin_sid where plugin_id = @pluginID;

INSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (@pluginID, 1, 'absolute-computrace', 'Absolute CompuTrace', 12);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 120757, 11, 138, 'Absolute CompuTrace: Computrace 30 Day', 1, 0);
call STICreateDatasourceEntry(@pluginID, 120756, 11, 134, 'Absolute CompuTrace: Computrace 60 Day', 1, 0);
call STICreateDatasourceEntry(@pluginID, 120754, 11, 141, 'Absolute CompuTrace: Computrace 90 Day', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108959, 11, 187, 'Absolute CompuTrace: Device Name Changed', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108961, 11, 139, 'Absolute CompuTrace: Drift', 1, 0);
call STICreateDatasourceEntry(@pluginID, 104684, 11, 139, 'Absolute CompuTrace: Last called 20 days ago', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108962, 11, 139, 'Absolute CompuTrace: Local IP Address Changed', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108964, 11, 139, 'Absolute CompuTrace: Network Change', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108965, 18, 206, 'Absolute CompuTrace: Operating System Changed', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108966, 11, 141, 'Absolute CompuTrace: Operating System Product Key Changed', 1, 0);
call STICreateDatasourceEntry(@pluginID, 118326, 11, 138, 'Absolute CompuTrace: Outside of Illinois', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108967, 11, 139, 'Absolute CompuTrace: Public IP Address Changed', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108968, 11, 139, 'Absolute CompuTrace: Username Changed', 1, 0);
call STICreateDatasourceEntry(@pluginID, 104693, 11, 139, 'Absolute CompuTrace: Agent Is Newly Installed', 1, 0);
call STICreateDatasourceEntry(@pluginID, 118475, 11, 139, 'Absolute CompuTrace: Credant Full Volume Encryption', 1, 0);
call STICreateDatasourceEntry(@pluginID, 104873, 11, 139, 'Absolute CompuTrace: Naperville Service Area Alert', 1, 0);
call STICreateDatasourceEntry(@pluginID, 108960, 11, 139, 'Absolute CompuTrace: Device Rebuild', 1, 0);
call STICreateDatasourceEntry(@pluginID, 119433, 11, 148, 'Absolute CompuTrace: Agent Tampered With or Removed', 1, 0);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
