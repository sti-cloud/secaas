--
-- Proofpoint Plugin
-- Written By: Tom Martin
-- Last Modified: 2019-02-14
--


-- Set plugin id variable
SET @pluginID = '21009';

-- Insert product if not already exists
INSERT INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'Proofpoint API', 'STI Plugin for Proofpoint API', 15)
	ON DUPLICATE KEY UPDATE type=1, name='Proofpoint API', description='STI Plugin for Proofpoint API', product_type=15;

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 12, 98, 'Proofpoint API: message blocked', 10, 2);
call STICreateDatasourceEntry(@pluginID, 2, 7, 63, 'Proofpoint API: suspicious message allowed', 10, 2);
call STICreateDatasourceEntry(@pluginID, 3, 13, 113, 'Proofpoint API: click blocked', 10, 2);
call STICreateDatasourceEntry(@pluginID, 4, 7, 126, 'Proofpoint API: click allowed', 10, 2);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
