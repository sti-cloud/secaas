--
-- FortiGate Missing Datasources
-- Last signatures update: NA
-- Written By: Sidney Eaton
-- Last Modified: 3-14-2017
--

-- Set plugin id variable
SET @pluginID = '1554';

-- Insert product if not already exists ASSUMED TO EXISTS

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 26003, 13, 218, 'FortiGate: DHCP Statistics', 5, 1);
call STICreateDatasourceEntry(@pluginID, 32212, 10, 71, 'FortiGate: Configuration Revision Deleted From Database', 5, 0);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
