--
-- Office 365 Plugin
-- Written By: Tom Martin
-- Last Modified: 2017-12-14
--


-- Set plugin id variable
SET @pluginID = '21004';

-- Insert product if not already exists
INSERT INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'Office 365', 'STI Plugin for Office 365 and Azure', 9)
	ON DUPLICATE KEY UPDATE type=1, name='Office 365', description='STI Plugin for Office 365 and Azure';

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 10, 71, 'O365 Exchange Admin Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2, 10, 71, 'O365 Exchange Item Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 3, 10, 71, 'O365 Exchange Item Group Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4, 10, 71, 'O365 SharePoint Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 6, 10, 71, 'O365 SharePoint File Operation Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 8, 10, 71, 'O365 Azure Active Directory Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 9, 10, 71, 'O365 Azure Active Directory Logon Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 10, 10, 71, 'O365 Data Center Security Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 11, 10, 71, 'O365 Compliance DLP SharePoint Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 12, 10, 71, 'O365 Sway Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 13, 10, 71, 'O365 Compliance DLP Exchange Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 14, 10, 71, 'O365 SharePoint Sharing Operation Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 15, 10, 71, 'O365 Secure Token Service Logon Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 18, 10, 71, 'O365 Secuirty and Compliance Center Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 20, 10, 71, 'O365 Power BI Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 24, 10, 71, 'O365 Discovery Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 25, 10, 71, 'O365 Microsoft Teams Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 26, 10, 71, 'O365 Microsoft Teams Add-On Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 27, 10, 71, 'O365 Microsoft Teams Settings Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100, 10, 71, 'Microsoft Azure Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1000, 10, 71, 'O365 Generic Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1001, 10, 71, 'Microsoft Azure Generic Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1, 10, 71, 'O365 Exchange Admin Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2, 10, 71, 'O365 Exchange Item Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 3, 10, 71, 'O365 Exchange Item Group Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4, 10, 71, 'O365 SharePoint Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 6, 10, 71, 'O365 SharePoint File Operation Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 8, 10, 71, 'O365 Azure Active Directory Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 9, 10, 71, 'O365 Azure Active Directory Logon Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 10, 10, 71, 'O365 Data Center Security Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 11, 10, 71, 'O365 Compliance DLP SharePoint Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 12, 10, 71, 'O365 Sway Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 13, 10, 71, 'O365 Compliance DLP Exchange Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 14, 10, 71, 'O365 SharePoint Sharing Operation Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 15, 10, 71, 'O365 Secure Token Service Logon Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 18, 10, 71, 'O365 Secuirty and Compliance Center Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 20, 10, 71, 'O365 Power BI Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 24, 10, 71, 'O365 Discovery Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 25, 10, 71, 'O365 Microsoft Teams Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 26, 10, 71, 'O365 Microsoft Teams Add-On Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 27, 10, 71, 'O365 Microsoft Teams Settings Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100, 10, 71, 'Microsoft Azure Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1000, 10, 71, 'O365 Generic Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1001, 10, 71, 'Microsoft Azure Generic Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100001, 10, 71, 'Microsoft Azure Add member to Group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100002, 10, 71, 'Microsoft Azure Remove member from group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100003, 10, 71, 'Microsoft Azure Update group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100004, 10, 71, 'Microsoft Azure Delete group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100005, 10, 71, 'Microsoft Azure Add group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100006, 10, 71, 'Microsoft Azure Add owner to group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100007, 10, 71, 'Microsoft Azure Remove owner from group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100008, 10, 71, 'Microsoft Azure Hard delete group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100009, 10, 71, 'Microsoft Azure Finish applying group based license to users', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100010, 10, 71, 'Microsoft Azure Start applying group based license to users', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100011, 10, 71, 'Microsoft Azure Trigger group license recalculation', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100012, 10, 71, 'Microsoft Azure Add app role assignment to group', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100013, 10, 71, 'Microsoft Azure Set group license', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100014, 10, 71, 'Microsoft Azure Download group members - started (bulk)', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100015, 10, 71, 'Microsoft Azure Download group members - finished (bulk)', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100016, 10, 71, 'Microsoft Update lifecycle management policy', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100017, 10, 71, 'Microsoft Azure Download groups - started (bulk)', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100018, 10, 71, 'Microsoft Azure Download groups - finished (bulk)', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100019, 10, 71, 'Microsoft Azure Create lifecycle management policy', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100020, 10, 71, 'Microsoft Azure Bulk import group members - started (bulk)', 1, 1);
call STICreateDatasourceEntry(@pluginID, 100021, 10, 71, 'Microsoft Azure Bulk import group members - finished (bulk)', 1, 1);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
