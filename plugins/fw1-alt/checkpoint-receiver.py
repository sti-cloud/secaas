#!/usr/bin/python
#
# Daemon to receive syslog messages from Checkpoint's log server on UDP/515,
# reformat them properly, and relay them to USM on UDP/514.
#
# 2018-11-16 - Tom Martin
#

# from scapy.all import *
import subprocess
import socket

month_names = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]
def MonthName(num) :
  return month_names[int(num)-1]


# Determine the USM management IP address
mgmt_ip = subprocess.check_output('ifconfig eth0 | grep "inet addr"', stderr=subprocess.STDOUT, shell=True)
mgmt_ip = mgmt_ip.split(":")[1]
mgmt_ip = mgmt_ip.split(" ")[0]

# Listen for syslog messages on UDP port 515
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind( (mgmt_ip, 515) )

# Main body loop
while True:
  data, addr = sock.recvfrom(16384)
  if data :
    addr = addr[0]
    bracket = data.index('>')
    code = data[0:bracket+1]
    data = data[bracket+1:]
    if data[1] == ' ' and data[22] == ' ' :
      try :
        data = data[2:]  # Remove leading 1
        parts = data.split(" ", 2)   # Split into date, hostname, and "rest"

        yyyy = data[0:4] # Parse date into variables
        mm = data[5:7]
        dd = data[8:10]
        h = data[11:13]
        m = data[14:16]
        s = data[17:19]

        # Reformat the syslog properly
        newlog = MonthName(mm) + " " + dd + " " + h + ":" + m + ":" + s + " "   # Correct date format
        newlog += parts[1] + " "                                                # Hostname of sender
        newlog += yyyy + "-" + mm + "-" + dd + " " + h + ":" + m + ":" + s + " X " # Timestamp in different format and
                                                                                   # unused string (timezone?) required
                                                                                   # by USM rule
        newlog += parts[2]  # Remainder of syslog message without changes

        # Resend packet, spoofing the source
        # spoofed_packet = IP(src=addr, dst=mgmt_ip, ttl=128) / UDP(sport=10000, dport=514) / newlog
        # send(spoofed_packet, verbose=False)
        logfile = "/var/log/alienvault/devices/" + addr + "/" + addr + ".log"
        handle = open(logfile, "a")
        handle.write(newlog + "\n")
        handle.close()
      except :
        pass
