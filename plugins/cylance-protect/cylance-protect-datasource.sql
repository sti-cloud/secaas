--
-- Cylance Protect Datasource Changes
-- Last signatures update: NA
-- Written By: Sidney Eaton
-- Last Modified: 4-11-2017
--

-- Set plugin id variable
SET @pluginID = '20000';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (@pluginID, 1, 'cylance-protect', 'Cylance PROTECT', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 2000, 10, 71, 'CylancePROTECT: Corrupt File Threat', 1, 0);
call STICreateDatasourceEntry(@pluginID, 2001, 4, 40, 'CylancePROTECT: Threat Quarantined', 2, 0);
call STICreateDatasourceEntry(@pluginID, 2002, 10, 71, 'CylancePROTECT: Threat Changed', 1, 0);
call STICreateDatasourceEntry(@pluginID, 2003, 4, 40, 'CylancePROTECT: Threat Found', 1, 0);
call STICreateDatasourceEntry(@pluginID, 2004, 10, 71, 'CylancePROTECT: Threat Cleared', 1, 0);
call STICreateDatasourceEntry(@pluginID, 2100, 1, 17, 'CylancePROTECT: Memory Exploit Attempt Blocked', 2, 0);
call STICreateDatasourceEntry(@pluginID, 2103, 1, 17, 'CylancePROTECT: Memory Exploit Attempt Detected - No Policy Defined', 2, 0);
call STICreateDatasourceEntry(@pluginID, 2101, 1, 17, 'CylancePROTECT: Memory Exploit Attempt Allowed', 2, 0);
call STICreateDatasourceEntry(@pluginID, 2102, 1, 17, 'CylancePROTECT: Memory Exploit Attempt Terminated', 2, 0);
call STICreateDatasourceEntry(@pluginID, 100, 2, 216, 'CylancePROTECT: Research Saved', 2, 0);
call STICreateDatasourceEntry(@pluginID, 1002, 2, 86, 'CylancePROTECT: Registration', 2, 0);
call STICreateDatasourceEntry(@pluginID, 1001, 2, 79, 'CylancePROTECT: Device Assigned to Zone', 2, 0);
call STICreateDatasourceEntry(@pluginID, 1003, 2, 80, 'CylancePROTECT: Device Policy Assigned', 2, 0);
call STICreateDatasourceEntry(@pluginID, 1005, 2, 87, 'CylancePROTECT: Device Removed', 2, 0);
call STICreateDatasourceEntry(@pluginID, 1004, 2, 88, 'CylancePROTECT: Device Updated', 2, 0);
call STICreateDatasourceEntry(@pluginID, 1000, 18, 208, 'CylancePROTECT: System Security Event', 5, 0);
call STICreateDatasourceEntry(@pluginID, 20000000, NULL, NULL, 'CylancePROTECT: Generic', 0, 0);
call STICreateDatasourceEntry(@pluginID, 20, 11, 139, 'Cylance: Audit - Zone Add Device', 2, 2);


insert IGNORE into host_source_reference (id, name, relevance) VALUES (10000,"CylancePROTECT",7);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;