#!/usr/bin/python
import sys
import os
import fileinput
import re
import json
import sys

# For MYSQL INTEGRATION
import MySQLdb
sids = []
lost = []
CATEGORY = {}
CATEGORY['APP-DETECT'] = 1
CATEGORY['BLACKLIST'] = "NULL"
CATEGORY['BROWSER-CHROME'] = "NULL"
CATEGORY['BROWSER-FIREFOX'] = "NULL"
CATEGORY['BROWSER-IE'] = "NULL"
CATEGORY['BROWSER-OTHER'] = "NULL"
CATEGORY['BROWSER-PLUGINS'] = "NULL"
CATEGORY['BROWSER-WEBKIT'] = "NULL"
CATEGORY['CONTENT-REPLACE'] = "NULL"
CATEGORY['DELETED'] = "NULL"
CATEGORY['DOS'] = "NULL"
CATEGORY['EXPLOIT-KIT'] = "NULL"
CATEGORY['FILE-EXECUTABLE'] = "NULL"
CATEGORY['FILE-FLASH'] = "NULL"
CATEGORY['FILE-IDENTIFY'] = "NULL"
CATEGORY['FILE-IMAGE'] = "NULL"
CATEGORY['FILE-JAVA'] = "NULL"
CATEGORY['FILE-MULTIMEDIA'] = "NULL"
CATEGORY['FILE-OFFICE'] = "NULL"
CATEGORY['FILE-OTHER'] = "NULL"
CATEGORY['FILE-PDF'] = "NULL"
CATEGORY['INDICATOR-COMPROMISE'] = "NULL"
CATEGORY['INDICATOR-OBFUSCATION'] = "NULL"
CATEGORY['INDICATOR-SCAN'] = "NULL"
CATEGORY['INDICATOR-SHELLCODE'] = "NULL"
CATEGORY['MALWARE-BACKDOOR'] = "NULL"
CATEGORY['MALWARE-CNC'] = "NULL"
CATEGORY['MALWARE-OTHER'] = "NULL"
CATEGORY['MALWARE-TOOLS'] = "NULL"
CATEGORY['NETBIOS'] = "NULL"
CATEGORY['OS-LINUX'] = "NULL"
CATEGORY['OS-MOBILE'] = "NULL"
CATEGORY['OS-OTHER'] = "NULL"
CATEGORY['OS-SOLARIS'] = "NULL"
CATEGORY['OS-WINDOWS'] = "NULL"
CATEGORY['POLICY-MULTIMEDIA'] = "NULL"
CATEGORY['POLICY-OTHER'] = "NULL"
CATEGORY['POLICY-SOCIAL'] = "NULL"
CATEGORY['POLICY-SPAM'] = "NULL"
CATEGORY['PROTOCOL-DNS'] = "NULL"
CATEGORY['PROTOCOL-FINGER'] = "NULL"
CATEGORY['PROTOCOL-FTP'] = "NULL"
CATEGORY['PROTOCOL-ICMP'] = "NULL"
CATEGORY['PROTOCOL-IMAP'] = "NULL"
CATEGORY['PROTOCOL-NNTP'] = "NULL"
CATEGORY['PROTOCOL-POP'] = "NULL"
CATEGORY['PROTOCOL-RPC'] = "NULL"
CATEGORY['PROTOCOL-SCADA'] = "NULL"
CATEGORY['PROTOCOL-SERVICES'] = "NULL"
CATEGORY['PROTOCOL-SNMP'] = "NULL"
CATEGORY['PROTOCOL-TELNET'] = "NULL"
CATEGORY['PROTOCOL-TFTP'] = "NULL"
CATEGORY['PROTOCOL-VOIP'] = "NULL"
CATEGORY['PUA-ADWARE'] = "NULL"
CATEGORY['PUA-OTHER'] = "NULL"
CATEGORY['PUA-P2P'] = "NULL"
CATEGORY['PUA-TOOLBARS'] = "NULL"
CATEGORY['SCADA'] = "NULL"
CATEGORY['SERVER-APACHE'] = "NULL"
CATEGORY['SERVER-IIS'] = "NULL"
CATEGORY['SERVER-MAIL'] = "NULL"
CATEGORY['SERVER-MSSQL'] = "NULL"
CATEGORY['SERVER-MYSQL'] = "NULL"
CATEGORY['SERVER-ORACLE'] = "NULL"
CATEGORY['SERVER-OTHER'] = "NULL"
CATEGORY['SERVER-SAMBA'] = "NULL"
CATEGORY['SERVER-WEBAPP'] = "NULL"
CATEGORY['SQL'] = "NULL"
CATEGORY['X11'] = "NULL"

dict_exploit = {
  "ACTIVEX": "4",
  "SHELLCODE": "1",
  "SHELL": "1",
  "SQL": "2",
  "TRAVERSAL": "10",
  "SPOOF": "15",
  "PDF": "13",
  "CROSS": "6",
  "XSS": "6",
  "COMMAND": "5",
  "BROWSER": "3",
  "FILE INCLUDE": "8",
  "BUFFER": "14",
  "OVERFLOW": "14",
  "FORMAT STRING": "16",
  "\sDOS": "12",
  "DENIAL OF SERVICE": "12",
  "LINUX": "21",
  "WINDOWS": "9"
}

dict_suspicious = {
  "DNS\s": "57",
  "DOMAIN": "57",
  "SSH": "58",
  "NFS": "59",
  "RPC": "62",
  "SMTP": "63",
  "POP": "63",
  "IMAP": "63",
  "MAIL": "63",
  "SQL": "60",
  "POSTGRE": "60",
  "NETBIOS": "61"
}

dict_policy = {
  "P2P": "43",
  "FILE-SHARING": "43",
  "IM ": "44",
  "IRC": "44",
  "AIM": "44",
  "MSN": "44",
  "CHAT": "44",
  "PORN": "42",
  "GAME": "46",
  "PROXY": "45",
  "ANONYMIZER": "45",
  "TUNNEL": "45",
  "PHISHING": "189",
  "MESSENGER": "44"
}

dict_DDoS = {
  "FLOOD": "50",
  "DDOS": "51",
}

dict_malware = {
  "BACKDOOR": "41",
  "LOGGER": "36",
  "SPYWARE": "33",
  "TROJAN": "37",
  "VIRUS": "38",
  "WORM": "39",
  "ADWARE": "34",
  "MALWARE": "40"
}


dict_classid = {}

# Not in use but intend to revamp the intelligentMatcher and make it more intelligent in the future with severity to category matching.
dict_severity = {
  "exploit": dict_exploit,
  "vulnerability": dict_exploit,
  "malware": dict_malware
}
  
def populateClassIDDict():
  # The next three lines make ossim_setup.conf a well formed INI - This is a hack
  import StringIO
  ini_str = '[root]\n' + open("/etc/ossim/ossim_setup.conf", 'r').read()
  ini_fp = StringIO.StringIO(ini_str)

  # Read the files from the OSSIM_SETUP.conf to be able to login to the database
  import ConfigParser
  Config = ConfigParser.ConfigParser()
  Config.readfp(ini_fp)
  server = Config.get("database","db_ip")
  username = Config.get("database","user")
  passwd = Config.get("database","pass")

  # Log into the database and get all classifications then store them in memory
  db = MySQLdb.connect(server,username,passwd,"alienvault")
  cursor = db.cursor()
  cursor.execute("Select name,id from classification")
  results = cursor.fetchall()
  for (i, row) in enumerate(results):
   dict_classid[row[0]] = row[1]
  return results


# Put more specicific matches first
def IntelligentMatcher(plugindescription):
  RET_CAT=None
  RET_SUBCAT=None
  policy_subcat_results = check_for_subcategory_match(plugindescription,dict_policy)
  suspicious_subcat_results = check_for_subcategory_match(plugindescription,dict_suspicious)
  exploit_subcat_results = check_for_subcategory_match(plugindescription,dict_exploit)
  malware_subcat_results = check_for_subcategory_match(plugindescription,dict_malware)

  if malware_subcat_results is not None:
    RET_CAT="4"
    RET_SUBCAT=malware_subcat_results
  elif policy_subcat_results is not None:
    RET_CAT="5"
    RET_SUBCAT=policy_subcat_results
  elif exploit_subcat_results is not None:
    RET_CAT="1"
    RET_SUBCAT=exploit_subcat_results
  elif suspicious_subcat_results is not None:
    RET_CAT="7"
    RET_SUBCAT=suspicious_subcat_results

  if RET_CAT is None and RET_SUBCAT is None:
    return None
  elif RET_SUBCAT is None:
    return RET_CAT + ":NULL"
  else:
    return RET_CAT + ":" + RET_SUBCAT

def cat_APPDETECT(plugindescription):
  RET_CAT="8"
  RET_SUBCAT="NULL"
  temp = IntelligentMatcher(plugindescription)
  if temp is not None:
    return temp
  else:
    return RET_CAT + ":" + RET_SUBCAT
def cat_BLACKLIST(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="52"
  return RET_CAT + ":" + RET_SUBCAT
def cat_BROWSERCHROME(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="3"
  temp = check_for_subcategory_match(plugindescription,dict_exploit)
  if temp is not None:
    RET_CAT="1"
    RET_SUBCAT=temp
  return RET_CAT + ":" + RET_SUBCAT
def cat_BROWSERFIREFOX(plugindescription):
  return cat_BROWSERCHROME(plugindescription)
def cat_BROWSERIE(plugindescription):
  return cat_BROWSERCHROME(plugindescription)
def cat_BROWSEROTHER(plugindescription):
  return cat_BROWSERCHROME(plugindescription)
def cat_BROWSERPLUGINS(plugindescription):
  return cat_BROWSERCHROME(plugindescription)
def cat_BROWSERWEBKIT(plugindescription):
  return cat_BROWSERCHROME(plugindescription)
def cat_CONTENTREPLACE(plugindescription):
  RET_CAT="NULL"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_DELETED(plugindescription):
  RET_CAT="NULL"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_DOS(plugindescription):
  RET_CAT="6"
  RET_SUBCAT="NULL"
  temp = check_for_subcategory_match(plugindescription,dict_DDoS)
  if temp is not None:
    RET_CAT="6"
    RET_SUBCAT=temp
  return RET_CAT + ":" + RET_SUBCAT
def cat_EXPLOITKIT(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="17"
  temp = check_for_subcategory_match(plugindescription,dict_exploit)
  if temp is not None:
    RET_CAT="1"
    RET_SUBCAT=temp
  return RET_CAT + ":" + RET_SUBCAT
def cat_FILEEXECUTABLE(plugindescription):
  return cat_FILEIMAGE(plugindescription)
def cat_FILEFLASH(plugindescription):
  return cat_FILEIMAGE(plugindescription)
def cat_FILEIDENTIFY(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_FILEIMAGE(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="NULL"
  temp = check_for_subcategory_match(plugindescription,dict_exploit)
  if temp is not None:
    RET_CAT="1"
    RET_SUBCAT=temp
  return RET_CAT + ":" + RET_SUBCAT
def cat_FILEJAVA(plugindescription):
  RET_CAT="21"
  RET_SUBCAT="244"
  return RET_CAT + ":" + RET_SUBCAT
def cat_FILEMULTIMEDIA(plugindescription):
  return cat_FILEIMAGE(plugindescription)
def cat_FILEOFFICE(plugindescription):
  RET_CAT="21"
  RET_SUBCAT="244"
  return RET_CAT + ":" + RET_SUBCAT
def cat_FILEOTHER(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="NULL"
  temp = IntelligentMatcher(plugindescription)
  if temp is not None:
    return temp
  else:
    return RET_CAT + ":" + RET_SUBCAT
def cat_FILEPDF(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="13"
  return RET_CAT + ":" + RET_SUBCAT
def cat_INDICATORCOMPROMISE(plugindescription):
  return  cat_APPDETECT(plugindescription)
def cat_INDICATOROBFUSCATION(plugindescription):
  return  cat_APPDETECT(plugindescription)
def cat_INDICATORSCAN(plugindescription):
  RET_CAT="9"
  RET_SUBCAT="70"
  return RET_CAT + ":" + RET_SUBCAT
def cat_INDICATORSHELLCODE(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="1"
  return RET_CAT + ":" + RET_SUBCAT
def cat_MALWAREBACKDOOR(plugindescription):
  RET_CAT="4"
  RET_SUBCAT="41"
  return RET_CAT + ":" + RET_SUBCAT
def cat_MALWARECNC(plugindescription):
  RET_CAT="4"
  RET_SUBCAT=check_for_subcategory_match(plugindescription,dict_malware)

  if RET_SUBCAT is None:
    RET_SUBCAT = "NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_MALWAREOTHER(plugindescription):
  RET_CAT="4"
  RET_SUBCAT=check_for_subcategory_match(plugindescription,dict_malware)

  if RET_SUBCAT is None:
    RET_SUBCAT = "NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_MALWARETOOLS(plugindescription):
  RET_CAT="4"
  RET_SUBCAT=check_for_subcategory_match(plugindescription,dict_malware)

  if RET_SUBCAT is None:
    RET_SUBCAT = "NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_NETBIOS(plugindescription):
  return cat_APPDETECT(plugindescription)
def cat_OSLINUX(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="21"
  return RET_CAT + ":" + RET_SUBCAT
def cat_OSMOBILE(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_OSOTHER(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_OSSOLARIS(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_OSWINDOWS(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="9"
  return RET_CAT + ":" + RET_SUBCAT
def cat_POLICYMULTIMEDIA(plugindescription):
  RET_CAT="5"
  RET_SUBCAT="NULL"
  temp = IntelligentMatcher(plugindescription)
  if temp is not None:
    return temp
  else:
    return RET_CAT + ":" + RET_SUBCAT
def cat_POLICYOTHER(plugindescription):
  return cat_POLICYMULTIMEDIA(plugindescription)
def cat_POLICYSOCIAL(plugindescription):
  return cat_POLICYMULTIMEDIA(plugindescription)
def cat_POLICYSPAM(plugindescription):
  RET_CAT="13"
  RET_SUBCAT="149"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLDNS(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="57"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLFINGER(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLFTP(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="128"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLICMP(plugindescription):
  RET_CAT="NULL"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLIMAP(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="63"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLNNTP(plugindescription):
  RET_CAT="8"
  RET_SUBCAT="72"
  temp = check_for_subcategory_match(plugindescription,dict_exploit)
  if temp is not None:
    RET_CAT="1"
    RET_SUBCAT=temp
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLPOP(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="63"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLRPC(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="62"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLSCADA(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="56"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLSERVICES(plugindescription):
  RET_CAT="NULL"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLSNMP(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="55"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLTELNET(plugindescription):
  RET_CAT="8"
  RET_SUBCAT="68"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLTFTP(plugindescription):
  RET_CAT="8"
  RET_SUBCAT="64"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PROTOCOLVOIP(plugindescription):
  RET_CAT="NULL"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PUAADWARE(plugindescription):
  RET_CAT="4"
  RET_SUBCAT="34"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PUAOTHER(plugindescription):
  RET_CAT="5"
  RET_SUBCAT="47"
  temp = check_for_subcategory_match(plugindescription,dict_policy)
  if temp is not None:
    RET_CAT="5"
    RET_SUBCAT=temp
  return RET_CAT + ":" + RET_SUBCAT
def cat_PUAP2P(plugindescription):
  RET_CAT="5"
  RET_SUBCAT="43"
  return RET_CAT + ":" + RET_SUBCAT
def cat_PUATOOLBARS(plugindescription):
  RET_CAT="5"
  RET_SUBCAT="47"
  return RET_CAT + ":" + RET_SUBCAT
def cat_SCADA(plugindescription):
  RET_CAT="7"
  RET_SUBCAT="56"
  return RET_CAT + ":" + RET_SUBCAT
def cat_SERVERAPACHE(plugindescription):
  return cat_SERVERWEBAPP(plugindescription)
def cat_SERVERIIS(plugindescription):
  return cat_SERVERWEBAPP(plugindescription)
def cat_SERVERMAIL(plugindescription):
  RET_CAT="NULL"
  RET_SUBCAT="NULL"
  temp = IntelligentMatcher(plugindescription)
  if temp is not None:
    return temp
  else:
    return RET_CAT + ":" + RET_SUBCAT
def cat_SERVERMSSQL(plugindescription):
  return cat_SERVERWEBAPP(plugindescription)
def cat_SERVERMYSQL(plugindescription):
  return cat_SERVERWEBAPP(plugindescription)
def cat_SERVERORACLE(plugindescription):
  return cat_SERVERWEBAPP(plugindescription)
def cat_SERVEROTHER(plugindescription):
  return cat_SERVERWEBAPP(plugindescription)
def cat_SERVERSAMBA(plugindescription):
  return cat_SERVERWEBAPP(plugindescription)
def cat_SERVERWEBAPP(plugindescription):
  RET_CAT="1"
  RET_SUBCAT="NULL"
  temp = check_for_subcategory_match(plugindescription,dict_exploit)
  if temp is not None:
    RET_CAT="1"
    RET_SUBCAT=temp
  return RET_CAT + ":" + RET_SUBCAT
def cat_SQL(plugindescription):
  return cat_SERVERWEBAPP(plugindescription)
def cat_X11(plugindescription):
  RET_CAT="NULL"
  RET_SUBCAT="NULL"
  return RET_CAT + ":" + RET_SUBCAT

def match(regex, string):
  mo  = re.match('(?i).*(' + regex + ').*', string, re.IGNORECASE)
  #print "REGEX: " + regex + " STRING: " + string
  try:
    #print "FOUND: " + mo.group(1).upper()
    return  mo.group(1).upper()
  except AttributeError:
    return None   

#def returnValue(value):
#  return value

# This uses the dictionary key specified as the crucial part of the regular expression.  If there
# is a match then it pulls the value associated with the key and returns it.  Dictionary keys should
# be ordered in most specific key terms to least.
def check_for_subcategory_match(plugindescription, dictionary):
  for key in dictionary:
    #print "Now testing key: " + key
    test_case = match(key, plugindescription)
    if test_case is not None:
      #print "FOUND MATCH: " + key
      return dictionary.get(test_case)
  return None

def classification_method(base_category,plugindescription):
#    print plugindescription
#    base_reg = re.compile(r"(\S+)")
#    base_cat = base_reg.match(plugindescription)
#    print base_category
    switcher = {
        "APP-DETECT": cat_APPDETECT,
        "BLACKLIST": cat_BLACKLIST,
        "BROWSER-CHROME": cat_BROWSERCHROME,
        "BROWSER-FIREFOX": cat_BROWSERFIREFOX,
        "BROWSER-IE": cat_BROWSERIE,
        "BROWSER-OTHER": cat_BROWSEROTHER,
        "BROWSER-PLUGINS": cat_BROWSERPLUGINS,
        "BROWSER-WEBKIT": cat_BROWSERWEBKIT,
        "CONTENT-REPLACE": cat_CONTENTREPLACE,
        "DELETED": cat_DELETED,
        "DOS": cat_DOS,
        "EXPLOIT-KIT": cat_EXPLOITKIT,
        "FILE-EXECUTABLE": cat_FILEEXECUTABLE,
        "FILE-FLASH": cat_FILEFLASH,
        "FILE-IDENTIFY": cat_FILEIDENTIFY,
        "FILE-IMAGE": cat_FILEIMAGE,
        "FILE-JAVA": cat_FILEJAVA,
        "FILE-MULTIMEDIA": cat_FILEMULTIMEDIA,
        "FILE-OFFICE": cat_FILEOFFICE,
        "FILE-OTHER": cat_FILEOTHER,
        "FILE-PDF": cat_FILEPDF,
        "INDICATOR-COMPROMISE": cat_INDICATORCOMPROMISE,
        "INDICATOR-OBFUSCATION": cat_INDICATOROBFUSCATION,
        "INDICATOR-SCAN": cat_INDICATORSCAN,
        "INDICATOR-SHELLCODE": cat_INDICATORSHELLCODE,
        "MALWARE-BACKDOOR": cat_MALWAREBACKDOOR,
        "MALWARE-CNC": cat_MALWARECNC,
        "MALWARE-OTHER": cat_MALWAREOTHER,
        "MALWARE-TOOLS": cat_MALWARETOOLS,
        "NETBIOS": cat_NETBIOS,
        "OS-LINUX": cat_OSLINUX,
        "OS-MOBILE": cat_OSMOBILE,
        "OS-OTHER": cat_OSOTHER,
        "OS-SOLARIS": cat_OSSOLARIS,
        "OS-WINDOWS": cat_OSWINDOWS,
        "POLICY-MULTIMEDIA": cat_POLICYMULTIMEDIA,
        "POLICY-OTHER": cat_POLICYOTHER,
        "POLICY-SOCIAL": cat_POLICYSOCIAL,
        "POLICY-SPAM": cat_POLICYSPAM,
        "PROTOCOL-DNS": cat_PROTOCOLDNS,
        "PROTOCOL-FINGER": cat_PROTOCOLFINGER,
        "PROTOCOL-FTP": cat_PROTOCOLFTP,
        "PROTOCOL-ICMP": cat_PROTOCOLICMP,
        "PROTOCOL-IMAP": cat_PROTOCOLIMAP,
        "PROTOCOL-NNTP": cat_PROTOCOLNNTP,
        "PROTOCOL-POP": cat_PROTOCOLPOP,
        "PROTOCOL-RPC": cat_PROTOCOLRPC,
        "PROTOCOL-SCADA": cat_PROTOCOLSCADA,
        "PROTOCOL-SERVICES": cat_PROTOCOLSERVICES,
        "PROTOCOL-SNMP": cat_PROTOCOLSNMP,
        "PROTOCOL-TELNET": cat_PROTOCOLTELNET,
        "PROTOCOL-TFTP": cat_PROTOCOLTFTP,
        "PROTOCOL-VOIP": cat_PROTOCOLVOIP,
        "PUA-ADWARE": cat_PUAADWARE,
        "PUA-OTHER": cat_PUAOTHER,
        "PUA-P2P": cat_PUAP2P,
        "PUA-TOOLBARS": cat_PUATOOLBARS,
        "SCADA": cat_SCADA,
        "SERVER-APACHE": cat_SERVERAPACHE,
        "SERVER-IIS": cat_SERVERIIS,
        "SERVER-MAIL": cat_SERVERMAIL,
        "SERVER-MSSQL": cat_SERVERMSSQL,
        "SERVER-MYSQL": cat_SERVERMYSQL,
        "SERVER-ORACLE": cat_SERVERORACLE,
        "SERVER-OTHER": cat_SERVEROTHER,
        "SERVER-SAMBA": cat_SERVERSAMBA,
        "SERVER-WEBAPP": cat_SERVERWEBAPP,
        "SQL": cat_SQL,
        "X11": cat_X11
    }
    # Get the function from switcher dictionary
    func = switcher.get(base_category, IntelligentMatcher(plugindescription))
    # Execute the function
    return func(plugindescription)

def parseLine(line):
    #Reject commented rules
    if line[0] == '#':
    	line = line[1:len(line)-1]
        line = line.lstrip()
    #Reject no alert actions
    if line[0:5] != 'alert':
        return None
    p = re.compile(r".*msg:\s*\"\s*([^;]+)\";")
    p = re.compile(r".*msg:\s*\"\s*((\S+)([^;]+))\";")
    m = p.match(line)
    if (m):
	    name = m.group(1)
	    #print name
            classification_result = classification_method(m.group(2),m.group(1))
	    p = re.compile(r".*sid:\s*([^;]+);")
	    m = p.match(line)
	    sid = m.group(1)
	    #print sid
	    p = re.compile(r".*[classtype|category]:\s*([^;]+);")
	    m = p.match(line)
	    classtype = m.group(1)
	    #print classtype
	    p = re.compile(r"^ET\s([^\s]+).*")
	    m = p.match(name)
	    if (m):
		    group = m.group(1)
	    else:
		    p = re.compile(r"^([^\s]+).*")
		    m = p.match(name)
		    if (m):
			    group = m.group(1)
			    #print group
            category = classification_result.split(':')[0]
            subcategory = classification_result.split(':')[1]
	    data = [name,sid,classtype,group,category,subcategory]
	    return data
	    #print data


populateClassIDDict()
#print dict_classid

snort_dir = sys.argv[1]
snort_files = []
rules = {}
#print "Looking for snort rule files:" + snort_dir
for root, dirs, files in os.walk(snort_dir, topdown=True):
	for name in files:
		p = re.compile(r".*rules")
		m = p.match(name)
		if (m):
			#print "\t" + m.group(0)	
			snort_files.append(m.group(0))
for fi in snort_files:
	#[name,sid,classtype,group]
	for line in fileinput.input(snort_dir + fi):
		data = parseLine(line)
		if data:
			#print "INSERT IGNORE INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, class_id, name) VALUES (1001, %s, %s, %s, %s, '%s');" % (data[1], data[4], data[5], dict_classid.get(data[2], "NULL"), data[0].replace("'",r"\'"))
			print "INSERT IGNORE INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, class_id, name) VALUES (1727, %s, %s, %s, %s, '%s');" % (data[1], data[4], data[5], dict_classid.get(data[2], "NULL"), data[0].replace("'",r"\'"))
			#print "INSERT IGNORE INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, class_id, name) VALUES (1727, %s, %s, %s, %s, 'Cisco-Firesight: %s') ON DUPLICATE KEY UPDATE name = 'Cisco-Firesight: %s';" % (data[1], data[4], data[5], dict_classid.get(data[2], "NULL"), data[0].replace("'",r"\'"),data[0].replace("'",r"\'"))

