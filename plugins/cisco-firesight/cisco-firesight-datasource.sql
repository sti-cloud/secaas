--
-- Cisco FireSight Update By Sentinel Technologies
-- Written By: Sidney Eaton
-- Last Modified: 7-20-2016
--

insert into host_source_reference (id, name, relevance) VALUES (10002,"FireSight",3) ON DUPLICATE KEY UPDATE relevance = 3;