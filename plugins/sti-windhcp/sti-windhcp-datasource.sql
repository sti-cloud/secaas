--
-- Sentinel Windows DHCP Plugin
-- Written By: Tom Martin
-- Last Modified: 2017-03-16
--

SET @pluginID = '21003';

-- Insert product if not already exists
-- DELETE FROM plugin WHERE id = @pluginID;
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
VALUES (@pluginID, 1, 'STI-WinDHCP', 'STI Windows DHCP', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

DELETE FROM plugin_sid WHERE plugin_id = @pluginID;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 0, 10, 71, 'The log was stopped', 5, 1);
call STICreateDatasourceEntry(@pluginID, 1, 10, 71, 'The log was started', 5, 1);
call STICreateDatasourceEntry(@pluginID, 2, 13, 222, 'The log was paused due to low disk space', 5, 3);
call STICreateDatasourceEntry(@pluginID, 10, 13, 220, 'A new IP address was leased to a client', 5, 1);
call STICreateDatasourceEntry(@pluginID, 11, 13, 220, 'An IP lease was renewed by a client', 5, 1);
call STICreateDatasourceEntry(@pluginID, 12, 13, 219, 'An IP address was released by a client', 5, 1);
call STICreateDatasourceEntry(@pluginID, 13, 13, 218, 'An IP address conflict was detected', 5, 3);
call STICreateDatasourceEntry(@pluginID, 14, 13, 222, 'A DHCP lease failed because the scope is exhausted', 5, 3);
call STICreateDatasourceEntry(@pluginID, 20, 13, 220, 'A BOOTP address was leased to a client', 5, 1);
call STICreateDatasourceEntry(@pluginID, 30, 10, 71, 'DNS Update Request', 1, 1);
call STICreateDatasourceEntry(@pluginID, 99, 10, 71, 'Miscellaneous DHCP Event', 1, 1);

-- Add host source reference for IDM
INSERT IGNORE INTO host_source_reference (id, name, relevance) VALUES (11000, 'STI WinDHCP', 7) ON DUPLICATE KEY UPDATE relevance=7;
