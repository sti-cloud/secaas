--
-- Sentinel BloxOne Plugin
-- Written By: Matt Weiss
-- Last Modified: 2021-05-20
--

SET @pluginID = '40008';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'STI-BloxOne', 'BloxOne', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 2, 24, 'BloxOne Passthru', 4, 2);
call STICreateDatasourceEntry(@pluginID, 2, 2, 25, 'BloxOne Redirect', 4, 3);
call STICreateDatasourceEntry(@pluginID, 3, 2, 89, 'BloxOne Deny', 4, 2);
call STICreateDatasourceEntry(@pluginID, 4, 10, 71, 'BloxOne Nxdomain', 4, 3);
call STICreateDatasourceEntry(@pluginID, 5, 10, 71, 'BloxOne Refuse', 4, 3);
call STICreateDatasourceEntry(@pluginID, 20000000, 10, 71, 'BloxOne Generic', 4, 3);

-- Cleanup
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;