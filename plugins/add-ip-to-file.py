#!/usr/bin/python
import sys
sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")


ip = str(sys.argv[1])
file = str(sys.argv[2])

if len(sys.argv) == 4:
    comment = str(sys.argv[3])
else :
    comment = None

ipExists=0
with open(file,'r') as readFile:
    if ip != '0.0.0.0' and ip.strip() !="":
        for line in readFile:
            if line.strip() != "":
                currentip = line.split(None, 1)[0]
                if ip == currentip:
                    ipExists=1
                    break
    else:
        ipExists = 1
if ipExists == 0:
    with open(file, 'a+') as writeFile:
        if comment is None:
            writeFile.write(ip+'\n')
        else:
            writeFile.write(ip+' #'+comment+'\n')