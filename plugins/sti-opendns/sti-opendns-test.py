#!/usr/bin/python
###############################################################################
#
# Script: STI OpenDNS Tester
# Author: Tom Martin
# Date:   2017-02-16
# Version: 1.1
#
# Description: This program verifies connectivity to OpenDNS, intended to be
#    used before activating the OpenDNS plugin
#
# Enhancements:
# 1.0 - Initial Release
# 1.1 - Sidney Eaton - Allows for PREFIX filtering, required for Cisco Umbrella
###############################################################################

import sys
import os.path
import zlib
import ConfigParser
import time
import string
sys.path.append( "/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
import boto3
from botocore.exceptions import ClientError

### Default settings
CONFIG_FILE='/etc/ossim/agent/plugins/sti-opendns.config'
LOG_FILE='/var/log/opendns.log'
CYCLE_INTERVAL=600
ACCESS_KEY='?'
SECRET_KEY='?'
REGION='us-east-1'
BUCKET='stiopendnslogs'
PREFIX=''     # Prefix should be followed with a '/' in many cases.
DELETE_FROM_AWS=False
INCLUDE_ALLOWED=True

if os.path.isfile(CONFIG_FILE):
    config = ConfigParser.ConfigParser()
    config.read(CONFIG_FILE)
    if config.has_option('default', 'ACCESS_KEY'):
        ACCESS_KEY = config.get('default', 'ACCESS_KEY')
    if config.has_option('default', 'SECRET_KEY'):
        SECRET_KEY = config.get('default', 'SECRET_KEY')
    if config.has_option('default', 'REGION'):
        REGION = config.get('default', 'REGION')
    if config.has_option('default', 'BUCKET'):
        BUCKET = config.get('default', 'BUCKET')
    if config.has_option('default', 'PREFIX'):
        PREFIX = config.get('default', 'PREFIX')


s3 = boto3.resource('s3',
                    aws_access_key_id=ACCESS_KEY,
                    aws_secret_access_key=SECRET_KEY,
                    region_name=REGION)

print "===== TEST RESULTS ====="
try:
    bucketlist=None
    bucket = s3.Bucket(BUCKET)
    if PREFIX.strip():
      bucketlist=bucket.objects.filter(Prefix=PREFIX)
      print "FILTER/PREFIX used.....yes"
    else:
      bucketlist=bucket.objects.all()
      print "FILTER/PREFIX used.....no"

    for res in bucketlist:
        print "Found file %s" % res.key
        break

    print "Everything read looks good!"
except:
    print "Error accessing AWS:", sys.exc_info()

try:
    bucket = s3.Bucket(BUCKET)
    bucket.put_object(Key=PREFIX+'sti-write-test.txt', Body="This is a test file")
    print "Can write to AWS S3.....yes"

    try:
       awsfile = s3.Object(res.bucket_name, PREFIX+'sti-write-test.txt')
       awsfile.delete()
       print "Can delete from AWS S3.....yes"
    except:
       print "Can delete from AWS S3.....NO", sys.exc_info()
except:
    print "Can write to AWS S3.....NO", sys.exc_info()
    print "Can delete from AWS S3.....Unknown"