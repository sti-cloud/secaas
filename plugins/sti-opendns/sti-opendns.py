#!/usr/bin/python
###################################################################################
#
# Script: STI OpenDNS
# Author: Tom Martin
# Date:   2017-25-11
# Version: 1.2
#
# Description: This program downloads OpenDNSAVAULTs from Amazon S3 and appends
#    them to aAVAULT file monitored by AlienVault for processing.
#
# Version
# 1.0  - Initial Release
# 1.1  - Sidney: Added filtering for Cisco managed umbrella and local bookmarking
# 1.2  - Sidney: Fix issue where object already read wouldn't be deleted if delete
#        delete setting was changed.
###################################################################################

import sys
import os.path
import zlib
import ConfigParser
import time
import string
sys.path.append( "/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
import boto3
from time import gmtime, strftime

### Default settings
BOOKMARK_FILE='/etc/ossim/agent/plugins/sti-opendns.bookmark'
CONFIG_FILE='/etc/ossim/agent/plugins/sti-opendns.config'
DEBUG_FILE='/var/log/opendns.debug'
AVAULT_FILE='/var/log/opendns.log'
CYCLE_INTERVAL=600
ACCESS_KEY='?'
SECRET_KEY='?'
REGION='us-east-1'
BUCKET='stiopendnslogs'
PREFIX=''     # Prefix should be followed with a '/' in many cases.
DELETE_FROM_AWS=False
INCLUDE_ALLOWED=True
DEBUG_HISTORY=False
AWS_READ_ONLY=False

### Global variables
DEBUG = None             # Debug output file
AVAULT = None            # AlienVault log file (to generate events)
s3 = None                # Connection to S3
LAST_FILE_PROCESSED=''   # Track the last S3 file processed
bucket = None            # S3 bucket that contains OpenDNS log files

##############################################################################
def DLOG(line) :
    global DEBUG

    DEBUG.write(strftime("%Y-%m-%d %H:%M:%S", gmtime()) + ': ' + line + '\n')
    DEBUG.flush()

##############################################################################
def ConnectToS3() :
    global LAST_FILE_PROCESSED, s3, last_processed, AVAULT, bucket

    s3 = boto3.resource('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY, region_name=REGION)

    # Attempt to use the connection and see if it causes an error.
    try :
        bucket = s3.Bucket(BUCKET)
        # Old code used objects.all() which does not work when permissions are applied at subfolder or file levels
        # new code allows using filter() syntax when specified in the configuration.
        bucketlist=None
        if PREFIX.strip():
          bucketlist=bucket.objects.filter(Prefix=PREFIX)
        else:
          bucketlist=bucket.objects.all()

        for res in bucketlist:
          DLOG('Found ' + res.key + ' in the AWS S3 bucket. AWS settings look good.')
          break
    except Exception as e:
        DLOG('Unable to access the AWS S3 Bucket. AWS settings are probably incorrect.')
        AVAULT.write('STI-OPENDNS ERROR S3 BUCKET: AWS settings are probably incorrect.\n')
        s3 = None
        return

    # Get the last processed file so that we can continue from where we left off
    try:
        if (AWS_READ_ONLY) :
          bookmark = open(BOOKMARK_FILE, "r")
          LAST_FILE_PROCESSED = bookmark.read()
          bookmark.close()
        else :
          last_processed = s3.Object(BUCKET, "sti-opendns.plugin")
          LAST_FILE_PROCESSED = last_processed.get()["Body"].read()
    except Exception as e:
        DLOG('Error while trying to get last processed file, file may not exists and this is normal on fresh installs')
        True       # On fresh installs, the file will not exist. This may not be an error.

##############################################################################
def ReadConfig() :
    global ACCESS_KEY, SECRET_KEY, REGION, BUCKET, PREFIX, DEBUG_FILE, DEBUG_HISTORY, DELETE_FROM_AWS, AVAULT_FILE
    global CYCLE_INTERVAL, INCLUDE_ALLOWED, DEBUG, DEBUG_HISTORY, AWS_READ_ONLY, BOOKMARK_FILE

    # Track changes to settings
    OLD_ACCESS_KEY = ACCESS_KEY
    OLD_SECRET_KEY = SECRET_KEY
    OLD_REGION = REGION
    OLD_BUCKET = BUCKET
    OLD_DEBUG_FILE = DEBUG_FILE
    OLD_DEBUG_HISTORY = DEBUG_HISTORY

    # Update settings from the configuration file
    if os.path.isfile(CONFIG_FILE):
        config = ConfigParser.ConfigParser()
        config.read(CONFIG_FILE)
        if config.has_option('default', 'ACCESS_KEY'):
            ACCESS_KEY = config.get('default', 'ACCESS_KEY')
        if config.has_option('default', 'SECRET_KEY'):
            SECRET_KEY = config.get('default', 'SECRET_KEY')
        if config.has_option('default', 'REGION'):
            REGION = config.get('default', 'REGION')
        if config.has_option('default', 'BUCKET'):
            BUCKET = config.get('default', 'BUCKET')
        if config.has_option('default', 'DELETE_FROM_AWS'):
            DELETE_FROM_AWS = config.getboolean('default', 'DELETE_FROM_AWS')
        if config.has_option('default', 'AVAULT_FILE'):
            AVAULT_FILE = config.get('default', 'AVAULT_FILE')
        if config.has_option('default', 'CYCLE_INTERVAL'):
            CYCLE_INTERVAL = config.getint('default', 'CYCLE_INTERVAL')
        if config.has_option('default', 'INCLUDE_ALLOWED'):
            INCLUDE_ALLOWED = config.getboolean('default', 'INCLUDE_ALLOWED')
        if config.has_option('default', 'DEBUG_FILE'):
            DEBUG_FILE = config.get('default', 'DEBUG_FILE')
        if config.has_option('default', 'DEBUG_HISTORY'):
            DEBUG_HISTORY = config.getboolean('default', 'DEBUG_HISTORY')
        if config.has_option('default', 'AWS_READ_ONLY'):
            AWS_READ_ONLY = config.getboolean('default', 'AWS_READ_ONLY')
        if config.has_option('default', 'PREFIX'):
            PREFIX = config.get('default', 'PREFIX')
        if config.has_option('default', 'BOOKMARK_FILE'):
            BOOKMARK_FILE = config.get('default', 'BOOKMARK_FILE')


    # If any debug settings have changed, use the new settings
    if (DEBUG == None or OLD_DEBUG_FILE != DEBUG_FILE or OLD_DEBUG_HISTORY != DEBUG_HISTORY) :
        if DEBUG :
            DEBUG.close()
        if (DEBUG_HISTORY == True) :
            DEBUG = open(DEBUG_FILE, 'a')
        else :
            DEBUG = open(DEBUG_FILE, 'w')

    # If any S3 settings have changed, reconnect to S3
    if (s3 == None or OLD_ACCESS_KEY != ACCESS_KEY or OLD_SECRET_KEY != SECRET_KEY or OLD_REGION != REGION or OLD_BUCKET != BUCKET) :
        ConnectToS3()

##############################################################################
def WaitForOpenDNS() :
    global DEBUG, DEBUG_HISTORY

    # Detect service startup to minimize the sleep delay
    service_startup = False
    if (ACCESS_KEY == "?") :
        service_startup = True

    # Refresh settings from config every wait cycle
    ReadConfig()

    # Note the start of a new processing cycle in the debug log
    DLOG('---------------------------------------')

    # Only sleep for 60 seconds if the service is starting up. We want to make
    # sure AlienVault's file monitor process has started before we append data
    # to the log file. If not starting up, then sleep longer while we wait for
    # OpenDNS to add more data to S3 (roughly every 10 minutes)
    sleep_duration = CYCLE_INTERVAL
    if (service_startup and sleep_duration > 60) :
        sleep_duration = 60
        DLOG('Service startup detected. Sleeping for 60 seconds instead of ' + str(CYCLE_INTERVAL))
    else :
        DLOG('Sleeping for ' + str(sleep_duration) + ' seconds')

    time.sleep(sleep_duration)

    # Wipe the debug history if long-term history is not enabled
    if (DEBUG_HISTORY == False) :
        DEBUG.close()
        DEBUG = open(DEBUG_FILE, 'w')

#####################
# Main program loop
#####################
AVAULT = open(AVAULT_FILE, 'a')

while True:
    # Do not check too often. OpenDNS only adds a new file to AWS every 10 minutes.
    WaitForOpenDNS()

    # Read any new OpenDNS logs from AWS S3 and put them in the AlienVault monitored file
    num_processed = 0
    num_skipped = 0
    num_error = 0
    try :
        bucketlist=None
        if PREFIX.strip():
          bucketlist=bucket.objects.filter(Prefix=PREFIX)
        else:
          bucketlist=bucket.objects.all()

        for res in bucketlist:
            aws_filename = res.key
            awsfile = None

            # OpenDNSAVAULTs are gzipped, so ignore any other files
            if aws_filename.endswith(".gz"):
              try:
                # SIDNEY: Make awsfile non-null or else already processed files from previous runs will
                # not be deleted.
                awsfile = s3.Object(res.bucket_name, res.key)
                # Process only newer files than the last processed
                if aws_filename > LAST_FILE_PROCESSED:
                    try :
                        #awsfile = s3.Object(res.bucket_name, res.key)
                        data = zlib.decompress(awsfile.get()["Body"].read(), 31)
                        # Write the data to the file AlienVault monitors
                        if INCLUDE_ALLOWED :
                            AVAULT.write(data)
                        else :
                            lines = data.split('\n')
                            for line in lines:
                                if "Allowed" not in line:
                                    AVAULT.write(line + '\n')
                        LAST_FILE_PROCESSED = aws_filename
                        num_processed = num_processed + 1
                        DLOG('Successfully processed file ' + res.key)
                    except Exception as e:
                        num_error = num_error + 1
                        DLOG('Unable to process ' + res.key)
                        AVAULT.write('STI-OPENDNS ERROR S3 File process error: ' + res.key + '\n')
                        # CONTINUE Added by Sidney in case bucket zlib decompress error.  This should avoid deleting a file
                        # that hasn't been processed correctly.  Unsure, if this is the desired result at the moment since deleting
                        # files has been a fix in the past.
                        continue
                else :
                    DLOG('Skipping ' + res.key + '. It has already been processed.')
                    num_skipped = num_skipped + 1

                # If desired, delete OpenDNS logs that have been processed. If the
                # file was skipped or could not be read, then do not delete it.
                if DELETE_FROM_AWS and awsfile:
                    DLOG('Deleting ' + res.key + ' from AWS S3')
                    awsfile.delete()
              except Exception as e:
                num_error = num_error + 1
                DLOG('Unable to process ' + res.key)
                AVAULT.write('STI-OPENDNS ERROR S3 File process error: ' + res.key + '\n')
                continue
    except :
        DLOG('An unexpected error was encountered during the processing loop. Resetting AWS.')
        s3 = None
        continue

    # Provide a debug summary
    DLOG('AWS processing complete. Summary:')
    DLOG('* Number of files processed: ' + str(num_processed))
    DLOG('* Number of files skipped: ' + str(num_skipped))
    DLOG('* Number of files with errors: ' + str(num_error))

    # Processed all AWS files. Save our position back to AWS.
    if (num_processed > 0 and AWS_READ_ONLY == False ) :
        DLOG('Saving our place back to AWS:')
        try :
            last_processed = bucket.put_object(Key='sti-opendns.plugin', Body=LAST_FILE_PROCESSED)
        except Exception as e:
            DLOG('Unable to save processing position back to AWS')
            AVAULT.write('STI-OPENDNS ERROR S3 Write: Attempting to save position back to AWS\n')
    elif  (num_processed > 0 and AWS_READ_ONLY == True ) :
        DLOG('Saving our place back to local system:')
        try :
            bookmark = open(BOOKMARK_FILE, 'w+')
            bookmark.write(LAST_FILE_PROCESSED)
            bookmark.close()
        except Exception as e:
            DLOG('Unable to save processing position back to local drive')
            AVAULT.write('STI-OPENDNS ERROR LOCAL Write: Attempting to save position back to local drive\n')
