--
-- Websense Email Security Gateway Datasource
-- Last signatures update: NA
-- Written By: Sidney Eaton
-- Last Modified: 1-20-2017
--

SET @pluginID = '20003';

DELETE FROM plugin WHERE id = @pluginID;
DELETE FROM plugin_sid where plugin_id = @pluginID;

INSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (@pluginID, 1, 'Websense ESG', 'WebSense Email Security Gateway', 16);

INSERT IGNORE INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES
(@pluginID, 1, 10, 71, 'Websense ESG: Connection Open', 3, 1),
(@pluginID, 2, 7, 63, 'Websense ESG: Message', 3, 1),
(@pluginID, 3, 13, 174, 'Websense ESG: Email Delivered', 3, 1),
(@pluginID, 100, 13, 176, 'Websense ESG: Clean Email Event', 3, 1),
(@pluginID, 101, 13, 150, 'Websense ESG: Block Email Event', 3, 1),
(@pluginID, 102, 7, 63, 'Websense ESG: Commercial Email Event', 3, 1),
(@pluginID, 103, 7, 63, 'Websense ESG: Data Email Event', 3, 1),
(@pluginID, 104, 5, 189, 'Websense ESG: Phishing Email Event', 3, 1),
(@pluginID, 105, 13, 149, 'Websense ESG: Spam Email Event', 3, 1),
(@pluginID, 106, 12, 97, 'Websense ESG: Virus Email Event', 3, 1),
(@pluginID, 107, 10, 71, 'Websense ESG: Custom Email Event', 3, 1);



-- Turns out AlienVault has several duplicate tables for plugin_sid.  They seem to get populated when a manual datasource is entered from the GUI.  This is just to mimick that.
DELETE FROM plugin_sid_changes where plugin_id = @pluginID;
DELETE FROM plugin_sid_orig where plugin_id = @pluginID;
INSERT IGNORE INTO plugin_sid_changes (plugin_ctx, plugin_id, sid, class_id, aro, category_id, subcategory_id, name, reliability, priority) SELECT plugin_ctx, plugin_id, sid, class_id, aro, category_id, subcategory_id, name, reliability, priority FROM plugin_sid WHERE plugin_id = @pluginID;
INSERT IGNORE INTO plugin_sid_orig (plugin_ctx, plugin_id, sid, class_id, aro, category_id, subcategory_id, name, reliability, priority) SELECT plugin_ctx, plugin_id, sid, class_id, aro, category_id, subcategory_id, name, reliability, priority FROM plugin_sid WHERE plugin_id = @pluginID;


