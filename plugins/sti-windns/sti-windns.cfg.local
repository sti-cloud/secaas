# Sentinel Technologies - STI WinDNS Plugin
# Author: Sidney Eaton
# Plugin sti-windns id:20004 version: 0.0.1
# Last modification: 2017-05-8
#
# Plugin Selection Info:
# STI:WinDNS:Server 2008
#
# END-HEADER
# Description:
#   This plugin processes Windows DNS Debug log entries
#

[DEFAULT]
plugin_id=20004

[config]
custom_functions_file=/etc/ossim/agent/plugins/custom_functions/sti-windns-custom_functions.cfg
type=detector
enable=yes
source=log
location=/var/ossec/logs/alerts/alerts.log
create_file=false

# The -x checks scripts of the pidof command.  Required for python or the whole thing breaks.
process=
start=   ; launch plugin process when agent starts
stop=    ; shutdown plugin process when agent stops
restart= ; restart plugin process after each interval
startup=
shutdown=


[0010 - Recursive DNS Request]
event_type=event
precheck="Snd"
regexp="SRCIP:\s+\"(?P<remote_addr>\S+)\"; HOSTNAME:\s+\"\((?P<server_name>\S*?)\)\s*(?P<server>[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})->.+\s+\w+\s+PACKET\s+\S+\s+(?P<proto>UDP|TCP)\s+Snd\s+\S+\s+(?P<dnsid>\w+)\s+Q\s+\[\d+\s+(\w+\s+)*(?P<status>[^]]+)\]\s+(?P<record_type>\S+)\s+(?P<domain>\S+)\[END\]"
dst_port=53
dst_ip={$remote_addr}
device={$server}
src_ip={$server}
protocol={normalize_protocol($proto)}
plugin_sid=1
userdata1={$server_name}
userdata2={$status}
userdata3={$record_type}
userdata4={:parseDNS($domain)}
userdata5={$dnsid}

[0011 - DNS Request Received]
event_type=event
precheck="Rcv"
regexp="SRCIP:\s+\"(?P<remote_addr>\S+)\"; HOSTNAME:\s+\"\((?P<server_name>\S*?)\)\s*(?P<server>[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})->.+\s+\w+\s+PACKET\s+\S+\s+(?P<proto>UDP|TCP)\s+Rcv\s+\S+\s+(?P<dnsid>\w+)\s+Q\s+\[\d+\s+(\w+\s+)*(?P<status>[^]]+)\]\s+(?P<record_type>\S+)\s+(?P<domain>\S+)\[END\]"
dst_port=53
src_ip={$remote_addr}
device={$server}
dst_ip={$server}
protocol={normalize_protocol($proto)}
plugin_sid=2
userdata1={$server_name}
userdata2={$status}
userdata3={$record_type}
userdata4={:parseDNS($domain)}
userdata5={$dnsid}

[0012 - DNS Response Received]
event_type=event
precheck="Rcv"
regexp="SRCIP:\s+\"(?P<remote_addr>\S+)\"; HOSTNAME:\s+\"\((?P<server_name>\S*?)\)\s*(?P<server>[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})->.+\s+\w+\s+PACKET\s+\S+\s+(?P<proto>UDP|TCP)\s+Rcv\s+\S+\s+(?P<dnsid>\w+)\s+R\s+Q\s+\[\d+\s+(\w+\s+)*(?P<status>[^]]+)\]\s+(?P<record_type>\S+)\s+(?P<domain>\S+)\[END\]"
src_port=53
src_ip={$remote_addr}
device={$server}
dst_ip={$server}
protocol={normalize_protocol($proto)}
plugin_sid=3
userdata1={$server_name}
userdata2={$status}
userdata3={$record_type}
userdata4={:parseDNS($domain)}
userdata5={$dnsid}

[0013 - DNS Response Sent]
event_type=event
precheck="Snd"
regexp="SRCIP:\s+\"(?P<remote_addr>\S+)\"; HOSTNAME:\s+\"\((?P<server_name>\S*?)\)\s*(?P<server>[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})->.+\s+\w+\s+PACKET\s+\S+\s+(?P<proto>UDP|TCP)\s+Snd\s+\S+\s+(?P<dnsid>\w+)\s+R\s+Q\s+\[\d+\s+(\w+\s+)*(?P<status>[^]]+)\]\s+(?P<record_type>\S+)\s+(?P<domain>\S+)\[END\]"
src_port=53
dst_ip={$remote_addr}
device={$server}
src_ip={$server}
protocol={normalize_protocol($proto)}
plugin_sid=4
userdata1={$server_name}
userdata2={$status}
userdata3={$record_type}
userdata4={:parseDNS($domain)}
userdata5={$dnsid}

[9000 - Default]
event_type=event
precheck="STI-DNS"
regexp="SRCIP:\s+\"(?P<remote_addr>\S+)\"; HOSTNAME:\s+\"\((?P<server_name>\S*?)\)\s*(?P<server>[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})->.+\s+\w+\s+PACKET\s+\S+\s+(?P<proto>UDP|TCP)\s+(Rcv|Snd)\s+\S+\s+(?P<dnsid>\w+)\s+(?P<dnsopts>(\w+\s*)*)\s+\[\d+\s+(\w+\s+)*(?P<status>[^]]+)\]\s+(?P<record_type>\S+)\s+(?P<domain>\S+)\[END\]"
protocol={normalize_protocol($proto)}
src_ip={$remote_addr}
dst_ip={$server}
device={$server}
plugin_sid=20000000
userdata1={$server_name}
userdata2={$status}
userdata3={$record_type}
userdata4={:parseDNS($domain)}
userdata5={$dnsid}
userdata6={$dnsopts}