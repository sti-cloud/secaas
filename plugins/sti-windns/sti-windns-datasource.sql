--
-- Sentinel Windows DNS Plugin
-- Written By: Sidney Eaton
-- Last Modified: 2017-05-9
--

SET @pluginID = '20004';

-- Insert product if not already exists
-- DELETE FROM plugin WHERE id = @pluginID;
INSERT INTO plugin (id, type, name, description, product_type)
VALUES (@pluginID, 1, 'STI-WinDNS', 'STI Windows DNS', 4) ON DUPLICATE KEY UPDATE product_type=4;

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 13, 230, 'STI-WinDNS: DNS Request Sent', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2, 13, 230, 'STI-WinDNS: DNS Request Received', 1, 1);
call STICreateDatasourceEntry(@pluginID, 3, 13, 230, 'STI-WinDNS: DNS Response Received', 1, 1);
call STICreateDatasourceEntry(@pluginID, 4, 13, 230, 'STI-WinDNS: DNS Response Sent', 1, 1);
call STICreateDatasourceEntry(@pluginID, 20000000, 13, 230, 'STI-WinDNS: Generic DNS Log', 1, 1);
