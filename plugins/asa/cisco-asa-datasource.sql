SET @pluginID = '1636';

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STIUpdateRiskEntry;
DELIMITER $$
CREATE PROCEDURE STIUpdateRiskEntry(PID int(11),SID int(11), REL int(11), PRIO int(11))
BEGIN
  UPDATE IGNORE plugin_sid SET reliability = REL, priority = PRIO where plugin_id = PID and sid = SID;
  INSERT IGNORE INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) select plugin_id, sid, category_id, subcategory_id, name, reliability, priority FROM plugin_sid where plugin_sid.plugin_id = PID and plugin_sid.sid = SID ON DUPLICATE KEY UPDATE reliability=REL, priority=PRIO;
  INSERT IGNORE INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) select plugin_id, sid, category_id, subcategory_id, name, reliability, priority FROM plugin_sid where plugin_sid.plugin_id = PID and plugin_sid.sid = SID ON DUPLICATE KEY UPDATE reliability=REL, priority=PRIO;
END$$

DELIMITER ;

call STIUpdateRiskEntry(@pluginID, 106006, 1, 1);
call STIUpdateRiskEntry(@pluginID, 106100, 1, 1);