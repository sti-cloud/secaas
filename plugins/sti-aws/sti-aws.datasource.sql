--
-- Okta Plugin
-- Written By: Tom Martin
-- Last Modified: 2019-05-02
--

-- Set plugin id variable
SET @pluginID = '21011';

-- Insert product if not already exists
INSERT INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'AWS', 'STI Plugin for AWS', 15)
	ON DUPLICATE KEY UPDATE type=1, name='AWS', description='STI Plugin for AWS', product_type=15;

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 10, 3, 32, 'AWS CloudTrail Allowed Event', 1, 10);
call STICreateDatasourceEntry(@pluginID, 11, 3, 78, 'AWS CloudTrail Denied Event', 1, 10);
call STICreateDatasourceEntry(@pluginID, 20, 11, 187, 'AWS Configuration Change', 1, 10);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
