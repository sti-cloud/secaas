--
-- Sentinel Proofpoint CASB Plugin
-- Written By: Matt Weiss
-- Last Modified: 2020-10-22
--

SET @pluginID = '40007';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
        VALUES (@pluginID, 1, 'sti-sentinelone', 'SentinelOne', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 10, 69, 'SentinelOne: General', 1, 1);
call STICreateDatasourceEntry(@pluginID, 2, 7, 195, 'SentinelOne: Abnormalities', 1, 1);
call STICreateDatasourceEntry(@pluginID, 3, 7, 45, 'SentinelOne: Hiding/Stealthiness', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4, 7, 69, 'SentinelOne: Injection', 3, 3);
call STICreateDatasourceEntry(@pluginID, 5, 7, 69, 'SentinelOne: Persistence', 2, 2);
call STICreateDatasourceEntry(@pluginID, 6, 1, 69, 'SentinelOne: Exploitation', 1, 1);
call STICreateDatasourceEntry(@pluginID, 7, 7, 69, 'SentinelOne: Evasion', 3, 3);
call STICreateDatasourceEntry(@pluginID, 8, 3, 69, 'SentinelOne: File Access', 1, 1);
call STICreateDatasourceEntry(@pluginID, 20000000, 10, 69, 'SentinelOne: Generic Event', 1, 1);

-- Cleanup
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;