--
-- Sentinel DeepSecurity Agent Modifications
-- Written By: Matt Weiss
-- Last Modified: 2020-11-13
--



SET @pluginID = '1862';


-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 4000000, 10, 69, 'Deep Security Agent: Anti-Malware - Real-Time Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000001, 10, 69, 'Deep Security Agent: Anti-Malware - Manual Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000002, 10, 69, 'Deep Security Agent: Anti-Malware - Scheduled Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000003, 10, 69, 'Deep Security Agent: Anti-Malware - Quick Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000010, 10, 69, 'Deep Security Agent: Anti-Spyware - Real-Time Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000011, 10, 69, 'Deep Security Agent: Anti-Spyware - Manual Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000012, 10, 69, 'Deep Security Agent: Anti-Spyware - Scheduled Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000013, 10, 69, 'Deep Security Agent: Anti-Spyware - Quick Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000020, 10, 69, 'Deep Security Agent: Suspicious Activity - Real-Time Scan', 3, 3);
call STICreateDatasourceEntry(@pluginID, 4000030, 10, 69, 'Deep Security Agent: Unauthorized Change - Real-Time Scan', 3, 3);



-- Cleanup
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;