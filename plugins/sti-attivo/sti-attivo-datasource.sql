--
-- Sentinel Attivo Plugin
-- Written By: Tom Martin
-- Last Modified: 2018-07-02
--

SET @pluginID = '21007';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'sti-attivo', 'Attivo', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 2, 216, 'Attivo BOTsink Event', 4, 4);
call STICreateDatasourceEntry(@pluginID, 2, 2, 216, 'Attivo BOTsink Event (Blocked)', 4, 1);
call STICreateDatasourceEntry(@pluginID, 10, 2, 216, 'Attivo - UDP Port Scan', 4, 4);

-- Cleanup
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
