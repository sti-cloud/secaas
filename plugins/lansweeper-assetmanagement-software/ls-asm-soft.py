#!/usr/bin/python
#######################################################################################
#
# SCRIPT NAME: Lansweeper Asset Management Software Inventory Connector
#      AUTHOR: Sidney Eaton
#        DATE: 3/1/2017
#
# PURPOSE:
# Alienvault does not allow IDM data from SQL databases.  This script will periodically
# poll a lansweeper asset management database and generate logs that the corresponding
# Alienvault plugin will then interpret.
#######################################################################################
import logging
import logging.handlers
import signal
import time
import traceback
import pymssql
from contextlib import closing
import sys
import re
import ConfigParser

# Specify DEFAULT options here.
LOG_FILENAME="/var/log/lansweeper-assetmanagement-software-stdout.log"
OUTPUT_FILENAME="/var/log/lansweeper-assetmanagement-software.log"
LOG_LEVEL = logging.DEBUG
DATABASE=""
PASSWORD=""
USER=""
HOST=""
PORT="1433"
DB_PER_QUERY_LIMIT=100000
CHECK_INTERVAL=60

# Config file parsing constants.
CONFIG_FILE='/etc/ossim/agent/plugins/lansweeper-assetmanagement.config'
DATABASE_SECTION='DB'
DB_SERVER_OPTION='SERVER'
DB_PORT_OPTION='PORT'
DB_USER_OPTION='USERNAME'
DB_PASS_OPTION='PASSWORD'
DB_NAME_OPTION='DBNAME'
DB_CHECK_OPTION='CHECK_INTERVAL'
DB_LIMIT_OPTION='QUERY_RESULT_LIMIT'
DB_LOG_LEVEL_OPTION='LOGGING_LEVEL'

# Create a separate logger for alienvault destined messages.
global OUTPUT_LOG
OUTPUT_LOG = logging.getLogger("AlienVault")

# This value is used for subsequent queries to limit retrieved results to only relevant ones.
global START_VALUE
START_VALUE=""

global logger
logger = logging.getLogger("System Messages")

# This function sets up the global logger for debugging purposes.
def log_setup(LEVEL):
    log_handler = logging.handlers.WatchedFileHandler(LOG_FILENAME)
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)s %(module)s(%(funcName)s) [%(process)d-%(thread)d]: %(message)s',
        '%b %d %H:%M:%S')
    formatter.converter = time.gmtime  # if you want UTC time
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)
    logger.setLevel(LEVEL)

# This function sets up the logging for AlienVault consumption.
def output_log_setup(LEVEL):
    log_handler = logging.handlers.WatchedFileHandler(OUTPUT_FILENAME)
    formatter = logging.Formatter(
        '%(asctime)s %(funcName)s %(message)s',
        '%b %d %H:%M:%S')
    formatter.converter = time.gmtime  # if you want UTC time
    log_handler.setFormatter(formatter)
    OUTPUT_LOG.addHandler(log_handler)
    OUTPUT_LOG.setLevel(LEVEL)

def sigterm_handler(signal, frame):
    # save the state here or do whatever you want
    logger.critical("Caught signal to terminate, performing graceful cleanup first...")
    #cleanup() # With the finally block in the while loop this shouldn't be needed.
    sys.exit(0)

# This method tries to make sure all the threads exit gracefully.
def cleanup():
  logger.warn("Starting cleanup tasks....")

def genericQuery(queryString):
  while True:
    try:
      with closing ( pymssql.connect(host="%s:%s" % (HOST,PORT), user=USER, password=PASSWORD, database=DATABASE) ) as conn:
        with closing ( conn.cursor() ) as cursor:
          logger.debug("Attempting to execute query: %s" % str(queryString))
          cursor.execute("%s" % (queryString))
          #results = cursor.fetchall()
          results = cursor.fetchall_asdict()
          logger.debug("Query returned: %s" % str(results))
          return results
    except pymssql.OperationalError, e:
      # An operational error generally includes "too many connections" thus this may not be a database problem but rather
      # a code issue. We will attempt to retry the operation.
      logger.warning("Operational Error in executing query: %s  -- ERROR DETAILS: %s" % (queryString,e))
      time.sleep(30)
    except Exception, e:
      logger.exception("Error in executing query: %s -- ERROR DETAILS: %s" % (queryString,e))
      return None

# The start query is designed to prevent downloading the whole database on script start up.  It should
# be used to set the starting point for subequent queries.
def startQuery():
  sql = "SELECT TOP 1 tblSoftwareUni.Added AS START_VALUE from tblSoftwareUni ORDER BY tblSoftwareUni.Added DESC;"
  results = genericQuery(sql)
  if results is None or len(results) < 1:
    logger.critical("startQuery yielded no results, terminating program.")
    exit(0)
  else:
    #return re.search('\d+-\d+-\d+\s+\d+:\d+\d+',str(results[0]['START_VALUE'])).group(0)
    return str(results[0]['START_VALUE'])


# This function performs queries of asset information for recently seen computers only.
def getAssetInventoryData():
  sql = "SELECT "
  if DB_PER_QUERY_LIMIT > 0:
    sql += "TOP %s " % (DB_PER_QUERY_LIMIT)
  sql += "tblSoftwareUni.softwareName AS NAME, tblSoftwareUni.SoftwarePublisher AS PUBLISHER, tblSoftwareUni.Added AS ADDED, tblAssets.IPAddress AS IP from tblSoftwareUni, tblAssets, tblSoftware where tblSoftwareUni.softID = tblSoftware.softID and tblSoftware.AssetID = tblAssets.AssetID and tblSoftwareUni.Added > convert(datetime,left('%s',23),121) and not tblAssets.IPAddress = '' ORDER BY tblSoftwareUni.Added ASC;" % (START_VALUE)

  results = genericQuery(sql)
  if not results is None:
    logger.info("Found %s new assets." % (len(results)))
    if not len(results) < 1:
      makeLog(results, ["ADDED", "IP", "NAME"], "IPSoftwareInventory")
      return str(results[-1]['ADDED'])
  return START_VALUE

# Sets the new start value and issues the logging command.  This is just a quick helper.
def setSTART_VALUE(newValue):
  global START_VALUE
  START_VALUE=newValue
  logger.info("START_VALUE set to %s" % (START_VALUE))

# This method takes a sql dictionary select query with an array of columns names and prints
# a log message in array order.  The skipEmpty option avoids printing lines where any of the
# fields are empty.
def makeLog(dictResults, arrayOrderedColumnNames, logTypeDescription = "Generic", skipEmpty = True):
  if not dictResults is None:
    logger.info("Found %s new assets with log type %s" % (len(dictResults), logTypeDescription))
    if not len(dictResults) < 1:
      for row in dictResults:
        PRINT_LOG = True
        LOG_LINE = "%s - " % (logTypeDescription)
        for key in arrayOrderedColumnNames:
          if skipEmpty and isValueEmpty(row[key]):
            PRINT_LOG = False
            break
          else:
            LOG_LINE+="%s: %s, " % (key,row[key])
        if PRINT_LOG:
          LOG_LINE = LOG_LINE[:-2]
          OUTPUT_LOG.info(LOG_LINE)

# This function checks to make sure the value is not empty.
def isValueEmpty(value):
  try:
    if value is None:
      return True
    value = str(value).strip()
    if value == "" or value.lower() == "none":
      return True
    else:
      return False
  except Exception, e:
    logger.exception("Error encountered while determining if string is empty.")
    return True

log_setup(LOG_LEVEL)
output_log_setup(logging.INFO)
logger.info("Starting up program...")

signal.signal(signal.SIGTERM, sigterm_handler)
signal.signal(signal.SIGINT, sigterm_handler)
logger.debug("Signal handlers configured.")

# Read configuration file.
#config = ConfigParser.RawConfigParser({ DATABASE_SECTION: { DB_SERVER_OPTION: HOST, DB_USER_OPTION: USER, DB_PASS_OPTION: PASSWORD, DB_NAME_OPTION: DATABASE, DB_PORT_OPTION: PORT, DB_CHECK_OPTION: CHECK_INTERVAL, DB_LIMIT_OPTION: DB_PER_QUERY_LIMIT }})
config = ConfigParser.RawConfigParser()
config.read(CONFIG_FILE)

# Validate config file options
if config.has_option(DATABASE_SECTION,DB_SERVER_OPTION) and config.has_option(DATABASE_SECTION,DB_USER_OPTION) and config.has_option(DATABASE_SECTION,DB_PASS_OPTION) and config.has_option(DATABASE_SECTION,DB_NAME_OPTION):
  DATABASE=config.get(DATABASE_SECTION,DB_NAME_OPTION)
  PASSWORD=config.get(DATABASE_SECTION,DB_PASS_OPTION)
  USER=config.get(DATABASE_SECTION,DB_USER_OPTION)
  HOST=config.get(DATABASE_SECTION,DB_SERVER_OPTION)
else:
  logger.critical("Unknown or incorrect database configuration specified in config file.")
  exit(0)

# Load other optional items from configuration file.
if config.has_option(DATABASE_SECTION,DB_PORT_OPTION):
  PORT=str(config.get(DATABASE_SECTION,DB_PORT_OPTION))
if config.has_option(DATABASE_SECTION,DB_CHECK_OPTION):
  CHECK_INTERVAL=int(config.get(DATABASE_SECTION,DB_CHECK_OPTION))
if config.has_option(DATABASE_SECTION,DB_LIMIT_OPTION):
  DB_PER_QUERY_LIMIT=int(config.get(DATABASE_SECTION,DB_LIMIT_OPTION))
if config.has_option(DATABASE_SECTION,DB_LOG_LEVEL_OPTION):
  TEMP = str(config.get(DATABASE_SECTION,DB_LOG_LEVEL_OPTION))
  if TEMP.lower() == "debug":
    LOG_LEVEL = logging.DEBUG
  elif TEMP.lower() == "info":
    LOG_LEVEL = logging.INFO
  elif TEMP.lower() == "critical":
    LOG_LEVEL = logging.CRITICAL
  elif TEMP.lower() == "error":
    LOG_LEVEL = logging.ERROR
  elif TEMP.lower() == "warning":
    LOG_LEVEL = logging.WARN
  elif TEMP.lower() == "fatal":
    LOG_LEVEL = logging.FATAL
  logger.setLevel(LOG_LEVEL)


try:
  setSTART_VALUE(startQuery()) # Sets the inital start value.
  while True:
    setSTART_VALUE(getAssetInventoryData()) # Gets logs and sets the new start value
    time.sleep(CHECK_INTERVAL)
except KeyboardInterrupt:
  pass
finally:
  cleanup()