--
-- Sentinel Zixcorp
-- Written By: Matt Weiss
-- Last Modified: 2020-11-09
--

SET @pluginID = '40006';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
        VALUES (@pluginID, 1, 'sti-zixcorp', 'Zixcorp', 15);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 7, 17, 'Zixcorp: Reject', 5, 5);
call STICreateDatasourceEntry(@pluginID, 20000000, 10, 69, 'Zixcorp: Generic Event', 1, 1);

-- Cleanup
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;