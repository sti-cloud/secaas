--
-- Cisco FirePower Sensor IPS By Sentinel Technologies
-- Last signatures update: NA
-- Written By: Sidney Eaton
-- Last Modified: 10-16-2015
--

DELETE FROM plugin WHERE id = "15970";
DELETE FROM plugin_sid where plugin_id = "15970";

INSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (15970, 1, 'cisco-firepower-sensor', 'Cisco FirePower Intrusion Prevention System', 14);

INSERT IGNORE INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name) VALUES
(15970, 1100, 3, 75, 'cisco-firepower-sensor: Begin Connection Allow Event'),
(15970, 1101, 3, 76, 'cisco-firepower-sensor: Begin Connection Block Event'),
(15970, 11100, 3, 143, 'cisco-firepower-sensor: End Connection Allow Event'),
(15970, 11101, 3, 76, 'cisco-firepower-sensor: End Connection Block Event'),
(15970, 1150, NULL, NULL, 'cisco-firepower-sensor: Sensor Command Execution'),
(15970, 1151, NULL, NULL, 'cisco-firepower-sensor: Action Queue Scrape');
