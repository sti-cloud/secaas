--
-- Cisco FirePower Defense Center By Sentinel Technologies
-- Last signatures update: NA
-- Written By: Sidney Eaton
-- Last Modified: 3-14-2017
--

-- Set plugin id variable
SET @pluginID = '15971';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (@pluginID, 1, 'cisco-defensecenter', 'Cisco Defense Center', 14);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1160, NULL, NULL, 'cisco-defensecenter: Defense Center Job/Policy Activity', 1, 0);
call STICreateDatasourceEntry(@pluginID, 1161, NULL, NULL, 'cisco-defensecenter: Defense Center Login Activity', 1, 1);
call STICreateDatasourceEntry(@pluginID, 1162, NULL, NULL, 'cisco-defensecenter: Defense Center Web GUI Page/Policy View', 1, 0);
call STICreateDatasourceEntry(@pluginID, 1190, NULL, NULL, 'cisco-defensecenter: Health Notification', 1, 0);
call STICreateDatasourceEntry(@pluginID, 1150, NULL, NULL, 'cisco-defensecenter: Sensor Command Execution', 1, 0);
call STICreateDatasourceEntry(@pluginID, 1151, NULL, NULL, 'cisco-defensecenter: Action Queue Scrape', 1, 0);
call STICreateDatasourceEntry(@pluginID, 2000, NULL, NULL, 'cisco-defensecenter: Host IOC Set', 1, 0);
call STICreateDatasourceEntry(@pluginID, 2001, NULL, NULL, 'cisco-defensecenter: Network Based Retrospective', 1, 0);

insert into host_source_reference (id, name, relevance) VALUES (10002,"FireSight",3) ON DUPLICATE KEY UPDATE relevance = 3;

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
