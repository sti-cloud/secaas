#!/bin/bash
SED="/bin/sed"
GREP="/bin/grep"
OSSIM_AGENT_CONFIG_FILE="/etc/ossim/agent/config.cfg"
PLUGIN_DIR="/etc/ossim/agent/plugins"
RSYSLOG_CONF_DIR="/etc/rsyslog.d"
CP="/bin/cp"
OSSIMDB="/usr/bin/ossim-db"
CHOWN="/bin/chown"
FIND="/usr/bin/find"
function install_plugin {
  echo "Installing plugin $1"
  if $GREP -q "$1=" $OSSIM_AGENT_CONFIG_FILE
  then
    echo "--Plugin $1 already exists in agent configuration file, will not override existing configuration. Verify that it is correctly set!"
  else
    $SED -i "/\[plugins\]/a $1=/etc/ossim/agent/plugins/$1.cfg" $OSSIM_AGENT_CONFIG_FILE
    if $GREP -q "$1=" $OSSIM_AGENT_CONFIG_FILE
    then
      echo "--Plugin $1 successfully added to the agent configuration file."
    else
      echo "--FAILED adding plugin $1 to the agent configuration file."
    fi
  fi

  echo "--Installing parsing files for $1." 
  $CP -fb $1.cfg $PLUGIN_DIR/$1.cfg
  $CHOWN root:alienvault $PLUGIN_DIR/$1.cfg

  echo "--Updating database for $1."
  cat $1.sql | $OSSIMDB

  echo "--Copying related rsyslog files (if any)."
  $FIND . -name "*$1-rsyslog.conf" -exec cp '{}' $RSYSLOG_CONF_DIR/ \;
}


install_plugin "cisco-defensecenter"
install_plugin "cisco-firepower-sensor"

read -p "Do you want to restart Alienvault Services? " -n 1 choice
echo " "
case "$choice" in
  y|Y ) /etc/init.d/ossim-server restart; /etc/init.d/ossim-agent restart;;
  n|N ) echo "no";;
  * ) echo "Invalid Option.";;
esac

read -p "Do you want to restart rsyslog services? " -n 1 choice
echo " "
case "$choice" in
  y|Y ) /etc/init.d/rsyslog reload;;
  n|N ) echo "no";;
  * ) echo "Invalid Option.";;
esac
echo "All operations finished!"
