#!/usr/bin/python
#######################################################################################
#
# SCRIPT NAME: Cisco AMP Connector
#      AUTHOR: Sidney Eaton
#        DATE: 7/28/2016
#
# PURPOSE:
# To poll and download AMP requests via Cisco/Sourcefire's AMP for end point API.
#######################################################################################
import ConfigParser
import sys

import re

# Required for AlienVault package imports
sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
import requests

# Cosmetic, disables InsecureRequestWarning from being printed on screen.
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# CONFIG FILE STATIC PARAMETERS
CONFIG_FILE = 'cisco-amp.config'
API_SECTION = 'API'
API_KEY_OPTION = 'KEY'
CLIENTID_KEY_OPTION = 'CLIENTID'
SERVER_KEY_OPTION='SERVER'
SERVERVERSION_KEY_OPTION='VERSION'

# API INPUTS
KEY=''                                   # The API key from sourcefire's portal
CLIENTID=''                              # The client ID from sourcefire's portal
SERVER='api.amp.sourcefire.com'          # The sourcefire API server to make requests to.
VERSION='1'                              # The Sourcefire API version (0 or 1)
DEBUG=False

# STATIC NON-CONFIGURABLE
PLUGIN_ID = "20001"
PRODUCT_NAME = "CiscoAMP"


# Start of dictionaries for autoclassification
dict_suspicious = {
  "BOT": "54",
  "SUSPICIOUS": "NULL",
  "POSSIBLE COMPROMISE": "NULL"
}
dict_malware = {
  "ROOTKIT": "40",
  "RANSOMWARE": "40",
  "ADWARE": "34",
  "SPYWARE": "33",
  "TROJAN": "37",
  "VIRUS": "38",
  "WORM": "39",
  "THREAT.*DETECT": "40",
  "MALWARE": "40",
  "BACKDOOR": "41"
}

dict_exploit = {
  "SHELL": "1",
  "MICROSOFT": "9",
  "JAVA": "17",
  "PDF": "13",
  "READER": "13"
}

dict_definitions = {
  "DEFINITION.*START": "101",
  "DEFINITION.*FAILURE": "102",
  "DEFINITION.*SUCCESS": "101",
  "CLOUD": "103",
  "QUARANTINE.*FAIL": "99",
  "THREAT.*QUARANTINED": "98",
  "QUARANTINE": "NULL"
}

dict_scan = {
  "SCAN.*STARTED": "106",
  "SCAN.*COMPLETED": "107",
  "SCAN.*SCHEDULE": "103",
  "SCAN.*FAIL": "108",
  "REBOOT": "103",
  "PRODUCT UPDATE": "NULL"
}

dict_policy = {
  "POLICY": "47"
}

dict_system = {
  "REBOOT": "151",
  "INSTALL.*START": "223",
  "INSTALL.*FAIL": "138",
  "UNINSTALL": "139",
  "COMPUTER.*CHANGED": "187"
}



# Put more specicific matches first
def IntelligentMatcher(plugindescription):
  RET_CAT=None
  RET_SUBCAT=None
  definitions_subcat_results = check_for_subcategory_match(plugindescription,dict_definitions)
  scan_subcat_results = check_for_subcategory_match(plugindescription,dict_scan)
  policy_subcat_results = check_for_subcategory_match(plugindescription,dict_policy)
  system_subcat_results = check_for_subcategory_match(plugindescription,dict_system)
  suspicious_subcat_results = check_for_subcategory_match(plugindescription,dict_suspicious)
  exploit_subcat_results = check_for_subcategory_match(plugindescription,dict_exploit)
  malware_subcat_results = check_for_subcategory_match(plugindescription,dict_malware)



  if definitions_subcat_results is not None:
    RET_CAT="12"
    RET_SUBCAT=definitions_subcat_results
  elif scan_subcat_results is not None:
    RET_CAT="12"
    RET_SUBCAT=scan_subcat_results
  elif policy_subcat_results is not None:
    RET_CAT="5"
    RET_SUBCAT=policy_subcat_results
  elif system_subcat_results is not None:
    RET_CAT="11"
    RET_SUBCAT=system_subcat_results
  elif malware_subcat_results is not None:
    RET_CAT="4"
    RET_SUBCAT=malware_subcat_results
  elif exploit_subcat_results is not None:
    RET_CAT="1"
    RET_SUBCAT=exploit_subcat_results
  elif suspicious_subcat_results is not None:
    RET_CAT="7"
    RET_SUBCAT=suspicious_subcat_results
  debugLog("IntelligentMatcher Results, cat: %s, sub cat: %s" % (str(RET_CAT),str(RET_SUBCAT)))
  if RET_CAT is None and RET_SUBCAT is None:
    return ["NULL", "NULL"]
  elif RET_SUBCAT is None:
    return [RET_CAT, "NULL"]
  else:
    return [RET_CAT, RET_SUBCAT]


def match(regex, string):
  mo  = re.match('(?i).*(' + regex + ').*', string, re.IGNORECASE)
  #print "REGEX: " + regex + " STRING: " + string
  try:
    #print "FOUND: " + mo.group(1).upper()
    return  mo.group(1).upper()
  except AttributeError:
    return None



# This uses the dictionary key specified as the crucial part of the regular expression.  If there
# is a match then it pulls the value associated with the key and returns it.  Dictionary keys should
# be ordered in most specific key terms to least.
def check_for_subcategory_match(plugindescription, dictionary):
  for key in dictionary:
    if match(key, plugindescription) is not None:
      debugLog("FOUND MATCH: %s:%s" % (key,dictionary.get(key)))
      return dictionary.get(key)
  return None



def debugLog(line):
  if DEBUG:
    print str("DEBUG: %s" % line)

# Inputs a URL and retrieves JSON data from Cisco FireAMP.
def getData(url):
  headers = {'accept': 'application/json','content-type': 'application/json','Accept-Encoding': 'gzip, deflate' }
  r = requests.get(url,auth=(CLIENTID,KEY),headers=headers, verify=False)
  debugLog("JSON RAW DATA: %s" % r.json())
  return r

def parseEventTypes(jsondata):
  print "--\n-- Cisco AMP for End Point Datasource\n-- Written By: Sidney Eaton\n-- Last Modified: 8-1-2016\n--"
  print "SET @pluginID = '%s';" % PLUGIN_ID
  print "\nDELETE FROM plugin WHERE id = @pluginID;\nDELETE FROM plugin_sid where plugin_id = @pluginID;"
  print "\nINSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (@pluginID, 1, 'cisco-amp', 'Cisco AMP', 9);\n"
  for entry in jsondata['data']:
    CATEGORY = IntelligentMatcher(entry['name'] + " - " + entry['description'])
    OUTPUT = "INSERT IGNORE INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (@pluginID, %s, %s, %s, '%s: %s" % (entry['id'],CATEGORY[0],CATEGORY[1],PRODUCT_NAME,entry['name'].replace("'","\\'"))
    if entry['description'] == "":
      print "%s', 3, 0);" % (OUTPUT)
    else:
      print "%s - %s', 3, 0);" % (OUTPUT,entry['description'].replace("'","\\'"))




# Read API Configuration parameters
config = ConfigParser.RawConfigParser({ SERVER_KEY_OPTION: SERVER, SERVERVERSION_KEY_OPTION: VERSION })
config.read(CONFIG_FILE)

# Validate required configuration options and set global parameters
if config.has_option(API_SECTION,API_KEY_OPTION) and config.has_option(API_SECTION,SERVER_KEY_OPTION) and config.has_option(API_SECTION,CLIENTID_KEY_OPTION):
  KEY = config.get(API_SECTION, API_KEY_OPTION)
  CLIENTID = config.get(API_SECTION,CLIENTID_KEY_OPTION)
  SERVER = config.get(API_SECTION,SERVER_KEY_OPTION)
else:
  print "API Client ID option found: %s" % config.has_option(API_SECTION,CLIENTID_KEY_OPTION)
  print "API Key option found: %s" % config.has_option(API_SECTION,API_KEY_OPTION)
  print "API Server option found: %s" % config.has_option(API_SECTION,SERVER_KEY_OPTION)
  print "Some required settings have not been found, exiting..."
  sys.exit(0)

if config.has_option(API_SECTION,SERVERVERSION_KEY_OPTION):
  VERSION = config.get(API_SECTION,SERVERVERSION_KEY_OPTION)

debugLog("Using Client ID %s with API key %s and server %s (Version: %s)." % (CLIENTID,KEY,SERVER,VERSION))

r = getData('https://%s/v%s/%s' % (SERVER,VERSION,"event_types"))
parseEventTypes(r.json())