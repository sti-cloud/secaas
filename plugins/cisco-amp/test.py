#!/usr/bin/python
#######################################################################################
#
# SCRIPT NAME: Cisco AMP Connector
#      AUTHOR: Sidney Eaton
#        DATE: 7/28/2016
#
# PURPOSE:
# To poll and download AMP requests via Cisco/Sourcefire's AMP for end point API.
#######################################################################################
import ConfigParser
import sys
import datetime
import time
from sets import Set
#import pytz
#import timezone

# Load the logging modules
import logging
import logging.handlers

# Required for AlienVault package imports
sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
import requests
import dateutil.parser

# Try/except block added because it was failing after upgrades.
# Cosmetic, disables InsecureRequestWarning from being printed on screen.
try:
     from requests.packages.urllib3.exceptions import InsecureRequestWarning
     requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
except:
     pass

# CONFIG FILE STATIC PARAMETERS
CONFIG_FILE = '/etc/ossim/agent/plugins/cisco-amp.config'
API_SECTION = 'API'
API_KEY_OPTION = 'KEY'
CLIENTID_KEY_OPTION = 'CLIENTID'
SERVER_KEY_OPTION='SERVER'
SERVERVERSION_KEY_OPTION='VERSION'
EVENTCHECKINTERVAL_KEY_OPTION='EVENT_CHECK_INTERVAL'
LIMIT_KEY_OPTION='LIMIT'
LOGFILE_KEY_OPTION='LOG_FILE'
REQ_TIME_DIFF_KEY_OPTION='REQUEST_TIME_DIFFERENTIAL'
RETRY_INTERVAL_KEY_OPTION='RETRY_INTERVAL'
ENABLE_COMPUTER_INV_KEY_OPTION='ENABLE_COMPUTER_INVENTORY'
NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE_KEY_OPTION='NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE'
LOG_LEVEL_OPTION='LOGGING_LEVEL'

# API INPUTS
KEY=''                                   # The API key from sourcefire's portal
CLIENTID=''                              # The client ID from sourcefire's portal
SERVER='api.amp.sourcefire.com'          # The sourcefire API server to make requests to.
VERSION='1'                              # The Sourcefire API version (0 or 1)
STDOUT_LOG_FILE="/var/log/cisco-amp-stdout.log"
LOG_FILE="/var/log/cisco-amp-test.log"        # The local log file where events should be stored.
EVENT_CHECK_INTERVAL='60'                # How many seconds should pass between checking Sourcefire AMP for new events. Starting after the last request.
HTTP_RESULT_LIMIT=1                      # At last check 500 was maximum. 0 = Maximum Supported/No Limit.
RETRY_INTERVAL=30                        # When a request fails, wait x seconds and then retry.  Applies to paged requests as well.
REQUEST_TIME_DIFFERENTIAL=1              # How many seconds to add to the time retrieved by the newest AMP event.  This should be one second for optimal operation.
DEBUG=True                             # Set to True to enable debug logs on the console.
DEEP_DEBUG=True                         # Modifies date data and poll time, to increase the chance of events being downloaded.  Should be False for production use.
ENABLE_COMPUTER_INVENTORY=False
#ENABLE_COMPUTER_INVENTORY=True           # When set to true the script will poll the connector_guid and write a separate log for MAC address and OS information.
NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE=30 # The number of EVENT_CHECK_INTERVALs before clearing the inventory cache.  Lower = more accurate and uptodate, but more HTTP requests.

#DebugTheseEventsOnly=[]
DebugTheseEventsOnly=[2164260880]
#DebugTheseEventsOnly=[553648145]
#DebugTheseEventsOnly=[1090519054]      # Originally it was thought I could do multiple event_types not sure that is possible.  So code may need to be adjusted.  This is not important unless debugging.
#DebugTheseEventsOnly= [553648143]
#DebugTheseEventsOnly=[1107296260]
#DebugTheseEventsOnly=[1107296265]
#DebugTheseEventsOnly=[1107296274]
# This is the new logging level mechanism which will be much cleaner and versatile.
LOG_LEVEL = logging.DEBUG
global logger
logger = logging.getLogger("System Messages")


# This function sets up the global logger for debugging purposes.
def log_setup(LEVEL):
    log_handler = logging.handlers.WatchedFileHandler(STDOUT_LOG_FILE)
    formatter = logging.Formatter(
        'Date: %(asctime)s UTC %(levelname)s %(module)s(%(funcName)s) [%(process)d-%(thread)d]: %(message)s',
        '%Y-%m-%d %H:%M:%S')
    formatter.converter = time.gmtime  # if you want UTC time
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)
    logger.setLevel(LEVEL)



def getDateTimeFromISO8601String(s):
  return dateutil.parser.parse(s)

# This property allows for per key/attribute flexible parsing and error handling.
def GetJSONProperty(jsondata,KeyName,Location,addEventEvenIfEmpty=False):
  value = jsondata
  try:
    for key in Location.split(","):
      value = value[key]
    return ", %s: %s" % (KeyName,value)
  except KeyError, e:
    if (addEventEvenIfEmpty):
      return ", %s: " % (KeyName)
    else:
      #logger.exception("JSON Property Retrieval KeyError: %s" % str(e))
      return ""

# Some values have multiple entries and this method is a spin off of the GetJSONProperty accomodating them.
def GetJSONIndexedProperty(jsondata,KeyName,Location,addEventEvenIfEmpty=True,IndexNum=0):
  value = jsondata
  try:
    return GetJSONProperty(value[IndexNum],KeyName,Location,addEventEvenIfEmpty)
  except KeyError, e:
    #print 'I got a KeyError, reason "%s"' % (str(e))
    if (addEventEvenIfEmpty):
      return ", %s: " % (KeyName)
    else:
      return ""

# Write log entry to file.
def writeLog(line):
  with open(LOG_FILE,'a') as f: f.write(line + '\n')

# Inputs a partial URL and retrieves JSON data from Cisco FireAMP.
def getData(url):
  headers = {'accept': 'application/json','content-type': 'application/json','Accept-Encoding': 'gzip, deflate' }
  r = requests.get(url,auth=(CLIENTID,KEY),headers=headers, verify=False)
  logger.debug("URL: %s, JSON RESPONSE: %s" % (url,r.json()))
  return r

# This is for reading in a date string from Sourcefire AMP and converting it to an aware datetime object.
def readInUTCDateString(dateString):
  DATE_ARRAY = dateString.split("+")
  return datetime.datetime.strptime(DATE_ARRAY[0] + " UTC","%Y-%m-%dT%H:%M:%S %Z")

# The while loop here is used for retries if the HTTP request fails a 60 second delay
# will occur before the program retries the HTTP request with the original poll time.
def queryEvents(POLL_TIME):

  while True:
    try:
      #print "POLLING START TIME: %s" % str(POLL_TIME.isoformat())
      #r = getData('events?start_date=' + str(POLL_TIME.isoformat()) + '&limit=' + str(HTTP_RESULT_LIMIT))
      APPEND_LIMIT=""
      if int(HTTP_RESULT_LIMIT) > 0:
        APPEND_LIMIT = "&limit=" + str(HTTP_RESULT_LIMIT)

      if DEEP_DEBUG and len(DebugTheseEventsOnly) > 0:
        APPEND_LIMIT += "&event_type[]="
        EventDebug_i = 0
        for debugevententry in DebugTheseEventsOnly:
          APPEND_LIMIT += str(debugevententry)
          EventDebug_i += 1
          if EventDebug_i < len(DebugTheseEventsOnly):
            APPEND_LIMIT += ","

      # Poll for more events      
      POLL_URL='https://%s/v%s/%s' % (SERVER,VERSION,'events?start_date=' + str(POLL_TIME.isoformat()) + APPEND_LIMIT)
      logger.info("Polling for %s" % POLL_URL)
      r = getData('https://%s/v%s/%s' % (SERVER,VERSION,'events?start_date=' + str(POLL_TIME.isoformat()) + APPEND_LIMIT))
      logger.info("Found %s results!" % (r.json()['metadata']['results']['total']))

      # Data is returned with newest timestamp first.  Grab the last entry in the json format and add 1 second to that, then use that as the next START_DATE in the next poll cycle.  This ensures
      # That all events since then have been entered grabbed.  If their are no results use the POLL_TIME passed to this function as the next START_DATE.
      if int(r.json()['metadata']['results']['total']) > 0:
#        DATE_ARRAY = r.json()['data'][0]['date'].split("+")
#        NEXT_POLL_TIME = datetime.datetime.strptime(DATE_ARRAY[0] + " UTC","%Y-%m-%dT%H:%M:%S %Z") + datetime.timedelta(seconds=int(REQUEST_TIME_DIFFERENTIAL))
        NEXT_POLL_TIME = readInUTCDateString(r.json()['data'][0]['date']) + datetime.timedelta(seconds=int(REQUEST_TIME_DIFFERENTIAL))
      else:
        NEXT_POLL_TIME = POLL_TIME # Used to set variable scope, set with original POLL_TIME in case no results are found.  This will ensure that none get missed on the next call to this function.

      # This NextPageCondition and while loop is designed to simulate a do while loop.  If all items are
      # retrieved the first time around the code in this loop should only be executed once.
      # By checking the total results returned we can reuse the original POLL_TIME for the next call to this method, which will make sure no events are lost in this common scenario.  We
      # do this by not entering the while loop below if at any point the results are 0 in the json data.
      NextPageCondition = True
      while NextPageCondition and int(r.json()['metadata']['results']['total']) > 0:
        # Since the start_time is passed in the next page links we don't need to worry about parsing time
        # while data is still being retrieved.  We should only need to poll the next time starting from
        # the poll time of the last page.
        logger.debug("Original Poll Time: %s, Next Poll Time: %s" % (POLL_TIME.isoformat(),NEXT_POLL_TIME.isoformat()))

        data = r.json()
        parseEvents(r.json())

        # Check to see if there is another page.  This should be false when no more pages.  It is important to keep trying to retrieve pages if there is a network outage.
        # Otherwise we might miss events or get duplicate events on subsequent polls.
        if 'next' in data['metadata']['links']:
          # If more pages, make sure that there is no errors in getting the next pages. If there
          # is an issue keep retrying with RETRY_INTERVAL delays in between.
          while True:
            try:
              r = getData(data['metadata']['links']['next'])
              break
            except Exception, e:
              logger.exception("Error retrieving next page, will retry in %s seconds. - Error %s" % (RETRY_INTERVAL,str(e)))
              time.sleep(RETRY_INTERVAL)
        else:
          NextPageCondition = False

      # Return the time that the calling method should use for the next START_TIME/POLL_TIME for AMP.
      return NEXT_POLL_TIME
    except Exception, e:
      logger.exception("queryEvents - Error retrieving main page, will retry in %s seconds. - Error %s" % (RETRY_INTERVAL,str(e)))
    time.sleep(RETRY_INTERVAL)

# Grabs the internal IP address of the connector_guid either from cache or queries Cisco/Sourcefire AMP.
def getInternalIP(connector_guid):
  if IdentityLookupCache.has_key(connector_guid):
    logger.info("Getting %s:%s from cache" % (connector_guid,IdentityLookupCache[connector_guid]))
    return IdentityLookupCache[connector_guid]
  else:
    try:
      #r = getData("computers/" + connector_guid)
      url = 'https://%s/v%s/%s' % (SERVER,VERSION,"computers/" + connector_guid)
      r = getData(url)
      data = r.json()

      # It seems that some times the JSON code is 404.
      try:
        logger.warn("Received JSON error %s, for query %s." % (url,str(data['errors']['error_code'])))
        if (int(data['errors']['error_code']) > 200):
          logger.warn("Received JSON error %s, for query %s." % (url,str(data['errors']['error_code'])))
          return "0.0.0.0"
      except Exception, e:
          pass # Do not report anything it is good if we made an exception here.

      # Will generate a separate log entry containing computer information.
      if ENABLE_COMPUTER_INVENTORY:
        generateIdentityEvent(data)
      logger.debug("Adding %s:%s to cache" % (connector_guid,data['data']['network_addresses'][0]['ip']))
      IdentityLookupCache[connector_guid] = data['data']['network_addresses'][0]['ip']

      #return data['data']['internal_ips']
      return data['data']['network_addresses'][0]['ip']
    except Exception, e:
      logger.exception(str(e))
      return "0.0.0.0"

# Generates an identity event to be consumed by AlienVault.
def generateIdentityEvent(jsondata):
  try:
    #entry = jsondata['data']
    log_entry = "Date: %s" % datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")
    log_entry += GetJSONProperty(jsondata['data'],"Computer Name","hostname",True)
    log_entry += GetJSONProperty(jsondata['data'],"Connector GUID","connector_guid",True)
#    log_entry += GetJSONProperty(jsondata['data'],"IP","internal_ips",True)
    log_entry += GetJSONIndexedProperty(jsondata['data']['network_addresses'],"IP","ip",True)
    log_entry += GetJSONIndexedProperty(jsondata['data']['network_addresses'],"MAC","mac",True)
    log_entry += GetJSONProperty(jsondata['data'],"Active","active",True)
    log_entry += GetJSONProperty(jsondata['data'],"OS","operating_system",True)
    log_entry += GetJSONProperty(jsondata['data'],"Client Version","connector_version",True)
    logger.debug("Parsed entry: %s" % log_entry)
    writeLog(log_entry)
  except KeyError, e:
    logger.exception('I got a KeyError, reason "%s"' % (str(e)))
    return None
  except Exception, e:
    print str(e)


# Parses AMP events and turns them into log entries for consumption.
def parseEvents(jsondata):
  for entry in jsondata['data']:

    try:
      log_entry = "Date: %s, Event Type: %s (%s)" % (readInUTCDateString(entry['date']).strftime("%Y-%m-%d %H:%M:%S UTC"), entry['event_type_id'], entry['event_type'].encode('utf-8'))

      if 'computer' in entry:
        log_entry += GetJSONProperty(entry,"Computer Name","computer,hostname",True)
        log_entry += GetJSONProperty(entry,"Connector GUID","computer,connector_guid",True)
#        This entry below gives the internal IP which is probably not a problem in most cases
#        log_entry += ", IP: %s" % getInternalIP(entry['computer']['connector_guid'])[0]
        log_entry += ", IP: %s" % getInternalIP(entry['computer']['connector_guid'])
        log_entry += GetJSONProperty(entry,"Active","computer,active",True)
        log_entry += GetJSONProperty(entry,"User","computer,user",True)

      log_entry += GetJSONProperty(entry,"Detection","detection")

      if 'error' in entry:
        log_entry += (GetJSONProperty(entry,"Reason","error,description",True)).encode('utf-8')

      if 'generic_ioc' in entry:
        log_entry += GetJSONProperty(entry,"Reason","generic_ioc,short_description",True)

      if 'network_info' in entry:
        log_entry += GetJSONProperty(entry,"Dirty URL","network_info,dirty_url",True)

      if 'scan' in entry:
        log_entry += GetJSONProperty(entry,"Description","scan,description",True)
        log_entry += GetJSONProperty(entry,"Scanned Files","scan,scanned_files",True)
        log_entry += GetJSONProperty(entry,"Scanned Processes","scan,scanned_processes",True)
        log_entry += GetJSONProperty(entry,"Clean","scan,clean",True)
        log_entry += GetJSONProperty(entry,"Scanned Paths","scan,scanned_paths",True)
        log_entry += GetJSONProperty(entry,"Malicious Detections","scan,malicious_detections",True)

      if 'file' in entry:
        log_entry += GetJSONProperty(entry,"Filename","file,file_name",True)
        log_entry += GetJSONProperty(entry,"Path","file,file_path",True)
        log_entry += GetJSONProperty(entry,"Disposition","file,disposition",True)
        log_entry += GetJSONProperty(entry,"SHA256","file,identity,sha256",True)
        log_entry += GetJSONProperty(entry,"SHA1","file,identity,sha1",True)
        log_entry += GetJSONProperty(entry,"MD5","file,identity,md5",True)
        if 'parent' in entry['file']:
          log_entry += GetJSONProperty(entry,"Parent Filename","file,parent,file_name",True)
          log_entry += GetJSONProperty(entry,"Parent Path","file,parent,file_path",True)
          log_entry += GetJSONProperty(entry,"Parent SHA256","file,parent,identity,sha256",True)
          log_entry += GetJSONProperty(entry,"Parent Disposition","file,parent,disposition",True)

      if 'vulnerabilities' in entry:
        log_entry += GetJSONIndexedProperty(entry['vulnerabilities'],"Vulnerable Software","name",True)
        log_entry += GetJSONIndexedProperty(entry['vulnerabilities'],"Score","score",True)

      logger.debug("Parsed entry: %s" % log_entry)
      writeLog(log_entry)
    except KeyError, e:
      logger.exception('I got a KeyError on %s (%s), reason "%s"' % (entry['event_type_id'],entry['event_type'],str(e)))
      writeLog('parseEvent Error: KeyError on %s (%s), reason "%s"' % (entry['event_type_id'],entry['event_type'],str(e)))
    except Exception, e:
      logger.exception('parseEvent Error: %s (%s), reason "%s"' % (entry['event_type_id'],entry['event_type'],str(e)))
      writeLog('parseEvent Error: %s (%s), reason "%s"' % (entry['event_type_id'],entry['event_type'],str(e)))

# Setup logger
log_setup(LOG_LEVEL)

# Read API Configuration parameters
config = ConfigParser.RawConfigParser({ SERVER_KEY_OPTION: SERVER, SERVERVERSION_KEY_OPTION: VERSION, EVENTCHECKINTERVAL_KEY_OPTION: EVENT_CHECK_INTERVAL, LIMIT_KEY_OPTION: HTTP_RESULT_LIMIT, LOGFILE_KEY_OPTION: LOG_FILE, REQ_TIME_DIFF_KEY_OPTION: REQUEST_TIME_DIFFERENTIAL, RETRY_INTERVAL_KEY_OPTION: RETRY_INTERVAL, ENABLE_COMPUTER_INV_KEY_OPTION: ENABLE_COMPUTER_INVENTORY, NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE_KEY_OPTION: NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE})
config.read(CONFIG_FILE)

# Validate required configuration options and set global parameters
if config.has_option(API_SECTION,API_KEY_OPTION) and config.has_option(API_SECTION,SERVER_KEY_OPTION) and config.has_option(API_SECTION,CLIENTID_KEY_OPTION):
  KEY = config.get(API_SECTION, API_KEY_OPTION)
  CLIENTID = config.get(API_SECTION,CLIENTID_KEY_OPTION)
  SERVER = config.get(API_SECTION,SERVER_KEY_OPTION)
else:
  print "API Client ID option found: %s" % config.has_option(API_SECTION,CLIENTID_KEY_OPTION)
  print "API Key option found: %s" % config.has_option(API_SECTION,API_KEY_OPTION)
  print "API Server option found: %s" % config.has_option(API_SECTION,SERVER_KEY_OPTION)
  print "Some required settings have not been found, exiting..."
  sys.exit(0)

if config.has_option(API_SECTION,SERVERVERSION_KEY_OPTION):
  VERSION = config.get(API_SECTION,SERVERVERSION_KEY_OPTION)

if config.has_option(API_SECTION,EVENTCHECKINTERVAL_KEY_OPTION):
  EVENT_CHECK_INTERVAL = config.get(API_SECTION,EVENTCHECKINTERVAL_KEY_OPTION)

if config.has_option(API_SECTION,LIMIT_KEY_OPTION):
  HTTP_RESULT_LIMIT = config.get(API_SECTION,LIMIT_KEY_OPTION)

if config.has_option(API_SECTION,LOGFILE_KEY_OPTION):
  LOG_FILE = config.get(API_SECTION,LOGFILE_KEY_OPTION)

if config.has_option(API_SECTION,LOG_LEVEL_OPTION):
  TEMP = str(config.get(API_SECTION,LOG_LEVEL_OPTION))
  if TEMP.lower() == "debug":
    LOG_LEVEL = logging.DEBUG
  elif TEMP.lower() == "info":
    LOG_LEVEL = logging.INFO
  elif TEMP.lower() == "critical":
    LOG_LEVEL = logging.CRITICAL
  elif TEMP.lower() == "error":
    LOG_LEVEL = logging.ERROR
  elif TEMP.lower() == "warning":
    LOG_LEVEL = logging.WARN
  elif TEMP.lower() == "fatal":
    LOG_LEVEL = logging.FATAL
  logger.setLevel(LOG_LEVEL)

if config.has_option(API_SECTION,REQ_TIME_DIFF_KEY_OPTION):
  REQUEST_TIME_DIFFERENTIAL=int(config.get(API_SECTION,REQ_TIME_DIFF_KEY_OPTION))

if config.has_option(API_SECTION,RETRY_INTERVAL_KEY_OPTION):
  RETRY_INTERVAL=int(config.get(API_SECTION,RETRY_INTERVAL_KEY_OPTION))

if config.has_option(API_SECTION,ENABLE_COMPUTER_INV_KEY_OPTION):
  ENABLE_COMPUTER_INVENTORY=bool(config.get(API_SECTION,ENABLE_COMPUTER_INV_KEY_OPTION))

if config.has_option(API_SECTION,NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE_KEY_OPTION):
  NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE=int(config.get(API_SECTION,NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE_KEY_OPTION))

logger.info("Starting script....")
logger.debug("Using Client ID %s with API key %s and server %s (Version: %s)." % (CLIENTID,KEY,SERVER,VERSION))


# This while loops ensures a constant check Interval time of the main thread.
# The queryEvents method will return the time of when the HTTP request finished.
# This time should then be used as a starting time for the next query that way
# parsing the events (no matter how many) doesn't disrupt the timing.
NEXT_QUERY_TIME = datetime.datetime.utcnow()

# Disable expand time window if in deep debugging mode.
if DEEP_DEBUG:
  NEXT_QUERY_TIME = datetime.datetime.utcnow() - datetime.timedelta(days=14) # TESTING ONLY

# This cache reduces computer identity look ups if they have already been performed since the last iteration.
IdentityLookupCache = {}
POLL_COUNTER = 1
try:
  while True:

    # This is a simplistic mechanism to cache computer IP addresses and connector_guid mappings.  This will lower the number of requests to Cisco, which are
    # rate-limited and finite at the present time per hour, and increase performance for subsequent events of the same connector_guid.  It will also reduce
    # repetative logging that AlienVault will have to process with regards to Indentity Events.  This is an accuracy vs performance tradeoff mechanism.
    if POLL_COUNTER > NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE:
      # This cache reduces computer identity look ups if they have already been performed since the last iteration.
      logger.debug("Clearing cache, reached %s of %s iterations" % (str(POLL_COUNTER),str(NUMBER_OF_POLLS_BEFORE_CLEARING_CACHE)))
      IdentityLookupCache.clear()
      POLL_COUNTER = 1
    else:
      POLL_COUNTER += 1
    logger.info("Starting main request query....")
    NEXT_QUERY_TIME = queryEvents(NEXT_QUERY_TIME)
    logger.info("Main Thread Sleeping for %s seconds. Next query time is %s." % (str(EVENT_CHECK_INTERVAL),str(NEXT_QUERY_TIME.isoformat())))
    time.sleep(int(EVENT_CHECK_INTERVAL))
except KeyboardInterrupt, e:
    logger.fatal("Keyboard interrupt received....exiting")
    sys.exit(0)
