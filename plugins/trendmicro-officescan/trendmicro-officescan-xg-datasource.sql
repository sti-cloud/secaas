--
-- Trendmicro OfficeScan XG Datasource
-- Written By: Sidney Eaton
-- Last Modified: 2019-03-04
--

-- Set plugin id variable
SET @pluginID = '20009';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
VALUES (@pluginID, 1, 'tm-officescan-xg', 'Trend Micro OfficeScan XG', 3);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 12, 99, 'OfficeScan: Virus detected, cannot perform clean action', 2, 3);
call STICreateDatasourceEntry(@pluginID, 2, 12, 97, 'OfficeScan: Virus detected, access was denied', 2, 3);


-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;



insert IGNORE into host_source_reference (id, name, relevance) VALUES (10004,"Trend Micro Office Scan XG",5);
