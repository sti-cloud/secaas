--
-- IPSwitch WS-FTP
-- Written By: Sidney Eaton
-- Last Modified: 1-19-2017
-- Version: 0.5
--

SET @pluginID = '20002';

DELETE FROM plugin WHERE id = @pluginID;
DELETE FROM plugin_sid where plugin_id = @pluginID;

INSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (@pluginID, 1, 'IPSwitch-WSFTP', 'IPSwitch-WSFTP', 4);

INSERT IGNORE INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES
(@pluginID, 1, 13, 234, 'IPSwitch WS-FTP: SSH Client closed connection', 5, 1),
(@pluginID, 3, 13, 234, 'IPSwitch WS-FTP: SSH Connection closed', 5, 1),
(@pluginID, 2, 13, 233, 'IPSwitch WS-FTP: SSH Connection established', 5, 1),
(@pluginID, 4, 13, 234, 'IPSwitch WS-FTP: FTP Client closed connection', 5, 1),
(@pluginID, 5, 13, 233, 'IPSwitch WS-FTP: FTP Connection established', 5, 1),
(@pluginID, 6, 13, 234, 'IPSwitch WS-FTP: FTP Connection closed', 5, 1),
(@pluginID, 7, 13, 234, 'IPSwitch WS-FTP: SFTP Client closed connection', 5, 1),
(@pluginID, 8, 13, 233, 'IPSwitch WS-FTP: SFTP Connection established', 5, 1),
(@pluginID, 9, 13, 234, 'IPSwitch WS-FTP: SFTP Connection closed', 5, 1),
(@pluginID, 10, 13, 234, 'IPSwitch WS-FTP: SSH: An existing connection was forcibly closed by the remote host', 5, 1),
(@pluginID, 21, 2, 82, 'IPSwitch WS-FTP: SSH Completed password Authentication. User logged in', 5, 1),
(@pluginID, 18, 2, 83, 'IPSwitch WS-FTP: FTP No User', 5, 2),
(@pluginID, 19, 2, 83, 'IPSwitch WS-FTP: SFTP No User', 5, 2),
(@pluginID, 20, 2, 83, 'IPSwitch WS-FTP: SSH No User', 5, 2),
(@pluginID, 30, 3, 32, 'IPSwitch WS-FTP: SFTP IP Address was removed from the IP Lockout List', 5, 1),
(@pluginID, 31, 3, 32, 'IPSwitch WS-FTP: FTP IP Address was removed from the IP Lockout List', 5, 1),
(@pluginID, 33, 3, 32, 'IPSwitch WS-FTP: IPLock IP Address was removed from the IP Lockout List', 5, 1),
(@pluginID, 34, 7, 52, 'IPSwitch WS-FTP: SFTP: IP Address was added to the IP Lockout List', 5, 2),
(@pluginID, 35, 7, 52, 'IPSwitch WS-FTP: FTP: IP Address was added to the IP Lockout List', 5, 2),
(@pluginID, 51, 3, 31, 'IPSwitch WS-FTP: SFTP Open file', 5, 1),
(@pluginID, 52, 13, 235, 'IPSwitch WS-FTP: SFTP Finished writing file', 5, 1),
(@pluginID, 53, 13, 232, 'IPSwitch WS-FTP: SFTP File not found', 5, 1),
(@pluginID, 50, 13, 231, 'IPSwitch WS-FTP: channel: SFTP subsystem started in channel', 5, 1),
(@pluginID, 68, 13, 232, 'IPSwitch WS-FTP: SSH Failed to start SSH Transport', 5, 1),
(@pluginID, 69, 13, 232, 'IPSwitch WS-FTP: SSH Connection timed out or failed to respond', 5, 1),
(@pluginID, 70, 13, 232, 'IPSwitch WS-FTP: SSH Unsupported client version', 5, 1),
(@pluginID, 71, 13, 232, 'IPSwitch WS-FTP: SSH Failed to select a host key algorithm', 5, 1),
(@pluginID, 22, 2, 83, 'IPSwitch WS-FTP: FTP Failed PASS Authentication', 5, 2),
(@pluginID, 23, 2, 83, 'IPSwitch WS-FTP: SSH Failed password Authentication', 5, 2),
(@pluginID, 24, 2, 82, 'IPSwitch WS-FTP: FTP logon success', 5, 2),
(@pluginID, 80, 13, 232, 'IPSwitch WS-FTP: FTP Unknown security mechanism', 5, 1),
(@pluginID, 81, 13, 232, 'IPSwitch WS-FTP: FTP Command HELP not accepted in Connected state', 5, 1),
(@pluginID, 82, 13, 232, 'IPSwitch WS-FTP: FTP Command FEAT not accepted in Connected state', 5, 1),
(@pluginID, 54, 8, 65, 'IPSwitch WS-FTP: SFTP Translated', 5, 1),
(@pluginID, 55, 13, 232, 'IPSwitch WS-FTP: SFTP Folder not found', 5, 1),
(@pluginID, 49, 8, 65, 'IPSwitch WS-FTP: Generic Channel Message', 5, 1),
(@pluginID, 1001, 8, 65, 'IPSwitch WS-FTP: FTP Generic Channel Message', 5, 1),
(@pluginID, 1002, 8, 65, 'IPSwitch WS-FTP: SFTP Generic Channel Message', 5, 1),
(@pluginID, 1000, 8, 65, 'IPSwitch WS-FTP: Generic COM_API Event', 5, 1),
(@pluginID, 200000000, 8, 65, 'IPSwitch WS-FTP: Generic Event', 5, 1);

-- Turns out AlienVault has several duplicate tables for plugin_sid.  They seem to get populated when a manual datasource is entered from the GUI.  This is just to mimick that.
DELETE FROM plugin_sid_changes where plugin_id = @pluginID;
DELETE FROM plugin_sid_orig where plugin_id = @pluginID;
INSERT IGNORE INTO plugin_sid_changes (plugin_ctx, plugin_id, sid, class_id, aro, category_id, subcategory_id, name, reliability, priority) SELECT plugin_ctx, plugin_id, sid, class_id, aro, category_id, subcategory_id, name, reliability, priority FROM plugin_sid WHERE plugin_id = @pluginID;
INSERT IGNORE INTO plugin_sid_orig (plugin_ctx, plugin_id, sid, class_id, aro, category_id, subcategory_id, name, reliability, priority) SELECT plugin_ctx, plugin_id, sid, class_id, aro, category_id, subcategory_id, name, reliability, priority FROM plugin_sid WHERE plugin_id = @pluginID;