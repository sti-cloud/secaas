--
-- Sentinel Proofpoint CASB Plugin
-- Written By: Matt Weiss
-- Last Modified: 2020-10-22
--

SET @pluginID = '40005';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
        VALUES (@pluginID, 1, 'sti-proofpoint-casb', 'Proofpoint CASB', 18);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 15, 55, 'Proofpoint CASB: Suspicious Login Alert', 5, 5);
call STICreateDatasourceEntry(@pluginID, 2, 15, 55, 'Proofpoint CASB: Suspicious Activity Alert', 5, 5);
call STICreateDatasourceEntry(@pluginID, 3, 15, 55, 'Proofpoint CASB: Data Leakage Alert', 5, 5);
call STICreateDatasourceEntry(@pluginID, 4, 10, 236, 'Proofpoint CASB: Successful Login', 1, 1);
call STICreateDatasourceEntry(@pluginID, 5, 10, 237, 'Proofpoint CASB: Unsuccessful Login', 1, 1);
call STICreateDatasourceEntry(@pluginID, 6, 10, 69, 'Proofpoint CASB: File/Folder Download', 1, 1);
call STICreateDatasourceEntry(@pluginID, 7, 10, 69, 'Proofpoint CASB: File Modify', 1, 1);
call STICreateDatasourceEntry(@pluginID, 8, 10, 69, 'Proofpoint CASB: File Access', 1, 1);
call STICreateDatasourceEntry(@pluginID, 9, 10, 69, 'Proofpoint CASB: File Modify Extend', 1, 1);
call STICreateDatasourceEntry(@pluginID, 10, 10, 69, 'Proofpoint CASB: File Access Extend', 1, 1);
call STICreateDatasourceEntry(@pluginID, 11, 10, 69, 'Proofpoint CASB: File Preview', 1, 1);
call STICreateDatasourceEntry(@pluginID, 12, 10, 69, 'Proofpoint CASB: File Upload', 1, 1);
call STICreateDatasourceEntry(@pluginID, 13, 10, 69, 'Proofpoint CASB: Folder Add', 1, 1);
call STICreateDatasourceEntry(@pluginID, 14, 10, 69, 'Proofpoint CASB: File/Folder Move', 1, 1);
call STICreateDatasourceEntry(@pluginID, 15, 10, 69, 'Proofpoint CASB: File sync Upload', 1, 1);
call STICreateDatasourceEntry(@pluginID, 16, 10, 69, 'Proofpoint CASB: Sharing Inheritance Break', 1, 1);
call STICreateDatasourceEntry(@pluginID, 17, 10, 69, 'Proofpoint CASB: File Rename', 1, 1);
call STICreateDatasourceEntry(@pluginID, 18, 10, 69, 'Proofpoint CASB: Folder Modify', 1, 1);
call STICreateDatasourceEntry(@pluginID, 19, 10, 69, 'Proofpoint CASB: Folder Sync Download', 1, 1);
call STICreateDatasourceEntry(@pluginID, 20, 10, 69, 'Proofpoint CASB: Send to Recycle Bin', 1, 1);
call STICreateDatasourceEntry(@pluginID, 21, 10, 69, 'Proofpoint CASB: File Sharing Revoked', 1, 1);
call STICreateDatasourceEntry(@pluginID, 22, 10, 69, 'Proofpoint CASB: Sharing Invitation Create', 1, 1);
call STICreateDatasourceEntry(@pluginID, 23, 10, 69, 'Proofpoint CASB: Secure Link Usage', 1, 1);
call STICreateDatasourceEntry(@pluginID, 24, 10, 69, 'Proofpoint CASB: Secure Link Create', 1, 1);
call STICreateDatasourceEntry(@pluginID, 25, 10, 69, 'Proofpoint CASB: Folder Rename', 1, 1);
call STICreateDatasourceEntry(@pluginID, 26, 10, 69, 'Proofpoint CASB: Send As', 1, 1);
call STICreateDatasourceEntry(@pluginID, 27, 10, 69, 'Proofpoint CASB: Send On Behalf', 1, 1);
call STICreateDatasourceEntry(@pluginID, 28, 10, 69, 'Proofpoint CASB: Message Blocked', 1, 1);
call STICreateDatasourceEntry(@pluginID, 29, 10, 69, 'Proofpoint CASB: Message Delivered', 1, 1);
call STICreateDatasourceEntry(@pluginID, 30, 10, 69, 'Proofpoint CASB: User Updated', 1, 1);
call STICreateDatasourceEntry(@pluginID, 31, 10, 69, 'Proofpoint CASB: Group Member Add', 1, 1);
call STICreateDatasourceEntry(@pluginID, 32, 10, 69, 'Proofpoint CASB: 3rd Party App Authorize', 1, 1);
call STICreateDatasourceEntry(@pluginID, 33, 10, 69, 'Proofpoint CASB: Site Collection User Add', 1, 1);
call STICreateDatasourceEntry(@pluginID, 34, 7, 237, 'Proofpoint CASB: Suspicious Login', 1, 1);
call STICreateDatasourceEntry(@pluginID, 35, 10, 69, 'Proofpoint CASB: Group Update', 1, 1);
call STICreateDatasourceEntry(@pluginID, 20000000, 10, 69, 'Proofpoint CASB: Generic Event', 1, 1);
call STICreateDatasourceEntry(@pluginID, 20000001, 10, 69, 'Proofpoint CASB: Generic Alert', 5, 5);

-- Cleanup
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
