--
-- McAfee ePO Datasource Update
-- Last signatures update: NA
-- Written By: Sidney Eaton
-- Last Modified: 3-14-2017
--

-- Set plugin id variable
SET @pluginID = '4008';

-- Insert product if not already exists XXX Assumed it always exists, probably bad assumption.

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, name, reliability, priority) VALUES (PID, SID, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, name, reliability, priority) VALUES (PID, SID, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, name, reliability, priority) VALUES (PID, SID, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

call STICreateDatasourceEntry(@pluginID, 19100, 'McAfee ePO: Policy Changed (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19101, 'McAfee ePO: Agent Installed (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19102, 'McAfee ePO: Agent Enters Bypass Mode (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19103, 'McAfee ePO: Agent Leaves Bypass Mode (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19104, 'McAfee ePO: User Logged Into Safe Mode (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19105, 'McAfee ePO: Evidence Replication Failed (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19106, 'McAfee ePO: Application File Access Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19107, 'McAfee ePO: File System Discovery (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19108, 'McAfee ePO: Email Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19109, 'McAfee ePO: Network Share Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19110, 'McAfee ePO: Network Communication Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19111, 'McAfee ePO: Printing Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19112, 'McAfee ePO: Removable Storage Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19113, 'McAfee ePO: Screen Capture Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19114, 'McAfee ePO: Web Post Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19115, 'McAfee ePO: Device Plug (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19116, 'McAfee ePO: Device Unplug (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19117, 'McAfee ePO: New Device Class Found (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19118, 'McAfee ePO: File Released from Quarantine (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19119, 'McAfee ePO: DRM Failure (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19120, 'McAfee ePO: Email Storage Discovery (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19121, 'McAfee ePO: User has tried to disable outlook plug-in (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19122, 'McAfee ePO: Outlook plug-in was added to disabled items (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19123, 'McAfee ePO: Release Code Locked (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19124, 'McAfee ePO: Automatic Dump Created (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19125, 'McAfee ePO: Agent Exceeded Memory Limit on Load (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19126, 'McAfee ePO: Discovery Summary Report (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19127, 'McAfee ePO: Clipboard Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19128, 'McAfee ePO: Policy Applied (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19129, 'McAfee ePO: Agent Up (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19130, 'McAfee ePO: Agent Down (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19131, 'McAfee ePO: Agent Uninstall Key Generated (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19132, 'McAfee ePO: Agent Quarantine Release Key Generated (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19133, 'McAfee ePO: Agent Override Key Generated (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19134, 'McAfee ePO: Cloud Protection (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19135, 'McAfee ePO: Wrong Driver Version (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19170, 'McAfee ePO: Network Discover File Information (Info)', 1, 0);
call STICreateDatasourceEntry(@pluginID, 19171, 'McAfee ePO: Network Discover Incident (Info)', 1, 0);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;