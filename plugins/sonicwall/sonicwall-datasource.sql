--
-- Sonicwall Datasource Modifications
-- Written By: Sidney Eaton
-- Last Modified: 2-10-2017
--
SET @pluginID = '1573';

DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;


call STICreateDatasourceEntry(@pluginID,1233,8,188,'SonicWALL: Unhandled link-local or multicast IPv6 packet dropped',1,1);
call STICreateDatasourceEntry(@pluginID,1391,15,172,'SonicWALL: Attack - Packet Data',1,1);
call STICreateDatasourceEntry(@pluginID,171,11,140,'SonicWALL: VPN IKE DEBUG',1,0);
call STICreateDatasourceEntry(@pluginID,1420,13,219,'SonicWALL: DHCPv6 Server received message',1,1);
call STICreateDatasourceEntry(@pluginID,1420,13,220,'SonicWALL: DHCPv6 Server sent message',1,1);
call STICreateDatasourceEntry(@pluginID,1419,13,218,'SonicWALL: DHCPv6 Server - Deleted scope',1,1);
call STICreateDatasourceEntry(@pluginID,1257,3,76,'SonicWALL: ICMPv6 packet dropped due to policy',1,1);
call STICreateDatasourceEntry(@pluginID,1254,3,76,'SonicWALL: ICMPv6 packet from LAN dropped',1,1);
call STICreateDatasourceEntry(@pluginID,1079,10,71,'SonicWALL: Generic SSL VPN Message',1,1);

DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;