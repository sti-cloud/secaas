--
-- Sentinel Defender ATP Plugin
-- Written By: Tom Martin
-- Written: 2020-05-29
--

SET @pluginID = '21013';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
VALUES (@pluginID, 1, 'Windows Defender ATP', 'Defender ATP', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 3, 32, 'Defender ATP: Initial Access', 4, 4);
call STICreateDatasourceEntry(@pluginID, 2, 1, 5, 'Defender ATP: Execution', 4, 4);
call STICreateDatasourceEntry(@pluginID, 3, 4, 41, 'Defender ATP: Persistence', 4, 4);
call STICreateDatasourceEntry(@pluginID, 4, 6, 130, 'Defender ATP: Impact', 4, 4);
call STICreateDatasourceEntry(@pluginID, 5, 1, 17, 'Defender ATP: Defense Evasion', 4, 4);
call STICreateDatasourceEntry(@pluginID, 6, 7, 55, 'Defender ATP: Suspicious Activity', 4, 4);
call STICreateDatasourceEntry(@pluginID, 7, 9, 70, 'Defender ATP: Discovery', 4, 4);
call STICreateDatasourceEntry(@pluginID, 8, 7, 122, 'Defender ATP: Lateral Movement', 4, 4);
call STICreateDatasourceEntry(@pluginID, 9, 2, 28, 'Defender ATP: Collection', 4, 4);
call STICreateDatasourceEntry(@pluginID, 10, 2, 28, 'Defender ATP: Exfiltration', 4, 4);
call STICreateDatasourceEntry(@pluginID, 11, 1, 17, 'Defender ATP: Command and Control', 4, 4);
call STICreateDatasourceEntry(@pluginID, 12, 1, 17, 'Defender ATP: Exploit', 4, 4);
call STICreateDatasourceEntry(@pluginID, 13, 4, 40, 'Defender ATP: Malware', 4, 4);
call STICreateDatasourceEntry(@pluginID, 14, 4, 40, 'Defender ATP: Unwanted Software', 4, 4);

