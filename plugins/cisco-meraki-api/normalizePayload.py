#!/usr/bin/python3.7
# Created By Sidney Eaton
# Created On 10/30/2019
# Version 0.8
# TO DO:
#
# Adjust python paths
import sys
import re

# This is used for Docker Image
sys.path.append("/usr/local/lib/python3.7/site-packages")

# Import standard web request packages
import logging

try:
    """
         This is to allow backwards compatibility with older and very new python versions
    """
    import collections.abc as collections
except Exception as ex:
    import collections as collections


''' This class is dedicated to scalable and modular normalization '''
class normalizePayload(object):
    FALLBACK_RULE_TYPE = "FALLBACK"
    ALWAYS_CHECK_RULE_TYPE = "ALWAYS"
    ALWAYS_CREATE_RULE_TYPE = "ALWAYS_CREATE"
    def __init__(self, ruleset = {}):
        self.ruleset = ruleset

    def addEventType(self, event_type_string, mapping_dictionary):
        self.ruleset[event_type_string] = mapping_dictionary

    def addFallbackRule(self, mapping_dictionary):
        self.addEventType(self.FALLBACK_RULE_TYPE, mapping_dictionary)

    def addCheckAllRule(self, mapping_dictionary):
        self.addEventType(self.ALWAYS_CHECK_RULE_TYPE, mapping_dictionary)

    def addCheckAllRule(self, mapping_dictionary):
        self.addEventType(self.ALWAYS_CREATE_RULE_TYPE, mapping_dictionary)

    def normalizePayload(self, event_type_string, dictionary_payload):
        if event_type_string in self.ruleset:
            self.normalizePayload_helper(event_type_string, dictionary_payload)
            self.createDictionaryKeys(event_type_string, dictionary_payload)
        elif self.FALLBACK_RULE_TYPE in self.ruleset:
            self.normalizePayload_helper(self.FALLBACK_RULE_TYPE, dictionary_payload)

        if self.ALWAYS_CHECK_RULE_TYPE in self.ruleset:
            self.normalizePayload_helper(self.ALWAYS_CHECK_RULE_TYPE, dictionary_payload)
        
        self.createDictionaryKeys(self.ALWAYS_CREATE_RULE_TYPE, dictionary_payload)



        return dictionary_payload

    def createDictionaryKeys(self, event_type_string, dictionary_payload):
        if event_type_string in self.ruleset:
            for k in self.ruleset[event_type_string]:
                try:
                    overwrite_key = self.ruleset[event_type_string][k].get("OVERWRITE", "FALSE")
                    if overwrite_key.upper() == "TRUE" or k not in dictionary_payload:
                        dictionary_payload[k] = self.ruleset[event_type_string][k]["CREATE"]
                except KeyError:
                    logging.exception("Key error while trying to create a key for specific event type, likely CREATE command with value missing.")

    def isScalar(self, value):
        if type(value) is str or type(value) is int or type(value) is bool or type(value) is unicode or type(value) is float:
            return True
        else:
            return False
                     

    def normalizePayload_helper(self, event_type_string, dictionary):
        # Figure out what type of objet was passed
        if isinstance(dictionary, collections.Mapping):
            for k in list(dictionary.keys()):
                logging.debug("k(%s) is in dictionary key list." % str(k))
                ### Iterate through list to find out if values need to be renamed or are not dictionary


                if not self.isScalar(dictionary[k]):
                     logging.debug("dictionary[k] is type %s, this is not a scalar value." % str(type(dictionary[k])))
                     self.normalizePayload_helper(event_type_string, dictionary[k])
                else:
                    if k in self.ruleset[event_type_string]:
                        if "SIMPLE_MATCH" in self.ruleset[event_type_string][k]:
                            if "SIMPLE_MATCH_NEW_FIELD" in self.ruleset[event_type_string][k]:
                                dictionary[self.ruleset[event_type_string][k]['SIMPLE_MATCH_NEW_FIELD']] = self.simple_pattern_match(str(dictionary[k]),self.ruleset[event_type_string][k]['SIMPLE_MATCH'])
                            else:
                                dictionary[k] = self.simple_pattern_match(str(dictionary[k]),self.ruleset[event_type_string][k]['SIMPLE_MATCH'])

                        if "UPPERCASE" in self.ruleset[event_type_string][k]:
                            if self.ruleset[event_type_string][k]["UPPERCASE"].upper() == "TRUE":
                                dictionary[k] = dictionary[k].upper()

                        if "LOWERCASE" in self.ruleset[event_type_string][k]:
                            if self.ruleset[event_type_string][k]["LOWERCASE"].upper() == "TRUE":
                                dictionary[k] = dictionary[k].lower()

                        if "LOOKUP" in self.ruleset[event_type_string][k]:
                            if "LOOKUP_NEW_FIELD" in self.ruleset[event_type_string][k]:
                                print(self.ruleset[event_type_string][k]['LOOKUP'].get(str(dictionary[k]), dictionary[k]))
                                dictionary[self.ruleset[event_type_string][k]['LOOKUP_NEW_FIELD']] = self.ruleset[event_type_string][k]['LOOKUP'].get(str(dictionary[k]), dictionary[k])
                            else:
                                dictionary[k] = self.ruleset[event_type_string][k]['LOOKUP'].get(str(dictionary[k]), dictionary[k])


                        if "SPLIT" in self.ruleset[event_type_string][k] and "SPLIT_FIELDS" in self.ruleset[event_type_string][k]:
                            split_value = dictionary[k].split(self.ruleset[event_type_string][k]["SPLIT"],len(self.ruleset[event_type_string][k]["SPLIT_FIELDS"]))
                            i = 0
                            try:
                                for myValue in split_value:
                                    dictionary[self.ruleset[event_type_string][k]['SPLIT_FIELDS'][i]] = myValue
                                    i += 1
                            except IndexError as ex:
                                pass

                        if "REX" in self.ruleset[event_type_string][k] and "REX_VALUE" in self.ruleset[event_type_string][k]:
                          try:
                            if "REX_NEW_FIELD" in self.ruleset[event_type_string][k]:
                                dictionary[self.ruleset[event_type_string][k]['REX_NEW_FIELD']] = re.sub(r'{}'.format(self.ruleset[event_type_string][k]['REX']),r'{}'.format(self.ruleset[event_type_string][k]['REX_VALUE']),dictionary[k])
                            else:
                                dictionary[k] = re.sub(r'{}'.format(self.ruleset[event_type_string][k]['REX']),r'{}'.format(self.ruleset[event_type_string][k]['REX_VALUE']),dictionary[k])
                          except re.error as ex:
                              logging.error("Regex error for %s." % str(k))

                        if "COPY_TO" in self.ruleset[event_type_string][k]:
                            dictionary[self.ruleset[event_type_string][k]['COPY_TO']] = dictionary[k]


                        if "RENAME_FIELD" in self.ruleset[event_type_string][k]:
                            dictionary[self.ruleset[event_type_string][k]['RENAME_FIELD']] = dictionary.pop(k)
                        elif "DELETE_FIELD" in self.ruleset[event_type_string][k]:
                            dictionary.pop(k)



        elif type(dictionary) is not str and isinstance(dictionary, collections.Sequence):

            for value in list(dictionary):
#                if type(value) is not str and type(value) is not int:
                    self.normalizePayload_helper(event_type_string, value)



    def flatten(self, d, parent_key='', sep='_'):

        items = []
        for k, v in d.items():
            new_key = parent_key + sep + k if parent_key else k
            if isinstance(v, collections.MutableMapping):
                items.extend(self.flatten(v, new_key, sep=sep).items())
            else:
                items.append((new_key, v))
        return dict(items)


    def convertToString(self, dict_payload):
        message = ""
        for k in dict_payload:
            message += " " + k + "=" + str(dict_payload[k]) + ";"
        return message[1:]


#    def simple_pattern_match(self, string_to_match, value_mapping_dict, case_insensitive = True):
#        for value in string_to_match.split():
#            if value in value_mapping_dict:
#                return value_mapping_dict[value]
#            elif case_insensitive:
#                for k in value_mapping_dict.keys():
#                    if value.upper() == k.upper():
#                        return value_mapping_dict[k]
#
#        return ""
    def simple_pattern_match(self, string_to_match, value_mapping_dict, case_sensitive = False):
        for value in value_mapping_dict.keys():
            if case_sensitive:
                if value in string_to_match:
                    return value_mapping_dict[value]
            else:
                if value.lower() in string_to_match.lower():
                    return value_mapping_dict[value]
        return ""






    # A generic test case handler for testing complex code in this normalization ruleset.
    def testCase(self):
        sample_data = {"id": 6758530751387402248, "timestamp": 1573593065, "timestamp_nanoseconds": 911000000, "date": "2019-11-12T21:11:05+00:00", "event_type": "Threat Detected", "event_type_id": 1090519054, "detection": "W32.5E1BF2E0A7-95.SBX.TG", "detection_id": "6758530751387402244", "connector_guid": "a0fa93f4-27d1-483b-aa59-1c9fd8e96988", "group_guids": ["e1af182d-b21b-4cad-8611-f599b70e682c"], "severity": "Medium", "computer": {"connector_guid": "a0fa93f4-27d1-483b-aa59-1c9fd8e96988", "hostname": "EQKT347", "external_ip": "24.13.246.81", "user": "fkhan@SENTINEL-DG", "active": "true", "network_addresses": [{"ip": "192.168.1.8", "mac": "10:02:b5:bc:5d:09"}], "links": {"computer": "https://api.amp.cisco.com/v1/computers/a0fa93f4-27d1-483b-aa59-1c9fd8e96988", "trajectory": "https://api.amp.cisco.com/v1/computers/a0fa93f4-27d1-483b-aa59-1c9fd8e96988/trajectory", "group": "https://api.amp.cisco.com/v1/groups/e1af182d-b21b-4cad-8611-f599b70e682c"}}, "file": {"disposition": "Malicious", "file_name": "Adobe_Flash_Player_(Firefox,_Mozilla,_Opera,_Chrome)_64-bit_0955003576.exe", "file_path": "\\\\?\\C:\\Users\\fkhan\\Downloads\\Adobe_Flash_Player_(Firefox,_Mozilla,_Opera,_Chrome)_64-bit_0955003576.exe", "identity": {"sha256": "5e1bf2e0a776da739c05a0dbd151541defbb8fb158c80889a448044cddb40ede"}, "parent": {"process_id": 12544, "disposition": "Unknown", "file_name": "chrome.exe", "identity": {"sha256": "92ac89c4590ca570b51dfcadb2eface63fde765622d0095754348995b15615c6", "sha1": "b524a416e53b25cd648e6ffde3790434475112c7", "md5": "8698e468bc379e30383a72ce63da7972"}}}}
        #sample_rule = { 'file_file_name' : { 'RENAME_FIELD': "file_name" },
        #        'file_parent_file_name': { 'RENAME_FIELD': "parent_file_name"},
        #        'file_file_path' : { 'RENAME_FIELD': "file_path"},
        #        'computer_hostname' : { 'RENAME_FIELD': "src_host"},
        #        'file_identity_sha256' : { 'RENAME_FIELD': "file_hash", "UPPERCASE": "True", "COPY_TO": "file_hash_sha256"},
        #        'file_identity_sha1' : { 'RENAME_FIELD': "file_hash_sha1", "UPPERCASE": "True"},
        #        'file_identity_md5' : { 'RENAME_FIELD': "file_hash_md5", "UPPERCASE": "True"},

        #        'file_parent_identity_sha256' : { 'RENAME_FIELD': "parent_file_hash_sha256", "UPPERCASE": "True"},
        #        'file_parent_identity_sha1' : { 'RENAME_FIELD': "parent_file_hash_sha1", "UPPERCASE": "True"},
        #        'file_parent_identity_md5' : { 'RENAME_FIELD': "parent_file_hash_md5", "UPPERCASE": "True"},
        #        'file_parent_process_id' : { 'RENAME_FIELD': "parent_process_id"},
        #        'detection' : { 'RENAME_FIELD': "signature" }
        #        }



        #self.addCheckAllRule(sample_rule)
        sample_data = self.flatten(sample_data)
        print("FLATTEND KEYS: " + str(sample_data) + "\n\n\n")
        self.normalizePayload(sample_data['event_type'], sample_data)
        print("NORMALIZED KEYS: " + str(sample_data) + "\n\n\n")
        #print(self.flatten(sample_data))

