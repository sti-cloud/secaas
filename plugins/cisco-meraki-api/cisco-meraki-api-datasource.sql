--
-- Cisco Meraki API Datasource
-- Written By: Sidney Eaton
-- Last Modified: 11-20-2019
--
SET @pluginID = '20020';

-- Probably don't want this since I plan to dynamically add the new signatures using the script.
-- DELETE FROM plugin WHERE id = @pluginID;
-- DELETE FROM plugin_sid where plugin_id = @pluginID;

INSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (@pluginID, 1, 'cisco-meraki-api', 'Cisco Meraki', 14);