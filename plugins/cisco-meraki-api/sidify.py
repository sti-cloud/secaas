#!/usr/bin/python

# Weird name I know, but AlienVault irritates me into solutions because of its rigidness. So deal with the module name.

import threading
import MySQLdb

import sys
sys.path.append("/usr/local/bin/STITicketSystem")
from AlienVaultConfig import AlienVaultConfig

import logging
from contextlib import closing

class Cache(object):
    def __init__(self):
        self._cache = {}
        self._timers = []
        self._wlock = threading.RLock()
        self._rlock = threading.Event()
        self._rlock.set()

    def check(self, key):
        read_lock = self._rlock.wait(timeout=None) 
        if key in self._cache:
             return True
        else:
             return False
    def add(self, key, value, TTL = 3600):
        try:
            if not self.check(key):
                logging.debug("Key %s does not exist in cache." % key)
                self._rlock.clear()
                with self._wlock:
                    logging.debug("Obtained write lock on cache.")
                    self._cache[key] = value
                    timer = threading.Timer(TTL, self.remove, [key])
                    timer.setDaemon(True)
                    timer.start()
                return True
            else:
                logging.debug("Key %s exists in cache." % key)
                return False
        except Exception as ex:
            logging.exception("Unknown error in cache database.")
            raise Exception
        finally:
            self._rlock.set()

    def remove(self, key):
        logging.debug("Attempting to remove '%s' from cache." % key)
        with self._wlock:
            return self._cache.pop(key, None)

    def getCount(self):
        return len(self._cache)

class sido(object):
    def __init__(self, **kwargs):
        self._properties = {}
        self._name_attribute = kwargs.get('name_attribute',"name")
        self._table_name = kwargs.get('table_name', "plugin_sid")
        self._add_ignore = kwargs.get('ignore', True)
        self._add_on_duplicate = kwargs.get('duplicate', False)

    def add(self, key, value):
        self._properties[key] = value

    def to_sql_insert(self):
        base_msg = "INSERT "
        if self._add_ignore:
            base_msg += "IGNORE "
        insert_into_string = ""
        insert_into_value = ""
        on_duplicate_string = " ON DUPLICATE KEY UPDATE "
        for (k, v) in self._properties.items():
            insert_into_string += str(k) + ","
            on_duplicate_string += str(k) + "="
            if type(v) is int or type(v) is bool:
                insert_into_value += "%s," % (str(v))
                on_duplicate_string += str(v) + ","
            else:
                insert_into_value += "'%s'," % (str(v))
                on_duplicate_string += "'%s'," % (str(v))
            
        insert_into_string = insert_into_string[:-1]
        insert_into_value = insert_into_value[:-1]
        base_msg += "INTO %s (%s) VALUES (%s)" % (self._table_name,insert_into_string,insert_into_value)
        if self._add_on_duplicate:
            base_msg += on_duplicate_string[:-1]

        return base_msg + ";"

    def bulk_add(self, **kwargs):
        for (k, v) in dict(**kwargs).items():
            self.add(k,v)

    def get(self, key):
        return self._properties[key]

    def getName(self):
        #logging.debug("Returning sido name '%s'." %s self.get(self._name_attribute))
        return self.get(self._name_attribute)
        


class sidify_engine(object):
    def __init__(self):
        self._cache = Cache()
        

    def process(self, sido_obj):
        if self._cache.add(sido_obj.getName(), sido_obj):
            logging.debug("Added '%s' to the cache." % sido_obj.getName())
            self.genericQuery(sido_obj.to_sql_insert())


    def genericQuery(self, queryString):
        db = None
        try:
            AVCONFIG = AlienVaultConfig()
            db = MySQLdb.connect(host=AVCONFIG.DB_IP,user=AVCONFIG.DB_USER , passwd=AVCONFIG.DB_PASSWD ,db=AVCONFIG.DB)
            with closing( db.cursor(MySQLdb.cursors.DictCursor) ) as cursor:
                logging.debug("Executing SQL query: %s" % queryString)
                cursor.execute(queryString)
                db.commit()
        except Exception, e:
            logging.exception("ERROR attempting to execute sql query: %s" % e)
            return False
        finally:
            if not db is None:
                try:
                    db.close()
                except:
                    pass
        return True

    def getCount(self):
        return self._cache.getCount()