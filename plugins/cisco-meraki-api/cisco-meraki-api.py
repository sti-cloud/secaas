#!/usr/bin/python
# Created By Sidney Eaton
# Created On 10/30/2019
# Version 0.8
# TO DO:
#
# Adjust python paths
import sys
import os

# This is used for Docker Image
sys.path.append("/usr/local/lib/python3.7/site-packages")

#AlienVault specific imports
#sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")

# Import standard web request packages
import json
import requests
import logging
from datetime import datetime
from datetime import timedelta
import time

from normalizePayload import normalizePayload
from sidify import sidify_engine
from sidify import sido
#from datetime import timestamp

# Disable insecure HTTPS warnings
try:
    from requests.packages.urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
except Exception:
    pass

try:
    import configparser as configparser
except Exception:
    import ConfigParser as configparser


# Setup initial logging.
logging.basicConfig(level=logging.INFO, filename="/var/log/cisco-meraki-api-std.log", format='%(asctime)s UTC %(levelname)s %(module)s(%(funcName)s) [%(process)d-%(thread)d]: %(message)s')


# Create Normalization Rule Set
NormalizationRules = {
#        'Snort': {
#            'classification' : { 'LOOKUP': {
#                '12' : "attempted-admin",
#                '2' : "attempted-user",
#                '3' : "inappropriate-content",
#                '4' : "policy-violation",
#                '5' : "shellcode-detect",
#                '6' : "successful-admin",
#                '7' : "successful-user",
#                '8' : "trojan-activity",
#                '9' : "unsuccessful-user",
#                '28' : "web-application-attack"
#                '11' : "attempted-dos",
#                '12' : "attempted-recon",
#                '13' : "bad-unknown",
#                '14' : "default-login-attempt",
#                '15' : "denial-of-service",
#                '16' : "misc-attack",
#                '17' : "non-standard-protocol",
#                '18' : "rpc-portmap-decode",
#                '19' : "successful-dos",
#                '20' : "successful-recon-largescale",
#                '21' : "successful-recon-limited",
#                '22' : "suspicious-filename-detect",
#                '23' : "suspicious-login",
#                '24' : "system-call-detect",
#                '25' : "unusual-client-port-connection",
#                '26' : "web-application-activity",
#                '27' : "icmp-event",
#                '28' : "misc-activity",
#                '29' : "network-scan",
#                '30' : "not-suspicious",
#                '31' : "protocol-command-decode",
#                '32' : "string-detect",
#                '33' : "unknown",
#                '34' : "tcp-connection"
#            }, 'LOOKUP_NEW_FIELD' : "category" }
#        },
        normalizePayload.ALWAYS_CHECK_RULE_TYPE : {
            'blocked' : { 'RENAME_FIELD': "action", 'LOOKUP' : { 'True': "blocked", 'False': "allowed"} },
            'destIp' : { 'RENAME_FIELD': "meraki_dest", 'SPLIT': ":", 'SPLIT_FIELDS': [ 'dest_ip', 'dest_port' ]},
            'srcIp' : { 'RENAME_FIELD': "meraki_src", 'SPLIT': ":", 'SPLIT_FIELDS': [ 'src_ip', 'src_port' ]},
            'src_ip': { 'COPY_TO': "src" },
            'dest_ip': { 'COPY_TO': "dest" },
            'category' : { 'LOOKUP' : {
                'attempted-admin':"high",
                'attempted-user':"high",
                'inappropriate-content':"high",
                'policy-violation':"high",
                'shellcode-detect':"high",
                'successful-admin':"high",
                'successful-user':"high",
                'trojan-activity':"high",
                'unsuccessful-user':"high",
                'web-application-attack':"high",
                'attempted-dos':"medium",
                'attempted-recon':"medium",
                'bad-unknown':"medium",
                'default-login-attempt':"medium",
                'denial-of-service':"medium",
                'misc-attack':"medium",
                'non-standard-protocol':"medium",
                'rpc-portmap-decode':"medium",
                'successful-dos':"medium",
                'successful-recon-largescale':"medium",
                'successful-recon-limited':"medium",
                'suspicious-filename-detect':"medium",
                'suspicious-login':"medium",
                'system-call-detect':"medium",
                'unusual-client-port-connection':"medium",
                'web-application-activity':"medium",
                'icmp-event':"low",
                'misc-activity':"low",
                'network-scan':"low",
                'not-suspicious':"low",
                'protocol-command-decode':"low",
                'string-detect':"low",
                'unknown':"low",
                'tcp-connection':"informational"
                }, 'LOOKUP_NEW_FIELD' : "severity"
            },
            'protocol': { 'SPLIT': "/", 'SPLIT_FIELDS': ['transport'], "RENAME_FIELD" : "meraki_protocol"},
            'signature': { 'REX' : "^([0-9]+):([0-9]+):([0-9]+)", 'REX_VALUE': r"http://snort.org/rule-docs/\1-\2", 'REX_NEW_FIELD' : 'signature_url', 'RENAME_FIELD' : "signature_id"},
            'ruleId': { 'REX' : "^(\S+?)\/([0-9]+)$", 'REX_VALUE': r"\2", 'REX_NEW_FIELD' : 'snort_id'},
            'message' : { 'REX' : "^(\S+).*", 'REX_VALUE': r"\1", 'REX_NEW_FIELD' : "signature_group", 'SIMPLE_MATCH' : {
                'injection' : "Injection",
                'buffer' : "Buffer overflow",
                'malware' : "Malware",
                'trojan' : "Malware",
                'overflow' : "Buffer overflow",
                'denial' : "Denial of service",
                'indicator' : "IOC",
                'backdoor' : "Malware",
                'command' : "Command Execution",
                'remote': "Remote Code Execution",
                'sql' : "Injection",
                'exploit': "Exploit"
                }, 'SIMPLE_MATCH_NEW_FIELD': "sentinel_category", 'RENAME_FIELD' : "signature" }
        },
        normalizePayload.ALWAYS_CREATE_RULE_TYPE : {

            'ids_type' : { 'CREATE': "network"},
            'vendor_product' : { 'CREATE': "Cisco Meraki IPS"}
        }
}


class FileOutput(object):
    def __init__(self, **kwargs):
        self.file_name = kwargs.get('file_name', "/var/log/cisco-meraki-api.log")
        self.open_as = kwargs.get('open_as', "a")

    def convertDictionaryToString(self, dictionary):
        ComposeString = ""
        for k, v in dictionary.items():
            ComposeString += k + "=" + v + ", "
        ComposeString = ComposeString[:-2]
        return ComposeString

    def write(self, string_to_write):
        with open(self.file_name, self.open_as) as f:
            f.write(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC ") + string_to_write + "\n")


class ReadConfigurationFile(object):
    def __init__(self, **kwargs):
        self.Arguments = self.format_dictionary(dict(**kwargs))
        logging.debug("Created ReadConfiguration object with the following parameters: %s" % str(self.Arguments))
        self.config = configparser.ConfigParser()
        self.read()


    # Get Value Method - Returns either the configured option or the default option
    # RETURN - STRING OR INT
    # VALID INPUT PARAMS:
    #      SECTION - The section title in an INI file to look under for the option

    #      OPTION_NAME - The Option key name within the INI file.
    #      OPTION_VALUE_DEFAULT - The default value to return should the option not be found.
    def getValue(self, **kwargs):
        args = self.format_dictionary(dict(**kwargs))
        logging.debug("Attempting to derive the correct value with parameters: %s" % str(args))
        value = None
        try:
            if self.config.has_option(args['SECTION'], args['OPTION_NAME']):
                value = self.config.get(args['SECTION'], args['OPTION_NAME'])
            else:
                value = args['OPTION_VALUE_DEFAULT']
            self.setValue(args['OPTION_NAME'], value)
            return value
        except Exception as ex:
            logging.exception("Could not properly parse options")
            return None


    def read(self):
        try:
            self.config.read(self.Arguments['CONFIG_FILE'])
            return True
        except Exception as ex:
            logging.exception("Error occurred trying to read configuration file %s." % str(self.CONFIG_FILE))
            return False
    def format_dictionary(self, dictionary):
        newDict = {}
        for k, v in dictionary.items():
            newDict[k.upper()]=v
        return newDict
    def setValue(self, key, value):
        setattr(self, key, value)




class API_CLIENT_BASE(object):
    def __init__(self, **kwargs):
        global HTTP_TIMEOUT, HTTP_MAX_RETRIES, HTTP_RATE_LIMIT_WAIT_TIME, HTTP_RETRY_WAIT_TIME

        setattr(self, "HTTP_TIMEOUT", HTTP_TIMEOUT)
        setattr(self, "HTTP_MAX_RETRIES", HTTP_MAX_RETRIES)

        setattr(self, "HTTP_RETRY_WAIT_TIME", HTTP_RETRY_WAIT_TIME)
        setattr(self, "VERIFY_SSL", False)
        setattr(self, "HTTP_RATE_LIMIT_WAIT_TIME", HTTP_RATE_LIMIT_WAIT_TIME)

        for k, v in kwargs.items():
            logging.debug("Setting attribute %s to %s in API object." % (str(k),str(v)))
            setattr(self, k, v)

    """
        This method allows standardizing HTTP get methods.  Including on what to do with certain error codes.
    """
    def getRaw(self, **kwargs):
        current_attempts = 0
        while (current_attempts < self.HTTP_MAX_RETRIES):
            try:
                resp = requests.get(verify=self.VERIFY_SSL, timeout=self.HTTP_TIMEOUT, allow_redirects=True, **kwargs)
                
                if resp.status_code >= 200 or resp.status_code <= 299:
                    logging.debug("Payload response for request %s is %s" % (resp.url,str(resp.text)))
                    return resp
                elif resp.status_code == 429:
                    logging.warning("Reached rate-limit threshold on API when requesting %s, HTTP CODE: %s. Attempt %s of %s. PAYLOAD RETURNED: %s." % (kwargs['url'], str(resp.status_code), current_attempts, str(self.HTTP_MAX_RETRIES), str(resp.text)))
                    time.sleep(self.HTTP_RATE_LIMIT_WAIT_TIME)
                elif resp.status_code == 404 or resp.status_code == 400:
                    logging.error("Unknown permanent error when requesting %s, HTTP CODE: %s." % (kwargs['url'], str(resp.status_code)))
                    return resp
                else: 
                    logging.error("Unknown error when requesting %s, HTTP CODE: %s. Attempt %s of %s. PAYLOAD RETURNED: %s." % (kwargs['url'], str(resp.status_code), current_attempts, str(self.HTTP_MAX_RETRIES), str(resp.text)))
                    time.sleep(self.HTTP_RETRY_WAIT_TIME)
            except Exception as ex:
                logging.exception("Unknown error occurred while attempt to get URL.")
                return None
            finally:
                current_attempts += 1
        return None

    def get(self, **kwargs):
        try:
            return (self.getRaw(**kwargs)).text
        except Exception as ex:
            return None


class MerakiInterfaceV0(API_CLIENT_BASE):
    def __init__(self, **kwargs):
        #super().__init__(**kwargs)
        super(MerakiInterfaceV0, self).__init__(**kwargs)
        self.headers = { "Accept": "application/json", "X-Cisco-Meraki-API-Key": self.AUTH_TOKEN }
        self.BASE_URL = kwargs.get("base_url", "https://api.meraki.com/api/v0")
        self.lastPollTime = kwargs.get('start_poll_time', datetime.utcnow())
        #self.BASE_URL = "https://n2.meraki.com/api/v0"

    def getSecurityEvents(self, timeSpan = 2678400, PageSize = 100):
        if timeSpan > 31535999:
            logging.warning("Per Meraki API, the timespan argument cannot be greater than 365 days. Setting value to 365 days.")
            timeSpan = 31535900
        
        if PageSize > 1000 or PageSize < 3:
            logging.warning("Page size cannot be greater than 1000 or less than 3 per Meraki API specs.  Setting value to 1000.")
            PageSize = 1000

        
        resultList = self.getPagedResults(self.BASE_URL + "/organizations/%s/securityEvents?timespan=%s&perPage=%s" % (str(self.ORG_ID), str(timeSpan), str(PageSize)), headers=self.headers)
        return resultList

    def setNewLastPollTimeFromEpoch(self, result_list):
        # Datetime object is in UTC
        if result_list is not None:
            for v in result_list:
                try:
                    if self.lastPollTime.timestamp() < v['ts']:
                        self.lastPollTime = datetime.utcfromtimestamp(v['ts'])
                except AttributeError as ex:
                    timestamp = time.mktime(self.lastPollTime.timetuple())
                    if timestamp < v['ts']:
                        self.lastPollTime = datetime.utcfromtimestamp(v['ts'])


    def getOrganizations(self):
        return super().get(url=self.BASE_URL + "/organizations", headers=self.headers)

    def extractNextLink(self, headers):
        try:
            if "Link" in headers:
                rel_links = headers['Link'].split(",")
                for v in rel_links:
                    if "rel=next" in v:
                        next_link = (v.split(";"))[0].strip().strip('<>').split("?")[1]
                        logging.info("Found another page, going to pull those results as well from %s." % (str(next_link)))
                        return next_link
            return None
        except IndexError as ex:
            return None

    def getNetworks(self):
        return super().get(url=self.BASE_URL + "/organizations/%s/networks" % (str(self.ORG_ID)), headers=self.headers)

    def getSecurityEventsSinceLastPoll(self, PageSize = 100):
        current_time = datetime.utcnow()
        time_delta = current_time - self.lastPollTime
        delta_in_seconds = time_delta.total_seconds()
        resultsList = self.getSecurityEvents(delta_in_seconds, PageSize)
        self.setNewLastPollTimeFromEpoch(resultsList)
        return resultsList


    def getPagedResults(self, url, **kwargs):
        next_url = url
        results = []
        base_url = url.split("?")[0]
        #mid_url = url.split("/",3)[3].split("?")[0]
        #print(mid_url)
        while next_url is not None:
            try:
                resp = self.getRaw(url=next_url, **kwargs)
                if resp is not None and resp.status_code == 200:
                    results += json.loads(resp.text)
                    ## Meraki seems to give incomplete next links, this will take the query strings of the original request and combine with the new query strings from the next request.
                    query_string = self.extractNextLink(resp.headers)
                    if query_string is not None:
                        next_url = base_url + "?" + query_string
                    else:
                        break
                else:
                    break
            except Exception as ex:
                logging.exception("Exception in trying to do paged results.")
                break
        return results







#def convert_datetime_string(date_time_string):
#
#    datetime_object = datetime.strptime(date_time_string, '%m-%d-%Y')
#
#    return datetime_object
#
#def convert_datetime_object(date_time_object):
#    datetime_string = date_time_object.strftime("%m-%d-%Y")
#    return datetime_string
#
def get_datetime(date_time):
    try:
        return date_time.timestamp()
    except:
        return date_time.total_seconds()

def main_loop():

     global START_POLLTIME, Configuration, NormalizationRules
#     current_poll_time = get_datetime(START_POLLTIME)

     api = MerakiInterfaceV0(ORG_ID=Configuration.ORG_ID,AUTH_TOKEN=Configuration.TOKEN,start_poll_time=START_POLLTIME)
     np = normalizePayload(NormalizationRules)
     fo = FileOutput()
     sengine = sidify_engine()
     while True:
         alienvault_plugin_id = 20020
         alienvault_plugin_sid = None
         alienvault_plugin_string = None
         try:
             results =api.getSecurityEventsSinceLastPoll(1000)
             
             for result in results:
                 sido_obj = sido(ignore=True)
                 n_payload = np.normalizePayload("Snort", result)
                 sido_obj.bulk_add(plugin_id=alienvault_plugin_id,sid=int(n_payload['snort_id']),name=n_payload['signature'],category_id=8,subcategory_id=188,reliability=1,priority=1)
                 sengine.process(sido_obj)
                 fo.write(str(np.convertToString(n_payload)))
             time.sleep(Configuration.POLL_INTERVAL)
         except KeyboardInterrupt as ex:
             break
         except Exception as ex:
             logging.exception("Unhandled exception in main loop")



# Initialize Standard Variables
Configuration = ReadConfigurationFile(CONFIG_FILE='/etc/ossim/agent/plugins/cisco-meraki-api.config')
START_POLLTIME = (datetime.utcnow() - timedelta(minutes=5))
LOGGING_LEVEL = logging.INFO
HTTP_TIMEOUT = Configuration.getValue(option_name="HTTP_TIMEOUT", option_value_default=30, section="HTTP")
POLL_INTERVAL = Configuration.getValue(option_name="POLL_INTERVAL", option_value_default=120, section="API")
HTTP_MAX_RETRIES = Configuration.getValue(option_name="HTTP_MAX_RETRIES", option_value_default=10, section="HTTP")
HTTP_RATE_LIMIT_WAIT_TIME = Configuration.getValue(option_name="HTTP_RATE_LIMIT_WAIT_TIME", option_value_default=60, section="HTTP")
HTTP_RETRY_WAIT_TIME = Configuration.getValue(option_name="HTTP_RETRY_WAIT_TIME", option_value_default=30, section="HTTP")
START_FROM_BEGINNING = Configuration.getValue(option_name="START_FROM_BEGINNING", option_value_default="True", section="API")

# Meraki specific inputs
ORG_ID = Configuration.getValue(option_name="ORG_ID", option_value_default="ORG_ID_REQUIRED", section="API")
TOKEN = Configuration.getValue(option_name="TOKEN", option_value_default="TOKEN_REQUIRED", section="API")


if (str(START_FROM_BEGINNING).upper() == "FALSE" ):
    START_POLLTIME = (datetime.utcnow() - timedelta(days=30))







main_loop()
