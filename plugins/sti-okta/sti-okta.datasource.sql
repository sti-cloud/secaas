--
-- Okta Plugin
-- Written By: Tom Martin
-- Last Modified: 2019-03-13
--


-- Set plugin id variable
SET @pluginID = '21010';

-- Insert product if not already exists
INSERT INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'Okta', 'STI Plugin for Okta API', 15)
	ON DUPLICATE KEY UPDATE type=1, name='Okta API', description='STI Plugin for Okta API', product_type=6;

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 2, 24, 'Okta: successful authentication', 1, 10);
call STICreateDatasourceEntry(@pluginID, 2, 2, 25, 'Okta: failed authentication', 1, 10);
call STICreateDatasourceEntry(@pluginID, 3, 5, 212, 'Okta: policy allowed sign-on', 1, 10);
call STICreateDatasourceEntry(@pluginID, 4, 11, 134, 'Okta: Active Directory user import failed', 1, 10);
call STICreateDatasourceEntry(@pluginID, 5, 11, 139, 'Okta: Agent operation', 1, 10);
call STICreateDatasourceEntry(@pluginID, 20000000, 11, 139, 'Okta: Interal operation', 1, 10);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
