--
-- PaloAlto Plugin Modifications
-- Written By: Sidney Eaton
-- Last Modified: 9-11-2017
--

-- Set plugin id variable
SET @pluginID = '1615';

-- Insert product if not already exists

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 34221, 1, 5, 'paloalto: Apache Struts Jakarta Multipart Parser Remote Code Execution',1,0);
call STICreateDatasourceEntry(@pluginID, 38598, 1, 14, 'paloalto: Android Stagefright Library Overflow Vulnerability',1,0);
call STICreateDatasourceEntry(@pluginID, 39869, 7, 55, 'paloalto: Suspicious HTTP Response Found',1,0);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
