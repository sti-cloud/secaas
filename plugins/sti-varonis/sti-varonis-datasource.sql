--
-- Varonis DatAdvantage Datasource
-- Written By: Tom Martin
-- Last Modified: 2018-12-11
--

-- Set plugin id variable
SET @pluginID = '21008';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
VALUES (@pluginID, 1, 'sti-varonis', 'Varonis DatAdvantage', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 3, 31, 'Varonis DatAdvantage: Abnormal access behavior', 5, 5);
call STICreateDatasourceEntry(@pluginID, 2, 3, 31, 'Varonis DatAdvantage: Abnormal admin behavior', 5, 5);
call STICreateDatasourceEntry(@pluginID, 3, 3, 31, 'Varonis DatAdvantage: Abnormal behavior', 5, 5);
call STICreateDatasourceEntry(@pluginID, 4, 3, 31, 'Varonis DatAdvantage: Abnormal computer behavior', 5, 5);
call STICreateDatasourceEntry(@pluginID, 5, 3, 31, 'Varonis DatAdvantage: Abnormal service behavior', 5, 5);
call STICreateDatasourceEntry(@pluginID, 6, 3, 31, 'Varonis DatAdvantage: Abnormal user behavior', 5, 5);
call STICreateDatasourceEntry(@pluginID, 7, 3, 31, 'Varonis DatAdvantage: Data Loss Prevention', 5, 5);
call STICreateDatasourceEntry(@pluginID, 8, 3, 31, 'Varonis DatAdvantage: User actions resemble malware', 5, 5);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
