#!/usr/bin/python

import _mssql
import ConfigParser
import os
import sys

CONFIG_FILE = '/etc/ossim/agent/plugins/sti-sophos-epm.ini'
BOOKMARK_FILE = '/var/log/sti-sophos-epm.bookmark'
LOG_FILE = '/var/log/sti-sophos-epm.log'
DEBUG_FILE = '/var/log/sti-sophos-epm.debug'

if os.path.isfile(CONFIG_FILE) :
  print "Configuration file is present. Good."
else :
  print "Configuration file missing:", CONFIG_FILE
  print "Fix that and rerun."
  sys.exit(1) 

config = ConfigParser.RawConfigParser()
config.read(CONFIG_FILE)

SERVER = config.get('plugin', 'SERVER')
INSTANCE = config.get('plugin', 'INSTANCE')
DATABASE = config.get('plugin', 'DATABASE')
USER = config.get('plugin', 'USER')
PASS = config.get('plugin', 'PASS')
if INSTANCE != "":
  SERVER = SERVER + "\\" + INSTANCE

#############################################################################################################
def EventCycle(SERVER, USER, PASS, DATABASE):
  print "Connecting to SQL Server..."
  conn = _mssql.connect(server=SERVER, user=USER, password=PASS, database=DATABASE) 
  print "Querying the SQL Server for event data..."
  conn.execute_query("""
SELECT TOP 1
  T.ThreatInstanceID as Id,
  CASE T.ThreatType
    WHEN 1 THEN 'PUP'
	WHEN 2 THEN 'Malware'
	WHEN 3 THEN 'System'
	ELSE 'Unknown'
  END As Category,
  T.ThreatName as Threat,
  T.FullFilePathChecksum as CRC,
  T.FullFilePath as Path,
  T.FirstDetectedAt as Timestamp,
  C.ComputerName as Computer,
  C.IPAddressText as Addr,
  C.LastLoggedOnUser as Username
FROM ThreatInstancesAll AS T, ComputerListData2 AS C
WHERE C.ComputerID = T.ComputerID 
ORDER BY T.ThreatInstanceID
  """)

  count = 0
  for row in conn :
    count = count + 1 

  conn.close()

  if count == 0 :
    print "No data was read. This is not normal. Is the database configured being used?"
    sys.exit(1)
  else :
    print "Data was read. Everything looks good!"

######################################
# Main Loop
######################################
EventCycle(SERVER, USER, PASS, DATABASE)
