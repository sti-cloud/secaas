#!/usr/bin/python

import time
import _mssql
import ConfigParser

CONFIG_FILE = '/etc/ossim/agent/plugins/sti-sophos-epm.ini'
BOOKMARK_FILE = '/var/log/sti-sophos-epm.bookmark'
LOG_FILE = '/var/log/sti-sophos-epm.log'
DEBUG_FILE = '/var/log/sti-sophos-epm.debug'

# Startup initialization
config = ConfigParser.RawConfigParser()
config.read(CONFIG_FILE)

SERVER = config.get('plugin', 'SERVER')
INSTANCE = config.get('plugin', 'INSTANCE')
DATABASE = config.get('plugin', 'DATABASE')
USER = config.get('plugin', 'USER')
PASS = config.get('plugin', 'PASS')
if INSTANCE != "":
  SERVER = SERVER + "\\" + INSTANCE

LAST_ID = '0'
try :
  bookmark = open(BOOKMARK_FILE, 'r')
  LAST_ID = + bookmark.readline()
  bookmark.close()
except :
  pass

#############################################################################################################
def NotNone(var) :
  if var is None :
    return ''
  if isinstance(var, basestring) :
    return var
  return str(var)

#############################################################################################################
def EventCycle(SERVER, USER, PASS, DATABASE):
  global LAST_ID

  debug = open(DEBUG_FILE, 'w')
  debug.write('Operation starting at ' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + '\n')
  debug.write('Connecting to MSSQL...\n')
  try :
    conn = _mssql.connect(server=SERVER, user=USER, password=PASS, database=DATABASE) 
    debug.write('  ... connected.\n')
    debug.write('Reading data from MSSQL...\n')
    conn.execute_query("""
SELECT TOP 1000
  T.ThreatInstanceID as Id,
  CASE T.ThreatType
    WHEN 1 THEN 'PUP'
	WHEN 2 THEN 'Malware'
	WHEN 3 THEN 'System'
	ELSE 'Unknown'
  END As Category,
  T.ThreatName as Threat,
  T.FullFilePathChecksum as CRC,
  T.FullFilePath as Path,
  T.FirstDetectedAt as Timestamp,
  C.ComputerName as Computer,
  C.IPAddressText as Addr,
  C.LastLoggedOnUser as Username
FROM ThreatInstancesAll AS T, ComputerListData2 AS C
WHERE C.ComputerID = T.ComputerID AND T.ThreatInstanceID > """ + LAST_ID + """
ORDER BY T.ThreatInstanceID
  """)
    debug.write('... query completed\n')
  except Exception, e:
    debug.write('An error occured: ' + str(e) + '. Aborting until next processing cycle.\n')
    debug.close()
    return

  log = open(LOG_FILE, 'a')
  count = 0
  for row in conn :
    LAST_ID = str(row['Id'])
    timestamp = str(row['Timestamp'])
    computer = NotNone(row['Computer'])
    category = NotNone(row['Category'])
    threat = NotNone(row['Threat'])
    crc = NotNone(row['CRC'])
    path = NotNone(row['Path'])
    addr = NotNone(row['Addr'])
    username = NotNone(row['Username'])
    log.write(timestamp + ' ' + computer + ' [' + username + '] [' + category + '] [' + threat + '] [' + crc + '] [' + path + '] [' + addr + '] ' + LAST_ID + '\n')
    count = count + 1
  debug.write(str(count) + ' records retrieved this cycle.\n')

  log.close()
  bookmark = open(BOOKMARK_FILE, 'w')
  bookmark.write(LAST_ID)
  bookmark.close()

  debug.write('Processing cycle completed successfully. Sleeping.\n')
  debug.close()
  conn.close()

######################################
# Main Loop
######################################
while True :
  EventCycle(SERVER, USER, PASS, DATABASE)
  time.sleep(15)
