--
-- Sentinel Sophos EPM Plugin
-- Written By: Tom Martin
-- Last Modified: 2018-06-27
--

SET @pluginID = '21006';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'sti-sophos-epm', 'Sophos EPM', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 4, 34, 'Sophos EPM Spyware/Adware', 4, 4);
call STICreateDatasourceEntry(@pluginID, 2, 4, 38, 'Sophos EPM Virus/Malware', 4, 4);
call STICreateDatasourceEntry(@pluginID, 3, 4, 25, 'Sohpos EPM System Process', 4, 3);
call STICreateDatasourceEntry(@pluginID, 1000, 10, 71, 'Sophos EPM Other', 4, 3);

-- Cleanup
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
