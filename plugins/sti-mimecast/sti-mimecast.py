#!/usr/bin/python
import sys
sys.path.append("/usr/share/python/alienvault-api-core/lib/python2.7/site-packages")
import hmac
import hashlib
import base64
import datetime
import uuid
import requests
import json
import time
import codecs
import ConfigParser

CONFIG_FILE = '/etc/ossim/agent/plugins/sti-mimecast.config'

# Load plugin settings from the configuration INI file
def load_settings() :
  global APP_ID, APP_KEY, ACCESS_KEY, SECRET_KEY, MIMECAST_API, TOKEN_FILE, OUTPUT_FILE, LOG_FILE

  config = ConfigParser.RawConfigParser()
  config.read(CONFIG_FILE)

  APP_ID = config.get('default', 'APP_ID')
  APP_KEY = config.get('default', 'APP_KEY')
  ACCESS_KEY = config.get('default', 'ACCESS_KEY')
  SECRET_KEY = config.get('default', 'SECRET_KEY')
  MIMECAST_API = config.get('default', 'MIMECAST_API')
  OUTPUT_FILE = config.get('default', 'OUTPUT_FILE')
  LOG_FILE = config.get('default', 'LOG_FILE')
  TOKEN_FILE = config.get('default', 'TOKEN_FILE')

# Helper function for post_request to calculate the authentication signature hash
def create_signature(data_to_sign):
    digest = hmac.new(SECRET_KEY.decode("base64"), data_to_sign, digestmod=hashlib.sha1).digest()
    return base64.encodestring(digest).rstrip()

# Helper function for post_request to calculate the date in the header
def get_hdr_date():
    date = datetime.datetime.utcnow()
    dt = date.strftime("%a, %d %b %Y %H:%M:%S")
    return dt + " UTC"

# Make a REST API call to Mimecast
def post_request(post_body):
    # Calculate authentication headers
    request_id = str(uuid.uuid4())
    request_date = get_hdr_date()
    signature = 'MC ' + ACCESS_KEY + ':' + create_signature(':'.join([request_date, request_id, "/api/audit/get-siem-logs", APP_KEY]))
    headers = {'Authorization': signature, 'x-mc-app-id': APP_ID, 'x-mc-req-id': request_id, 'x-mc-date': request_date}

    try:
        retry_loop = True
        while retry_loop:
          retry_loop = False
          # Send request to API
          r = requests.post(url=MIMECAST_API + "/api/audit/get-siem-logs", data=json.dumps(post_body), headers=headers)

          # Handle Rate Limiting
          if r.status_code == 429:
              LOG('Rate limit hit. Sleeping for 60 seconds')
              retry_loop = True
              time.sleep(60)
    except Exception as e:
        LOG('Unexpected error connecting to API. Exception: ' + str(e))
        return None, None

    if r.status_code != 200:
        LOG('API request returned status code: ' + str(r.status_code))
        return None, None

    return r.text, r.headers

# Save output to a file instead of stdout
def LOG(info):
  now = datetime.datetime.now()
  f = open(LOG_FILE, "ab")
  f.write(now.isoformat())
  f.write(": ")
  f.write(info)
  f.write("\n")
  f.close()

# Save the current token to disk to allow continuation
def save_token(token):
  f = open(TOKEN_FILE, "wb")
  f.write(token)
  f.close()

# Read last valid token from the local disk
def get_saved_token():
  token = ""
  try :
    f = open(TOKEN_FILE, "r")
    token = f.readline()
    f.close()
  except Exception as e:
    print(e)
    pass
  return token

###############################
# Main Program
###############################
last_token_file=''
while True :
  # Allow updates to the config while the downloader is running.
  # If the token file changes, re-read the token from it (also causes
  # the token to load on startup)
  load_settings()
  if last_token_file != TOKEN_FILE :
    last_token = get_saved_token()
    last_token_file = TOKEN_FILE

  # REST API call to Mimecast to download log data
  post_body = {'data': [{'type': 'MTA'}]}
  if last_token != '' :
    post_body['data'][0]['token'] = last_token
  body, headers = post_request(post_body)

  # API call failed
  if body is None :
    LOG('API error detected. Sleeping for 60 seconds')
    time.sleep(60)
    continue

  # Check if the payload is JSON. If so, it is special
  try :
    jdata = json.loads(body)
    if 'fail' in jdata and len(jdata['fail'])>0 and 'errors' in jdata['fail'][0]:
      LOG("API error: " + jdata['fail'][0]['errors'][0]['message'])
    elif 'meta' in jdata and 'isLastToken' in jdata['meta'] and jdata['meta']['isLastToken'] == True :
      # LOG("DEBUG: No further logs available. Sleeping for 60 seconds.")
      pass
    else :
      LOG("Unknown API error: " + body)
    time.sleep(60)
    continue
  except Exception as e:
    pass

  # API call succeeded but returned an error
  if not 'mc-siem-token' in headers :
    LOG("API error:" + body)
    time.sleep(60)
    continue

  # API successful. Append the contents to a file.
  last_token = headers['mc-siem-token']
  lines = body.split("\r")
  f = codecs.open(OUTPUT_FILE, "a", encoding='utf-8')
  for line in lines :
    if len(line) > 0 :
      try :
        pipe = line.index("|")
        dStr = line[9:pipe-5]
        dTime = datetime.datetime.strptime(dStr, "%Y-%m-%dT%H:%M:%S")
        direction = -1
        if line[pipe-5] == "-" :
          direction = 1
        dTime = dTime + datetime.timedelta(hours=int(line[pipe-4:pipe-2])*direction, minutes=int(line[pipe-2:pipe])*direction)
        line = "datetime=" + dTime.strftime("%Y-%m-%dT%H:%M:%S") + line[pipe:]
      except :
        pass
      f.write(line + "\n")
  f.close()

  # Save the token
  save_token(last_token)
