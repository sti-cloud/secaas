--
-- Sentinel Mimecast Plugin
-- Written By: Matt Weiss
-- Last Modified: 2020-04-08
--

SET @pluginID = '40003';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
VALUES (@pluginID, 1, 'sti-mimecast', 'Mimecast', 9);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1000, 10, 176, 'Mimecast: Message Received', 4, 1);
call STICreateDatasourceEntry(@pluginID, 2000, 10, 174, 'Mimecast: Message Delivery', 4, 1);
call STICreateDatasourceEntry(@pluginID, 3000, 7, 149, 'Mimecast: URL Protect', 4, 2);
call STICreateDatasourceEntry(@pluginID, 3001, 7, 149, 'Mimecast: Threat Protect', 4, 2);
call STICreateDatasourceEntry(@pluginID, 4000, 10, 176, 'Mimecast: Message Processed', 4, 1);
call STICreateDatasourceEntry(@pluginID, 20000000, 10, 71, 'Mimecast: Generic Event', 0, 1);
