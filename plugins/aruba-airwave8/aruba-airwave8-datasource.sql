--
-- Aruba Airwave 8 Custom Datasources
-- Last signatures update: NA
-- Written By: Sidney Eaton
-- Last Modified: 3-3-2017
--
-- It is important not to drop previous entries, as some are alienvaults.
SET @pluginID = '20006';

INSERT IGNORE INTO plugin (id, type, name, description, product_type) VALUES (@pluginID, 1, 'Aruba-Airwave8', 'Aruba Airwave 8.x', 30);

DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

call STICreateDatasourceEntry(@pluginID,1,17,198,'Aruba-Airwave8: Rebooted Access Point',2,1);
call STICreateDatasourceEntry(@pluginID,2,17,198,'Aruba-Airwave8: Access Point Status Changed',2,1);
call STICreateDatasourceEntry(@pluginID,3,17,198,'Aruba-Airwave8: Access Point Down',2,1);
call STICreateDatasourceEntry(@pluginID,4,17,198,'Aruba-Airwave8: Access Point Up',2,1);
call STICreateDatasourceEntry(@pluginID,5,17,198,'Aruba-Airwave8: Access Point Configuration Good',2,1);
call STICreateDatasourceEntry(@pluginID,6,17,198,'Aruba-Airwave8: Access Point Configuration Does Not Match',2,1);

DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;