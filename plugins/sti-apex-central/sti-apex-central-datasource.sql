--
-- Apex Central Plugin
-- Written By: Matt Weiss
-- Last Modified: 2020-9-10
--


-- Set plugin id variable
SET @pluginID = '40010';

-- Insert product if not already exists
INSERT INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'Apex Central', 'STI Plugin for Apex Central', 17)
	ON DUPLICATE KEY UPDATE type=1, name='Apex Central', description='STI Plugin for Apex Central';

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.

call STICreateDatasourceEntry(@pluginID,1,4,32,'Apex-Central: Attack Discovery Detection',2,1);
call STICreateDatasourceEntry(@pluginID,2,4,32,'Apex-Central: Behavior Monitoring',2,1);
call STICreateDatasourceEntry(@pluginID,3,4,32,'Apex-Central: C&C Callback',2,1);
call STICreateDatasourceEntry(@pluginID,4,4,32,'Apex-Central: Content Security',2,1);
call STICreateDatasourceEntry(@pluginID,5,4,32,'Apex-Central: Data Loss Prevention',2,1);
call STICreateDatasourceEntry(@pluginID,6,4,32,'Apex-Central: Device Access Control',2,1);
call STICreateDatasourceEntry(@pluginID,7,4,32,'Apex-Central: Endpoint Application Control',2,1);
call STICreateDatasourceEntry(@pluginID,8,10,32,'Apex-Central: Engine Update Status',2,1);
call STICreateDatasourceEntry(@pluginID,9,10,32,'Apex-Central: Managed Product Logon/Logoff Event',2,1);
call STICreateDatasourceEntry(@pluginID,10,4,32,'Apex-Central: Network Content Inspection',2,1);
call STICreateDatasourceEntry(@pluginID,11,10,32,'Apex-Central: Pattern Update Status',2,1);
call STICreateDatasourceEntry(@pluginID,12,4,32,'Apex-Central: Predictive Machine Learning',2,1);
call STICreateDatasourceEntry(@pluginID,13,4,32,'Apex-Central: Sandbox Detection',2,1);
call STICreateDatasourceEntry(@pluginID,14,4,32,'Apex-Central: Spyware/Grayware',2,1);
call STICreateDatasourceEntry(@pluginID,15,7,32,'Apex-Central: Suspicious File',2,1);
call STICreateDatasourceEntry(@pluginID,16,4,32,'Apex-Central: Virus/Malware',2,1);
call STICreateDatasourceEntry(@pluginID,17,4,32,'Apex-Central: Web Security',2,1);
call STICreateDatasourceEntry(@pluginID,20000000,10,32,'Apex-Central: Generic Event',2,1);

DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;