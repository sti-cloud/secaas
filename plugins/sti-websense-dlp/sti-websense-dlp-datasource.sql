--
-- Websense DLP Datasource Changes
-- Written By: Tom Martin
-- Last Modified: 2017-01-09
--

-- Set plugin id variable
SET @pluginID = '21002';

-- Insert product if not already exists
INSERT IGNORE INTO plugin (id, type, name, description, product_type)
VALUES (@pluginID, 1, 'sti-websense-dlp', 'STI WebSense DLP', 7);

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 1, 5, 212, 'WebSense DLP Permitted', 3, 1);
call STICreateDatasourceEntry(@pluginID, 2, 5, 211, 'WebSense DLP Denied', 3, 1);
call STICreateDatasourceEntry(@pluginID, 1000, 10, 71, 'WebSense DLP Unrecognized', 1, 1);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;