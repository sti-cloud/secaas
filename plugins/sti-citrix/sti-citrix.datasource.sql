--
-- Citrix XenDesktop Plugin
-- Written By: Tom Martin
-- Last Modified: 2019-05-13
--

-- Set plugin id variable
SET @pluginID = '21012';

-- Insert product if not already exists
INSERT INTO plugin (id, type, name, description, product_type)
	VALUES (@pluginID, 1, 'Citrix XenDesktop', 'STI Plugin for Citrix XenDesktop', 23)
	ON DUPLICATE KEY UPDATE type=1, name='Citrix XenDesktop', description='STI Plugin for Citrix XenDesktop', product_type=23;

-- Drop procedure if exists
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
DELIMITER $$
CREATE PROCEDURE STICreateDatasourceEntry(PID int(11),SID int(11), CAT int(11), SUBCAT int(11), EVENTNAME varchar(512), REL int(11), PRIO int(11))
BEGIN
  INSERT INTO plugin_sid (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_changes (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
  INSERT INTO plugin_sid_orig (plugin_id, sid, category_id, subcategory_id, name, reliability, priority) VALUES (PID, SID, CAT, SUBCAT, EVENTNAME, REL, PRIO) ON DUPLICATE KEY UPDATE name=EVENTNAME, reliability=REL, priority=PRIO;
END$$

DELIMITER ;

-- Add datasource entries.
call STICreateDatasourceEntry(@pluginID, 10, 3, 143, 'Citrix Session Log', 2, 5);

-- Cleanup procedure
DROP PROCEDURE IF EXISTS STICreateDatasourceEntry;
