#!/usr/bin/python
###############################################################################
# Script: STI Duo Two-Factor Event Downloader
# Author: Tom Martin
# Date:   2018-06-06
#
# Description: This program downloads log data from Duo's Admin API endpoints
#    and writes data to a file monitored by AlienVault, which generates events.
###############################################################################
import sys
sys.path.append(
    '/usr/share/python/alienvault-api-core/lib/python2.7/site-packages')

import os
import base64
import email
import hashlib
import hmac
import requests
import urllib
import json
import ConfigParser
import time

CONFIG_FILE = '/etc/ossim/agent/plugins/sti-duo.config'
LOG_FILE = '/var/log/sti-duo.log'
DEBUG_FILE = '/var/log/sti-duo.debug'
CYCLE_INTERVAL = 30

AUTHLOGS_URL = '/admin/v1/logs/authentication'
ADMINLOGS_URL = '/admin/v1/logs/administrator'
TELEPHONYLOGS_URL = '/admin/v1/logs/telephony'

###############################################################################
def sign(method, host, path, params, skey, ikey):
    """
    Helper method to generate the required HTTP headers required to access
    the APIs. Specifically, 'Authorization' and 'Date'.
    """
    now = email.Utils.formatdate()
    canon = [now, method.upper(), host.lower(), path]
    args = []
    for key in sorted(params.keys()):
        val = params[key]
        if isinstance(val, unicode):
            val = val.encode("utf-8")
        args.append('%s=%s' % (urllib.quote(key, '~'), urllib.quote(val, '~')))
    canon.append('&'.join(args))
    canon = '\n'.join(canon)

    # sign canonical string
    sig = hmac.new(skey, canon, hashlib.sha1)
    auth = '%s:%s' % (ikey, sig.hexdigest())

    # return headers
    return {'Date': now, 'Authorization': 'Basic %s' % base64.b64encode(auth)}


###############################################################################
def getEvents(host, url, mintime, skey, ikey):
    """
    Download events for a Duo log API endpoint. Returns a list of objects.
    """
    global API_DELAY
    params = {}

    try:
        while True:
            params['mintime'] = str(mintime)
            headers = sign('GET', host, url, params, skey, ikey)
            req = requests.get(
                'https://' + host + url + '?mintime=' + str(mintime),
                headers=headers)
            data = json.loads(req.text)
            if data['stat'] != 'OK':
                Debug("Unexpected response from server: " + req.text)
                return []

            Debug('Received ' + str(len(data['response'])) + ' records.')
            return data['response']

    except:
        Debug('Exception connecting to API ' + host + url)
        return []


##############################################################################
def Debug(line):
    global DEBUG

    DEBUG.write(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + ': ' + line + '\n')
    DEBUG.flush()


##############################################################################
def Log(line):
    global LOG
    LOG.write(line + '\n')


##############################################################################
def SaveConfig():
    global config, auth_marker, admin_marker, telephony_marker
    config.set('plugin', 'auth_marker', str(auth_marker))
    config.set('plugin', 'admin_marker', str(admin_marker))
    config.set('plugin', 'telephony_marker', str(telephony_marker))
    with open(CONFIG_FILE, 'wb') as configfile:
        config.write(configfile)


##############################################################################
def ToString(s) :
    if isinstance(s, basestring) :
        return s.encode('utf-8')
    return str(s).encode('utf-8')


##############################################################################
def ExtractValue(obj, key1, key2):
    if key1 in obj:
        if key2:
            sub = obj[key1]
            if key2 in sub:
                return ToString(sub[key2])
            return ''
        return ToString(obj[key1])
    return ''

###############################################################################
# API Delay to prevent throttle
###############################################################################
def CycleDelay(newCycle) :
    global DEBUG, LOG

    if DEBUG:
        Debug("Waiting " + str(CYCLE_INTERVAL) + " seconds.")
        SaveConfig()
        DEBUG.close()
        LOG.close()
        time.sleep(CYCLE_INTERVAL)

    if newCycle :
        DEBUG = open(DEBUG_FILE, 'w')
    else :
        DEBUG = open(DEBUG_FILE, 'a')

    LOG = open(LOG_FILE, 'a')

###############################################################################
# Main program loop
###############################################################################
config = ConfigParser.RawConfigParser()
config.read(CONFIG_FILE)
host = config.get('plugin', 'host')
ikey = config.get('plugin', 'ikey')
skey = config.get('plugin', 'skey')
include_bypass = config.getint('plugin', 'include_bypass')
auth_marker = config.getint('plugin', 'auth_marker')
admin_marker = config.getint('plugin', 'admin_marker')
telephony_marker = config.getint('plugin', 'telephony_marker')

LOG = None
DEBUG = None
while True:
    # Authentication logs
    CycleDelay(True)            # Do not call the API too often. Also opens files if necessary.
    Debug('Retrieving authentication logs')
    data = getEvents(host, AUTHLOGS_URL, auth_marker, skey, ikey)
    if len(data) > 0:
        for rec in data:
            username = ExtractValue(rec, 'username', None)
            duotime = ExtractValue(rec, 'timestamp', None)
            epochtime = time.localtime(float(duotime))
            timestamp = time.strftime('%b %d %Y %H:%M:%S', epochtime)
            ip = ExtractValue(rec, 'ip', None)
            os = ExtractValue(rec, 'access_device', 'os') + ' ' + ExtractValue( rec, 'access_device', 'os_version')
            browser = ExtractValue(rec, 'access_device', 'browser') + ' ' + ExtractValue(rec, 'access_device', 'browser_version')
            integration = ExtractValue(rec, 'integration', None)
            location = ExtractValue(rec, 'location', 'city') + ', ' + ExtractValue(rec, 'location', 'state') + ', ' + ExtractValue(rec, 'location', 'country')
            result = ExtractValue(rec, 'result', None)
            if ip != '0.0.0.0' or result != 'SUCCESS' or include_bypass :
                Log(timestamp + " DUO-AUTH [" + username + "] [" + ip + "] [" + os +
                    "] [" + browser + "] [" + integration + "] [" + location +
                    "] [" + result + "]")
            auth_marker = int(duotime) + 1

    # Administrator logs           
    # CycleDelay(False)
    Debug('Retrieving administration logs')
    data = getEvents(host, ADMINLOGS_URL, admin_marker, skey, ikey)
    if len(data) > 0:
        for rec in data:
            action = ExtractValue(rec, 'action', None)
            duotime = ExtractValue(rec, 'timestamp', None)
            epochtime = time.localtime(float(duotime))
            timestamp = time.strftime('%b %d %Y %H:%M:%S', epochtime)
            username = ExtractValue(rec, 'username', None)
            obj = ExtractValue(rec, 'object', None)
            description = ExtractValue(rec, 'description', None)
            desc_json = json.loads('{}')
            try :
                desc_json = json.loads(description)     # JSON object embedded as string
            except :
                pass                                    # Sometimes it is an empty string instead of {}
            status = ExtractValue(desc_json, 'status', None)
            ip = ExtractValue(desc_json, 'ip_address', None)
            factor = ExtractValue(desc_json, 'factor', None)
            device = ExtractValue(desc_json, 'device', None)
            Log(timestamp + " DUO-ADMIN [" + username + "] [" + ip + "] [" + obj +
                    "] [" + status + "] [" + factor + "] [" + device + "]")
            admin_marker = int(duotime) + 1

    # Telephony logs
    # CycleDelay(False)
    Debug('Retrieving telephony logs')
    data = getEvents(host, TELEPHONYLOGS_URL, telephony_marker, skey, ikey)
    if len(data) > 0:
        for rec in data:
            credits = ExtractValue(rec, 'credits', None)
            duotime = ExtractValue(rec, 'timestamp', None)
            epochtime = time.localtime(float(duotime))
            timestamp = time.strftime('%b %d %Y %H:%M:%S', epochtime)
            typ = ExtractValue(rec, 'type', None)
            context = ExtractValue(rec, 'context', None)
            phone = ExtractValue(rec, 'phone', None)
            Log(timestamp + " DUO-TELEPHONY [" + credits + "] [" + typ + "] [" + context +
                    "] [" + phone + "]")
            telephony_marker = int(duotime) + 1
