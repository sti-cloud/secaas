#!/usr/bin/python
###############################################################################
# Script: STI Duo Two-Factor Plugin Test
# Author: Tom Martin
# Date:   2018-06-06
#
# Description: This program will test to see if the plugin should work properly
###############################################################################
import sys
sys.path.append(
    '/usr/share/python/alienvault-api-core/lib/python2.7/site-packages')

import os
import base64
import email
import hashlib
import hmac
import requests
import urllib
import json
import ConfigParser
import time

CONFIG_FILE = '/etc/ossim/agent/plugins/sti-duo.config'

AUTHLOGS_URL = '/admin/v1/logs/authentication'
ADMINLOGS_URL = '/admin/v1/logs/administrator'
TELEPHONYLOGS_URL = '/admin/v1/logs/telephony'

###############################################################################
def sign(method, host, path, params, skey, ikey):
    """
    Helper method to generate the required HTTP headers required to access
    the APIs. Specifically, 'Authorization' and 'Date'.
    """
    now = email.Utils.formatdate()
    canon = [now, method.upper(), host.lower(), path]
    args = []
    for key in sorted(params.keys()):
        val = params[key]
        if isinstance(val, unicode):
            val = val.encode("utf-8")
        args.append('%s=%s' % (urllib.quote(key, '~'), urllib.quote(val, '~')))
    canon.append('&'.join(args))
    canon = '\n'.join(canon)

    # sign canonical string
    sig = hmac.new(skey, canon, hashlib.sha1)
    auth = '%s:%s' % (ikey, sig.hexdigest())

    # return headers
    return {'Date': now, 'Authorization': 'Basic %s' % base64.b64encode(auth)}


###############################################################################
def getEvents(host, url, mintime, skey, ikey):
    """
    Download events for a Duo log API endpoint. Returns a list of objects.
    """
    global API_DELAY
    params = {}

    try:
        while True:
            params['mintime'] = str(mintime)
            headers = sign('GET', host, url, params, skey, ikey)
            req = requests.get(
                'https://' + host + url + '?mintime=' + str(mintime),
                headers=headers)
            data = json.loads(req.text)
            if data['stat'] != 'OK':
                Debug("Unexpected response from server: " + req.text)
                return []

            Debug('Received ' + str(len(data['response'])) + ' records.')
            Debug('Everything should work!')
            return data['response']

    except:
        Debug('Exception connecting to API ' + host + url)
        Debug('Verify that the API hostname, integration key, and secret key are correct')
        return []


##############################################################################
def Debug(line):
    print line


###############################################################################
# Main program loop
###############################################################################
config = ConfigParser.RawConfigParser()
config.read(CONFIG_FILE)
host = config.get('plugin', 'host')
ikey = config.get('plugin', 'ikey')
skey = config.get('plugin', 'skey')
include_bypass = config.getint('plugin', 'include_bypass')
auth_marker = config.getint('plugin', 'auth_marker')
admin_marker = config.getint('plugin', 'admin_marker')
telephony_marker = config.getint('plugin', 'telephony_marker')

Debug('Contacting Duo Admin API')
data = getEvents(host, AUTHLOGS_URL, 0, skey, ikey)
